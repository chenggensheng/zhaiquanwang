/**

 @Name：类友网络-官网
 @Author：star1029
    
*/

layui.define(['jquery', 'element', 'carousel', 'laypage', 'util'], function(exports){
  var $ = layui.jquery
  ,element = layui.element
  ,carousel = layui.carousel
  ,laypage = layui.laypage
  ,util = layui.util
  ,$win = $(window);
  
  //轮播渲染
  var setHeight = function(){
    var winWidth = $win.width();
    return (winWidth > 768 ? 660 : 398) + 'px'
  }, carIns = carousel.render({
    elem: '#banner'
    ,width: '100%'
    ,height: setHeight()
    ,anim: 'fade'
    ,arrow: 'hover'
    //,interval: 3000
  });
  
  $win.on('resize', function(){
    carIns.reload({
      height: setHeight()
    })
  });

  //滚动监听
  var setNav = function(){
    var scr = $(document).scrollTop();
    scr > 0 ? $(".nav").addClass('scroll') : $(".nav").removeClass('scroll');
  };
  setNav();
  $(window).on('scroll', setNav);

  //轮播文字
  $(function(){
    $('.banner').children('.title').addClass('active');
  })

  //导航切换
  var btn = $('.nav').find('.nav-list').children('button')
  ,spa = btn.children('span')
  ,ul = $('.nav').find('.nav-list').children('.layui-nav');
  btn.on('click', function(){
    if(!$(spa[0]).hasClass('spa1')){
      spa[0].className = 'spa1';
      spa[1].style.display = 'none';
      spa[2].className = 'spa3';
      $('.nav')[0].style.height = 90 + ul[0].offsetHeight + 'px';
    }else{
      spa[0].className = '';
      spa[1].style.display = 'block';
      spa[2].className = '';
      $('.nav')[0].style.height = 80 + 'px';
    }
  });

  
  //动态分页
  laypage.render({
    elem: 'newsPage'
    ,count: 1
    ,theme: '#2db5a3'
    ,layout: ['page', 'next']
  });

  //案例分页
  laypage.render({
    elem: 'casePage'
    ,count: 1
    ,theme: '#2db5a3' 
    ,layout: ['page', 'next']
  });

  //新闻字段截取
  $(function(){
    $(".main-news").find(".content").each(function(){
      var span = $(this).find(".detail").children("span")
      ,spanTxt = span.html();
      if(document.body.clientWidth > 463){
        span.html(spanTxt);
      }else{
        span.html(span.html().substring(0, 42)+ '...')
      };
      $(window).resize(function(){   
        if(document.body.clientWidth > 463){
          span.html(spanTxt);
        }else{
          span.html(span.html().substring(0, 42)+ '...')
        };
      });
    });
  });  
  
  util.fixbar({

  });

  exports('firm', {}); 
});