<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>债全网API</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $("#inputForm").validate({
                submitHandler: function(form){

//                    var apiType = $("input[type='radio']:checked").val();
//                    if(apiType == "0"){
//                        $("#showName").hide();
//                    }else if(apiType == "1"){
//                        $("#showName").show();
//                    }else if(apiType == "2"){
//                        $("#showName").show();
//                    }else if(apiType == "3"){
//                        $("#showName").show();
//                    }else if(apiType == "4"){
//                        $("#showName").show();
//                    }else if(apiType == "5"){
//                        $("#showName").show();
//                    }else if(apiType == "6"){
//                        $("#showName").show();
//                    }else if(apiType == "7"){
//                        $("#showName").show();
//                    }else if(apiType == "8"){
//                        $("#showName").show();
//                    }else if(apiType == "9"){
//                        $("#showName").show();
//                    }else if(apiType == "10"){
//                        $("#showName").show();
//                    }
//                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

        $(document).ready(function() {

            changShowName(0);
        });


        function changShowName() {
            var apiType = $("input[type='radio']:checked").val();
			if(apiType == "0" ||apiType == "2" ){
                $("#showIdCard").show();
				$("#showName").hide();
                $("#showCarNum").hide();
                $("#showLicensePlateType").hide();
                $("#showCycle").hide();
                $("#showCellphone").hide();
                $("#showBankcard").hide();


            }else if(apiType == "1"){
                $("#showIdCard").show();
                $("#showName").show();
                $("#showCarNum").hide();
                $("#showLicensePlateType").hide();
                $("#showCycle").hide();
                $("#showCellphone").hide();
                $("#showBankcard").hide();

            }else if(apiType == "3"){
                $("#showName").show();
                $("#showIdCard").hide();
                $("#showCarNum").show();
                $("#showLicensePlateType").show();
                $("#showCycle").hide();
                $("#showCellphone").hide();
                $("#showBankcard").hide();

            }else if(apiType == "4"){
                $("#showIdCard").hide();
                $("#showName").hide();
                $("#showCarNum").hide();
                $("#showLicensePlateType").hide();
                $("#showCycle").show();
                $("#showCellphone").show();

            }else if(apiType == "5"){

                $("#showIdCard").show();
                $("#showName").show();
                $("#showBankcard").show();
                $("#showCellphone").show();
                $("#showLicensePlateType").hide();
                $("#showCarNum").hide();
                $("#showCycle").hide();

            }else if(apiType == "6"){


                $("#showIdCard").hide();
                $("#showName").show();
                $("#showBankcard").show();
                $("#showCellphone").hide();
                $("#showLicensePlateType").hide();
                $("#showCarNum").hide();
                $("#showCycle").hide();


            }else if(apiType == "7"){


                $("#showIdCard").show();
                $("#showName").show();
                $("#showBankcard").hide();
                $("#showCellphone").show();
                $("#showLicensePlateType").hide();
                $("#showCarNum").hide();
                $("#showCycle").hide();

            }else if(apiType == "8"){

                $("#showIdCard").show();
                $("#showName").show();
                $("#showBankcard").hide();
                $("#showCellphone").hide();
                $("#showLicensePlateType").hide();
                $("#showCarNum").hide();
                $("#showCycle").hide();

            }else if(apiType == "9"){
                $("#showIdCard").show();
                $("#showName").show();
                $("#showBankcard").hide();
                $("#showCellphone").show();
                $("#showLicensePlateType").hide();
                $("#showCarNum").hide();
                $("#showCycle").hide();
            }else if(apiType == "10"){
                $("#showIdCard").show();
                $("#showName").show();
                $("#showBankcard").hide();
                $("#showCellphone").hide();
                $("#showLicensePlateType").hide();
                $("#showCarNum").hide();
                $("#showCycle").hide();

            }
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/api/">API查询页面</a></li>
		<li><a href="#">查询结构页面</a></li>
	</ul>


	<form:form id="inputForm" modelAttribute="api" action="${ctx}/zqw/api/searchResult" method="post" class="form-horizontal">
		<div class="control-group">
				<form:radiobuttons path="apiType" items="${fns:getDictList('zqw_api')}"  onchange="changShowName();" itemLabel="label" itemValue="value" htmlEscape="false"/>
		</div>
		<div class="control-group" id="showName">
			<label class="control-label">姓名:</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="200" class="required"/>
			</div>
		</div>
		<div class="control-group" id="showIdCard">
			<label class="control-label">身份证号码:</label>
			<div class="controls">
				<form:input path="idCard" htmlEscape="false" maxlength="200" class="required"/>
			</div>
		</div>

		<div class="control-group" id="showCellphone">
			<label class="control-label">手机号:</label>
			<div class="controls">
				<form:input path="cellphone" htmlEscape="false" maxlength="200" class="required"/>

			</div>
		</div>


		<div class="control-group" id="showCycle">
			<label class="control-label">时间段(1,3,6,9,12,24)单位:月:</label>
			<div class="controls">
				<form:select path="cycle" class="input-medium">
					<form:options items="${fns:getDictList('zqw_cycle')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>

		<div class="control-group" id="showCarNum">
			<label class="control-label">号牌号码:</label>
			<div class="controls">
				<form:input path="carNum" htmlEscape="false" maxlength="200" class="required"/>

			</div>
		</div>

		<div class="control-group" id="showLicensePlateType">
			<label class="control-label">号牌类型:</label>
			<div class="controls">
				<form:select path="licensePlateType" class="input-medium">
					<form:options items="${fns:getDictList('license_plate_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>


		<div class="control-group" id="showBankcard">
			<label class="control-label">银行卡:</label>
			<div class="controls">
				<form:input path="bankcard" htmlEscape="false" maxlength="200" class="required"/>

			</div>
		</div>


		<div class="form-actions">
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="查 询"/>
		</div>

	</form:form>


</body>
</html>