<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>律师管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/zqwLayer/">律师管理列表</a></li>
		<shiro:hasPermission name="zqw:zqwLayer:edit"><li><a href="${ctx}/zqw/zqwLayer/form">律师管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="zqwLayer" action="${ctx}/zqw/zqwLayer/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>律师名称：</label><form:input path="layerName" htmlEscape="false" maxlength="50" class="input-medium"/></li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>律师名称</th>
				<th>律师电话</th>
				<th>律师类别</th>
				<shiro:hasPermission name="zqw:zqwLayer:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="zqwLayer">
			<tr>
				<td><a href="${ctx}/zqw/zqwLayer/form?id=${zqwLayer.id}">
					${zqwLayer.layerName}
				</a>
				</td>
				<td>
					${zqwLayer.layerPhone}
				</td>
				<td>
					${fns:getDictLabel(zqwLayer.layerType, 'layer_type', '其他')}
				</td>
				<shiro:hasPermission name="zqw:zqwLayer:edit"><td>
    				<a href="${ctx}/zqw/zqwLayer/form?id=${zqwLayer.id}">修改</a>
					<a href="${ctx}/zqw/zqwLayer/delete?id=${zqwLayer.id}" onclick="return confirmx('确认要删除该律师管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>