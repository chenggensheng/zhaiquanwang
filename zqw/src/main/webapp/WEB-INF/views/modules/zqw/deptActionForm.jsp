<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>债行动管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/zqw/deptAction/">债行动列表</a></li>
		<li class="active"><a href="${ctx}/zqw/deptAction/form?id=${deptAction.id}">债行动<shiro:hasPermission name="zqw:deptAction:edit">${not empty deptAction.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="zqw:deptAction:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="deptAction" action="${ctx}/zqw/deptAction/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">债权类型：</label>
			<div class="controls">
				<form:select path="debtType" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('debt_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>



		<div class="control-group">
			<label class="control-label">债权人名称：</label>
			<div class="controls">
				<form:input path="debtOwernName" htmlEscape="false"  class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">债权金额（元）：</label>
			<div class="controls">
				<form:input path="debtPrice" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">行动结果：</label>
			<div class="controls">
				<form:input path="debtInfo" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">行动时间：</label>
			<div class="controls">
				<input name="debtActionTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${deptAction.debtActionTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="zqw:deptAction:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>