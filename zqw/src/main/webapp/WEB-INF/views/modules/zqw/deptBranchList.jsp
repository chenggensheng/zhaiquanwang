<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>曝光台管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/deptBranch/">曝光台列表</a></li>
		<shiro:hasPermission name="zqw:deptBranch:edit"><li><a href="${ctx}/zqw/deptBranch/form">曝光台添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="deptBranch" action="${ctx}/zqw/deptBranch/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>类型</th>
				<th>债权编号</th>
				<th>债务人名称</th>
				<th>债权金额（元）</th>
				<th>发布时间</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="deptBranch">
			<tr>
				<td>
					<a href="${ctx}/zqw/deptBranch/form?id=${deptBranch.id}">
							${fns:getDictLabel(deptBranch.deptType, 'debt_type', '')}

					</a>
				</td>

				<td>
						${deptBranch.deptNum}
				</td>

				<td>
						${deptBranch.deptName}
				</td>


				<td>
						${deptBranch.deptPrice}
				</td>

				<td>
					<fmt:formatDate value="${deptBranch.flluTime}" pattern="yyyy-MM-dd"/>
				</td>
				<shiro:hasPermission name="zqw:deptBranch:edit"><td>
    				<a href="${ctx}/zqw/deptBranch/form?id=${deptBranch.id}">修改</a>
					<a href="${ctx}/zqw/deptBranch/delete?id=${deptBranch.id}" onclick="return confirmx('确认要删除该曝光台吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>