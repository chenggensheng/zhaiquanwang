<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>车辆档案查询</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
    <fieldset>
        <legend>车辆档案查询--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >车辆所有人：</label>
            ${api.name}&nbsp;&nbsp;
            <label class="control-label"  >车牌号：</label>
            ${api.carNum}&nbsp;&nbsp;

            <label class="control-label"  >号牌类型：</label>
            ${fns:getDictLabel(api.licensePlateType, 'license_plate_type', '未知')}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交代码：</label>
            ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
            ${mapReturn['resMsg']}&nbsp;&nbsp;
        </div>

        <c:set var="data" value="${mapReturn['data']}" />

        <c:set var="result" value="${data['result']}" />

        <div class="control-group">
            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}
        </div>
    </fieldset>

    <fieldset>
        <legend>车辆档案查询</legend>
        <div class="control-group">
            <label class="control-label" >车辆品牌名称：</label>
            ${result['vehicleBrand']}
        </div>

        <div class="control-group">
            <label class="control-label" >车辆型号：</label>
            ${result['vehicleModel']}
        </div>


        <div class="control-group">
            <label class="control-label" >车架号：</label>
            ${result['vin']}
        </div>



        <div class="control-group">
            <label class="control-label" >发动机号：</label>
            ${result['engineNo']}
        </div>


        <div class="control-group">
            <label class="control-label" >车辆类型：</label>
            ${result['vehicleType']}
        </div>


        <div class="control-group">
            <label class="control-label" >车身颜色：</label>
            ${result['vehicleColor']}
        </div>

        <div class="control-group">
            <label class="control-label" >车辆使用性质：</label>
            ${result['useCharacter']}
        </div>

        <div class="control-group">
            <label class="control-label" >车辆所有者：</label>
            ${result['owner']}
        </div>
        <div class="control-group">
            <label class="control-label" >初次登记日期：</label>
            ${result['initialRegDate']}
        </div>
        <div class="control-group">
            <label class="control-label" >检验有效期：</label>
            ${result['inspectionValidityDate']}
        </div>
        <div class="control-group">
            <label class="control-label" >强制报废期：</label>
            ${result['compulsoryRetirementDate']}
        </div>
        <div class="control-group">
            <label class="control-label" >机动车状态：</label>
            ${result['vehicleState']}
        </div>
        <div class="control-group">
            <label class="control-label" >发动机型号：</label>
            ${result['engineType']}
        </div>
        <div class="control-group">
            <label class="control-label" >燃料种类：</label>
            ${result['fuelType']}
        </div>


        <div class="control-group">
            <label class="control-label" >排量（ 按照 ml 为单位的）：</label>

            <c:if test="${result['emissions'] == 'A'}">汽油</c:if>
            <c:if test="${result['emissions'] == 'B'}">柴油</c:if>
            <c:if test="${result['emissions'] == 'C'}">电</c:if>
            <c:if test="${result['emissions'] == 'D'}">混合油</c:if>
            <c:if test="${result['emissions'] == 'E'}">天然气</c:if>
            <c:if test="${result['emissions'] == 'F'}">液化石油气</c:if>
            <c:if test="${result['emissions'] == 'L'}">甲醇</c:if>
            <c:if test="${result['emissions'] == 'M'}">乙醇</c:if>
            <c:if test="${result['emissions'] == 'O'}">混合动力</c:if>
            <c:if test="${result['emissions'] == 'P'}">氢</c:if>

            <c:if test="${result['emissions'] == 'Q'}">生物燃料</c:if>
            <c:if test="${result['emissions'] == 'N'}">太阳能</c:if>
            <c:if test="${result['emissions'] == 'Y'}">无</c:if>
            <c:if test="${result['emissions'] == 'Z'}">其他</c:if>

            <%--${result['emissions']}--%>
        </div>

        <div class="control-group">
            <label class="control-label" >核定载客人数：</label>
            ${result['approveCarryPersonNo']}
        </div>

        <div class="control-group">
            <label class="control-label" >出厂日期：</label>
            ${result['productionDate']}
        </div>

    </fieldset>
</body>
</html>