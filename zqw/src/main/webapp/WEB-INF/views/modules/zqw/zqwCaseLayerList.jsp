<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>合作律师管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/zqwCaseLayer/">合作律师列表</a></li>
		<shiro:hasPermission name="zqw:zqwCaseLayer:edit"><li><a href="${ctx}/zqw/zqwCaseLayer/form">合作律师添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="zqwCaseLayer" action="${ctx}/zqw/zqwCaseLayer/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<shiro:hasPermission name="zqw:zqwCaseLayer:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="zqwCaseLayer">
			<tr>
				<shiro:hasPermission name="zqw:zqwCaseLayer:edit"><td>
    				<a href="${ctx}/zqw/zqwCaseLayer/form?id=${zqwCaseLayer.id}">修改</a>
					<a href="${ctx}/zqw/zqwCaseLayer/delete?id=${zqwCaseLayer.id}" onclick="return confirmx('确认要删除该合作律师吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>