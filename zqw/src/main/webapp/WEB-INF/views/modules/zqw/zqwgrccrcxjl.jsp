<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>乘车人出行记录</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
    <fieldset>
        <legend>个人投资核查--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >身份证号码：</label>
             ${api.idCard}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交代码：</label>
            ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
            ${mapReturn['resMsg']}&nbsp;&nbsp;
        </div>

        <c:set var="data" value="${mapReturn['data']}" />

        <c:set var="result" value="${data['result']}" />

        <div class="control-group">
            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}
        </div>
    </fieldset>

    <fieldset>
        <legend>乘车人出行记录</legend>

        <div class="control-group">
            <label class="control-label" >年龄层：</label>
            ${result['S001']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-18)；2：[18-65)；3：[65-∞)
        </div>

        <div class="control-group">
            <label class="control-label" >关联身份证个数：</label>
            ${result['S002']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-5)；2：[5-11)；3：[11-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >关联手机号个数：</label>
            ${result['S003']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-4)；2：[4-8)；3：[8-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >作为乘车人购票总次数：</label>
            ${result['S004']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-10)；2：[10-20)；3：[20-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >作为乘车人退票总次数：</label>
            ${result['S005']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-6)；3：[6-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >作为乘车人改签总次数：</label>
            ${result['S006']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-6)；3：[6-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >作为乘车人GDC列车购票总次数：</label>
            ${result['S007']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-5)；2：[5-10)；3：[10-∞)
        </div>



        <div class="control-group">
            <label class="control-label" >作为乘车人普通列车购票总次数：</label>
            ${result['S008']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-4)；2：[4-8)；3：[8-∞)
        </div>



        <div class="control-group">
            <label class="control-label" >行程变化比例：</label>
            ${result['S009']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-15)；2：[15-40)；3：[40-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >车票挂失次数：</label>
            ${result['S010']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-6)；3：[6-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >车票挂失比例：</label>
            ${result['S011']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >作为乘车人近3月退票总次数：</label>
            ${result['S012']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-5)；3：[5-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >作为乘车人近3月改签总次数：</label>
            ${result['S013']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-5)；3：[5-∞)
        </div>




        <div class="control-group">
            <label class="control-label" >乘车总次数：</label>
            ${result['S014']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-10)；2：[10-20)；3：[20-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >GDC列车乘车总次数：</label>
            ${result['S015']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-6)；2：[6-18)；3：[18-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >普通列车乘车总次数：</label>
            ${result['S016']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-4)；2：[4-12)；3：[12-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >月平均乘车次数：</label>
            ${result['S017']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-2)；2：[2-4)；3：[4-∞)
        </div>



        <div class="control-group">
            <label class="control-label" >最繁忙出发月份的出发次数：</label>
            ${result['S018']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-6)；3：[6-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >乘车总里程：</label>
            ${result['S019']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-5400)；2：[5400-10800)；3：[10800-∞)
        </div>












        <div class="control-group">
            <label class="control-label" >近3月乘车总次数：</label>
            ${result['S020']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-8)；3：[8-∞)
        </div>















        <div class="control-group">
            <label class="control-label" >近3月月平均乘车次数：</label>
            ${result['S021']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-1)；2：[1-5)；3：[5-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >总旅行时长：</label>
            ${result['S022']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-24)；2：[24-48)；3：[48-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >工作日乘车比例：</label>
            ${result['S023']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >最近出发时间段：</label>
            ${result['S024']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-8) ；2：[8-16)；3：[16-24)
        </div>


        <div class="control-group">
            <label class="control-label" >最近到达时间段：</label>
            ${result['S025']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-8) ；2：[8-16)；3：[16-24)
        </div>


        <div class="control-group">
            <label class="control-label" >车费消费总金额：</label>
            ${result['S026']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-2400)；2：[2400-4800)；3：[4800-∞)
        </div>



        <div class="control-group">
            <label class="control-label" >GDC列车车费消费总金额：</label>
            ${result['S027']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-1800)；2：[1800-3600)；3：[3600-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >普通列车车费消费总金额：</label>
            ${result['S028']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-600)；2：[600-1200)；3：[1200-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >车费消费每公里平均金额：</label>
            ${result['S029']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-0.2)；2：[0.2-0.4)；3：[0.4-∞)
        </div>


        <div class="control-group">
            <label class="control-label" > GDC等级列车乘车比例：</label>
            ${result['S030']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-40)；2：[40-80)；3：[80-100]
        </div>














        <div class="control-group">
            <label class="control-label" >其他等级（ZTKYL数字）列车乘车比例：</label>
            ${result['S031']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-30)；2：[30-70)；3：[70-100]
        </div>


        <div class="control-group">
            <label class="control-label" >高端席别乘车次数（软卧、高软、特等、商务）：</label>
            ${result['S032']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-2)；2：[2-6)；3：[6-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >普通席别乘车数量（其他）：</label>
            ${result['S033']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-8)；2：[8-24)；3：[24-∞)
        </div>


        <div class="control-group">
            <label class="control-label" > 高端席别乘车比例（软卧、高软、特等、商务）：</label>
            ${result['S034']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" > 普通席别乘车比例（其他）：</label>
            ${result['S035']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-30)；2：[30-70)；3：[70-100]
        </div>



        <div class="control-group">
            <label class="control-label" >线下（窗口、自动售票机、电话、代售点）购票比例：</label>
            ${result['S036']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >互联网购票比例：</label>
            ${result['S037']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >手机购票比例：</label>
            ${result['S038']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >提前N小时购票比例（N=0-24）：</label>
            ${result['S039']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>

















        <div class="control-group">
            <label class="control-label" >提前N小时购票比例（N=24-48）：</label>
            ${result['S040']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >提前N小时购票比例（N=48以上）：</label>
            ${result['S041']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >交易未支付总张数：</label>
            ${result['S042']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-5)；2：[5-10)；3：[10-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >动车车票交易比例（GDC）：</label>
            ${result['S043']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >作为购票人的退票数量：</label>
            ${result['S044']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-6)；2：[6-48)；3：[48-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >作为购票人的改签数量：</label>
            ${result['S045']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-6)；2：[6-48)；3：[48-∞)
        </div>



        <div class="control-group">
            <label class="control-label" >孩票数量：</label>
            ${result['S046']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-9)；2：[9-24)；3：[24-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >残军票数量：</label>
            ${result['S047']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-6)；3：[6-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >孩票比例：</label>
            ${result['S048']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" > 残军票比例：</label>
            ${result['S049']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >高端席别（软卧、高软、特等、商务）交易数量：</label>
            ${result['S050']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-10)；2：[10-20)；3：[20-∞)
        </div>




        <div class="control-group">
            <label class="control-label" > 高端席别（软卧、高软、特等、商务）交易比例：</label>
            ${result['S051']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >网上第三方支付比例：</label>
            ${result['S052']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >网上银行卡支付比例：</label>
            ${result['S053']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" >储值卡支付比例：</label>
            ${result['S054']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>



        <div class="control-group">
            <label class="control-label" >购乘意险数量：</label>
            ${result['S055']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-10)；2：[10-20)；3：[20-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >退乘意险数量：</label>
            ${result['S056']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-10)；2：[10-20)；3：[20-∞)
        </div>


        <div class="control-group">
            <label class="control-label" >购乘意险数量占购票张数的比例：</label>
            ${result['S057']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" > 退乘意险数量占购乘意险数量的比例：</label>
            ${result['S058']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-20)；2：[20-50)；3：[50-100]
        </div>


        <div class="control-group">
            <label class="control-label" > 近3月作为购票人的退票次数：</label>
            ${result['S059']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-5)；3：[5-∞)
        </div>


        <div class="control-group">
            <label class="control-label" > 近3月作为购票人的改签次数：</label>
            ${result['S060']}  &nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >说明：</label>
            本数据库未查得；0：0；1：(0-3)；2：[3-5)；3：[5-∞)
        </div>


    </fieldset>
</body>
</html>