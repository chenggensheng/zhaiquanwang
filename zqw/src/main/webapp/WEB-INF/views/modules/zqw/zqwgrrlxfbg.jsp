<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>银联消费报告2.0</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
    <fieldset>
        <legend>银联消费报告2.0--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >姓名：</label>
            ${api.name}&nbsp;&nbsp;
            <label class="control-label"  >身份证：</label>
            ${api.idCard}&nbsp;&nbsp;
            <label class="control-label"  >银行卡：</label>
            ${api.bankcard}&nbsp;&nbsp;
            <label class="control-label"  >手机号码：</label>
            ${api.cellphone}&nbsp;&nbsp;


        </div>

        <c:set var="data" value="${mapReturn['data']}" />
        <c:set var="result" value="${data['result']}" />

        <s:set var="basicInformation" value="${result['basicInformation']}" />
        <s:set var="cardRisk" value="${result['cardRisk']}" />
        <s:set var="behaviourRisk" value="${result['behaviourRisk']}" />
        <s:set var="financialRisk" value="${result['financialRisk']}" />
        <s:set var="houseProperty" value="${result['houseProperty']}" />
        <s:set var="vehicleProperty" value="${result['vehicleProperty']}" />
        <s:set var="otherProperty" value="${result['otherProperty']}" />
        <s:set var="consumeLevel" value="${result['consumeLevel']}" />


        <div class="control-group">


            <label class="control-label" style="float:inherit" >提交代码：</label>
                ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
                ${mapReturn['resMsg']}&nbsp;&nbsp;

            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}

        </div>
    </fieldset>



<fieldset>
        <legend>卡片基础信息类</legend>
        <div class="control-group">
            <label class="control-label" >卡性质：</label>
            ${basicInformation['cardProperty']}&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="control-label" >卡产品：</label>
            ${basicInformation['cardProduct']}
        </div>

        <div class="control-group">
            <label class="control-label" >卡等级：</label>
            ${basicInformation['cardLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="control-label" >2011年至今第一笔交易时间：</label>
            ${basicInformation['firstTransDate']}
        </div>

        <div class="control-group">
            <label class="control-label" >近3月交易活跃度-天：</label>
            ${basicInformation['transActivity3m']}&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="control-label" >近12月交易活跃度-月：</label>
            ${basicInformation['transActivity12m']}
        </div>


        <div class="control-group">
            <label class="control-label" >2011年至今交易活跃度-月：</label>
            ${basicInformation['transActivitySince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="control-label" >近12月交易金额：</label>
            ${basicInformation['transAmt12m']}
        </div>

        <div class="control-group">
            <label class="control-label" >近12月交易笔数：</label>
            ${basicInformation['transCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
           </div>


    </fieldset>


<fieldset>
    <legend>风险类</legend>

    <div class="control-group">
        <label class="control-label" >近1月失败交易笔数：</label>
        ${cardRisk['failedTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月失败交易金额：</label>
        ${basicInformation['failedTransAmt1m']}
    </div>

    <div class="control-group">
        <label class="control-label" >近3月失败交易笔数：</label>
        ${basicInformation['failedTransCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近3月失败交易金额：</label>
        ${basicInformation['failedTransSingledayCnt1m']}
    </div>

    <div class="control-group">
        <label class="control-label" >近3月交易活跃度-天：</label>
        ${basicInformation['TransActivity3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月交易活跃度-月：</label>
        ${basicInformation['TransActivity12m']}
    </div>


    <div class="control-group">
        <label class="control-label" >2011年至今交易活跃度-月：</label>
        ${basicInformation['TransActivitySince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月最大单日失败交易笔数：</label>
        ${basicInformation['failedTransSingledayCnt1m']}
    </div>

    <div class="control-group">
        <label class="control-label" >近3月最大单日失败交易笔数：</label>
        ${basicInformation['failedTransSingledayCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月银行关闭交易笔数：</label>
        ${basicInformation['bankClosedTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近3月银行关闭交易笔数：</label>
        ${basicInformation['bankClosedTransCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月身份验证失败交易笔数：</label>
        ${basicInformation['authenticationFailedTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近3月身份验证失败交易笔数：</label>
        ${basicInformation['authenticationFailedTransCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月发生吞卡次数：</label>
        ${basicInformation['cardSwallowedCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近3月发生吞卡次数：</label>
        ${basicInformation['cardSwallowedCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月超过笔数限制交易笔数：</label>
        ${basicInformation['overlimitTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近3月超过笔数限制交易笔数：</label>
        ${basicInformation['overlimitTransCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月密码错误及超限交易笔数：</label>
        ${basicInformation['passwordFailedCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近3月密码错误及超限交易笔数：</label>
        ${basicInformation['passwordFailedCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月金额超限交易金额：</label>
        ${basicInformation['amountOverlimitAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="control-group">
        <label class="control-label" >近1月金额超限交易笔数：</label>
        ${basicInformation['amountOverlimitCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近3月金额超限交易金额：</label>
        ${basicInformation['amountOverlimitAmt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近3月金额超限交易笔数：</label>
        ${basicInformation['amountOverlimitCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月金额不足交易金额：</label>
        ${basicInformation['lessBalanceTransAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近1月金额不足交易笔数：</label>
        ${basicInformation['lessBalanceTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近3月金额不足交易金额：</label>
        ${basicInformation['lessBalanceTransAmt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近3月金额不足交易笔数：</label>
        ${basicInformation['lessBalanceTransCnt3m']}&nbsp;&nbsp;&nbsp;&nbsp;

    </div>



</fieldset>


<fieldset>
    <legend>行为风险类指标</legend>

    <div class="control-group">
        <label class="control-label" >近1月代付交易金额：</label>
        ${behaviourRisk['paymentForTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月代付交易笔数：</label>
        ${behaviourRisk['paymentForTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月代付交易金额：</label>
        ${behaviourRisk['paymentForTransAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月代付交易笔数：</label>
        ${behaviourRisk['paymentForTransCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近1月夜交易金额：</label>
        ${behaviourRisk['nightTransAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月夜交易笔数：</label>
        ${behaviourRisk['nightTransCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="control-group">
        <label class="control-label" >近1月夜交易天数：</label>
        ${behaviourRisk['nightTransDays1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月夜交易金额：</label>
        ${behaviourRisk['nightTransAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="control-group">
        <label class="control-label" >近6月夜交易笔数：</label>
        ${behaviourRisk['nightTransCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月夜交易天数：</label>
        ${behaviourRisk['nightTransDays6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近1月夜消费金额占比：</label>
        ${behaviourRisk['nightConsumeAmtper1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月夜消费笔数占比：</label>
        ${behaviourRisk['nightConsumeCntper1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近6月夜消费金额占比：</label>
        ${behaviourRisk['nightConsumeAmtper6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月夜消费笔数占比：</label>
        ${behaviourRisk['nightConsumeCntper6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >夜间消费活跃度：</label>
        ${behaviourRisk['nightConsumeActivity']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月饮酒场所夜消费金额：</label>
        ${behaviourRisk['nightConsumeInBarAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近1月饮酒场所夜消费笔数：</label>
        ${behaviourRisk['nightConsumeInBarCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月饮酒场所夜消费金额：</label>
        ${behaviourRisk['nightConsumeInBarAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月饮酒场所夜消费笔数：</label>
        ${behaviourRisk['nightConsumeInBarCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月KTV夜消费金额：</label>
        ${behaviourRisk['nightConsumeInKtvAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近1月KTV夜消费笔数：</label>
        ${behaviourRisk['nightConsumeInKtvCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月KTV夜消费金额：</label>
        ${behaviourRisk['nightConsumeInKtvAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月KTV夜消费笔数：</label>
        ${behaviourRisk['nightConsumeInKtvCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月工作日工作时间消费金额：</label>
        ${behaviourRisk['workDayWorkTimeConsumeAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近1月工作日工作时间消费笔数：</label>
        ${behaviourRisk['workDayWorkTimeConsumeCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月工作日工作时间消费金额：</label>
        ${behaviourRisk['workDayWorkTimeConsumeAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近6月工作日工作时间消费笔数：</label>
        ${behaviourRisk['workDayWorkTimeConsumeCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


</fieldset>


<fieldset>
    <legend>③财务风险类指标</legend>


    <div class="control-group">
        <label class="control-label" >近1月取现交易金额：</label>
        ${financialRisk['cashWithdrawalAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月取现交易笔数：</label>
        ${financialRisk['cashWithdrawalCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近1月笔均取现金额：</label>
        ${financialRisk['cashWithdrawalAveAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月取现交易金额：</label>
        ${financialRisk['cashWithdrawalAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月取现交易笔数：</label>
        ${financialRisk['cashWithdrawalCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月笔均取现金额：</label>
        ${financialRisk['cashWithdrawalAveAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近12月取现交易金额：</label>
        ${financialRisk['cashWithdrawalAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月取现交易笔数：</label>
        ${financialRisk['cashWithdrawalCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近12月笔均取现金额：</label>
        ${financialRisk['cashWithdrawalAveAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月取现金额占交易金额占比：</label>
        ${financialRisk['cashWithdrawalAmtPer1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近6月取现金额占交易金额占比：</label>
        ${financialRisk['cashWithdrawalAmtPer6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月最近一笔取现日期：</label>
        ${financialRisk['lastCashWithdrawalDate']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月余额查询笔数：</label>
        ${financialRisk['balanceInquiryCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月余额查询笔数：</label>
        ${financialRisk['balanceInquiryCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月分期付款交易金额：</label>
        ${financialRisk['instalmentsAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月分期付款交易笔数：</label>
        ${financialRisk['instalmentsCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近6月分期付款交易金额：</label>
        ${financialRisk['instalmentsAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月分期付款交易笔数：</label>
        ${financialRisk['instalmentsCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月单笔金额1万元以上的消费金额：</label>
        ${financialRisk['exceed10000TransAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月大额交易占比：</label>
        ${financialRisk['largeTransPer1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月超大额交易占比：</label>
        ${financialRisk['superLargeTransPer1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近3月单笔金额1万元以上的消费金额：</label>
        ${financialRisk['exceed10000TransAmt3m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近3月大额交易占比：</label>
        ${financialRisk['largeTransPer3m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近3月超大额交易占比：</label>
        ${financialRisk['superLargeTransPer3m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月博彩消费金额：</label>
        ${financialRisk['betConsumeAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月博彩消费笔数：</label>
        ${financialRisk['betConsumeCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近6月博彩消费金额：</label>
        ${financialRisk['betConsumeAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月博彩消费笔数：</label>
        ${financialRisk['betConsumeCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月整额消费金额：</label>
        ${financialRisk['roundConsumeAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月整额消费笔数：</label>
        ${financialRisk['roundConsumeCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近6月整额消费金额：</label>
        ${financialRisk['roundConsumeAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月整额消费笔数：</label>
        ${financialRisk['roundConsumeCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月最大单日取现交易金额：</label>
        ${financialRisk['mostCashWithdrawalAmtInOneDay1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月最大单日取现交易笔数：</label>
        ${financialRisk['mostCashWithdrawalCntInOneDay1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月最大单日同商户多笔交易金额：</label>
        ${financialRisk['mostCntTransAmtInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月最大单日同商户多笔交易笔数：</label>
        ${financialRisk['mostCntTransCntInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月最大单日同商户多笔等额交易金额：</label>
        ${financialRisk['mostSameAmtTransAmtInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月最大单日同商户多笔等额交易笔数：</label>
        ${financialRisk['mostSameAmtTransCntInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近6月最大单日取现交易金额：</label>
        ${financialRisk['mostCashWithdrawalAmtInOneDay6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月最大单日取现交易笔数：</label>
        ${financialRisk['mostCashWithdrawalCntInOneDay6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月最大单日同商户多笔交易金额：</label>
        ${financialRisk['mostCntTransAmtInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月最大单日同商户多笔交易笔数：</label>
        ${financialRisk['mostCntTransCntInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月最大单日同商户多笔等额交易金额：</label>
        ${financialRisk['mostSameAmtTransAmtInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月最大单日同商户多笔等额交易笔数：</label>
        ${financialRisk['mostSameAmtTransCntInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月单日同商户多笔交易金额：</label>
        ${financialRisk['multiTransAmtInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月单日同商户多笔交易笔数：</label>
        ${financialRisk['multiTransCntInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月单日同商户多笔等额交易金额：</label>
        ${financialRisk['multiSameAmtTransAmtInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月单日同商户多笔等额交易笔数：</label>
        ${financialRisk['multiSameAmtTransCntInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>




    <div class="control-group">
        <label class="control-label" >近1月单日同商户多笔交易商户数：</label>
        ${financialRisk['merchantCntWhereMultiTransInOneDayOneMerchant1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月单日同商户多笔交易金额：</label>
        ${financialRisk['multiTransAmtInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近6月单日同商户多笔交易笔数：</label>
        ${financialRisk['multiTransCntInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月单日同商户多笔等额交易金额：</label>
        ${financialRisk['multiSameAmtTransAmtInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近6月单日同商户多笔等额交易笔数：</label>
        ${financialRisk['multiSameAmtTransCntInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月单日同商户多笔交易商户数：</label>
        ${financialRisk['merchantCntWhereMultiTransInOneDayOneMerchant6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近1月医疗消费金额：</label>
        ${financialRisk['medicalConsumeAmt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月医疗消费笔数：</label>
        ${financialRisk['medicalConsumeCnt1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近6月医疗消费金额：</label>
        ${financialRisk['medicalConsumeAmt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近6月医疗消费笔数：</label>
        ${financialRisk['medicalConsumeCnt6m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



</fieldset>

<fieldset>
    <legend>三、经济实力类	--房产相关指标</legend>

    <div class="control-group">
        <label class="control-label" >是否有房：</label>
        ${houseProperty['haveBoughtHouse']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1年是否新购房：</label>
        ${houseProperty['isBuyingHouse12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="control-group">
        <label class="control-label" >近2年是否购房：</label>
        ${houseProperty['isBuyingHouse24m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >第一笔房产类交易时间：</label>
        ${houseProperty['firstHousePropertyTransDate']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="control-group">
        <label class="control-label" >最近一笔房产类交易时间：</label>
        ${houseProperty['lastHousePropertyTransDate']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

</fieldset>


<fieldset>

    <legend>三、经济实力类	--车产相关指标</legend>
    <div class="control-group">
        <label class="control-label" >是否购车：</label>
        ${vehicleProperty['haveBoughtVehicle']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >第一笔购车时间：</label>
        ${vehicleProperty['firstBuyVehicleDate']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >最近一笔购车时间：</label>
        ${vehicleProperty['lastBuyVehicleDate']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >换车频率：</label>
        ${vehicleProperty['transferVehicleFrequency']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >2011年至今汽车行业消费第一笔交易时间（4S店）：</label>
        ${vehicleProperty['firstConsumeIn4SSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >2011年至今汽车行业消费最近一笔交易时间（4S店）：</label>
        ${vehicleProperty['lastConsumeIn4SSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >2011年至今汽车行业消费金额（4S店）：</label>
        ${vehicleProperty['consumeAmtIn4SSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >2011年至今汽车行业消费笔数（4S店）：</label>
        ${vehicleProperty['consumeCntIn4SSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >是否有过二手车消费：</label>
        ${vehicleProperty['haveBoughtSecondHandVehicle']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >2011年至今二手车消费金额：</label>
        ${vehicleProperty['buySecondHandVehicleAmtSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="control-group">
        <label class="control-label" >2011年至今二手车消费笔数：</label>
        ${vehicleProperty['buySecondHandVehicleCntSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;

    </div>


</fieldset>




<fieldset>

    <legend>三、经济实力类	--③其他资产/收入关联指标</legend>
    <div class="control-group">
        <label class="control-label" >2011年至今贵重珠宝、首饰、钟表、银器消费金额：</label>
        ${otherProperty['jewelryConsumeAmtSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >2011年至今贵重珠宝、首饰、钟表、银器消费笔数：</label>
        ${otherProperty['jewelryConsumeCntSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >2011年至今古玩、艺术品消费金额：</label>
        ${otherProperty['antiqueArtConsumeAmtSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >2011年至今古玩、艺术品消费笔数：</label>
        ${otherProperty['antiqueArtConsumeCntSince2011']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



</fieldset>



<fieldset>

    <legend>三、经济实力类	--④消费档次及排名</legend>
    <div class="control-group">
        <label class="control-label" >近12月贵重珠宝、首饰、钟表、银器消费金额：</label>
        ${consumeLevel['jewelryConsumeAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月贵重珠宝、首饰、钟表、银器消费笔数：</label>
        ${consumeLevel['jewelryConsumeCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近12月航空消费金额：</label>
        ${consumeLevel['airlineConsumeAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月航空消费笔数：</label>
        ${consumeLevel['airlineConsumeCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近12月证券消费金额：</label>
        ${consumeLevel['securitiesConsumeAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月证券消费笔数：</label>
        ${consumeLevel['securitiesConsumeCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近12月古玩、艺术品消费金额：</label>
        ${consumeLevel['antiqueArtConsumeAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月古玩、艺术品消费笔数：</label>
        ${consumeLevel['antiqueArtConsumeCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近12月保险支出金额：</label>
        ${consumeLevel['insuranceTransAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月保险支出笔数：</label>
        ${consumeLevel['insuranceTransCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



    <div class="control-group">
        <label class="control-label" >近12月教育机构类消费金额：</label>
        ${consumeLevel['educationConsumeAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月教育机构类消费笔数：</label>
        ${consumeLevel['educationConsumeCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >境外消费档次：</label>
        ${consumeLevel['overseasConsumeLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >境外消费活跃度：</label>
        ${consumeLevel['overseasConsumeActivity']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >境外游平均逗留时间：</label>
        ${consumeLevel['overseasTravelAverageStayDays']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >国人境外游次数：</label>
        ${consumeLevel['overseasTravelCnt']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="control-group">
        <label class="control-label" >近12月消费城市枚举：</label>
        ${consumeLevel['listCityWhereHaveConsumed12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月跨境交易金额：</label>
        ${consumeLevel['overseasTransAmt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近12月跨境交易笔数：</label>
        ${consumeLevel['overseasTransCnt12m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >综合异地消费档次：</label>
        ${consumeLevel['outHometownConsumeLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >综合异地消费活跃度：</label>
        ${consumeLevel['outHometownConsumeActivity']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >体育健身消费档次：</label>
        ${consumeLevel['gymConsumeLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >奢侈品消费档次：</label>
        ${consumeLevel['luxuriesConsumeLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >奢侈品消费活跃度：</label>
        ${consumeLevel['luxuriesConsumeActivity']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >酒店星级偏好：</label>
        ${consumeLevel['hotelStarPreference']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >酒店消费档次：</label>
        ${consumeLevel['hotelConsumeLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >爱美消费档次：</label>
        ${consumeLevel['beautyConsumeLevel']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近1月消费金额同城排名：</label>
        ${consumeLevel['consumeAmtRankInSameCity1m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>


    <div class="control-group">
        <label class="control-label" >近6月消费金额同城排名：</label>
        ${consumeLevel['consumeAmtRankInSameCity6m']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >近12月消费金额同城排名：</label>
        ${consumeLevel['consumeAmtRankInSameCity12m']}&nbsp;&nbsp;&nbsp;&nbsp;
    </div>



</fieldset>


</body>
</html>