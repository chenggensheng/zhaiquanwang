<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/zqw/zqwCase/">案件管理列表</a></li>
		<li class="active"><a href="${ctx}/zqw/zqwCase/form?id=${zqwCase.id}">案件管理<shiro:hasPermission name="zqw:zqwCase:edit">${not empty zqwCase.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="zqw:zqwCase:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="zqwCase" action="${ctx}/zqw/zqwCase/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">客户全称：</label>
				<form:input path="customId" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			<label class="control-label" style="float:inherit" >客户简称：</label>
				<form:input path="customShortName" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
		</div>

		<div class="control-group">
			<label class="control-label">侵权人姓名：</label>
				<form:input path="nameOfInfringer" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">身份证号码：</label>
				<form:input path="idCard" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">电话：</label>
				<form:input path="contactsPhone" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>

		<div class="control-group">
			<label class="control-label">户籍地：</label>
				<form:input path="householdRegister" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">债权金额：</label>
				<form:input path="amountCreditor" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>

		<div class="control-group">
			<label class="control-label">承办人员：</label>
			<form:input path="undertaker" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">是否委外：</label>
			<form:select path="isOut" class="input-medium " style="width:180px">
				<form:options items="${fns:getDictList('is_out')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
		</div>

	<fieldset>
		<legend>基本资料</legend>
		<div class="control-group">
			<label class="control-label">案由：</label>
				<form:select path="causeOfAction" class="input-medium " style="width:180px">
					<form:options items="${fns:getDictList('cause_action')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			<label class="control-label" style="float:inherit">诉讼时效：</label>
				<form:input path="limitationOfAction" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>

		<div class="control-group">
			<label class="control-label" >管辖法院：</label>
				<form:input path="jurisdictionalCourt" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">保险事故发生地：</label>
				<form:input path="insurancePlace" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
		</div>


		<div class="control-group">
			<label class="control-label">被保险人：</label>
			<form:input path="insured" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">驾驶人：</label>
			<form:input path="driver" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>
		<div class="control-group">
			<label class="control-label">理赔日期：</label>
			<div class="controls">
				<input name="claimDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					   value="<fmt:formatDate value="${zqwCase.claimDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>


	</fieldset>


		<div class="control-group">
			<label class="control-label">案件材料领取时间：</label>
			<div class="controls">
				<input name="holdDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${zqwCase.holdDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">案件材料领取清单：</label>
			<div class="controls">
				<form:checkboxes path="caseList" items="${fns:getDictList('case_list')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</div>
		</div>
	<fieldset>
		<legend>其他资料</legend>
		<div class="control-group">
			<label class="control-label">责任承担：</label>
			<form:select path="responsibility" class="input-medium " style="width:180px">
				<form:options items="${fns:getDictList('case_responsibility')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
			<label class="control-label" style="float:inherit">其他赔偿主体：</label>
				<form:input path="otherPenson" htmlEscape="false" class="input-xlarge "/>
		</div>
		<div class="control-group">
			<label class="control-label">交强险：</label>
				<form:input path="compulsoryInsurance" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">商业险：</label>
				<form:input path="commercialInsurace" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
		</div>
	</fieldset>



		<div class="control-group">
			<label class="control-label">欠款原因：</label>
				<form:select path="reasonsForArrears" class="input-medium " style="width:180px">
					<form:options items="${fns:getDictList('reasons_arrears')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			<label class="control-label" style="float:inherit">目前阶段：</label>
				<form:input path="currentStage" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>

		<div class="control-group">
			<label class="control-label">确定合作时间：</label>
				<input name="hzLayerTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${zqwCase.hzLayerTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			<label class="control-label" style="float:inherit">委托查找时间：</label>
				<input name="wlLayerTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					   value="<fmt:formatDate value="${zqwCase.wlLayerTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
		</div>

		<div class="control-group">
			<label class="control-label">案号：</label>
				<form:input path="caseNum" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">收费比例：</label>
				<form:input path="chargeRatio" htmlEscape="false" maxlength="1" class="input-xlarge "/>


		</div>

		<div class="control-group">
			<label class="control-label" >法官名称：</label>
			<form:input path="layerName" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">法官电话：</label>
				<form:input path="layerTel" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>

		<div class="control-group">
			<label class="control-label">结案日期：</label>
				<input name="closeCaseDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${zqwCase.closeCaseDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			<label class="control-label" style="float:inherit">归档日期：</label>
				<input name="filingDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${zqwCase.filingDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
		</div>

		<div class="control-group">
			<label class="control-label">归档号：</label>
				<form:input path="filingNum" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			<label class="control-label"  style="float:inherit">总案卷号：</label>
				<form:input path="recordNum" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			<label class="control-label" style="float:inherit">诉讼通告：</label>
				<form:input path="noticeOfAction" htmlEscape="false" class="input-xlarge "/>
		</div>


		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>

	<fieldset>
		<legend>导出选项</legend>
		<div class="control-group">
			<label class="control-label">是否同一人：</label>
			<form:select path="civilIndictment" class="input-medium " style="width:180px">
				<form:options items="${fns:getDictList('civil_indictment')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
			<label class="control-label"  style="float:inherit">授权：</label>
			<form:select path="grantAuthorization" class="input-medium " style="width:180px">
				<form:options items="${fns:getDictList('grant_authorization')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
			<label class="control-label" style="float:inherit">证据目录：</label>
			<form:select path="catalogueEvidence" class="input-medium " style="width:180px">
				<form:options items="${fns:getDictList('catalogue_evidence')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
		</div>

		<div class="control-group">
			<label class="control-label">保险代位求偿：</label>
			<div class="controls">
				<form:checkboxes path="insuranceSubrogation"  items="${fns:getDictList('insurance_subrogation')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">保险追偿：</label>
			<div class="controls">
				<form:checkboxes path="insuranceRecourse" items="${fns:getDictList('insurance_subrogation')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</div>
		</div>

	</fieldset>



		<div class="form-actions">
			<shiro:hasPermission name="zqw:zqwCase:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="下载保存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>