<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>债行动管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/deptAction/">债行动列表</a></li>
		<shiro:hasPermission name="zqw:deptAction:edit"><li><a href="${ctx}/zqw/deptAction/form">债行动添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="deptAction" action="${ctx}/zqw/deptAction/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>类型</th>
				<th>债权编号</th>
				<th>债务人名称</th>
				<th>未履行金额（元）</th>
				<th>行动时间</th>
				<th>行动结果</th>
				<shiro:hasPermission name="zqw:deptAction:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="deptAction">
			<tr>
				<td>
					<a href="${ctx}/zqw/deptAction/form?id=${deptAction.id}">
							${fns:getDictLabel(deptAction.debtType, 'debt_type', '')}
				</td>

				<td>
						${deptAction.debtNum}
				</td>

				<td>
						${deptAction.debtOwernName}
				</td>


				<td>
						${deptAction.debtPrice}
				</td>

				<td>
					<fmt:formatDate value="${deptAction.debtActionTime}" pattern="yyyy-MM-dd"/>
				</td>

				<td>
						${fns:abbr(deptAction.debtInfo,260)}
				</td>

				<shiro:hasPermission name="zqw:deptAction:edit"><td>
    				<a href="${ctx}/zqw/deptAction/form?id=${deptAction.id}">修改</a>
					<a href="${ctx}/zqw/deptAction/delete?id=${deptAction.id}" onclick="return confirmx('确认要删除该债行动吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>