<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>个人投资核查</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
<fieldset>
        <legend>个人投资核查--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >身份证号码：</label>
             ${api.idCard}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交代码：</label>
            ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
            ${mapReturn['resMsg']}&nbsp;&nbsp;
        </div>

        <c:set var="data" value="${mapReturn['data']}" />

        <c:set var="result" value="${data['result']}" />


        <!--️行政处罚信息-->
        <c:set var="administrativeSanctions" value="${result['administrativeSanction']}" />

        <!--人员失信被执行人-->
        <c:set var="punishedbreaks" value="${result['punishedbreak']}"/>

        <!--人员被执行人-->
        <c:set var="punisheds" value="${result['punished']}"/>

        <!--人员法人信息-->
        <c:set var="legalPersons" value="${result['legalPerson']}" />

        <!--人员高管信息-->
        <c:set var="managers" value="${result['manager']}"/>

        <!--人员股东信息-->
        <c:set var="shareholders" value="${result['shareholder']}"/>


        <div class="control-group">
            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}
        </div>
    </fieldset>

<fieldset>
    <legend>行政处罚信息</legend>
    <table  class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>统一社会信用代码</th>
            <th>主要违法事实</th>
            <th>处罚机关</th>
            <th>处罚机关名称</th>
            <th>行政处罚内容</th>
            <th>处罚决定书签发日期</th>
            <th>处罚决定文书</th>
            <th>处罚种类</th>
            <th>处罚种类名称</th>
            <th>公示日期</th>
            <th>当事人名称</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${administrativeSanctions}" var="administrativeSanction">
            <tr>
                <td>
                        ${administrativeSanction['creditCode']}
                </td>

                <td>
                        ${administrativeSanction['illegalFact']}
                </td>

                <td>
                        ${administrativeSanction['sanctionOrg']}
                </td>


                <td>
                        ${administrativeSanction['sanctionOrgName']}
                </td>

                <td>
                        ${administrativeSanction['sanctionOrgContent']}
                </td>

                <td>
                        ${administrativeSanction['sanctionSignDate']}
                </td>

                <td>
                        ${administrativeSanction['punishDecisionDoc']}
                </td>

                <td>
                        ${administrativeSanction['sanctionType']}
                </td>

                <td>
                        ${administrativeSanction['sanctionTypeName']}
                </td>

                <td>
                        ${administrativeSanction['publicationDate']}
                </td>

                <td>
                        ${administrativeSanction['name']}
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>


</fieldset>

<fieldset>
    <legend>人员失信被执行人</legend>
    <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>年龄</th>
            <th>省份</th>
            <th>法定代表人/负责人姓名</th>
            <th>身份证号码/工商注册号</th>
            <th>案号</th>
            <th>执行法院</th>
            <th>失信被执行人行为具体情形</th>
            <th>生效法律文书确定的义务</th>
            <th>退出日期</th>
            <th>关注次数</th>
            <th>执行依据文号</th>
            <th>做出执行依据单位</th>
            <th>被执行人姓名 /名称</th>

            <th>被执行人的履行情况</th>
            <th>已履行 ( 元 )</th>
            <th>发布时间</th>


            <th>立案时间</th>
            <th>性别</th>
            <th>失信人类型</th>
            <th>未履行 ( 元 )</th>
            <th>身份证原始发证地</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${punishedbreaks}" var="punishedbreak">
            <tr>
                <td>
                        ${punishedbreak['age']}
                </td>

                <td>
                        ${punishedbreak['province']}
                </td>

                <td>
                        ${punishedbreak['legalPersonName']}
                </td>


                <td>
                        ${punishedbreak['cardNo']}
                </td>

                <td>
                        ${punishedbreak['caseCode']}
                </td>

                <td>
                        ${punishedbreak['courtName']}
                </td>

                <td>
                        ${punishedbreak['dishonestSituation']}
                </td>

                <td>
                        ${punishedbreak['duty']}
                </td>

                <td>
                        ${punishedbreak['exitDate']}
                </td>



                <td>
                        ${punishedbreak['focusNumber']}
                </td>

                <td>
                        ${punishedbreak['executionNumber']}
                </td>

                <td>
                        ${punishedbreak['department']}
                </td>

                <td>
                        ${punishedbreak['executedName']}
                </td>


                <td>
                        ${punishedbreak['performance']}
                </td>
                <td>
                        ${punishedbreak['performedPart']}
                </td>
                <td>
                        ${punishedbreak['publishDate']}
                </td>

                <td>
                        ${punishedbreak['filingTime']}
                </td>

                <td>
                        ${punishedbreak['sex']}
                </td>


                <td>
                        ${punishedbreak['type']}
                </td>


                <td>
                        ${punishedbreak['unperformPart']}
                </td>

                <td>
                        ${punishedbreak['idAddress']}
                </td>


            </tr>
        </c:forEach>
        </tbody>
    </table>

</fieldset>

<fieldset>
    <legend>人员被执行人</legend>

    <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>年龄</th>
            <th>省份</th>
            <th>身份证号码/工商注册号</th>
            <th>案号</th>
            <th>案件状态</th>
            <th>执行法院</th>
            <th>执行标的（元）</th>
            <th>关注次数</th>
            <th>被执行人姓名/名称</th>
            <th>立案时间</th>
            <th>性别</th>
            <th>被执行人类型</th>
            <th>身份证原始发证地</th>


        </tr>
        </thead>
        <tbody>
        <c:forEach items="${punisheds}" var="punished">
            <tr>
                <td>
                        ${punished['age']}
                </td>

                <td>
                        ${punished['province']}
                </td>

                <td>
                        ${punished['cardNo']}
                </td>


                <td>
                        ${punished['caseNum']}
                </td>

                <td>
                        ${punished['caseStatus']}
                </td>

                <td>
                        ${punished['courtName']}
                </td>

                <td>
                        ${punished['executionTarget']}
                </td>

                <td>
                        ${punished['focusNumber']}
                </td>

                <td>
                        ${punished['executedName']}
                </td>

                <td>
                        ${punished['filingTime']}
                </td>

                <td>
                        ${dishonestPunished['sex']}
                </td>

                <td>
                        ${dishonestPunished['type']}
                </td>
                <td>
                        ${dishonestPunished['idAddress']}
                </td>

            </tr>
        </c:forEach>
        </tbody>
    </table>

</fieldset>

<fieldset>
    <legend>企业法人信息</legend>
    <table id="contentTable" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>统一社会信用代码</th>
            <th>企业(机构)名称</th>
            <th>企业状态</th>
            <th>企业(机构)类型</th>
            <th>成立时间</th>
            <th>行业名称</th>
            <th>个人标识码</th>
            <th>注册资本(企业万元个体元)</th>
            <th>注册资本币种</th>
            <th>注册号</th>
            <th>省份</th>
            <th>查询人姓名</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${legalPersons}" var="legalPerson">
            <tr>
                <td>
                        ${legalPerson['creditCode']}
                </td>

                <td>
                        ${legalPerson['entName']}
                </td>

                <td>
                        ${legalPerson['entStatus']}
                </td>


                <td>
                        ${legalPerson['entType']}
                </td>

                <td>
                        ${legalPerson['estabDate']}
                </td>

                <td>
                        ${legalPerson['industryName']}
                </td>

                <td>
                        ${legalPerson['personalCode']}
                </td>

                <td>
                        ${legalPerson['regCap']}
                </td>

                <td>
                        ${legalPerson['regCapCur']}
                </td>
                <td>
                        ${legalPerson['regNo']}
                </td>
                <td>
                        ${legalPerson['regOrgProvince']}
                </td>
                <td>
                        ${legalPerson['name']}
                </td>

            </tr>
        </c:forEach>
        </tbody>
    </table>


</fieldset>

<fieldset>
    <legend>企业主要管理人员信息</legend>
    <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>统一社会信用代码</th>
            <th>企业(机构)名称</th>
            <th>企业状态</th>
            <th>企业(机构)类型</th>
            <th>成立时间</th>
            <th>行业名称</th>
            <th>个人标识码</th>
            <th>职务</th>
            <th>注册资本(万元个体元)</th>
            <th>注册资本币种</th>
            <th>注册号</th>
            <th>省份</th>
            <th>查询人姓名</th>

        </tr>
        </thead>
        <tbody>
        <c:forEach items="${managers}" var="manager">
            <tr>
                <td>
                        ${manager['creditCod']}
                </td>

                <td>
                        ${manager['entName']}
                </td>

                <td>
                        ${manager['entStatus']}
                </td>


                <td>
                        ${manager['entType']}
                </td>

                <td>
                        ${manager['estabDate']}
                </td>

                <td>
                        ${manager['industryName']}
                </td>

                <td>
                        ${manager['personalCode']}
                </td>

                <td>
                        ${manager['position']}
                </td>

                <td>
                        ${manager['regCap']}
                </td>
                <td>
                        ${manager['regCapCur']}
                </td>
                <td>
                        ${manager['regNo']}
                </td>
                <td>
                        ${manager['regOrgProvince']}
                </td>
                <td>
                        ${manager['name']}
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>企业股东信息</legend>
    <table id="" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th>出资方式</th>
            <th>统一社会信用代码</th>
            <th>认缴出资币种</th>
            <th>企业(机构)名称</th>
            <th>企业状态</th>
            <th>企业(机构)类型</th>
            <th>成立时间</th>
            <th>出资比例</th>
            <th>行业名称</th>
            <th>个人标识码</th>
            <th>注册资本(万元)</th>
            <th>注册资本币种</th>
            <th>注册号</th>
            <th>省份</th>
            <th>查询人姓名</th>
            <th>出资比例</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${shareholders}" var="shareholder">
            <tr>
                <td>
                        ${shareholder['contriForm']}
                </td>

                <td>
                        ${shareholder['creditCode']}
                </td>

                <td>
                        ${shareholder['currency']}
                </td>


                <td>
                        ${shareholder['entName']}
                </td>

                <td>
                        ${shareholder['entStatus']}
                </td>

                <td>
                        ${shareholder['entType']}
                </td>

                <td>
                        ${shareholder['estabDate']}
                </td>

                <td>
                        ${shareholder['contriRatio']}
                </td>

                <td>
                        ${shareholder['industryName']}
                </td>

                <td>
                        ${shareholder['personalCode']}
                </td>


                <td>
                        ${shareholder['regCap']}
                </td>

                <td>
                        ${shareholder['regCapCur']}
                </td>
                <td>
                        ${shareholder['regNo']}
                </td>
                <td>
                        ${shareholder['regOrgProvince']}
                </td>
                <td>
                        ${shareholder['name']}
                </td>
                <td>
                        ${shareholder['contriAmount']}
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</fieldset>






</body>
</html>