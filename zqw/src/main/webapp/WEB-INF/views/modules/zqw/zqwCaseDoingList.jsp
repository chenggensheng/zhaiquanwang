<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件进展管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/zqwCaseDoing/">案件进展列表</a></li>
		<shiro:hasPermission name="zqw:zqwCaseDoing:edit"><li><a href="${ctx}/zqw/zqwCaseDoing/form">案件进展添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="zqwCaseDoing" action="${ctx}/zqw/zqwCaseDoing/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="zqw:zqwCaseDoing:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="zqwCaseDoing">
			<tr>
				<td><a href="${ctx}/zqw/zqwCaseDoing/form?id=${zqwCaseDoing.id}">
					<fmt:formatDate value="${zqwCaseDoing.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</a></td>
				<td>
					${zqwCaseDoing.remarks}
				</td>
				<shiro:hasPermission name="zqw:zqwCaseDoing:edit"><td>
    				<a href="${ctx}/zqw/zqwCaseDoing/form?id=${zqwCaseDoing.id}">修改</a>
					<a href="${ctx}/zqw/zqwCaseDoing/delete?id=${zqwCaseDoing.id}" onclick="return confirmx('确认要删除该案件进展吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>