<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>民事起诉状管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqw/zqwCaseIndictment/">民事起诉状列表</a></li>
		<shiro:hasPermission name="zqw:zqwCaseIndictment:edit"><li><a href="${ctx}/zqw/zqwCaseIndictment/form">民事起诉状添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="zqwCaseIndictment" action="${ctx}/zqw/zqwCaseIndictment/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="zqw:zqwCaseIndictment:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="zqwCaseIndictment">
			<tr>
				<td><a href="${ctx}/zqw/zqwCaseIndictment/form?id=${zqwCaseIndictment.id}">
					<fmt:formatDate value="${zqwCaseIndictment.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</a></td>
				<td>
					${zqwCaseIndictment.remarks}
				</td>
				<shiro:hasPermission name="zqw:zqwCaseIndictment:edit"><td>
    				<a href="${ctx}/zqw/zqwCaseIndictment/form?id=${zqwCaseIndictment.id}">修改</a>
					<a href="${ctx}/zqw/zqwCaseIndictment/delete?id=${zqwCaseIndictment.id}" onclick="return confirmx('确认要删除该民事起诉状吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>