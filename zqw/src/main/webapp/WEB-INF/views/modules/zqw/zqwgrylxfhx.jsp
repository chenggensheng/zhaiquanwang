<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>银联消费画像2.0</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
    <fieldset>
        <legend>银联消费画像2.0--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >姓名：</label>
            ${api.name}&nbsp;&nbsp;
            <label class="control-label"  >银行卡号：</label>
            ${api.bankcard}&nbsp;&nbsp;

        </div>

        <c:set var="data" value="${mapReturn['data']}" />
        <c:set var="result" value="${data['result']}" />

        <div class="control-group">


            <label class="control-label" style="float:inherit" >提交代码：</label>
                ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
                ${mapReturn['resMsg']}&nbsp;&nbsp;

            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}

        </div>
    </fieldset>



<fieldset>
        <legend>银联消费画像</legend>
        <div class="control-group">
            <label class="control-label" >卡种：</label>
            ${result['BP1001']}&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="control-label" >卡性质：</label>
            ${result['BP1002']}
        </div>



    <div class="control-group">
        <label class="control-label" >卡品牌：</label>
        ${result['BP1003']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >卡产品：</label>
        ${result['BP1004']}
    </div>


    <div class="control-group">
        <label class="control-label" >卡等级：</label>
        ${result['BP1005']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >发卡行：</label>
        ${result['BP1006']}
    </div>



    <div class="control-group">
        <label class="control-label" >卡名称：</label>
        ${result['BP1008']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >卡类别：</label>
        ${result['BP1009']}
    </div>




    <div class="control-group">
        <label class="control-label" >是否银联标准卡：</label>
        ${result['BP1010']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >是否银联标识卡：</label>
        ${result['BP1011']}
    </div>



    <div class="control-group">
        <label class="control-label" >发卡行名称：</label>
        ${result['BP1041']}&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="control-label" >消费性别：</label>
        ${result['BP1013']}
    </div>




    <div class="control-group">
        <label class="control-label" >消费年龄：</label>
        ${result['BP1014']}&nbsp;&nbsp;&nbsp;&nbsp;

    </div>



    <div class="control-group">
        根据需要添加。。。。。
    </div>

    </fieldset>

</body>
</html>