<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>个人整体涉诉</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
    <fieldset>
        <legend>个人整体涉诉--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >身份证号码：</label>
             ${api.idCard}&nbsp;&nbsp;
            <label class="control-label"  >姓名：</label>
            ${api.name}&nbsp;&nbsp;

            <label class="control-label" style="float:inherit" >提交代码：</label>
            ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
            ${mapReturn['resMsg']}&nbsp;&nbsp;
        </div>

        <c:set var="data" value="${mapReturn['data']}" />


        <c:set var="result" value="${data['result']}" />

        <c:set var="lists" value="${result['list']}" />


        <div class="control-group">
            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}

            <label class="control-label" style="float:inherit" >总条数：</label>
            ${result['count']}

        </div>
    </fieldset>

    <fieldset>
        <legend>结果信息</legend>
        <table id="contentTable" class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>文书编号</th>
                <th>时间 </th>
                <th>内容概要</th>
                <th>标题</th>
                <th>数据类型</th>
                <th>匹配度</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${lists}" var="list">
                <tr>
                    <td>
                            ${list['id']}
                    </td>

                    <td>
                            ${list['caseTime']}
                    </td>

                    <td>
                            ${list['content']}
                    </td>


                    <td>
                            ${list['title']}
                    </td>

                    <td>
                            ${list['dataType']}
                    </td>

                    <td>
                            ${list['matchRatio']}
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>


    </fieldset>
</body>
</html>