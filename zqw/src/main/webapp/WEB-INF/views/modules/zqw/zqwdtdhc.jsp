<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>多头借贷核查</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">


//	   $(document).ready(function(){
//
//		// 身份证号码验证
//		   jQuery.validator.addMethod("isIdCardNo", function(value, element) {
//			   var tel=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
//		   return this.optional(element) || (tel.test(value));
//		   }, "请正确输入您的身份证号码");
//
//	   });
	   	</script>
	<script type="text/javascript">
		<%--$(document).ready(function() {--%>
			<%--$("#inputForm").validate({--%>
                <%--rules: {--%>
                    <%--number: {remote: "${ctx}/hr/hrUser/checkNumber?oldNumber=" + encodeURIComponent('${hrUser.number}')},--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:true--%>
                    <%--}--%>
                <%--},--%>
                <%--messages: {--%>
                    <%--certificates:{--%>
                    	<%--isIdCardNo:"请输入正确的身份证号"--%>
                    <%--}--%>
                <%--},--%>
				<%--submitHandler: function(form){--%>
                        <%--loading('正在提交，请稍等...');--%>
                        <%--form.submit();--%>

				<%--},--%>
				<%--errorContainer: "#messageBox",--%>
				<%--errorPlacement: function(error, element) {--%>
					<%--$("#messageBox").text("输入有误，请先更正。");--%>
					<%--if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){--%>
						<%--error.appendTo(element.parent().parent());--%>
					<%--} else {--%>
						<%--error.insertAfter(element);--%>
					<%--}--%>
				<%--}--%>
			<%--});--%>
		<%--});--%>


	</script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/zqw/api/">API查询页面</a></li>
    <li class="active"> <a href="#"> 查询结果页面 </a></li>
</ul>
    <fieldset>
        <legend>多头借贷核查--基本情况</legend>

        <div class="control-group">
            <label class="control-label"  >手机号码：</label>
            ${api.cellphone}&nbsp;&nbsp;
            <label class="control-label"  >时间段：</label>
            ${api.cycle}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交代码：</label>
            ${mapReturn['resCode']}&nbsp;&nbsp;
            <label class="control-label" style="float:inherit" >提交信息：</label>
            ${mapReturn['resMsg']}&nbsp;&nbsp;
        </div>

        <c:set var="data" value="${mapReturn['data']}" />
        <c:set var="result" value="${data['result']}" />

        <c:set var="resultData" value="${result['data']}"/>

        <c:set var="S002" value="${resultData['S002']}"/>

        <c:set var="S002Datas" value="${S002[data]}"/>

        <c:set var="S004" value="${resultData['S004']}"/>

        <c:set var="S004Datas" value="${S004[data]}"/>

        <c:set var="S007" value="${resultData['S007']}"/>

        <c:set var="S007Datas" value="${S007[data]}" />

        <c:set var="S009" value="${resultData['S009']}"/>

        <c:set var="S009Datas" value="${S009[data]}" />

        <c:set var="S012" value="${resultData['S012']}"/>

        <c:set var="S012Datas" value="${S012[data]}" />

        <c:set var="S013" value="${resultData['S013']}"/>
        <c:set var="S013Datas" value="${S013[data]}" />


        <div class="control-group">
            <label class="control-label" >结果代码：</label>
            ${data['statusCode']}
            <label class="control-label" style="float:inherit" >结果信息：</label>
            ${data['statusMsg']}

        </div>


        <div class="control-group">
            <label class="control-label" >是否有异常：</label>
            <c:if test="${result['code'] == '200'}">正常</c:if>
            <c:if test="${result['code'] == '400'}">异常</c:if>
            <label class="control-label" style="float:inherit" >省：</label>
            ${result['province']}
            <label class="control-label" style="float:inherit" >省：</label>
            ${result['city']}
        </div>
    </fieldset>

    <fieldset>
        <legend>信贷平台注册详情</legend>

        <table id="contentTable" class="table table-striped table-bordered table-condensed">

            <thead>
            <tr>
                <th>周期</th>
                <th colspan="2">${S002['cycle']}</th>
            </tr>
            </thead>

            <thead>
            <tr>
                <th>平台类型(0 全部 1 银行 2 非银行)</th>
                <th>平台代码</th>
                <th>注册时间</th>
            </tr>
            </thead>
            <tbody>


            <c:forEach items="${S002Datas}" var="S002Data">
                <tr>
                    <td>
                        <c:if test="${S002Data['platformType'] == '0'}">全部</c:if>
                        <c:if test="${S002Data['platformType'] == '1'}">银行</c:if>
                        <c:if test="${S002Data['platformType'] == '2'}">非银行</c:if>
                    </td>

                    <td>
                            ${S002Data['platformCode']}
                    </td>

                    <td>
                            ${S002Data['registerTime']}
                    </td>


                </tr>
            </c:forEach>
            </tbody>
        </table>



    </fieldset>


<fieldset>
    <legend>贷款申请详情</legend>
    <thead>
    <tr>
        <th>周期</th>
        <th colspan="4">${S004['cycle']}</th>
    </tr>
    </thead>

    <thead>
    <tr>
        <th>平台类型(0 全部 1 银行 2 非银行)</th>
        <th>平台代码</th>
        <th>申请时间</th>
        <th>申请金额区间</th>
        <th>申请金额区间</th>
    </tr>
    </thead>
    <tbody>


    <c:forEach items="${S004Datas}" var="S004Data">
        <tr>
            <td>
                <c:if test="${S004Data['platformType'] == '0'}">全部</c:if>
                <c:if test="${S004Data['platformType'] == '1'}">银行</c:if>
                <c:if test="${S004Data['platformType'] == '2'}">非银行</c:if>
            </td>

            <td>
                    ${S004Data['platformCode']}
            </td>

            <td>
                    ${S004Data['applicationTime']}
            </td>
            <td>
                    ${S004Data['applicationAmount']}
            </td>
            <td>
                    ${S004Data['applicationResult']}
            </td>

        </tr>
    </c:forEach>
    </tbody>
    </table>

</fieldset>


<fieldset>
    <legend>贷款放款详情</legend>
    <thead>
    <tr>
        <th>周期</th>
        <th colspan="3">${S007['cycle']}</th>
    </tr>
    </thead>

    <thead>
    <tr>
        <th>平台类型(0 全部 1 银行 2 非银行)</th>
        <th>平台代码</th>
        <th>放款时间</th>
        <th>放款金额区间</th>
    </tr>
    </thead>
    <tbody>


    <c:forEach items="${S007Datas}" var="S007Data">
        <tr>
            <td>
                <c:if test="${S007Data['platformType'] == '0'}">全部</c:if>
                <c:if test="${S007Data['platformType'] == '1'}">银行</c:if>
                <c:if test="${S007Data['platformType'] == '2'}">非银行</c:if>
            </td>

            <td>
                    ${S007Data['platformCode']}
            </td>

            <td>
                    ${S007Data['loanLendersTime']}
            </td>
            <td>
                    ${S007Data['loanLendersAmount']}
            </td>

        </tr>
    </c:forEach>
    </tbody>
    </table>

</fieldset>


<fieldset>
    <legend>贷款驳回详情</legend>
    <thead>
    <tr>
        <th>周期</th>
        <th colspan="2">${S009['cycle']}</th>
    </tr>
    </thead>

    <thead>
    <tr>
        <th>平台类型(0 全部 1 银行 2 非银行)</th>
        <th>平台代码</th>
        <th>驳回时间</th>
    </tr>
    </thead>
    <tbody>


    <c:forEach items="${S009Datas}" var="S009Data">
        <tr>
            <td>
                <c:if test="${S009Data['platformType'] == '0'}">全部</c:if>
                <c:if test="${S009Data['platformType'] == '1'}">银行</c:if>
                <c:if test="${S009Data['platformType'] == '2'}">非银行</c:if>
            </td>

            <td>
                    ${S009Data['platformCode']}
            </td>

            <td>
                    ${S009Data['rejectionTime']}
            </td>

        </tr>
    </c:forEach>
    </tbody>
    </table>

</fieldset>

<fieldset>
    <legend>逾期平台详情查询</legend>

    <thead>
    <tr>
        <th>周期</th>
        <th colspan="3">${S012['cycle']}</th>
    </tr>
    </thead>

    <thead>
    <tr>
        <th>逾期数量</th>
        <th>逾期金额区间</th>
        <th>平台代码</th>
        <th>最近逾期时间</th>
    </tr>
    </thead>
    <tbody>


    <c:forEach items="${S012Datas}" var="S012Data">
        <tr>
            <td>
                    ${S012Data['counts']}
            </td>

            <td>
                    ${S012Data['money']}
            </td>

            <td>
                    ${S012Data['platformCode']}
            </td>

            <td>
                    ${S012Data['dTime']}
            </td>

        </tr>
    </c:forEach>
    </tbody>
    </table>


</fieldset>


<fieldset>
    <legend>逾期平台详情查询</legend>

    <thead>
    <tr>
        <th>周期</th>
        <th >${S013['cycle']}</th>
    </tr>
    </thead>

    <thead>
    <tr>
        <th>欠款金额区间</th>
        <th>平台代码</th>

    </tr>
    </thead>
    <tbody>


    <c:forEach items="${S013Datas}" var="S013Data">
        <tr>
            <td>
                    ${S013Data['money']}
            </td>
            <td>
                    ${S013Data['platformCode']}
            </td>
        </tr>
    </c:forEach>
    </tbody>
    </table>

</fieldset>

</body>
</html>