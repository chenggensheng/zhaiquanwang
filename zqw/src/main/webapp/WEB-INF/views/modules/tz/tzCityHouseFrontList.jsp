<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>城市查询</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" href="${ctxStatic}/amazeui/css/amazeui.min.css"/>
<link rel="stylesheet" href="${ctxStatic}/default/style.css"/>
<script src="${ctxStatic}/amazeui/js/jquery.min.js"></script>
<script src="${ctxStatic}/amazeui/js/amazeui.min.js"></script>
</head>

<body>
<div class="container">

    <header data-am-widget="header" class="am-header am-header-default my-header">
        <h1 class="am-header-title">
            <a href="#title-link" class="">南昌市住宅</a>
        </h1>
    </header>

    <div class="gray-panel">
        <div class="my-search-title-panel">
            <div class="am-input-group">
                <input type="text" class="am-form-field am-radius" name="searchValue" id="searchValue" value="${searchValue}">
                <span class="am-input-group-btn">
                <button id="searchButton" class="am-btn am-radius" type="button"><span class="am-icon-search"></span></button>
              </span>
            </div>
        </div>
    </div>
    <!--头部导航-->
    <div class="am-cf cart-panel">
        <c:forEach items="${tzCityHouseLists}" var="tzCityHouse">
    	<div class="withdrawals-panel">
        	<p class="groupby-t-p"><span class="am-fr">${tzCityHouse.numberOfBuilding}</span>${tzCityHouse.areaName}-${tzCityHouse.flowNum}</p>
            <p class="groupby-t-p"><span class="am-fr">${tzCityHouse.otherNameOne}</span>${tzCityHouse.otherNameTow}</p>
        </div>
        </c:forEach>
    </div>
    
    <footer data-am-widget="footer" class="am-footer am-footer-default" data-am-footer="{  }">
        <hr data-am-widget="divider" style="" class="am-divider am-divider-default"/>
      <div class="am-footer-miscs ">
        <p>CopyRight©2019 江西同致地产.</p>
      </div>
    </footer>
    <!--底部-->
</div>

<script type="text/javascript">

    $("#searchButton").click(function () {
        var searchValue = $("#searchValue").val();
        location.href ="${ctxf}/tz/frontHouse?searchValue="+searchValue;
    })
</script>
</body>
</html>
