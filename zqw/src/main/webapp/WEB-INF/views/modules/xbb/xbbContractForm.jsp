<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮合同管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					 var inputForm= $(parent.document.body).children("div[id='inputFormDiv']").append($("#inputForm"));
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);

				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>

	<div class="row">
	<div class="col-sm-12">
			<div class="ibox-content">

	<form:form id="inputForm" modelAttribute="xbbContract" action="${ctx}/xbb/xbbContract/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<div class="form-group">
			<label class="col-sm-2 control-label">案件/服务/项目名称：</label>
			<div class="col-sm-4">
				<form:input path="entryname" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">债权编号/项目编号：</label>
			<div class="col-sm-4">
				<form:input path="claimnumber" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">正式债权编号：</label>
			<div class="col-sm-4">
				<form:input path="officialclaimnumber" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">签订人：</label>
			<div class="col-sm-4">


				<form:hidden path="signedpersonid" />
				<form:input path="signedperson" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;" value="${user.name}" class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">钉钉用印审批编号：</label>
			<div class="col-sm-4">
				<form:input path="approvanumber" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<form:hidden path="customerid"/>
			<label class="col-sm-2 control-label">客户名称：</label>
			<div class="col-sm-4">
				<form:input path="customername" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">客户简称：</label>
			<div class="col-sm-4">
				<form:input path="customerabbreviation" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>

			<label class="col-sm-2 control-label">客户联系人：</label>
			<div class="col-sm-4">
				<form:select path="customerContract" cssStyle="width: 227px;" >
					<form:options items="${xbbContactsList}" itemLabel="xm" itemValue="xm" htmlEscape="false"/>
				</form:select>

			</div>
		</div>





		<div class="form-group">
			<label class="col-sm-2 control-label">债务人名称/对方名称：</label>
			<div class="col-sm-4">
				<form:input path="nameofdebtorpartyname" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">证照号码：</label>
			<div class="col-sm-4">
				<form:input path="certificatenumber" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">债务人联系人及职务：</label>
			<div class="col-sm-4">
				<form:input path="debtorcontactsandduties" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">债务人/对方联系方式：</label>
			<div class="col-sm-4">
				<form:input path="debtorpartycontactmode" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">合同状态：</label>
			<div class="col-sm-4">
				<form:select path="statecontract">
					<form:options items="${fns:getDictList('state_contract')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
			<label class="col-sm-2 control-label">签订日期：</label>
			<div class="col-sm-4">

				<form:input path="signeddate" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>

				<%--<input name="signeddate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "--%>
					   <%--value="<fmt:formatDate value="${xbbContract.signeddate}" pattern="yyyy-MM-dd"/>"--%>
					   <%--onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>--%>
			</div>

		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">合同商务条款：</label>
			<div class="col-sm-4">
				<form:input path="contracttermsbusiness" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">关联合同类型：</label>
			<div class="col-sm-4">
				<form:input path="typesrelatedcontracts" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">合同金额：</label>
			<div class="col-sm-4">
				<form:input path="contractamount" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">实际最后还款日：</label>
			<div class="col-sm-4">
				<input name="actualrepaymentdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					   value="<fmt:formatDate value="${xbbContract.actualrepaymentdate}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>

			</div>
		</div>


		<div class="form-group">
			<label class="col-sm-2 control-label">承办团队：</label>
			<div class="col-sm-4">
				<form:input path="hostteam" htmlEscape="false"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">承办律师：</label>
			<div class="col-sm-4">
				<form:input path="toundertakelawyer" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">承办状态：</label>
			<div class="col-sm-4">
				<form:input path="hoststate" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">收费方式：</label>
			<div class="col-sm-4">
				<form:select path="chargemode">
					<form:options items="${fns:getDictList('charge_mode')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>

		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">材料扫描件：</label>
			<div class="col-sm-4">
				<form:input path="materialscanning" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">材料清单：</label>
			<div class="col-sm-4">
				<form:input path="materiallist" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">案情简介：</label>
			<div class="col-sm-4">
				<form:input path="briefintroduction" htmlEscape="false" maxlength="1025"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">欠款总金额：</label>
			<div class="col-sm-4">
				<form:input path="totalamounarrears" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">纠纷合同签订日：</label>
			<div class="col-sm-4">
				<input name="disputesigningdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					   value="<fmt:formatDate value="${xbbContract.disputesigningdate}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</div>
			<label class="col-sm-2 control-label">约定最后还款日：</label>
			<div class="col-sm-4">
				<input name="agreedrepaymentdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					   value="<fmt:formatDate value="${xbbContract.agreedrepaymentdate}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>

			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">过往催收经过：</label>
			<div class="col-sm-4">
				<form:input path="pastcollection" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
			<label class="col-sm-2 control-label">逾期原因：</label>
			<div class="col-sm-4">
				<form:input path="reasooverdue" htmlEscape="false"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>



		<div class="form-group">
			<label class="col-sm-2 control-label">客户需求：</label>
			<div class="col-sm-4">
				<form:input path="customerdemand" htmlEscape="false"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>

		<div class="form-group" style="text-align: center">
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
			<input id="btnCancel" class="btn btn-info" type="button" value="关闭" onclick="parent.layer.closeAll();"/>
		</div>
	</form:form>
</body>
</html>