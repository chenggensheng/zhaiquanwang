<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮合同管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }


         $(function () {
            $('#contentTable').bootstrapTable({
                showColumns:false,
                showExport: false,                     //是否显示导出
                exportDataType: "all",              //basic', 'all', 'selected'.
                url: '${ctx}/xbb/xbbContract/listJSON',//请求后台的URL（*）
                method: 'post',//请求方式（*）
                contentType: "application/x-www-form-urlencoded",//必须要有！！！！
                toolbar: '#toolbar',//工具按钮用哪个容器
                striped: true,//是否显示行间隔色
                cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true,//是否显示分页（*）
                sortable: false, //是否启用排序

                sidePagination: "server",//分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1, //初始化加载第一页，默认第一页
                pageSize: 10,//每页的记录行数（*）
                pageList: [10, 25, 50, 100],//可供选择的每页的行数（*）
                strictSearch: true,
                clickToSelect: true,//是否启用点击选中行

                uniqueId: "id",//每一行的唯一标识，一般为主键列
                cardView: false,//是否显示详细视图
                detailView: false,//是否显示父子表
                queryParams:queryParams,//参数  
                columns: [
                    {
                        title:'序&nbsp;&nbsp;号',
                        align: 'center',
                        formatter:function(value, row, index){
                            return index+1;
                        }
                    },{
                        field: 'id',
                        title: '主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;键',
                        visible:false
                    },
                    	{
                        field: 'id',
                        title: '债务人/对方联系方式'
                        },
                    	{
                        field: 'debtorpartycontactmode',
                        title: 'debtorpartycontactmode'
                        },
                    	{
                        field: 'nameofdebtorpartyname',
                        title: '债务人名称/对方名称'
                        },
                    	{
                        field: 'debtorcontactsandduties',
                        title: '债务人联系人及职务'
                        },
                    	{
                        field: 'claimnumber',
                        title: '债权编号/项目编号'
                        },
                    	{
                        field: 'typesrelatedcontracts',
                        title: '关联合同类型'
                        },
                    	{
                        field: 'createid',
                        title: '创建人ID'
                        },
                    	{
                        field: 'createname',
                        title: '创建人'
                        },
                    	{
                        field: 'contracttermsbusiness',
                        title: '合同商务条款'
                        },
                    	{
                        field: 'statecontract',
                        title: '合同状态'
                        },
                    	{
                        field: 'contractamount',
                        title: '合同金额'
                        },
                    	{
                        field: 'actualrepaymentdate',
                        title: '实际最后还款日'
                        },
                    	{
                        field: 'customerid',
                        title: '客户ID'
                        },
                    	{
                        field: 'customername',
                        title: '客户名称'
                        },
                    	{
                        field: 'customerabbreviation',
                        title: '客户简称'
                        },
                    	{
                        field: 'customerdemand',
                        title: '客户需求'
                        },
                    	{
                        field: 'hostteam',
                        title: '承办团队'
                        },
                    	{
                        field: 'toundertakelawyer',
                        title: '承办律师'
                        },
                    	{
                        field: 'hoststate',
                        title: '承办状态'
                        },
                    	{
                        field: 'chargemode',
                        title: '收费方式'
                        },
                    	{
                        field: 'materialscanning',
                        title: '材料扫描件'
                        },
                    	{
                        field: 'materiallist',
                        title: '材料清单'
                        },
                    	{
                        field: 'entryname',
                        title: '案件/服务/项目名称'
                        },
                    	{
                        field: 'briefintroduction',
                        title: '案情简介'
                        },
                    	{
                        field: 'totalamounarrears',
                        title: '欠款总金额'
                        },
                    	{
                        field: 'officialclaimnumber',
                        title: '正式债权编号'
                        },
                    	{
                        field: 'signedperson',
                        title: '签订人'
                        },
                    	{
                        field: 'signedpersonid',
                        title: '签订人ID'
                        },
                    	{
                        field: 'signeddate',
                        title: '签订日期'
                        },
                    	{
                        field: 'disputesigningdate',
                        title: '纠纷合同签订日'
                        },
                    	{
                        field: 'agreedrepaymentdate',
                        title: '约定最后还款日'
                        },
                    	{
                        field: 'certificatenumber',
                        title: '证照号码'
                        },
                    	{
                        field: 'pastcollection',
                        title: '过往催收经过'
                        },
                    	{
                        field: 'reasooverdue',
                        title: '逾期原因'
                        },
                    	{
                        field: 'approvanumber',
                        title: '钉钉用印审批编号'
                        },

                    {
                        title: '操作',
                        field: 'id',
                        align: 'center',
                        formatter:function(value,row,index){
                            var e = '<a class="update" href="#" onclick="add(\''+row.id+'\')">修改</a> ';
                            var d = '<a href="${ctx}/xbb/xbbContract/delete?id='+row.id+'" onclick="return confirm(\'“确认要删除该用户吗？” \')">删除</a> ';
                            return e+d;
                        }
                    }
                ]
            });
            $('#btnSubmit').click(function () {
                $("#contentTable").bootstrapTable('refresh',{'url':'${ctx}/xbb/xbbContract/listJSON',queryParams:queryParams});

            });
            function queryParams(params) {  //配置参数
                $("#pageSize").val(params.limit);
                $("#pageNo").val(params.offset/params.limit+1);
                var jsonData = $(searchForm).serializeArray();
                return jsonData;
            }
            //三个参数，value代表该列的值
            function dateFormart(value,row,index){
                if(value !=null && value.length> 9){
                    return value.substr(0, 10);
                }else{
                    return value;
                }
            }
        });

        function add(id) {

            var titleText;
            if(id == "" || id==undefined){
                titleText = "添加";
                contentText = "${ctx}/xbb/xbbContract/form";
			}else{
                titleText = "修改";
                contentText = "${ctx}/xbb/xbbContract/form?id="+id;
			}

            layer.open({
				type:2,
				title:titleText + "肖帮帮合同管理",
                content:contentText,
                area: ['810px', $(top.document).height()-140],
                end: function(){
                    // 如果是通过单击关闭按钮关闭弹出层，父画面没有此表单
                    if($("#inputForm").length === 1){
                        $("#inputForm").submit();
                    }
                },
                cancel:function () {

                }
			});
        }

	</script>
</head>
<body >
    <div class="wrapper wrapper-content">
    <div class="row">
    <div class="col-sm-12" style="padding-right: 0px; padding-left: 0px;">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>肖帮帮合同管理管理</h5>
					<div class="ibox-tools">
						<a href="javascript:add();" class="btn btn-primary btn-xs">肖帮帮合同管理添加</a>
					</div>
				</div>

<div class="ibox-content">

	<form:form id="searchForm" modelAttribute="xbbContract" action="${ctx}/xbb/xbbContract/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>

			<button id="btnSubmit" class="btn btn-primary" type="submit" onclick="return page();">查询</button>
	</form:form>
			</div>
			</div>
		</div>
	<sys:message content="${message}"/>
		<div >
		<table id="contentTable"  data-mobile-responsive="true" >
		</table>
		</div>
     </div>
		</div>

	<div id="inputFormDiv" style="display:none;">
</div>
</body>
</html>