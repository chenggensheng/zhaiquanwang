<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮客户管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }


         $(function () {
            $('#contentTable').bootstrapTable({
                showColumns:false,
                showExport: false,                     //是否显示导出
                exportDataType: "all",              //basic', 'all', 'selected'.
                url: '${ctx}/xbb/xbbCustomer/listJSON',//请求后台的URL（*）
                method: 'post',//请求方式（*）
                contentType: "application/x-www-form-urlencoded",//必须要有！！！！
                toolbar: '#toolbar',//工具按钮用哪个容器
                striped: true,//是否显示行间隔色
                cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true,//是否显示分页（*）
                sortable: false, //是否启用排序

                sidePagination: "server",//分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1, //初始化加载第一页，默认第一页
                pageSize: 10,//每页的记录行数（*）
                pageList: [15, 25, 50, 100],//可供选择的每页的行数（*）
                strictSearch: true,
                clickToSelect: true,//是否启用点击选中行
				hight:"48%",

                uniqueId: "id",//每一行的唯一标识，一般为主键列
                cardView: false,//是否显示详细视图
                detailView: false,//是否显示父子表
                queryParams:queryParams,//参数  
                columns: [
                    {
                        title:'序&nbsp;&nbsp;号',
                        align: 'center',
                        formatter:function(value, row, index){
                            return index+1;
                        }
                    },{
                        field: 'id',
                        title: '主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;键',
                        visible:false
                    },
                    	{
                        field: 'createid',
                        title: '创建人ID',
                         visible:false
                        },

                    	{
                        field: 'addr',
                        title: '地址'
                        },
                    	{
                        field: 'customer',
                        title: '客户名称'
                        },
                    	{
                        field: 'customersource',
                        title: '客户来源'
                        },
                    	{
                        field: 'customerstatus',
                        title: '客户状态'
                        },
                    	{
                        field: 'customerphone',
                        title: '号码'
                        },

                    	{
                        field: 'creditcode',
                        title: '身份证号/信用代码'
                        },
                    	{
                        field: 'importance',
                        title: '重要程度'
                        },
                    	{
                        field: 'customerabbreviation',
                        title: '客户简称'
                        },
                    	 {
                        title: '操作',
                        field: 'id',
                        align: 'center',
                        formatter:function(value,row,index){
                            var e = '<a class="update" href="#" onclick="add(\''+row.id+'\')">添加合同</a> ';
                            return e;
                        }
                      }
                ]
            });
            $('#btnSubmit').click(function () {
                $("#contentTable").bootstrapTable('refresh',{'url':'${ctx}/xbb/xbbCustomer/listJSON',queryParams:queryParams});

            });
            function queryParams(params) {  //配置参数
                $("#pageSize").val(params.limit);
                $("#pageNo").val(params.offset/params.limit+1);
                var jsonData = $(searchForm).serializeArray();
                return jsonData;
            }
            //三个参数，value代表该列的值
            function dateFormart(value,row,index){
                if(value !=null && value.length> 9){
                    return value.substr(0, 10);
                }else{
                    return value;
                }
            }
        });

        function add(id) {
            var titleText = "添加";
            contentText = "${ctx}/xbb/xbbContract/form?customerid="+id;
            layer.open({
				type:2,
				title:titleText + "肖帮帮客户合同管理",
                content:contentText,
                area: ['810px', $(top.document).height()-140],
                end: function(){
                    // 如果是通过单击关闭按钮关闭弹出层，父画面没有此表单
                    if($("#inputForm").length === 1){
                        $("#inputForm").submit();
                    }
                },
                cancel:function () {

                }
			});
        }

	</script>
</head>
<body >
    <div class="wrapper wrapper-content">
    <div class="row">
    <div class="col-sm-12" style="padding-right: 0px; padding-left: 0px;">
		<form:form id="searchForm" modelAttribute="xbbCustomer" action="${ctx}/xbb/xbbCustomer/" method="post" class="form-inline">
			<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
			<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>

			<%--<button id="btnSubmit" class="btn btn-primary" type="submit" onclick="return page();">查询</button>--%>
		</form:form>
		</div>
	<sys:message content="${message}"/>
		<div >
		<table id="contentTable"  data-mobile-responsive="true" >
		</table>
		</div>
     </div>
		</div>

	<div id="inputFormDiv" style="display:none;">
</div>
</body>
</html>