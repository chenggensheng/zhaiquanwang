<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>联系人管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					 var inputForm= $(parent.document.body).children("div[id='inputFormDiv']").append($("#inputForm"));
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);

				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>

	<div class="row">
	<div class="col-sm-12">
			<div class="ibox-content">

	<form:form id="inputForm" modelAttribute="xbbContacts" action="${ctx}/xbb/xbbContacts/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="form-group">
			<label class="col-sm-2 control-label">sort1：</label>
			<div class="col-sm-4">
				<form:input path="sort1" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">sort2：</label>
			<div class="col-sm-4">
				<form:input path="sort2" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">sort3：</label>
			<div class="col-sm-4">
				<form:input path="sort3" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">关联客户：</label>
			<div class="col-sm-4">
				<form:input path="customerid" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">决策关系：</label>
			<div class="col-sm-4">
				<form:input path="jcgx" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">区/县：</label>
			<div class="col-sm-4">
				<form:input path="xq" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">地址：</label>
			<div class="col-sm-4">
				<form:input path="dz" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">备注：</label>
			<div class="col-sm-4">
				<form:input path="bz" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">姓名：</label>
			<div class="col-sm-4">
				<form:input path="xm" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">市：</label>
			<div class="col-sm-4">
				<form:input path="sjx" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">性别：</label>
			<div class="col-sm-4">
				<form:input path="xb" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">手机：</label>
			<div class="col-sm-4">
				<form:input path="sj" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">省：</label>
			<div class="col-sm-4">
				<form:input path="sjxx" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">级别：</label>
			<div class="col-sm-4">
				<form:input path="jb" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">职务：</label>
			<div class="col-sm-4">
				<form:input path="zw" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">部门：</label>
			<div class="col-sm-4">
				<form:input path="bm" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">重要程度：</label>
			<div class="col-sm-4">
				<form:input path="zycd" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<shiro:hasPermission name="xbb:xbbContacts:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn btn-info" type="button" value="关闭" onclick="parent.layer.closeAll();"/>
		</div>
	</form:form>
</body>
</html>