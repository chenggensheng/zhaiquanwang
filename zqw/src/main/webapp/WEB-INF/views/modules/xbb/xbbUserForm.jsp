<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮用户管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					 var inputForm= $(parent.document.body).children("div[id='inputFormDiv']").append($("#inputForm"));
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);

				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>

	<div class="row">
	<div class="col-sm-12">
			<div class="ibox-content">

	<form:form id="inputForm" modelAttribute="xbbUser" action="${ctx}/xbb/xbbUser/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="form-group">
			<label class="col-sm-2 control-label">active：</label>
			<div class="col-sm-4">
				<form:input path="active" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">addtime：</label>
			<div class="col-sm-4">
				<form:input path="addtime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">avatar：</label>
			<div class="col-sm-4">
				<form:input path="avatar" htmlEscape="false" maxlength="1024"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">department：</label>
			<div class="col-sm-4">
				<form:input path="department" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">isleaderindepts：</label>
			<div class="col-sm-4">
				<form:input path="isleaderindepts" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">name：</label>
			<div class="col-sm-4">
				<form:input path="name" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">position：</label>
			<div class="col-sm-4">
				<form:input path="position" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">updatetime：</label>
			<div class="col-sm-4">
				<form:input path="updatetime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<shiro:hasPermission name="xbb:xbbUser:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn btn-info" type="button" value="关闭" onclick="parent.layer.closeAll();"/>
		</div>
	</form:form>
</body>
</html>