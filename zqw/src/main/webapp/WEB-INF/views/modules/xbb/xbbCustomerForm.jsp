<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮客户管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					 var inputForm= $(parent.document.body).children("div[id='inputFormDiv']").append($("#inputForm"));
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);

				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>

	<div class="row">
	<div class="col-sm-12">
			<div class="ibox-content">

	<form:form id="inputForm" modelAttribute="xbbCustomer" action="${ctx}/xbb/xbbCustomer/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="form-group">
			<label class="col-sm-2 control-label">下次预约联系时间：</label>
			<div class="col-sm-4">
				<form:input path="nextContactTime" htmlEscape="false" maxlength="64"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">创建人ID：</label>
			<div class="col-sm-4">
				<form:input path="createid" htmlEscape="false" maxlength="64"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">创建时间：</label>
			<div class="col-sm-4">
				<form:input path="createtime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">区/县：</label>
			<div class="col-sm-4">
				<form:input path="districtcounty" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">图片：</label>
			<div class="col-sm-4">
				<form:input path="picture" htmlEscape="false" maxlength="1255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">地址：</label>
			<div class="col-sm-4">
				<form:input path="addr" htmlEscape="false" maxlength="1255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">remarks：</label>
			<div class="col-sm-4">
				<form:textarea path="remarks" htmlEscape="false" rows="4" class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户关注类别：</label>
			<div class="col-sm-4">
				<form:input path="customerfocuscategory" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户分级：</label>
			<div class="col-sm-4">
				<form:input path="customerclassification" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户名称：</label>
			<div class="col-sm-4">
				<form:input path="customer" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户官网：</label>
			<div class="col-sm-4">
				<form:input path="customerwebsite" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户性质：</label>
			<div class="col-sm-4">
				<form:input path="customernature" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户来源：</label>
			<div class="col-sm-4">
				<form:input path="customersource" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户状态：</label>
			<div class="col-sm-4">
				<form:input path="customerstatus" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">customerphone：</label>
			<div class="col-sm-4">
				<form:input path="customerphone" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户类型：</label>
			<div class="col-sm-4">
				<form:input path="customertype" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户行业：</label>
			<div class="col-sm-4">
				<form:input path="customerindustry" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">展会/客户会名称：</label>
			<div class="col-sm-4">
				<form:input path="customerassociationname" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">市：</label>
			<div class="col-sm-4">
				<form:input path="city" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">文件：</label>
			<div class="col-sm-4">
				<form:input path="filetemp" htmlEscape="false"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">更新时间：</label>
			<div class="col-sm-4">
				<form:input path="updatetime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">注册资金：</label>
			<div class="col-sm-4">
				<form:input path="registeredcapital" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">legalrepresentative：</label>
			<div class="col-sm-4">
				<form:input path="legalrepresentative" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">渠道/代理商名称：</label>
			<div class="col-sm-4">
				<form:input path="nameofchannel" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">省：</label>
			<div class="col-sm-4">
				<form:input path="province" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">纬度：</label>
			<div class="col-sm-4">
				<form:input path="iatitude" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">经度：</label>
			<div class="col-sm-4">
				<form:input path="iongitude" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">身份证号/信用代码：</label>
			<div class="col-sm-4">
				<form:input path="creditcode" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">重要程度：</label>
			<div class="col-sm-4">
				<form:input path="importance" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户简称：</label>
			<div class="col-sm-4">
				<form:input path="customerabbreviation" htmlEscape="false"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户简介：</label>
			<div class="col-sm-4">
				<form:input path="clientprofile" htmlEscape="false"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<shiro:hasPermission name="xbb:xbbCustomer:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn btn-info" type="button" value="关闭" onclick="parent.layer.closeAll();"/>
		</div>
	</form:form>
</body>
</html>