<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮销售计划管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }


         $(function () {
            $('#contentTable').bootstrapTable({
                showColumns:false,
                showExport: false,                     //是否显示导出
                exportDataType: "all",              //basic', 'all', 'selected'.
                url: '${ctx}/xbb/xbbOpportunity/listJSON',//请求后台的URL（*）
                method: 'post',//请求方式（*）
                contentType: "application/x-www-form-urlencoded",//必须要有！！！！
                toolbar: '#toolbar',//工具按钮用哪个容器
                striped: true,//是否显示行间隔色
                cache: false,//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true,//是否显示分页（*）
                sortable: false, //是否启用排序

                sidePagination: "server",//分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1, //初始化加载第一页，默认第一页
                pageSize: 10,//每页的记录行数（*）
                pageList: [10, 25, 50, 100],//可供选择的每页的行数（*）
                strictSearch: true,
                clickToSelect: true,//是否启用点击选中行

                uniqueId: "id",//每一行的唯一标识，一般为主键列
                cardView: false,//是否显示详细视图
                detailView: false,//是否显示父子表
                queryParams:queryParams,//参数  
                columns: [
                    {
                        title:'序&nbsp;&nbsp;号',
                        align: 'center',
                        formatter:function(value, row, index){
                            return index+1;
                        }
                    },{
                        field: 'id',
                        title: '主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;键',
                        visible:false
                    },
                    	{
                        field: 'id',
                        title: 'id'
                        },
                    	{
                        field: 'decisionmaker',
                        title: '决策者'
                        },
                    	{
                        field: 'createtime',
                        title: '创建时间'
                        },
                    	{
                        field: 'contractexpirationdate',
                        title: '合同到期日期'
                        },
                    	{
                        field: 'contracttermsbusiness',
                        title: 'contracttermsbusiness'
                        },
                    	{
                        field: 'contractscanning',
                        title: '合同扫描件'
                        },
                    	{
                        field: 'contracttext',
                        title: '合同文本'
                        },
                    	{
                        field: 'contractsigningdate',
                        title: 'contractsigningdate'
                        },
                    	{
                        field: 'typeofcontract',
                        title: '合同类型'
                        },
                    	{
                        field: 'customerid',
                        title: '客户ID'
                        },
                    	{
                        field: 'customername',
                        title: '客户名称'
                        },
                    	{
                        field: 'currency',
                        title: '币种'
                        },
                    	{
                        field: 'updatetime',
                        title: '更新时间'
                        },
                    	{
                        field: 'competitor',
                        title: '竞争对手'
                        },
                    	{
                        field: 'importance',
                        title: '重要程度'
                        },
                    	{
                        field: 'salesopportunity',
                        title: '销售机会'
                        },
                    	{
                        field: 'salesopportunitynumber',
                        title: '销售机会编号'
                        },
                    	{
                        field: 'salesphase',
                        title: '销售阶段'
                        },
                    	{
                        field: 'estimatedtime',
                        title: '预计结束时间'
                        },
                    	{
                        field: 'estimatedamount',
                        title: '预计金额'
                        },

                    {
                        title: '操作',
                        field: 'id',
                        align: 'center',
                        formatter:function(value,row,index){
                            var e = '<a class="update" href="#" onclick="add(\''+row.id+'\')">修改</a> ';
                            var d = '<a href="${ctx}/xbb/xbbOpportunity/delete?id='+row.id+'" onclick="return confirm(\'“确认要删除该用户吗？” \')">删除</a> ';
                            return e+d;
                        }
                    }
                ]
            });
            $('#btnSubmit').click(function () {
                $("#contentTable").bootstrapTable('refresh',{'url':'${ctx}/xbb/xbbOpportunity/listJSON',queryParams:queryParams});

            });
            function queryParams(params) {  //配置参数
                $("#pageSize").val(params.limit);
                $("#pageNo").val(params.offset/params.limit+1);
                var jsonData = $(searchForm).serializeArray();
                return jsonData;
            }
            //三个参数，value代表该列的值
            function dateFormart(value,row,index){
                if(value !=null && value.length> 9){
                    return value.substr(0, 10);
                }else{
                    return value;
                }
            }
        });

        function add(id) {

            var titleText;
            if(id == "" || id==undefined){
                titleText = "添加";
                contentText = "${ctx}/xbb/xbbOpportunity/form";
			}else{
                titleText = "修改";
                contentText = "${ctx}/xbb/xbbOpportunity/form?id="+id;
			}

            layer.open({
				type:2,
				title:titleText + "肖帮帮销售计划管理",
                content:contentText,
                area: ['810px', $(top.document).height()-140],
                end: function(){
                    // 如果是通过单击关闭按钮关闭弹出层，父画面没有此表单
                    if($("#inputForm").length === 1){
                        $("#inputForm").submit();
                    }
                },
                cancel:function () {

                }
			});
        }

	</script>
</head>
<body >
    <div class="wrapper wrapper-content">
    <div class="row">
    <div class="col-sm-12" style="padding-right: 0px; padding-left: 0px;">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>肖帮帮销售计划管理管理</h5>
					<div class="ibox-tools">
						<a href="javascript:add();" class="btn btn-primary btn-xs">肖帮帮销售计划管理添加</a>
					</div>
				</div>

<div class="ibox-content">

	<form:form id="searchForm" modelAttribute="xbbOpportunity" action="${ctx}/xbb/xbbOpportunity/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>

			<button id="btnSubmit" class="btn btn-primary" type="submit" onclick="return page();">查询</button>
	</form:form>
			</div>
			</div>
		</div>
	<sys:message content="${message}"/>
		<div >
		<table id="contentTable"  data-mobile-responsive="true" >
		</table>
		</div>
     </div>
		</div>

	<div id="inputFormDiv" style="display:none;">
</div>
</body>
</html>