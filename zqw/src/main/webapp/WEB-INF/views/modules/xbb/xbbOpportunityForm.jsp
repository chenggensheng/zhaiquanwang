<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖帮帮销售计划管理管理</title>
	<%@include file="/WEB-INF/views/include/hmhead.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					 var inputForm= $(parent.document.body).children("div[id='inputFormDiv']").append($("#inputForm"));
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);

				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>

	<div class="row">
	<div class="col-sm-12">
			<div class="ibox-content">

	<form:form id="inputForm" modelAttribute="xbbOpportunity" action="${ctx}/xbb/xbbOpportunity/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="form-group">
			<label class="col-sm-2 control-label">决策者：</label>
			<div class="col-sm-4">
				<form:input path="decisionmaker" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">创建时间：</label>
			<div class="col-sm-4">
				<form:input path="createtime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">合同到期日期：</label>
			<div class="col-sm-4">
				<form:input path="contractexpirationdate" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">contracttermsbusiness：</label>
			<div class="col-sm-4">
				<form:input path="contracttermsbusiness" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">合同扫描件：</label>
			<div class="col-sm-4">
				<form:input path="contractscanning" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">合同文本：</label>
			<div class="col-sm-4">
				<form:input path="contracttext" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">contractsigningdate：</label>
			<div class="col-sm-4">
				<form:input path="contractsigningdate" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">合同类型：</label>
			<div class="col-sm-4">
				<form:input path="typeofcontract" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户ID：</label>
			<div class="col-sm-4">
				<form:input path="customerid" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">客户名称：</label>
			<div class="col-sm-4">
				<form:input path="customername" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">币种：</label>
			<div class="col-sm-4">
				<form:input path="currency" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">更新时间：</label>
			<div class="col-sm-4">
				<form:input path="updatetime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">竞争对手：</label>
			<div class="col-sm-4">
				<form:input path="competitor" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">重要程度：</label>
			<div class="col-sm-4">
				<form:input path="importance" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">销售机会：</label>
			<div class="col-sm-4">
				<form:input path="salesopportunity" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">销售机会编号：</label>
			<div class="col-sm-4">
				<form:input path="salesopportunitynumber" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">销售阶段：</label>
			<div class="col-sm-4">
				<form:input path="salesphase" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">预计结束时间：</label>
			<div class="col-sm-4">
				<form:input path="estimatedtime" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">预计金额：</label>
			<div class="col-sm-4">
				<form:input path="estimatedamount" htmlEscape="false" maxlength="255"   cssStyle="display:inline;width: 95%;"  class="form-control "/>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<shiro:hasPermission name="xbb:xbbOpportunity:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn btn-info" type="button" value="关闭" onclick="parent.layer.closeAll();"/>
		</div>
	</form:form>
</body>
</html>