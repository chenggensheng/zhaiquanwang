<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件进展信息管理</title>
	<meta name="decorator" content="default"/>

	<link href="${ctxStatic}/weipingu/animate.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/bootstrap.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/font-awesome.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/style.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/stylenew.css" type="text/css" rel="stylesheet">
	<%--<link href="${ctxStatic}/weipingu/stylenew.css" type="text/css" rel="stylesheet">--%>
	<link rel="stylesheet" href="${ctxStatic}/jquery-Steps-scw/css/ystep.css">
	<script type="text/javascript" src=${ctxStatic}/jquery-Steps-scw/js/setStep.js></script>
	<script type="text/javascript" src=${ctxStatic}/layer/layer.min.js></script>

	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<%--<li><a href="${ctx}/zqwcase/caseSpeed/">案件进展信息列表</a></li>--%>
		<li class="active"><a href="${ctx}/zqwcase/caseSpeed/form?id=${caseSpeed.id}">案件进展信息<shiro:hasPermission name="zqwcase:caseSpeed:edit">${not empty caseSpeed.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="zqwcase:caseSpeed:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="caseSpeed" action="${ctx}/zqwcase/caseSpeed/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
	    <input type="hidden" name="caseInsuranceId" id="caseInsuranceId" value="${caseInsurance.id}"/>
		<sys:message content="${message}"/>
		<div class="form-items item-con">
			<div class="col-md-12 form-item-title">客户信息</div>
			<div class="row">
				<div class="row">
					<div class="col-md-1"><small>债权人:</small></div>
					<div class="col-md-3">
							${caseCustomer.creditor}
					</div>
					<div class="col-md-1"><small>债权人住所地:</small></div>
					<div class="col-md-3">
							${caseCustomer.creditorDomicile}
					</div>
					<div class="col-md-1"><small>债权人信用代码:</small></div>
					<div class="col-md-3">
							${caseCustomer.creditorIdcart}
					</div>
				</div>
				<div class="row">
					<div class="col-md-1"><small>债权人负责人:</small></div>
					<div class="col-md-3">
							${caseCustomer.creditorCharge}
					</div>
					<div class="col-md-1"><small>债权人负责人职位:</small></div>
					<div class="col-md-3">
							${caseCustomer.undertakingLawyerName}
					</div>
					<div class="col-md-1"><small>承办律师/电话:</small></div>
					<div class="col-md-3">
							${caseCustomer.undertakingLawyerName}/${caseCustomer.undertakingLawyerPhone}
					</div>
				</div>
			</div>
			<div class="col-md-12 form-item-title">诉讼情况</div>
				<div class="row">
					<div class="col-md-1"><small>诉讼部承办律师:</small></div>
					<div class="col-md-3">
						<form:select path="attorneyLitigation" disabled="true" class="input-xlarge ">
							<form:options items="${fns:getZqwLayerLists('1')}" itemLabel="layerName" itemValue="id" htmlEscape="false"/>
						</form:select>
					</div>
					<div class="col-md-1"><small>管辖法院:</small></div>
					<div class="col-md-3">
						${caseSpeed.jurisdictionalCourt}
						<%--<form:input path="jurisdictionalCourt" htmlEscape="false" maxlength="655" class="input-xlarge " cssStyle="height: 26px;"/>--%>
					</div>
					<div class="col-md-1"><small>本案判决书出具日期:</small></div>
					<div class="col-md-3">
						<fmt:formatDate value="${caseSpeed.judgmentDate}" pattern="yyyy-MM-dd"/>


					</div>
			  </div>


			<div class="row">
				<div class="col-md-1"><small>立案受理案号:</small></div>
				<div class="col-md-3">
						${caseSpeed.acceptingNum}
					<%--<form:input path="acceptingNum" htmlEscape="false" maxlength="64" class="input-xlarge " cssStyle="height: 26px;"/>--%>
				</div>
				<div class="col-md-1"><small>本案判决金额:</small></div>
				<div class="col-md-3">
						${caseSpeed.judgmentAmount}
					<%--<form:input path="judgmentAmount" htmlEscape="false" maxlength="655" class="input-xlarge " cssStyle="height: 26px;"/>--%>
				</div>
			</div>


			<div class="row">
				<div class="col-md-1"><small>判决内容:</small></div>
				<div class="col-md-5">
						${caseSpeed.judgmentContent}
					<%--<form:textarea path="judgmentContent" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>--%>
				</div>
				<div class="col-md-1"><small>事实和理由:</small></div>
				<div class="col-md-5">
						${caseSpeed.judgmentContent}
					<%--<form:textarea path="judgmentReason" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>--%>
				</div>
			</div>


		<div class="row">
			<div class="col-md-1"><small>执行法院:</small></div>
			<div class="col-md-3">
					${caseSpeed.courtOfExecution}
				<%--<form:input path="courtOfExecution" htmlEscape="false" maxlength="64" class="input-xlarge " cssStyle="height: 26px;"/>--%>
			</div>
			<div class="col-md-1"><small>执行案号:</small></div>
			<div class="col-md-3">
					${caseSpeed.executionNum}

				<%--<form:input path="executionNum" htmlEscape="false" maxlength="655" class="input-xlarge " cssStyle="height: 26px;"/>--%>
			</div>
		</div>

			<div class="row">
				<div class="col-md-1"><small>投资查询情况:</small></div>
				<div class="col-md-11">
						${caseSpeed.investmentInquiries}
					<%--<form:textarea path="investmentInquiries" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>--%>
				</div>
			</div>

			<div class="row">
				<div class="col-md-1"><small>未执行内容:</small></div>
				<div class="col-md-11">
						${caseSpeed.unexecutedContent}

					<%--<form:textarea path="unexecutedContent" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>--%>
				</div>
			</div>

			<div class="col-md-12 form-item-title">项目进展</div>
			<div class="row">
				<div class="col-md-1"><small>进展描述:</small></div>
				<div class="col-md-11">
						${caseSpeed.speedIntroduction}
					<%--<form:textarea path="speedIntroduction" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>--%>
				</div>
			</div>

		<div class="form-actions">
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>