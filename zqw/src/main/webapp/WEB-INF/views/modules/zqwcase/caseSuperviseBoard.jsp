<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <%--<%@include file="/WEB-INF/views/include/hmhead.jsp"%>--%>
        <meta name="decorator" content="newblank"/>

<%--<meta name="decorator" content="default"/>--%>
    <%--<%@include file="/WEB-INF/views/include/hmhead.jsp"%>--%>
    <meta charset="utf-8">
    <%--<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
    <!--360浏览器优先以webkit内核解析-->
    <title>统计展示页面</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="${ctxStatic}/hAdmin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${ctxStatic}/hAdmin/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${ctxStatic}/hAdmin/css/animate.css" rel="stylesheet">
    <link href="${ctxStatic}/hAdmin/css/style.css?v=4.1.2" rel="stylesheet">
    <%--<script src="${ctxStatic}/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>--%>


        <script src="${ctxStatic}/hAdmin/js/jquery.min.js?v=2.1.4"></script>
        <script src="${ctxStatic}/hAdmin/js/bootstrap.min.js?v=3.3.6"></script>

        <!-- layer javascript -->
        <script src="${ctxStatic}/hAdmin/js/plugins/layer/layer.min.js"></script>


        <script type="text/javascript">
        function incrementSyncAll() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/incrementSyncAll",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("同步成功");
                }
            });
        }


        function incrementSaveAll() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/incrementSaveAll",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("同步成功");
                }
            });
        }

        function showList(showType) {
            var contentText = "${ctx}/zqwcase/caseInsurance/showTypeList?showType="+showType;


            var openFull =  top.layer.open({
                type:2,
                title:"案例类别",
                content:contentText,
                area: ['1000px', '600px'],
                end: function(){

                },
                cancel:function () {

                }
            });
            top.layer.full(openFull);

        }

    </script>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12" >
                        <div class="ibox float-e-margins">
                            <div class="ibox-title" style="border-bottom:none;background:#fff;">
                                <h5>保险案件简报  全公司／2019-01-01至2019-12-31</h5>
                                <button type="button" class="btn btn-sm btn-primary" onclick="incrementSyncAll();" >同步数据</button>
                                <button type="button" class="btn btn-sm btn-primary" onclick="incrementSaveAll();">写入CRM</button>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="row row-sm text-center">
                            <div class="col-xs-3">
                                <div class="panel padder-v item">
                                    <div class="h1 text-info font-thin h1">${caseInsuranceBoard.allCount} 件</div>
                                    <span class="text-muted text-xs">案件总数</span>
                                    <div class="top text-right w-full">
                                        <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="panel padder-v item bg-info">
                                    <div class="h1 text-fff font-thin h1">${caseInsuranceBoard.doingCount} 件</div>
                                    <span class="text-muted text-xs">在办案件</span>
                                    <div class="top text-right w-full">
                                        <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="panel padder-v item bg-primary">
                                    <div class="h1 text-fff font-thin h1"><a href="javascript:void(0);" onclick="showList('didCount')">${caseInsuranceBoard.didCount}</a>  件</div>
                                    <span class="text-muted text-xs">完结案件</span>
                                    <div class="top text-right w-full">
                                        <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="panel padder-v item">
                                    <div class="font-thin h1"><a href="javascript:void(0);" onclick="showList('otherCount')">${caseInsuranceBoard.otherCount}</a>  件</div>
                                    <span class="text-muted text-xs">其他案件</span>
                                    <div class="bottom text-left">
                                        <i class="fa fa-caret-up text-warning m-l-sm"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row row-sm text-center">
                            <div class=" col-sm-3" >
                                <div class="ibox-content ">
                                    <ul class="todo-list m-t small-list ui-sortable">
                                        <li>
                                            <a href="widgets.html#" class="check-link"> </a>
                                            <span class="m-l-xs">预立案</span>
                                            <small class="label label-primary"> <a href="javascript:void(0);" onclick="showList('otherCount')">${caseInsuranceBoard.advanceProjectsCount}</a> 件</small>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"> </a>
                                            <span class="m-l-xs">进行中</span>
                                            <small class="label label-primary">${caseInsuranceBoard.doingCount} 件 </small>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"></a>
                                            <span class="m-l-xs">昨日新增</span>
                                            <small class="label label-primary"><a href="javascript:void(0);" onclick="showList('todayCount')">${caseInsuranceBoard.todayCount} </a>件</small>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class=" col-sm-3" >
                                <div class="ibox-content ">

                                    <ul class="todo-list m-t small-list ui-sortable">

                                        <li>
                                            <a href="widgets.html#" class="check-link"> </a>
                                            <span class="m-l-xs">非诉阶段</span>
                                            <small class="label label-primary"> ${caseInsuranceBoard.nonComplaintCount} 件</small>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"> </a>
                                            <span class="m-l-xs">诉讼阶段</span>
                                            <small class="label label-primary">${caseInsuranceBoard.litigationsCount} 件 </small>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"></a>
                                            <span class="m-l-xs">执行阶段</span>
                                            <small class="label label-primary">${caseInsuranceBoard.implementsCount} 件 </small>
                                        </li>



                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-3" >
                                <div class="ibox-content ">

                                    <ul class="todo-list m-t small-list ui-sortable">
                                        <li>
                                            <small class="label label-primary"></small>
                                            <span class="m-l-xs"></span>

                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"> </a>
                                            <small class="label label-primary">${caseInsuranceBoard.selfLitigationsCount} 件</small>
                                            <span class="m-l-xs">自办</span>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"></a>
                                            <small class="label label-primary">${caseInsuranceBoard.selfImplementsCount} 件</small>
                                            <span class="m-l-xs">自办</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-3" >
                                <div class="ibox-content ">
                                    <ul class="todo-list m-t small-list ui-sortable">
                                        <li>
                                            <small class="label label-primary"></small>
                                            <span class="m-l-xs"></span>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"> </a>
                                            <small class="label label-primary">${caseInsuranceBoard.outLitigationsCount} 件</small>
                                            <span class="m-l-xs">委外</span>
                                        </li>
                                        <li>
                                            <a href="widgets.html#" class="check-link"></a>
                                            <small class="label label-primary">${caseInsuranceBoard.outImplementsCount} 件</small>
                                            <span class="m-l-xs">委外</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row row-sm text-center">
                            <div class="col-xs-3">
                                <div class="panel padder-v item">
                                    <div class="h1 text-info font-thin h4">¥ ${caseInsuranceBoard.allMoneyCount} 元</div>
                                    <span class="text-muted text-xs">合同订单总金额</span>
                                    <div class="top text-right w-full">
                                        <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="panel padder-v item bg-info">
                                    <div class="h1 text-fff font-thin h4">¥  ${caseInsuranceBoard.planMoneyCount} 元</div>
                                    <span class="text-muted text-xs">计划收款金额</span>
                                    <div class="top text-right w-full">
                                        <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="panel padder-v item bg-primary">
                                    <div class="h1 text-fff font-thin h4">¥ ${caseInsuranceBoard.overMonthCount} 元</div>
                                    <span class="text-muted text-xs">超1个月合同订单金额</span>
                                    <div class="top text-right w-full">
                                        <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="panel padder-v item">
                                    <div class="font-thin h4"> ${caseInsuranceBoard.backRaio} %</div>
                                    <span class="text-muted text-xs">回款比例</span>
                                    <div class="bottom text-left">
                                        <i class="fa fa-caret-up text-warning m-l-sm"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">

                            <div class="ibox-content">
                                <div class="row m-b-sm m-t-sm">
                                    <div class="col-md-1">
                                    </div>

                                </div>
                                <div class="project-list">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th class="project-status">
                                                风险数据
                                            </th>
                                            <th class="project-status">
                                                不足一个月（件）
                                            </th>
                                            <th class="project-status">
                                                不足二个月（件）
                                            </th>

                                            <th class="project-status">
                                                不足三个月（件）
                                            </th>
                                        </tr>

                                        <c:forEach items="${caseInsuranceBoard.caseInsuranceRisks}" var="caseInsuranceRisk">
                                            <tr>
                                                <td class="project-status">
                                                    ${caseInsuranceRisk.typeName}
                                                </td>
                                                <td class="project-status">
                                                    <c:if test="${caseInsuranceRisk.type eq '0'}">
                                                        <a href="javascript:void(0);" onclick="showList('oneMounthImplementsRisk')">${caseInsuranceRisk.oneMounthCount}</a>
                                                    </c:if>
                                                    <c:if test="${caseInsuranceRisk.type eq '1'}">
                                                        <a href="javascript:void(0);" onclick="showList('oneMounthNonComplaintRisk')">${caseInsuranceRisk.oneMounthCount}</a>
                                                    </c:if>
                                                    <c:if test="${caseInsuranceRisk.type eq '2'}">
                                                        <a href="javascript:void(0);" onclick="showList('oneMounthLitigationsRisk')">${caseInsuranceRisk.oneMounthCount}</a>
                                                    </c:if>
                                                </td>
                                                <td class="project-status">
                                                        <%--${caseInsuranceRisk.towMounthCount}--%>
                                                            <c:if test="${caseInsuranceRisk.type eq '0'}">
                                                                <a href="javascript:void(0);" onclick="showList('towMounthImplementsRisk')">${caseInsuranceRisk.towMounthCount}</a>
                                                            </c:if>
                                                            <c:if test="${caseInsuranceRisk.type eq '1'}">
                                                                <a href="javascript:void(0);" onclick="showList('towMounthNonComplaintRisk')">${caseInsuranceRisk.towMounthCount}</a>
                                                            </c:if>
                                                            <c:if test="${caseInsuranceRisk.type eq '2'}">
                                                                <a href="javascript:void(0);" onclick="showList('towMounthLitigationsRisk')">${caseInsuranceRisk.towMounthCount}</a>
                                                            </c:if>
                                                </td>
                                                <td class="project-status">

                                                    <c:if test="${caseInsuranceRisk.type eq '0'}">
                                                        <a href="javascript:void(0);" onclick="showList('threeMounthImplementsRisk')">${caseInsuranceRisk.threeMounthCount}</a>
                                                    </c:if>
                                                    <c:if test="${caseInsuranceRisk.type eq '1'}">
                                                        <a href="javascript:void(0);" onclick="showList('threeMounthNonComplaintRisk')">${caseInsuranceRisk.threeMounthCount}</a>
                                                    </c:if>
                                                    <c:if test="${caseInsuranceRisk.type eq '2'}">
                                                        <a href="javascript:void(0);" onclick="showList('threeMounthLitigationsRisk')">${caseInsuranceRisk.threeMounthCount}</a>
                                                    </c:if>
                                                        <%--${caseInsuranceRisk.threeMounthCount}--%>
                                                </td>
                                            </tr>
                                        </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">

                            <div class="ibox-content">
                                <div class="row m-b-sm m-t-sm">
                                    <div class="col-md-1">
                                    </div>

                                </div>
                                <div class="project-list">
                                    <table class="table table-hover">
                                        <tbody>

                                        <tr>
                                            <th class="project-status">
                                                   姓名
                                                </th>

                                            <th class="project-status">

                                            </th>
                                            <th class="project-status">
                                                非诉阶段
                                            </th>

                                            <th class="project-status">

                                            </th>


                                            <th class="project-status">
                                                诉讼阶段
                                            </th>

                                            <th class="project-status">
                                                执行阶段
                                            </th>



                                        </tr>

                                        <c:forEach items="${caseInsuranceBoard.caseInsurancePersonBoards}" var="caseInsurancePersonBoard">
                                            <tr>
                                                <td class="project-status">
                                                   ${caseInsurancePersonBoard.name}
                                                       <br>
                                                       今日跟进
                                                       <br>
                                                       <small style="float: left">${caseInsurancePersonBoard.todayFollowAllCaseCount}</small><small style="float: right">跟进次数</small>
                                                       <br>
                                                       <small style="float: left">${caseInsurancePersonBoard.todayFollowCaseCount}</small><small style="float: right">涉及案件</small>
                                                </td>
                                                <td class="project-status"></td>
                                                <td class="project-title">
                                                    <a href="">${caseInsurancePersonBoard.nonComplaintCount}件</a>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.nonOverOneComplatinCount}</small><small style="float: right">超1</small>

                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.nonOverTowComplatinCount}</small><small style="float: right">超2</small>

                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.yCount}</small><small style="float: right">意</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.eCount}</small><small style="float: right">恶</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.wCount}</small><small style="float: right">无</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.sCount}</small><small style="float: right">失</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.qCount}</small><small style="float: right" >其</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.xCount}</small><small style="float: right">修</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.qqCount}</small><small style="float: right" >缺</small>
                                                    <br>
                                                    <small style="float: left"> ${caseInsurancePersonBoard.bCount}</small><small style="float: right">补</small>
                                                </td>
                                                <td class="project-status"></td>
                                                <td class="project-title">
                                                    <table><tr>
                                                        <td colspan="2">${caseInsurancePersonBoard.litigationsCount}件</td></tr>
                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfLitigationsCount}</small><small style="float: right"> 自办其中：</small> </td>
                                                            <td><small style="float: left" >${caseInsurancePersonBoard.outLitigationsCount}</small><small style="float: right">委外 其中：</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfOverOneCount}</small><small style="float: right">超1未立</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverfiveCount}</small><small style="float: right">委外超15天</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfOverTowCount}</small><small style="float: right">超2未立</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverthreeCount}</small><small style="float: right">委外超30天</small></td>
                                                        </tr>


                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selflhwkCount}</small><small style="float: right">立后无开</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverthreeCount}</small><small style="float: right">超1未立</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfykwpCount}</small><small style="float: right">已开未判</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverTowCount}</small><small style="float: right">超2未立</small></td>
                                                        </tr>


                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfypwsCount}</small><small style="float: right">以判未生</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outlhwkCount}</small><small style="float: right">立后无开</small></td>
                                                        </tr>


                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfpjsxCount}</small><small style="float: right">判决生效</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outykwpCount}</small><small style="float: right">已开未判</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfqsCount}</small><small style="float: right">缺</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outypwsCount}</small><small style="float: right">已判未生</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfpcCount}</small><small style="float: right">补</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outpjsxCount}</small><small style="float: right">判决生效</small></td>
                                                        </tr>


                                                        <tr>
                                                            <td><small style="float: left"></small><small style="float: right"></small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outpjsxCount}</small><small style="float: right">缺</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left"></small><small style="float: right"></small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outpjsxCount}</small><small style="float: right">补</small></td>
                                                        </tr>

                                                    </table>

                                                    <%--<a href=""> ${caseInsurancePersonBoard.litigationsCount}件</a>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfLitigationsCount}</small>自办 其中： <small style="float: right">${caseInsurancePersonBoard.outLitigationsCount}委外 其中：</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfOverOneCount}</small>超1未立    <small style="float: right">${caseInsurancePersonBoard.outOverfiveCount}委外超15天</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfOverTowCount}</small>超2未立    <small style="float: right">${caseInsurancePersonBoard.outOverthreeCount}委外超30天</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selflhwkCount}</small>立后无开    <small style="float: right" >${caseInsurancePersonBoard.outOverOneCount}超1未立</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfykwpCount}</small>已开未判    <small style="float: right">${caseInsurancePersonBoard.outOverTowCount}超2未立</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfypwsCount}</small>已判未生    <small style="float: right">${caseInsurancePersonBoard.outlhwkCount}立后无开</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfpjsxCount}</small>判决生效    <small style="float: right">${caseInsurancePersonBoard.outykwpCount}已开未判</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfqsCount}</small>缺   <small style="float: right">${caseInsurancePersonBoard.outypwsCount}已判未生</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left">${caseInsurancePersonBoard.selfpcCount}</small>补   <small style="float: right">${caseInsurancePersonBoard.outpjsxCount}判决生效</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left"></small>    <small style="float: right">${caseInsurancePersonBoard.outpjsxCount}缺</small>--%>
                                                    <%--<br>--%>
                                                    <%--<small style="float: left"></small>    <small style="float: right">${caseInsurancePersonBoard.outpjsxCount}补</small>--%>

                                                </td>
                                                <td class="project-title">






                                                    <table><tr>
                                                        <td colspan="2">${caseInsurancePersonBoard.implementsCount}件</td></tr>
                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfImplementsCount}</small><small style="float: right">自办 其中：</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outImplementsCount}</small><small style="float: right">委外 其中：</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfOverOneImplementsCount}</small><small style="float: right">超1未申立</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outFiveImplementsCount}</small><small style="float: right">委外超15天</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfOverThreeImplementsCount}</small><small style="float: right">超3未终</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outThreeImplementsCount}</small><small style="float: right">委外超30天</small></td>
                                                        </tr>


                                                        <tr>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.selfOverSixImplementsCount}</small><small style="float: right">超6未终</small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverOneImplementsCount}</small><small style="float: right">超1未申立</small></td>
                                                        </tr>



                                                        <tr>
                                                            <td><small style="float: left"></small><small style="float: right"></small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverThreeImplementsCount}</small><small style="float: right">超3未终</small></td>
                                                        </tr>

                                                        <tr>
                                                            <td><small style="float: left"></small><small style="float: right"></small> </td>
                                                            <td><small style="float: left">${caseInsurancePersonBoard.outOverSixImplementsCount}</small><small style="float: right">超6未终</small></td>
                                                        </tr>

                                                    </table>


                                                </td>

                                            </tr>
                                        </c:forEach>




                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
