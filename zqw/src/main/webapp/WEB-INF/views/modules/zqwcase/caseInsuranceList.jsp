<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件信息管理</title>
	<meta name="decorator" content="default"/>


	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/");
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
        function searchPage() {
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/");
            $("#searchForm").submit();
            return false;
        }

        function setcheckAllFlag() {
            var tempVal=$('input:checkbox[name="checkAll"]:checked').val();
            if(tempVal=="1"){
               $("#ckeckAllFlag").val("all");
            }
            else{
                $("#ckeckAllFlag").val("1");
            }
        }

        function chosesEexport() {
            top.$.jBox.open("iframe:${ctx}/zqwcase/caseInsurance/exportForm", "选择导出文件",1010,$(top.document).height()-640,{
                buttons:{"确定":"ok", "关闭":true}, bottomText:"选择。",submit:function(v, h, f){
                    var insuranceSubrogations = h.find("iframe")[0].contentWindow.$("input[name='insuranceSubrogation']:checked").map(function(index,element){ return $(element).val()}).get().join(',');
                    if (v=="ok"){

                        var chk_value =[];
                        $('input[name="exprotId"]:checked').each(function(){
                            chk_value.push($(this).val());
                        });
						$("#exprotIds").val(chk_value.join(","));
						$("#insuranceSubrogations").val(insuranceSubrogations);
						$("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportZip").submit();

                    }
                }, loaded:function(h){
                    $(".jbox-content", top.document).css("overflow-y","hidden");
                }
            });
        }

        function chosesAllEexport() {
            $("#ckeckAllFlag").val("all");
            top.$.jBox.open("iframe:${ctx}/zqwcase/caseInsurance/exportForm", "选择导出文件",1010,$(top.document).height()-640,{
                buttons:{"确定":"ok", "关闭":true}, bottomText:"选择。",submit:function(v, h, f){
                    var insuranceSubrogations = h.find("iframe")[0].contentWindow.$("input[name='insuranceSubrogation']:checked").map(function(index,element){ return $(element).val()}).get().join(',');
                    if (v=="ok"){

                        var chk_value =[];
                        $('input[name="exprotId"]:checked').each(function(){
                            chk_value.push($(this).val());
                        });
                        $("#exprotIds").val(chk_value.join(","));
                        $("#insuranceSubrogations").val(insuranceSubrogations);
                        $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportZip").submit();

                    }
                }, loaded:function(h){
                    $(".jbox-content", top.document).css("overflow-y","hidden");
                }
            });
        }

        function exportLayerNum() {
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportLayerNum").submit();
        }

        function exportCase() {
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exprotCaseInsurance").submit();
        }
        
        function allSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/allSync",
                data: {projcetApproverStep : $("#projcetApproverStep").val()},
                dataType: "json",
                async: false,
                success: function(data){
                        alert("同步成功");
                        searchPage();
                }
            });

        }
        
        function incrementSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/incrementSync",
                data: {projcetApproverStep : $("#projcetApproverStep").val()},
                dataType: "json",
                async: false,
                success: function(data){
                        alert("同步成功");
                        searchPage();

                }
            });
        }


        function communicateSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/communicateSync",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("同步成功");
                    searchPage();

                }
            });
        }



        function paymentSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/paymentSync",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("同步成功");
                    searchPage();

                }
            });
        }


        function changeLayers() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/changeLayers",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("生成成功");
                }
            });
        }

        function checkAll(obj){
            if (obj.checked){
                $("input[name='exprotId']").each(function(){
                    this.checked = true;
                })
            }else{
                $("input[name='exprotId']").each(function(){
                    this.checked = false;
                })
            }
        }


        function toForm(id) {
            var pageNo = $('#pageNo').val();
            var pageSize = $('#pageSize').val();
            var creditorSearch = $('#creditor').val();
            var caseTypeSeache = $('#caseTypeSeache').val();
//            var deptSearch =$('#deptSearch').val();
            var caseNameSearch =$('#caseNameSearch').val();
            var reportNumSearch =$('#reportNumSearch').val();
            var caseNumberSearch =$('#caseNumberSearch').val();
            location.href = "${ctx}/zqwcase/caseInsurance/form?id=" +id
				 +"&pageNo="+pageNo
				+"&pageSize="+pageSize
				+"&creditorSearch="+creditorSearch
				+"&caseTypeSeache="+caseTypeSeache
                +"&caseNameSearch="+caseNameSearch
				+"&reportNumSearch="+reportNumSearch
				+"&caseNumberSearch="+caseNumberSearch;
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqwcase/caseInsurance/">案件信息列表</a></li>
		<%--<shiro:hasPermission name="zqwcase:caseInsurance:edit"><li><a href="${ctx}/zqwcase/caseInsurance/form">案件信息添加</a></li></shiro:hasPermission>--%>
	</ul>
	<form:form id="searchForm" modelAttribute="caseInsurance" action="${ctx}/zqwcase/caseInsurance/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<input id="ckeckAllFlag" name="ckeckAllFlag" type="hidden"/>
		<input id="insuranceSubrogations" name="insuranceSubrogations" type="hidden"/>
		<input id="exprotIds" name="exprotIds" type="hidden"/>
		<ul class="ul-form">
			<li><label>债权人：</label><form:input path="creditor" htmlEscape="false" maxlength="50" class="input-medium"/></li>
			<li><label>案件类别：</label>
				<form:select path="caseTypeSeache" cssStyle="width: 127px;" >
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('case_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<%--<li><label>债务人：</label><form:input path="deptSearch" htmlEscape="false" maxlength="50" class="input-medium"/></li>--%>
			<li><label>案件名称：</label><form:input path="caseNameSearch" htmlEscape="false" maxlength="50" class="input-medium"/></li>
			<li><label>报案号：</label><form:input path="reportNumSearch" htmlEscape="false" maxlength="50" class="input-medium"/></li>
			<li><label>案件编号：</label><form:input path="caseNumberSearch" htmlEscape="false" maxlength="50" class="input-medium"/></li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input  class="btn btn-primary" type="button" onclick="chosesEexport();" value="一键生成"/></li>
			<%--<li class="btns"><input  class="btn btn-primary" type="button" onclick="chosesAllEexport();" value="全部生成"/></li>
            <li class="btns"><input  class="btn btn-primary" type="button" onclick="allSync();" value="全量同步"/></li>--%>
			<li class="btns"><input  class="btn btn-primary" type="button" onclick="incrementSync();" value="增量同步"/></li>
			<li class="btns"><input  class="btn btn-primary" type="button" onclick="communicateSync();" value="同步跟进记录"/></li>
			<li class="btns"><input  class="btn btn-primary" type="button" onclick="paymentSync();" value="同步回款记录"/></li>
			<li class="btns"><input  class="btn btn-primary" type="button" onclick="exportCase();" value="导出案件列表"/></li>
			<li class="btns"><input  class="btn btn-primary" type="button" onclick="changeLayers();" value="生成律师"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th><input type="checkbox" name="checkAll" value="1" onclick="checkAll(this);"/>全选</th>
				<%--<th>债权人</th>--%>
				<th>案件名称</th>
				<th>报案号</th>
				<th>案件编号</th>
				<th>案件类别</th>
				<th>第一债务人</th>
				<th>时效起算日期</th>
				<th>债权金额</th>
				<%--<th>案件阶段</th>--%>
				<th>收案时间</th>
				<%--<th>结案时间</th>--%>
				<shiro:hasPermission name="zqwcase:caseInsurance:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="caseInsurance">
			<tr>
				<td><input type="checkbox"  name="exprotId" value="${caseInsurance.id}"/></td>

				<%--<td>${caseInsurance.creditor}</td>--%>
				<td><a href="${ctx}/zqwcase/caseInsurance/view?id=${caseInsurance.id}">${caseInsurance.caseXbbInsurance.caseName}</a></td>
				<td>${caseInsurance.caseXbbInsurance.reportNum}</td>
				<td>${caseInsurance.caseXbbInsurance.caseNumber}</td>
				<td>
				${fns:getDictLabel(caseInsurance.caseType, 'case_type', '其他')}
				</td>

				<c:if test="${caseInsurance.caseType eq '0'}">
					<%--追偿--%>
					<td>
						${caseInsurance.insuredVehicleDriver}
					</td>
				</c:if>

				<c:if test="${caseInsurance.caseType eq '1'}">
					<%--代位--%>
					<td>
						${caseInsurance.victimDriverHarmful}
					</td>
				</c:if>

				<c:if test="${caseInsurance.caseType eq '2'}">
					<%--特殊--%>
					<td>

					</td>
				</c:if>


				<c:if test="${caseInsurance.caseType eq '0'}">
					<%--追偿--%>
					<td>
						<fmt:formatDate value="${caseInsurance.accidentJudgmentDate}" pattern="yyyy-MM-dd"/>
					</td>
				</c:if>

				<c:if test="${caseInsurance.caseType eq '1'}">
					<%--代位--%>
					<td>
						<fmt:formatDate value="${caseInsurance.insuranceCompanyTime}" pattern="yyyy-MM-dd"/>

					</td>
				</c:if>

				<c:if test="${caseInsurance.caseType eq '2'}">
					<%--特殊--%>
					<td>

					</td>
				</c:if>



				<td>
					${caseInsurance.creditorAmount}
				</td>

				<%--<td>--%>
				<%--</td>--%>

				<td>
					<fmt:formatDate value="${caseInsurance.createDate}" pattern="yyyy-MM-dd"/>
				</td>

				<%--<td>--%>
				<%--</td>--%>
				<td>
				<a href="javascript:void(0);" onclick="toForm('${caseInsurance.id}')">修改</a>

				<shiro:hasPermission name="zqwcase:caseInsurance:edit">
					<a href="${ctx}/zqwcase/caseInsurance/delete?id=${caseInsurance.id}" onclick="return confirmx('确认要删除该案件信息吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>