<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件信息管理</title>
	<meta name="decorator" content="default"/>

	<link href="${ctxStatic}/weipingu/animate.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/bootstrap.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/font-awesome.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/style.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/stylenew.css" type="text/css" rel="stylesheet">
	<%--<link href="${ctxStatic}/weipingu/stylenew.css" type="text/css" rel="stylesheet">--%>
	<link rel="stylesheet" href="${ctxStatic}/jquery-Steps-scw/css/ystep.css">
	<script type="text/javascript" src=${ctxStatic}/jquery-Steps-scw/js/setStep.js></script>
	<script type="text/javascript" src=${ctxStatic}/layer/layer.min.js></script>


	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					// loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});




	</script>
</head>
<body>

	<form:form id="inputForm" modelAttribute="caseInsurance" action="${ctx}/zqwcase/caseInsurance/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="caseCustomerId"/>
		<input type="hidden" id="saveFlag" name="saveFlag">
		<%--<sys:message content="${message}"/>--%>

	<div class="form-items item-con">
		<div class="col-md-12 form-item-title">导出选项</div>
		<div class="row"  >
			<div class="col-md-1"><small>选择项:</small></div>
			<div class="col-md-11">
				<form:checkboxes path="insuranceSubrogation"  items="${fns:getDictList('insurance_subrogation')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</div>
		</div>
	</div>
	</form:form>
</body>
</html>