<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖邦邦案件管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/zqwcase/caseXbbInsurance/">肖邦邦案件管理列表</a></li>
		<li class="active"><a href="${ctx}/zqwcase/caseXbbInsurance/form?id=${caseXbbInsurance.id}">肖邦邦案件管理<shiro:hasPermission name="zqwcase:caseXbbInsurance:edit">${not empty caseXbbInsurance.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="zqwcase:caseXbbInsurance:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="caseXbbInsurance" action="${ctx}/zqwcase/caseXbbInsurance/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">举证期限：</label>
			<div class="controls">
				<form:input path="limitTime" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">事故发生地事故发生地：</label>
			<div class="controls">
				<form:input path="accidentPlace" htmlEscape="false" maxlength="10240" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">事故发生日期：</label>
			<div class="controls">
				<form:input path="accidentDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">到期日期：</label>
			<div class="controls">
				<form:input path="maturityDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">合作律师信息：</label>
			<div class="controls">
				<form:input path="cooperativeLawyer" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">合同商务条款：</label>
			<div class="controls">
				<form:input path="commercialClause" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">合同状态：</label>
			<div class="controls">
				<form:input path="contractStatus" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">合同类型：</label>
			<div class="controls">
				<form:input path="contractType" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">合同金额：</label>
			<div class="controls">
				<form:input path="contractAmount" htmlEscape="false" maxlength="11" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">委外情况：</label>
			<div class="controls">
				<form:input path="outSourcing" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">委外日期：</label>
			<div class="controls">
				<form:input path="outSourcingDate" htmlEscape="false" maxlength="11" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">客户ID：</label>
			<div class="controls">
				<form:input path="caseCustomerId" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">客户名称：</label>
			<div class="controls">
				<form:input path="caseCustomerName" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">客户联系人：</label>
			<div class="controls">
				<form:input path="caseCustmoerContract" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">客户需求：</label>
			<div class="controls">
				<form:input path="caseCustomerDemand" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">开庭日期：</label>
			<div class="controls">
				<form:input path="openingTime" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">承办团队：</label>
			<div class="controls">
				<form:input path="undertakingTeam" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">承办律师：</label>
			<div class="controls">
				<form:input path="undertakingLawyer" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">承办状态：</label>
			<div class="controls">
				<form:input path="undertakingStatus" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">report_num：</label>
			<div class="controls">
				<form:input path="reportNum" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">收费方式收费方式：</label>
			<div class="controls">
				<form:input path="chargingMethod" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">是否归档：</label>
			<div class="controls">
				<form:input path="itFiled" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">更新时间：</label>
			<div class="controls">
				<form:input path="upTime" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">材料清单：</label>
			<div class="controls">
				<form:input path="listMaterials" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">案件名称：</label>
			<div class="controls">
				<form:input path="caseName" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">案件星级：</label>
			<div class="controls">
				<form:input path="caseStar" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">案件类型：</label>
			<div class="controls">
				<form:input path="caseType" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">案件类型：</label>
			<div class="controls">
				<form:input path="caseNumber" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">案情简介：</label>
			<div class="controls">
				<form:input path="caseIntroduction" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">正式债权编号：</label>
			<div class="controls">
				<form:input path="caseCreditorNub" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">理赔日期：</label>
			<div class="controls">
				<form:input path="claimDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">申请执行日期：</label>
			<div class="controls">
				<form:input path="executionDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">目前阶段：</label>
			<div class="controls">
				<form:input path="currentStage" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">签订人：</label>
			<div class="controls">
				<form:input path="signPersone" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">签订日期：</label>
			<div class="controls">
				<form:input path="signingDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">管辖法院：</label>
			<div class="controls">
				<form:input path="jurisdictionalCourt" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">续封续冻日期：</label>
			<div class="controls">
				<form:input path="refrigerationDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">诉讼/执行时效届满日：</label>
			<div class="controls">
				<form:input path="limitationDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">诉讼立案日期：</label>
			<div class="controls">
				<form:input path="lawsuitFillingDate" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">赔偿人名称：</label>
			<div class="controls">
				<form:input path="compensatorName" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">赔偿人联系方式：</label>
			<div class="controls">
				<form:input path="compensatorContact" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">赔偿总金额：</label>
			<div class="controls">
				<form:input path="compernsatorAmount" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="zqwcase:caseXbbInsurance:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>