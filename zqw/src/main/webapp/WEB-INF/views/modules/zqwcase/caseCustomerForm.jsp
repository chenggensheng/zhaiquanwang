<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件客户信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					// loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		
		function setsaveFlag(str) {
			$("#saveFlag").val(str);
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/zqwcase/caseCustomer/">案件客户信息列表</a></li>
		<li class="active"><a href="${ctx}/zqwcase/caseCustomer/form?id=${caseCustomer.id}">案件客户信息<shiro:hasPermission name="zqwcase:caseCustomer:edit">${not empty caseCustomer.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="zqwcase:caseCustomer:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="caseCustomer" action="${ctx}/zqwcase/caseCustomer/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<input type="hidden" id="saveFlag" name="saveFlag">
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">债权人：</label>
			<div class="controls">
				<form:input path="creditor" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">债权人住所地：</label>
			<div class="controls">
				<form:input path="creditorDomicile" htmlEscape="false" maxlength="1024" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">债权人信用代码：</label>
			<div class="controls">
				<form:input path="creditorIdcart" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">债权负责人：</label>
			<div class="controls">
				<form:input path="creditorCharge" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">债权人负责人职位：</label>
			<div class="controls">
				<form:input path="creditorChargePositon" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">承办律师：</label>
			<div class="controls">
				<form:select path="undertakingLawyer" class="input-xlarge ">
					<form:options items="${fns:getZqwLayerLists('0')}" itemLabel="layerName" itemValue="id" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">


			<shiro:hasPermission name="zqwcase:caseCustomer:edit">
				<input id="btnSubmit" class="btn btn-primary" type="submit" onclick="setsaveFlag('1');" value="保存并添加案件" />
				&nbsp;</shiro:hasPermission>

			<shiro:hasPermission name="zqwcase:caseCustomer:edit">
				<input id="btnSubmit" class="btn btn-primary" type="submit" onclick="setsaveFlag('0');" value="保 存"/>
			&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>