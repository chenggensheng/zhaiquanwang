<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>肖邦邦同步管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqwcase/caseTimeUp/">肖邦邦同步管理列表</a></li>
		<shiro:hasPermission name="zqwcase:caseTimeUp:edit"><li><a href="${ctx}/zqwcase/caseTimeUp/form">肖邦邦同步管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="caseTimeUp" action="${ctx}/zqwcase/caseTimeUp/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<shiro:hasPermission name="zqwcase:caseTimeUp:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="caseTimeUp">
			<tr>
				<shiro:hasPermission name="zqwcase:caseTimeUp:edit"><td>
    				<a href="${ctx}/zqwcase/caseTimeUp/form?id=${caseTimeUp.id}">修改</a>
					<a href="${ctx}/zqwcase/caseTimeUp/delete?id=${caseTimeUp.id}" onclick="return confirmx('确认要删除该肖邦邦同步管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>