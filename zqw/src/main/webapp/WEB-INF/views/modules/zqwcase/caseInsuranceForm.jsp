<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件信息管理</title>
	<meta name="decorator" content="default"/>

	<link href="${ctxStatic}/weipingu/animate.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/bootstrap.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/font-awesome.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/style.min.css" type="text/css" rel="stylesheet">
	<link href="${ctxStatic}/weipingu/stylenew.css" type="text/css" rel="stylesheet">
	<%--<link href="${ctxStatic}/weipingu/stylenew.css" type="text/css" rel="stylesheet">--%>
	<link rel="stylesheet" href="${ctxStatic}/jquery-Steps-scw/css/ystep.css">
	<script type="text/javascript" src=${ctxStatic}/jquery-Steps-scw/js/setStep.js></script>
	<script type="text/javascript" src=${ctxStatic}/layer/layer.min.js></script>


	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					// loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});

        $(document).ready(function() {
            caseTypeChange();
            isCompanyChange();

        });


        function creditorAmountChange() {
            var caseType = $("input[name='caseType']:checked").val();
            if (caseType =="0"){  //追偿
                var judgmentAmount = $("#judgmentAmount").val();

                if(isNaN(judgmentAmount)){
                    alert("输入的不是数字");
                    return false;
                }
                if(judgmentAmount ==''){
                    judgmentAmount =0;
                }
				var recoveryCasesAmount = $("#recoveryCasesAmount").val();
                if(isNaN(recoveryCasesAmount)){
                    alert("输入的不是数字");
                    return false;
                }
                if(recoveryCasesAmount ==''){
                    recoveryCasesAmount =0;
                }
				var amountPaidBack = $("#amountPaidBack").val();
                if(isNaN(amountPaidBack)){
                    alert("输入的不是数字");
                    return false;
                }
                if(amountPaidBack ==''){
                    amountPaidBack =0;
                }
                $("#creditorAmount").val(parseFloat(judgmentAmount)+parseFloat(recoveryCasesAmount)-parseFloat(amountPaidBack));

            } else if(caseType =="1"){//代为

                var claimAmountInsuranceCompany = $("#claimAmountInsuranceCompany").val();
                if(isNaN(claimAmountInsuranceCompany)){
                    alert("输入的不是数字");
                    return false;
                }
                if(claimAmountInsuranceCompany ==''){
                    claimAmountInsuranceCompany =0;
                }
                var  insuredVehicleDriverLiability  = $("#insuredVehicleDriverLiability").val();
                var varQuantity = 1;
                if (insuredVehicleDriverLiability =="0"){
                    varQuantity = 1;
                } else if (insuredVehicleDriverLiability =="1"){
                    varQuantity = 0.7;
                } else if (insuredVehicleDriverLiability =="2"){
                    varQuantity = 0.5;
                } else if (insuredVehicleDriverLiability =="3"){
                    varQuantity = 0;
                }

                var amountPaidBack = $("#amountPaidBack").val();
                if(isNaN(amountPaidBack)){
                    alert("输入的不是数字");
                    return false;
                }
                if(amountPaidBack ==''){
                    amountPaidBack =0;
                }
                $("#creditorAmount").val((parseFloat(claimAmountInsuranceCompany)-2000)*parseFloat(varQuantity)+2000-parseFloat(amountPaidBack));
			}
        }

        function isCompanyChange() {
            var isCompanyTemp = $("#victimVehicleOwenrIsCompany").val();
            if(isCompanyTemp == '0'){
                $("#subrogateOne").show();
                $("#subrogateTow").show();
                $("#subrogategrThree").hide();
                $("#subrogategrFroe").hide();
            }else {
                $("#subrogateOne").show();
                $("#subrogateTow").hide();
                $("#subrogategrThree").show();
                $("#subrogategrFroe").show();
            }
        }

		function caseTypeChange() {

            $("#towThings").show();
            $("#otherThings").hide();
		     var caseType = $("input[name='caseType']:checked").val();
            var isCompany = $("#isCompany").val();
		     if (caseType == "0" && isCompany =="0"){//追偿权界面--- 单位
		         $("#showAccidentDateText").show();
                 $("#showAccidentDate").show();
                 $("#recoveryOne").show();
                 $("#recoveryTow").show();
                 $("#recoveryThree").show();
                 $("#recoveryFour").show();
                 $("#dwOne").show();

                 $("#grOne").hide();
                 $("#grTow").hide();
                 $("#subrogateTow").hide();
                 $("#subrogategrThree").hide();
                 $("#subrogategrFroe").hide();


                 $("#showClaimAmountInsuranceCompanyText").hide();
                 $("#showClaimAmountInsuranceCompany").hide();
                 $("#subrogateOne").hide();
                 $("#subrogateTow").hide();
                 $("#subrogateThree").hide();
                 $("#subrogateFour").hide();
                 $("#subrogateFive").hide();
                 $("#subrogateSix").hide();
                 $("#subrogateSeven").hide();

			 }else if (caseType == "0" && isCompany =="1"){ //追偿权界面--- 个人

                 $("#showAccidentDateText").show();
                 $("#showAccidentDate").show();
                 $("#recoveryOne").show();
                 $("#recoveryTow").show();
                 $("#recoveryThree").show();
                 $("#recoveryFour").show();


                 $("#dwOne").hide();
                 $("#grOne").show();
                 $("#grTow").show();

                 $("#subrogateTow").hide();
                 $("#subrogategrThree").hide();
                 $("#subrogategrFroe").hide();


                 $("#showClaimAmountInsuranceCompanyText").hide();
                 $("#showClaimAmountInsuranceCompany").hide();
                 $("#subrogateOne").hide();
                 $("#subrogateTow").hide();
                 $("#subrogateThree").hide();
                 $("#subrogateFour").hide();
                 $("#subrogateFive").hide();
                 $("#subrogateSix").hide();
                 $("#subrogateSeven").hide();

			 } else if (caseType =="1" && isCompany =="0") { //代位求偿权界面 --- 单位
                 $("#showClaimAmountInsuranceCompanyText").show();
                 $("#showClaimAmountInsuranceCompany").show();
                 $("#subrogateOne").show();
                 $("#subrogateTow").show();
                 $("#subrogateThree").show();
                 $("#subrogateFour").show();
                 $("#subrogateFive").show();
                 $("#subrogateSix").show();
                 $("#subrogateSeven").show();


                 $("#subrogategrThree").hide();
                 $("#subrogategrFroe").hide();
                 $("#dwOne").hide();
                 $("#grOne").hide();
                 $("#grTow").hide();


                 $("#showAccidentDateText").hide();
                 $("#showAccidentDate").hide();
                 $("#recoveryOne").hide();
                 $("#recoveryTow").hide();
                 $("#recoveryThree").hide();
                 $("#recoveryFour").hide();
             }else if (caseType =="1" && isCompany=="1") { //

                 $("#showClaimAmountInsuranceCompanyText").show();
                 $("#showClaimAmountInsuranceCompany").show();
                 $("#subrogateOne").show();
                 $("#subrogateTow").hide();
                 $("#subrogateThree").show();
                 $("#subrogateFour").show();
                 $("#subrogateFive").show();
                 $("#subrogateSix").show();
                 $("#subrogateSeven").show();

                 $("#subrogategrThree").show();
                 $("#subrogategrFroe").show();
                 $("#dwOne").hide();
                 $("#grOne").hide();
                 $("#grTow").hide();


                 $("#showAccidentDateText").hide();
                 $("#showAccidentDate").hide();
                 $("#recoveryOne").hide();
                 $("#recoveryTow").hide();
                 $("#recoveryThree").hide();
                 $("#recoveryFour").hide();

			 }else {

			     $("#towThings").hide();
			     $("#otherThings").show();


			 }

        }

        function  setOtherResponsibility(){
            var  insuredVehicleDriverLiability  = $("#insuredVehicleDriverLiability").val();
            if (insuredVehicleDriverLiability =="0"){
//                $("#vehicleDriverInsuredResponsibility").val("不承担");
                $("#victimDriverHarmfulResponsibility").val("不承担");
			} else if (insuredVehicleDriverLiability =="1"){
//                $("#vehicleDriverInsuredResponsibility").val("次要");
                $("#victimDriverHarmfulResponsibility").val("次要");
            } else if (insuredVehicleDriverLiability =="2"){
//                $("#vehicleDriverInsuredResponsibility").val("主要");
                $("#victimDriverHarmfulResponsibility").val("主要");
            } else if (insuredVehicleDriverLiability =="3"){
//                $("#vehicleDriverInsuredResponsibility").val("同等");
                $("#victimDriverHarmfulResponsibility").val("同等");
            }else if (insuredVehicleDriverLiability =="4"){
//                $("#vehicleDriverInsuredResponsibility").val("同等");
                $("#victimDriverHarmfulResponsibility").val("全部");
            }

        }

        //根据身份证信息设置 被保险人性别及生日 2019-0705
        function insuredIdNumberChange() {
		    var insuredIdNumber = $("#insuredIdNumber").val();
            // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(reg.test(insuredIdNumber) === false) {
                alert("身份证输入不合法");
                return false;
            }else {
                if (insuredIdNumber.length==15){ //15位身份
                    alert("15位身份证请自己输入！");
				} else {//18位身份
                    var sexString = insuredIdNumber.substring(16,17);
                  //var bithString = insuredIdNumber.substring(7,14);
                    if (sexString =="0" || sexString =="2" || sexString =="4" || sexString =="6" ||sexString =="8"){
                        $("#insuredSex").val("女");
					}else{
                        $("#insuredSex").val("男");
					}
                    $("#insuredBith").val(insuredIdNumber.substring(6,10)+"年"+insuredIdNumber.substring(10,12)+"月"+insuredIdNumber.substring(12,14)+"日");
				}
			}
        }

        function victimVehicleOwnerNumberChange() {
            var victimVehicleOwnerNumber = $("#victimVehicleOwnerNumber").val();
            // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(reg.test(victimVehicleOwnerNumber) === false) {
                alert("身份证输入不合法");
                return false;
            }else {
                if (victimVehicleOwnerNumber.length==15){ //15位身份
                    alert("15位身份证请自己输入！");
                } else {//18位身份
                    var sexString = victimVehicleOwnerNumber.substring(16,17);
                    //var bithString = insuredIdNumber.substring(7,14);
                    if (sexString =="0" || sexString =="2" || sexString =="4" || sexString =="6" ||sexString =="8"){
                        $("#victimVehicleOwnerSex").val("女");
                    }else{
                        $("#victimVehicleOwnerSex").val("男");
                    }
                    $("#victimVehicleOwnerBith").val(victimVehicleOwnerNumber.substring(6,10)+"年"+victimVehicleOwnerNumber.substring(10,12)+"月"+victimVehicleOwnerNumber.substring(12,14)+"日");
                }
            }
        }

        function insuredVehicleChange() {
            var insuredVehicleDriverNumber = $("#insuredVehicleDriverNumber").val();
            // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(reg.test(insuredVehicleDriverNumber) === false) {
                alert("身份证输入不合法");
                return false;
            }else {
                if (insuredVehicleDriverNumber.length==15){ //15位身份
                    alert("15位身份证请自己输入！");
                } else {//18位身份
                    var sexString = insuredVehicleDriverNumber.substring(16,17);
                    //var bithString = insuredIdNumber.substring(7,14);
                    if (sexString =="0" || sexString =="2" || sexString =="4" || sexString =="6" ||sexString =="8"){
                        $("#insuredVehicleDriverSex").val("女");
                    }else{
                        $("#insuredVehicleDriverSex").val("男");
                    }
                    $("#insuredVehicleDriverBith").val(insuredVehicleDriverNumber.substring(6,10)+"年"+insuredVehicleDriverNumber.substring(10,12)+"月"+insuredVehicleDriverNumber.substring(12,14)+"日");
                }
            }
        }


        function victimDriverHarmfulIdcardChange() {

            var victimDriverHarmfulIdcard = $("#victimDriverHarmfulIdcard").val();
            // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(reg.test(victimDriverHarmfulIdcard) === false) {
                alert("身份证输入不合法");
                return false;
            }else {
                if (victimDriverHarmfulIdcard.length==15){ //15位身份
                    alert("15位身份证请自己输入！");
                } else {//18位身份
                    var sexString = victimDriverHarmfulIdcard.substring(16,17);
                    //var bithString = insuredIdNumber.substring(7,14);
                    if (sexString =="0" || sexString =="2" || sexString =="4" || sexString =="6" ||sexString =="8"){
                        $("#victimDriverHarmfulSex").val("女");
                    }else{
                        $("#victimDriverHarmfulSex").val("男");
                    }
                    $("#victimDriverHarmfulBith").val(victimDriverHarmfulIdcard.substring(6,10)+"年"+victimDriverHarmfulIdcard.substring(10,12)+"月"+victimDriverHarmfulIdcard.substring(12,14)+"日");
                }
            }
        }

        function setsaveFlag(str) {
            $("#saveFlag").val(str);
        }
        
        function changePhone() {
            var undertakingLawyer = $("#undertakingLawyer").val();
            $.ajax({
                type: "POST",
                url: "${ctx}/zqw/zqwLayer/changePhone",
                data: {id : undertakingLawyer},
                dataType: "json",
                async: false,
                success: function(data){
                    $("#phoneShow").html(data);
                }
            });
        }

        function syncCrmData() {

            var cooperativeLawyer = $("#cooperativeLawyer").val();
            var caseXbbInsuranceId = $("#caseXbbInsuranceId").val();
            var lawsuitFillingDate = $("#lawsuitFillingDate").val();


            var undertakingStatusId = $("#undertakingStatusId").val();
            var currentStageId = $("#currentStageId").val();
            var lawyerLetterId = $("#lawyerLetterId").val();
            var repaymentIntentionId = $("#repaymentIntentionId").val();

            alert(undertakingStatusId);

            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseXbbInsurance/syncCrmData",
                data: {id : caseXbbInsuranceId,
					   cooperativeLawyer:cooperativeLawyer,
					   lawsuitFillingDate:lawsuitFillingDate,
                    undertakingStatus:undertakingStatusId,
                    currentStage:currentStageId,
                    lawyerLetter:lawyerLetterId,
                    repaymentIntention:repaymentIntentionId
				},
                dataType: "json",
                async: false,
                success: function(data){
                    if(data==true)
                    alert("同步成功");
                    else alert("同步失败");
                }
            });


        }

	</script>
</head>
<body>
<a name="goback"></a>

	<ul class="nav nav-tabs">
		<li><a href="${ctx}/zqwcase/caseInsurance/">案件信息列表</a></li>
		<li class="active"><a href="${ctx}/zqwcase/caseInsurance/form?id=${caseInsurance.id}">案件信息<shiro:hasPermission name="zqwcase:caseInsurance:edit">${not empty caseInsurance.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="zqwcase:caseInsurance:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="caseInsurance" action="${ctx}/zqwcase/caseInsurance/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="caseCustomerId"/>
		<input type="hidden" id="saveFlag" name="saveFlag">
		<input id="pageNo" name="pageNo" type="hidden" value="${pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${pageSize}"/>
		<form:hidden path="creditorSearch"/>
		<form:hidden path="caseTypeSeache"/>
		<form:hidden path="deptSearch"/>
		<form:hidden path="caseNameSearch"/>
		<form:hidden path="reportNumSearch"/>
		<form:hidden path="caseNumberSearch"/>

		<%--<sys:message content="${message}"/>--%>

		<%--<div  style="position: fixed;top:45px;left:10px;">--%>
			<%--<a href="#goback1"><span class="btn btn-primary">${caseInsurance.caseXbbInsurance.caseName}</span></a>--%>
		<%--</div>--%>

	    <div class="form-items item-con">
		<div class="col-md-12 form-item-title">客户信息</div>
		<div class="row">
			<form:hidden path="caseCustomer.id"/>
			<div class="row">
				<div class="col-md-1"><small>债权人:</small></div>
				<div class="col-md-3">
					<form:input path="caseCustomer.creditor"  htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>

				</div>
				<div class="col-md-1"><small>债权人住所地:</small></div>
				<div class="col-md-3">
					<form:input path="caseCustomer.creditorDomicile" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>

				</div>
				<div class="col-md-1"><small>债权人信用代码:</small></div>
				<div class="col-md-3">
					<form:input path="caseCustomer.creditorIdcart"  htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-1"><small>债权人负责人:</small></div>
				<div class="col-md-3">
					<form:input path="caseCustomer.creditorCharge"  htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
				</div>
				<div class="col-md-1"><small>债权人负责人职位:</small></div>
				<div class="col-md-3">
					<form:input path="caseCustomer.creditorChargePositon"  htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
				</div>
				<div class="col-md-1"><small>承办律师/电话:</small></div>
				<div class="col-md-3">
					<form:select id="undertakingLawyer" path="caseCustomer.undertakingLawyer" class="input-xlarge " cssStyle="width: 100px;" onchange="changePhone();">
						<form:options items="${fns:getZqwLayerLists('0')}" itemLabel="layerName" itemValue="id" htmlEscape="false"/>
					</form:select>/
					<span id ="phoneShow">
						${caseCustomer.undertakingLawyerPhone}</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2"><small>案件类别:</small></div>
			<div class="col-md-2">
				<form:radiobuttons path="caseType" items="${fns:getDictList('case_type')}" itemLabel="label" onchange="caseTypeChange();" itemValue="value" htmlEscape="false"/>
			</div>
			<div class="col-md-1"><small>案件编号:</small></div>
			<div class="col-md-3">
				<form:input path="acceptanceNumebr"  htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
			</div>
		</div>
		<div id="towThings">
		<div class="col-md-12 form-item-title">案件信息-被保险人</div>
		<div class="row">
				<div class="col-md-2"><small>单位或个人:</small></div>
				<div class="col-md-2">
					<form:select path="isCompany" class="input-medium " style="width:180px" onchange="caseTypeChange();">
						<form:options items="${fns:getDictList('is_company')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
					</form:select>
				</div>
			</div>
		<div class="row">
				<div class="col-md-2"><small>被保险人:</small></div>
				<div class="col-md-2">
					<form:input path="insured" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
				</div>

				<div class="col-md-2"><small>被保险人住所地:</small></div>
				<div class="col-md-2">
					<form:input path="insuredDomicile" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
				</div>

				<div class="col-md-2"><small>被保险人联系电话:</small></div>
				<div class="col-md-2">
					<form:input path="insuredContactTelephone" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
				</div>


			</div>
		<div class="row" id="dwOne">
			<div class="col-md-2"><small>被保险人信用代码:</small></div>
			<div class="col-md-2">
				<form:input path="insuredIdcard" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>被保险人负责人:</small></div>
			<div class="col-md-2">
				  <form:input path="insuredPersonCharge" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>被保险人负责人职位:</small></div>
			<div class="col-md-2">
				  <form:input path="insuredPersonChargePosition" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			 </div>
		  </div>
		<div class="row">
				<div class="col-md-2"><small>被保险人车牌号:</small></div>
				<div class="col-md-2">
					<form:input path="insuredLicenseNumber" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
				</div>
				<div class="col-md-2" id="showAccidentDateText"><small>事故日期:</small></div>
				<div class="col-md-2" id="showAccidentDate">
					<input name="accidentDate" type="text" readonly="readonly" maxlength="20" style="height: 26px;" class="input-medium Wdate "
						   value="<fmt:formatDate value="${caseInsurance.accidentDate}" pattern="yyyy-MM-dd"/>"
						   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				</div>
				<div class="col-md-2" id="insuranceCompanyTimeText"><small>保险公司理赔时间:</small></div>
				<div class="col-md-2" id="insuranceCompanyTime">
					<input name="insuranceCompanyTime" type="text" readonly="readonly" maxlength="20" style="height: 26px;" class="input-medium Wdate "
						   value="<fmt:formatDate value="${caseInsurance.insuranceCompanyTime}" pattern="yyyy-MM-dd"/>"
						   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				</div>

			</div>
		<div class="row" id="grOne">
		<div class="col-md-2"><small>被保险人民族:</small></div>
		<div class="col-md-2">
				<form:input path="insuredPeople" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

		<div class="col-md-2"><small>被保险人身份证号:</small></div>
		<div class="col-md-2">
				<form:input path="insuredIdNumber" htmlEscape="false" maxlength="255" onblur="insuredIdNumberChange();" cssStyle="height: 26px;"/>
			</div>

		<div class="col-md-2" ><small>被保险人性别:</small></div>
		<div class="col-md-2" >
				<form:input path="insuredSex" htmlEscape="false" maxlength="11"  cssStyle="height: 26px;"/>
			</div>

		</div>
		<div class="row" id="grTow">
			<div class="col-md-2" ><small>被保险人生日:</small></div>
			<div class="col-md-2" >
				<form:input path="insuredBith" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"><small>被保险人车辆驾驶人:</small></div>
			<div class="col-md-2">
				<form:input path="insuredVehicleDriver" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>被保险人车辆驾驶人户籍地:</small></div>
			<div class="col-md-2">
				<form:input path="insuredVehicleDriverHousehlod" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>被保险人车辆驾驶人民族:</small></div>
			<div class="col-md-2" >
				<form:input path="insuredVehicleDriverPeople" htmlEscape="false" maxlength="11"  cssStyle="height: 26px;"/>
			</div>


		</div>
		<div class="row" >
			<div class="col-md-2" ><small>被保险人车辆驾驶人身份证:</small></div>
			<div class="col-md-2" >
				<form:input path="insuredVehicleDriverNumber" htmlEscape="false" maxlength="64"  onblur="insuredVehicleChange();" cssStyle="height: 26px;"/>
			</div>


			<div class="col-md-2" ><small>被保险人车辆驾驶人性别:</small></div>
			<div class="col-md-2" >
				<form:input path="insuredVehicleDriverSex" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>被保险人车辆驾驶人生日:</small></div>
			<div class="col-md-2" >
				<form:input path="insuredVehicleDriverBith" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"><small>被保险人车辆驾驶人联系电话:</small></div>
			<div class="col-md-2">
				<form:input path="insuredVehicleDriverTelephone" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>被保险人车辆驾驶人责任:</small></div>
			<div class="col-md-2">
				<form:select path="insuredVehicleDriverLiability" class="input-medium" onchange="setOtherResponsibility();" style="width:180px">
					<form:options items="${fns:getDictList('driver_liability')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>

			<div class="col-md-2" id="showClaimAmountInsuranceCompanyText" ><small>保险公司理赔金额:</small></div>
			<div class="col-md-2" id="showClaimAmountInsuranceCompany" >

				<%--onblur="creditorAmountChange();"--%>
				<form:input path="claimAmountInsuranceCompany" htmlEscape="false" maxlength="11"   cssStyle="height: 26px;"/>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2"><small>保险理赔承办人员:</small></div>
			<div class="col-md-2">
				<form:input path="insuranceClaimsContractors" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>承办人员联系电话:</small></div>
			<div class="col-md-2">
				<form:input path="insuranceClaimsTelephone" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

		</div>




		<div class="row"  id="recoveryOne">
			<div class="col-md-2"><small>受害方车辆驾驶人:</small></div>
			<div class="col-md-2">
				<form:input path="vehicleDriverInsuredParty" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>受害方车辆车牌号:</small></div>
			<div class="col-md-2">
				<form:input path="vehicleDriverInsuredLicense" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>受害方车辆驾驶人责任:</small></div>
			<div class="col-md-2">
				<form:input path="vehicleDriverInsuredResponsibility" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>
			</div>
		</div>
		<div class="row"  id="recoveryTow">

			<div class="col-md-2" ><small>受害方车辆被保险人:</small></div>
			<div class="col-md-2">
				<form:input path="vehicleInsuredVictim" htmlEscape="false" maxlength="255"   cssStyle="height: 26px;"/>
			</div>
			<div class="col-md-2"><small>交通事故判决书出具日期:</small></div>
			<div class="col-md-2">
				<input name="accidentJudgmentDate" type="text" readonly="readonly" maxlength="20"  class="input-medium Wdate "
					   value="<fmt:formatDate value="${caseInsurance.accidentJudgmentDate}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</div>

			<div class="col-md-2"><small>交通事故判决法院:</small></div>
			<div class="col-md-2">
				<form:input path="courtOfJudgment" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>
		</div>
		<div class="row"  id="recoveryThree">

			<div class="col-md-2" ><small>交通事故判决案号:</small></div>
			<div class="col-md-2">
				<form:input path="judgmentNumber" htmlEscape="false" maxlength="255"   cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>交通事故判决金额:</small></div>
			<div class="col-md-2">
				<form:input path="judgmentAmount" htmlEscape="false" maxlength="255"   cssStyle="height: 26px;"/>
			</div>
			<%-- onblur="creditorAmountChange();"--%>

			<div class="col-md-2"><small>追偿案件诉讼费金额:</small></div>
			<div class="col-md-2">
				<form:input path="recoveryCasesAmount" htmlEscape="false" maxlength="255"   cssStyle="height: 26px;"/>
				<%--onblur="creditorAmountChange();"--%>

			</div>


		</div>
		<div class="row" id="recoveryFour">

			<div class="col-md-2"><small>追偿原因:</small></div>
			<div class="col-md-2">
				<form:select path="reasonsRecourse" class="input-medium"  style="width:180px">
					<form:options items="${fns:getDictList('reasons_recourse')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>





			<%--<div class="col-md-1" ><small>交通事故判决案号:</small></div>--%>
			<%--<div class="col-md-3">--%>
				<%--<form:input path="judgmentNumber" htmlEscape="false" maxlength="1" class="input-xlarge "  cssStyle="height: 26px;"/>--%>
			<%--</div>--%>

			<%--<div class="col-md-1" ><small>交通事故判决金额:</small></div>--%>
			<%--<div class="col-md-3">--%>
				<%--<form:input path="judgmentAmount" htmlEscape="false" maxlength="1" class="input-xlarge "  cssStyle="height: 26px;"/>--%>
			<%--</div>--%>

		</div>

		<div class="row">
				<div class="col-md-2"><small>致害人单位或个人:</small></div>
				<div class="col-md-2">
					<form:select path="victimVehicleOwenrIsCompany" class="input-medium " style="width:180px" onchange="isCompanyChange();">
						<form:options items="${fns:getDictList('is_company')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
					</form:select>
				</div>
		</div>

		<div class="row"  id="subrogateOne">
			<div class="col-md-2"><small>致害人车辆车主:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwner" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>致害人车辆车主住所地:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerHousehlod" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>致害人车辆车主联系电话:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerTelephone" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>
		</div>


		<div class="row"  id="subrogateTow">
			<div class="col-md-2" ><small>致害人车辆车主信用代码:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerIdcard" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>
			<div class="col-md-2" ><small>致害人车辆车主负责人:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerResponsiblity" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>致害人车辆车主负责人职位:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerDuty" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>
		</div>

		<div class="row"  id="subrogateThree">
			<div class="col-md-2" ><small>致害人车牌号:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerLicense" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>
			<div class="col-md-2" ><small>致害人车辆驾驶人:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmful" htmlEscape="false" maxlength="64"   cssStyle="height: 26px;"/>
			</div>

		</div>

		<div class="row" id ="subrogategrThree">

			<div class="col-md-2" ><small>致害人车辆车主民族:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerPeople" htmlEscape="false" maxlength="64" cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>致害人车辆车主身份证号:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerNumber" htmlEscape="false" maxlength="255" onblur="victimVehicleOwnerNumberChange();"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>致害人车辆车主性别:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerSex" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>
			
		</div>


		<div class="row" id ="subrogategrFroe">

			<div class="col-md-2" ><small>致害人车辆车主生日:</small></div>
			<div class="col-md-2">
				<form:input path="victimVehicleOwnerBith" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>

		</div>

		<div class="row" id="subrogateFour">

			<div class="col-md-2" ><small>致害人保险公司:</small></div>
			<div class="col-md-2">
				<form:input path="injuriousInsuranceCompany" htmlEscape="false" maxlength="64"   cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>保险公司联系电话:</small></div>
			<div class="col-md-2">
				<form:input path="injuriousCompanyTelephone" htmlEscape="false" maxlength="64"  cssStyle="height: 26px;"/>
			</div>
			<div class="col-md-2" ><small>投保商业险名称:</small></div>
			<div class="col-md-2">
				<form:input path="insuredCommercialInsurance" htmlEscape="false" maxlength="64"   cssStyle="height: 26px;"/>
			</div>

		</div>


		<div class="row"  id="subrogateFive">
			<div class="col-md-2"><small>致害人车辆驾驶人民族:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulPeople" htmlEscape="false" maxlength="255"   cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>致害人车辆驾驶人户籍地:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulHousehlod" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>致害人车辆驾驶人身份证号:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulIdcard" htmlEscape="false"  onblur="victimDriverHarmfulIdcardChange();"   cssStyle="height: 26px;"/>
			</div>
		</div>

		<div class="row" id="subrogateSix">

			<div class="col-md-2" ><small>致害人车辆驾驶人性别:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulSex" htmlEscape="false"    cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>致害人车辆驾驶人生日:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulBith" htmlEscape="false"   cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2" ><small>致害人车辆驾驶人联系电话:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulTelephone" htmlEscape="false"    cssStyle="height: 26px;"/>
			</div>

		</div>


		<div class="row" id="subrogateSeven">

			<div class="col-md-2" ><small>致害人车辆驾驶人责任:</small></div>
			<div class="col-md-2">
				<form:input path="victimDriverHarmfulResponsibility" readonly="true" htmlEscape="false" maxlength="255"   cssStyle="height: 26px;"/>
			</div>


		</div>


		<div class="row"  >
			<div class="col-md-2"><small>已还金额:</small></div>
			<div class="col-md-2">
				<%-- onblur="creditorAmountChange();" --%>
				<form:input path="amountPaidBack" htmlEscape="false" maxlength="10" cssStyle="height: 26px;"/>
			</div>
			<div class="col-md-2"><small>债权金额:</small></div>
			<div class="col-md-2">
				<form:input path="creditorAmount" readonly="true" htmlEscape="false" maxlength="10" cssStyle="height: 26px;"/>
			</div>


			<div class="col-md-2"><small>立案诉讼标的金额:</small></div>
			<div class="col-md-2">
				<form:input path="recoveryAmount" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

		</div>

		<div class="row" >
			<div class="col-md-2"><small>授权:</small></div>
			<div class="col-md-2">
				<form:select path="grantAuthorization" class="input-medium " style="width:180px">
					<form:options items="${fns:getDictList('grant_authorization')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>

		</div>

		<div class="row"  >
			<div class="col-md-2"><small>案情简介:</small></div>
			<div class="col-md-10">
				<form:textarea path="caseIntroduction" readonly="true" htmlEscape="false" rows="4"  class="input-xxlarge "/>
			</div>
		</div>
		</div>


		<div id="otherThings">
			<div class="col-md-12 form-item-title">特殊案件信息</div>

			<div class="row"  >
				<div class="col-md-2"><small>债务人信息:</small></div>
				<div class="col-md-10">
					<form:textarea path="specialDebtorInformation" htmlEscape="false" rows="4" maxlength="255"  class="input-xxlarge " />
				</div>
			</div>

			<div class="row"  >
				<div class="col-md-2"><small>案情简介:</small></div>
				<div class="col-md-10">
					<form:textarea path="specialCaseIntroduction" readonly="true" htmlEscape="false" rows="4" maxlength="255"  class="input-xxlarge " />
				</div>
			</div>

			<div class="row"  >

			<div class="col-md-2"><small>债权金额:</small></div>
			<div class="col-md-4">
				<form:input path="specialCssreditorAmount" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			<div class="col-md-2"><small>已还金额:</small></div>
			<div class="col-md-4">
				<form:input path="specialAmountPaidBack" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>

			</div>

		</div>

		<div class="col-md-12 form-item-title">基础资料(CRM)</div>


		<div class="row"  >
			<input type="hidden" id="caseXbbInsuranceId" value="${caseInsurance.caseXbbInsurance.id}">

			<div class="col-md-1"><small>案件名称:</small></div>
			<div class="col-md-3" title="${caseInsurance.caseXbbInsurance.caseName}">
				${caseInsurance.caseXbbInsurance.caseName}
			</div>

			<div class="col-md-1"><small>客户名称:</small></div>
			<div class="col-md-3">
				${caseInsurance.caseXbbInsurance.caseCustomerName}
			</div>

			<div class="col-md-1"><small>签订人:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.signPersone}
			</div>

		</div>


		<div class="row">

			<div class="col-md-1"><small>签订日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.signingDate}
			</div>

			<div class="col-md-1"><small>到期日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.maturityDate}
			</div>


			<div class="col-md-1"><small>案件编号:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.caseNumber}
			</div>

		</div>


		<div class="row">

			<div class="col-md-1"><small>正式债权编号:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.caseCreditorNub}
			</div>

			<div class="col-md-1"><small>材料清单:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.listMaterials}
			</div>

			<div class="col-md-1"><small>是否归档:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.itFiled}
			</div>
		</div>

		<div class="row">

			<div class="col-md-1"><small>更新时间:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.createTime}
			</div>

			<div class="col-md-1"><small>理赔日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.claimDate}
			</div>


			<div class="col-md-1"><small>目前阶段:</small></div>
			<div class="col-md-3">
				<form:select id="currentStageId" path="caseXbbInsurance.currentStage" class="input-medium " style="width:180px" >
					<form:option value="" label=""/>

					<form:options items="${fns:getDictList('current_stage')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>

			</div>

		</div>


		<div class="row">


			<div class="col-md-1"><small>合同商务条款:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.commercialClause}
			</div>

			<div class="col-md-1"><small>合同类型:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.contractType}
			</div>

			<div class="col-md-1"><small>合同状态:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.contractStatus}
			</div>




		</div>

		<div class="row"  >
			<div class="col-md-1"><small>合同金额:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.contractAmount}
			</div>
			<div class="col-md-1"><small>赔偿人名称:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.compensatorName}
			</div>

			<div class="col-md-1"><small>赔偿人联系方式:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.compensatorContact}
			</div>
		</div>


		<div class="row" >

			<div class="col-md-1"><small>赔偿总金额:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.compernsatorAmount}
			</div>

			<div class="col-md-1"><small>创建人:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.createPerson}
			</div>
			<div class="col-md-1"><small>创建日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.createTime}
			</div>
		</div>


		<div class="row"  >

			<div class="col-md-1"><small>客户联系人:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.caseCustmoerContract}
			</div>

			<div class="col-md-1"><small>客户需求:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.caseCustomerDemand}
			</div>
			<div class="col-md-1"><small>案件星级:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.caseStar}
			</div>


		</div>


		<div class="row"  >

			<div class="col-md-1"><small>报案号:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.reportNum}
			</div>

			<div class="col-md-1"><small>案件类型:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.caseType}
			</div>

			<div class="col-md-1"><small>收费方式收费方式:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.chargingMethod}
			</div>

		</div>


		<div class="row"  >

			<div class="col-md-1"><small>承办团队:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.undertakingTeam}
				<%--<form:input path="caseXbbInsurance.undertakingTeam" readonly="true" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>--%>
			</div>

			<div class="col-md-1"><small>承办律师:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.undertakingLawyer}
				<%--<form:input path="caseXbbInsurance.undertakingLawyer" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>

			<div class="col-md-1"><small>承办状态:</small></div>
			<div class="col-md-3">

				<form:select id="undertakingStatusId" path="caseXbbInsurance.undertakingStatus" class="input-medium " style="width:180px" >
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('undertaking_status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>

					<%--	${caseInsurance.caseXbbInsurance.undertakingStatus}
                    <form:input path="caseXbbInsurance.undertakingStatus" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>

		</div>


		<div class="row"  >

			<div class="col-md-1"><small>委外情况:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.outSourcing}
				<%--<form:input path="caseXbbInsurance.outSourcing" readonly="true" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>--%>
			</div>

			<div class="col-md-1"><small>委外日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.outSourcingDate}
				<%--<form:input path="caseXbbInsurance.outSourcingDate" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>

			<div class="col-md-1"><small>合作律师信息:</small></div>
			<div class="col-md-3">

				<form:input id="cooperativeLawyer" path="caseXbbInsurance.cooperativeLawyer"  htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>
			</div>



		</div>


		<div class="row" >

			<div class="col-md-1"><small>诉讼/执行时效届满日:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.limitationDate}
				<%--<form:input path="caseXbbInsurance.limitationDate" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>

			<div class="col-md-1"><small>事故发生地:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.accidentPlace}

				<%--<form:input path="caseXbbInsurance.accidentPlace" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>

			<div class="col-md-1"><small>事故发生日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.accidentDate}
				<%--<form:input path="caseXbbInsurance.accidentDate" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>






		</div>



		<div class="row"  >

			<div class="col-md-1"><small>管辖法院:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.jurisdictionalCourt}
				<%--<form:input path="caseXbbInsurance.jurisdictionalCourt" readonly="true" htmlEscape="false" maxlength="255" cssStyle="height: 26px;"/>--%>
			</div>


			<div class="col-md-1"><small>诉讼立案日期:</small></div>
			<div class="col-md-3">
					<%--${caseInsurance.caseXbbInsurance.lawsuitFillingDate}--%>

						<input id="lawsuitFillingDate" name="caseXbbInsurance.lawsuitFillingDate" type="text" readonly="readonly" maxlength="20" style="height: 26px;" class="input-medium Wdate "
							   value="<fmt:formatDate value="${caseInsurance.caseXbbInsurance.lawsuitFillingDate}" pattern="yyyy-MM-dd"/>"
							   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</div>


			<div class="col-md-1"><small>开庭日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.openingTime}

				<%--<form:input path="caseXbbInsurance.openingTime" readonly="true" htmlEscape="false" maxlength="255"  cssStyle="height: 26px;"/>--%>
			</div>

		</div>





		<div class="row" >

			<div class="col-md-1"><small>举证期限:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.limitTime}

			</div>



			<div class="col-md-1"><small>续封续冻日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.refrigerationDate}

			</div>

			<div class="col-md-1"><small>申请执行日期:</small></div>
			<div class="col-md-3">
					${caseInsurance.caseXbbInsurance.executionDate}
			</div>




		</div>
			<div class="row" >

				<div class="col-md-1"><small>律师函:</small></div>
				<div class="col-md-3">
					<form:select id="lawyerLetterId" path="caseXbbInsurance.lawyerLetter" class="input-medium " style="width:180px" >
						<form:option value="" label=""/>
						<form:options items="${fns:getDictList('lawyer_letter')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
					</form:select>

				</div>


			<div class="col-md-1"><small>还款意向:</small></div>
			<div class="col-md-3">
				<form:select id="repaymentIntentionId" path="caseXbbInsurance.repaymentIntention" class="input-medium " style="width:180px" >
					<form:option value="" label=""/>

					<form:options items="${fns:getDictList('repayment_Intention')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>

			</div>



			</div>














		<div class="col-md-12 form-item-title">导出选项</div>
		<div class="row"  >
			<div class="col-md-1"><small>选择项:</small></div>
			<div class="col-md-11">
				<form:checkboxes path="insuranceSubrogation"  items="${fns:getDictList('insurance_subrogation')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</div>
		</div>
	</div>

		<div  style="position: fixed;bottom:45px;right:10px;">

			<a href="#goback"><span class="btn btn-primary">回到顶部</span></a>
		</div>

		<div class="form-actions">
			<%--<shiro:hasPermission name="zqwcase:caseInsurance:edit"><input id="btnSubmit" class="btn btn-primary" type="submit"  onclick="setsaveFlag('1');" value="继续添加"/>&nbsp;</shiro:hasPermission>--%>
			<input id="btnSubmit" class="btn btn-primary" type="submit"  onclick="setsaveFlag('0');" value="保 存"/>&nbsp;
			<input id="btnSubmit" class="btn btn-primary" type="submit"  onclick="setsaveFlag('2');" value="添加进度"/>&nbsp;
			<input id="btnSubmit" class="btn btn-primary" type="submit"  onclick="setsaveFlag('3');" value="导出文件"/>&nbsp;
			<input  class="btn btn-primary" type="button"  onclick="syncCrmData();" value="同步到CRM"/>&nbsp;
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>