<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件信息管理</title>
	<meta name="decorator" content="default"/>


	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/");
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
        function searchPage() {
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/");
            $("#searchForm").submit();
            return false;
        }

        function setcheckAllFlag() {
            var tempVal=$('input:checkbox[name="checkAll"]:checked').val();
            if(tempVal=="1"){
               $("#ckeckAllFlag").val("all");
            }
            else{
                $("#ckeckAllFlag").val("1");
            }
        }

        function chosesEexport() {
            top.$.jBox.open("iframe:${ctx}/zqwcase/caseInsurance/exportForm", "选择导出文件",1010,$(top.document).height()-640,{
                buttons:{"确定":"ok", "关闭":true}, bottomText:"选择。",submit:function(v, h, f){
                    var insuranceSubrogations = h.find("iframe")[0].contentWindow.$("input[name='insuranceSubrogation']:checked").map(function(index,element){ return $(element).val()}).get().join(',');
                    if (v=="ok"){

                        var chk_value =[];
                        $('input[name="exprotId"]:checked').each(function(){
                            chk_value.push($(this).val());
                        });
						$("#exprotIds").val(chk_value.join(","));
						$("#insuranceSubrogations").val(insuranceSubrogations);
						$("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportZip").submit();

                    }
                }, loaded:function(h){
                    $(".jbox-content", top.document).css("overflow-y","hidden");
                }
            });
        }

        function chosesAllEexport() {
            $("#ckeckAllFlag").val("all");
            top.$.jBox.open("iframe:${ctx}/zqwcase/caseInsurance/exportForm", "选择导出文件",1010,$(top.document).height()-640,{
                buttons:{"确定":"ok", "关闭":true}, bottomText:"选择。",submit:function(v, h, f){
                    var insuranceSubrogations = h.find("iframe")[0].contentWindow.$("input[name='insuranceSubrogation']:checked").map(function(index,element){ return $(element).val()}).get().join(',');
                    if (v=="ok"){

                        var chk_value =[];
                        $('input[name="exprotId"]:checked').each(function(){
                            chk_value.push($(this).val());
                        });
                        $("#exprotIds").val(chk_value.join(","));
                        $("#insuranceSubrogations").val(insuranceSubrogations);
                        $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportZip").submit();

                    }
                }, loaded:function(h){
                    $(".jbox-content", top.document).css("overflow-y","hidden");
                }
            });
        }

        function exportLayerNum() {
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportLayerNum").submit();
        }

        function exportCase() {
            $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exprotCaseInsurance").submit();
        }
        
        function allSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/allSync",
                data: {projcetApproverStep : $("#projcetApproverStep").val()},
                dataType: "json",
                async: false,
                success: function(data){
                        alert("同步成功");
                        searchPage();
                }
            });

        }
        
        function incrementSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/incrementSync",
                data: {projcetApproverStep : $("#projcetApproverStep").val()},
                dataType: "json",
                async: false,
                success: function(data){
                        alert("同步成功");
                        searchPage();

                }
            });
        }


        function communicateSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/communicateSync",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("同步成功");
                    searchPage();

                }
            });
        }



        function paymentSync() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/paymentSync",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("同步成功");
                    searchPage();

                }
            });
        }


        function changeLayers() {
            $.ajax({
                type: "POST",
                url: "${ctx}/zqwcase/caseInsurance/changeLayers",
                dataType: "json",
                async: false,
                success: function(data){
                    alert("生成成功");
                }
            });
        }

        function checkAll(obj){
            if (obj.checked){
                $("input[name='exprotId']").each(function(){
                    this.checked = true;
                })
            }else{
                $("input[name='exprotId']").each(function(){
                    this.checked = false;
                })
            }
        }


        function toForm(id) {
            var pageNo = $('#pageNo').val();
            var pageSize = $('#pageSize').val();
            var creditorSearch = $('#creditor').val();
            var caseTypeSeache = $('#caseTypeSeache').val();
//            var deptSearch =$('#deptSearch').val();
            var caseNameSearch =$('#caseNameSearch').val();
            var reportNumSearch =$('#reportNumSearch').val();
            var caseNumberSearch =$('#caseNumberSearch').val();
            location.href = "${ctx}/zqwcase/caseInsurance/form?id=" +id
				 +"&pageNo="+pageNo
				+"&pageSize="+pageSize
				+"&creditorSearch="+creditorSearch
				+"&caseTypeSeache="+caseTypeSeache
                +"&caseNameSearch="+caseNameSearch
				+"&reportNumSearch="+reportNumSearch
				+"&caseNumberSearch="+caseNumberSearch;
        }

	</script>
</head>
<body>
	<%--<ul class="nav nav-tabs">--%>
		<%--<li class="active"><a href="${ctx}/zqwcase/caseInsurance/">案件信息列表</a></li>--%>
		<%--&lt;%&ndash;<shiro:hasPermission name="zqwcase:caseInsurance:edit"><li><a href="${ctx}/zqwcase/caseInsurance/form">案件信息添加</a></li></shiro:hasPermission>&ndash;%&gt;--%>
	<%--</ul>--%>
	<form:form id="searchForm" modelAttribute="caseInsurance" action="${ctx}/zqwcase/caseInsurance/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>序号</th>
				<th>案件名称</th>
				<th>报案号</th>
				<th>案件编号</th>
				<th>案件类别</th>
				<th>第一债务人</th>
				<th>时效起算日期</th>
				<th>债权金额</th>
				<th>收案时间</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${caseXbbInsurances}" var="caseXbbInsurance" varStatus="varstaus">
			<tr>
				<td>${varstaus.index+1}</td>
				<td><a href="${ctx}/zqwcase/caseInsurance/view?id=${caseXbbInsurance.caseInsuranceId}">${caseXbbInsurance.caseName}</a></td>
				<td>${caseXbbInsurance.reportNum}</td>
				<td>${caseXbbInsurance.caseNumber}</td>
				<td>
				${caseXbbInsurance.caseType}
				</td>

				<c:if test="${caseXbbInsurance.caseType eq '0'}">
					<td>
						${caseXbbInsurance.insuredVehicleDriver}
					</td>
				</c:if>

				<c:if test="${caseXbbInsurance.caseType eq '1'}">
					<%--代位--%>
					<td>
						${caseXbbInsurance.victimDriverHarmful}
					</td>
				</c:if>

				<c:if test="${caseXbbInsurance.caseType eq '2'}">
					<%--特殊--%>
					<td>

					</td>
				</c:if>


				<c:if test="${caseXbbInsurance.caseType eq '0'}">
					<%--追偿--%>
					<td>
						<fmt:formatDate value="${caseXbbInsurance.accidentJudgmentDate}" pattern="yyyy-MM-dd"/>
					</td>
				</c:if>

				<c:if test="${caseXbbInsurance.caseType eq '1'}">
					<%--代位--%>
					<td>
						<fmt:formatDate value="${caseXbbInsurance.insuranceCompanyTime}" pattern="yyyy-MM-dd"/>

					</td>
				</c:if>

				<c:if test="${caseXbbInsurance.caseType eq '2'}">
					<%--特殊--%>
					<td>

					</td>
				</c:if>



				<td>
					${caseInsurance.creditorAmount}
				</td>

				<%--<td>--%>
				<%--</td>--%>

				<td>
					<fmt:formatDate value="${caseInsurance.createDate}" pattern="yyyy-MM-dd"/>
				</td>

			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>