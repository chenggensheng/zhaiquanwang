<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <%--<%@include file="/WEB-INF/views/include/hmhead.jsp"%>--%>

<%--<meta name="decorator" content="default"/>--%>
    <%--<%@include file="/WEB-INF/views/include/hmhead.jsp"%>--%>
    <meta charset="utf-8">
    <%--<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
    <!--360浏览器优先以webkit内核解析-->
    <title>统计展示页面</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="${ctxStatic}/hAdmin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${ctxStatic}/hAdmin/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${ctxStatic}/hAdmin/css/animate.css" rel="stylesheet">
    <link href="${ctxStatic}/hAdmin/css/style.css?v=4.1.2" rel="stylesheet">
    <%--<script src="${ctxStatic}/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>--%>


        <script src="${ctxStatic}/hAdmin/js/jquery.min.js?v=2.1.4"></script>
        <script src="${ctxStatic}/hAdmin/js/bootstrap.min.js?v=3.3.6"></script>

        <!-- layer javascript -->
        <script src="${ctxStatic}/hAdmin/js/plugins/layer/layer.min.js"></script>


        <script type="text/javascript">


        function showList(showType) {
            var contentText = "${ctx}/zqwcase/caseInsurance/showTypeList?showType="+showType;
            var openFull =  layer.open({
                type:2,
                title:"案例类别",
                content:contentText,
                area: ['1000px', '600px'],
                end: function(){

                },
                cancel:function () {

                }
            });
            layer.full(openFull);

        }

        function  exportCaseMaterial() {
                $("#searchForm").attr("action","${ctx}/zqwcase/caseInsurance/exportCaseMaterial").submit();
        }

    </script>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content">

<form:form id="searchForm" action="" method="post" class="breadcrumb form-search">
</form:form>

<div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">

                            <div class="ibox-content">
                                <div class="row m-b-sm m-t-sm">
                                    <div class="col-md-12" style="text-align: center">
                                        <h5> ${caseInsuranceBoard.yearString}年${caseInsuranceBoard.monthString} 月 ${caseInsuranceBoard.dayString}日保险案件资料缺补最新情况</h5>
                                        <button type="button" class="btn btn-sm btn-primary" onclick="exportCaseMaterial();" >导出案件资料列表</button>

                                    </div>

                                </div>
                                <div class="project-list">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th class="project-status" style="width: 4%;">
                                                序号
                                            </th>
                                            <th class="project-status" style="width: 10%;">
                                                案件名称
                                            </th>
                                            <th class="project-status" style="width: 8%;">
                                                承办人员
                                            </th>

                                            <th class="project-status"  style="width: 13%;">
                                                F资缺
                                            </th>
                                            <th class="project-status"  style="width: 15%;">
                                                F资补
                                            </th>
                                            <th class="project-status"  style="width: 15%;">
                                                S资缺
                                            </th>
                                            <th class="project-status"  style="width: 15%;">
                                                S资补
                                            </th>
                                            <th class="project-status" style="width: 10%;">
                                                SW资缺
                                            </th>
                                            <th class="project-status"  style="width: 10%;">
                                                SW资补
                                            </th>
                                        </tr>

                                        <c:forEach items="${caseInsuranceBoard.caseInsurancePersonBoards}" var="caseInsurancePersonBoard" varStatus="caseInsurancePersonBoardIndex">
                                            <tr>
                                                <td class="project-status">
                                                    ${caseInsurancePersonBoardIndex.index+1}
                                                </td>
                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.caseInsuranceName}
                                                </td>
                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.name}
                                                </td>

                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.fLackFunds}
                                                </td>

                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.fSupplement}
                                                </td>

                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.sShortage}
                                                </td>

                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.sSupplement}
                                                </td>

                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.swShortage}
                                                </td>

                                                <td class="project-status">
                                                        ${caseInsurancePersonBoard.swSupplement}
                                                </td>
                                            </tr>
                                        </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
