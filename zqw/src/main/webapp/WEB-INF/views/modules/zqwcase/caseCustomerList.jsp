<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>案件客户信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/zqwcase/caseCustomer/">案件客户信息列表</a></li>
		<shiro:hasPermission name="zqwcase:caseCustomer:edit"><li><a href="${ctx}/zqwcase/caseCustomer/form">案件客户信息添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="caseCustomer" action="${ctx}/zqwcase/caseCustomer/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>债权人：</label><form:input path="creditor" htmlEscape="false" maxlength="50" class="input-medium"/></li>

			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>债权人</th>
				<th>债权人信用代码</th>
				<th>债权负责人</th>
				<th>债权人负责人职位</th>
				<th>承办律师</th>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="zqwcase:caseCustomer:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="caseCustomer">
			<tr>
				<td><a href="${ctx}/zqwcase/caseCustomer/form?id=${caseCustomer.id}">
					${caseCustomer.creditor}
				</a></td>
				<td>
					${caseCustomer.creditorIdcart}
				</td>
				<td>
					${caseCustomer.creditorCharge}
				</td>
				<td>
					${caseCustomer.creditorChargePositon}
				</td>
				<td>
					${caseCustomer.undertakingLawyerName}
				</td>
				<td>
					<fmt:formatDate value="${caseCustomer.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${caseCustomer.remarks}
				</td>
				<shiro:hasPermission name="zqwcase:caseCustomer:edit"><td>
    				<a href="${ctx}/zqwcase/caseCustomer/form?id=${caseCustomer.id}">修改</a>
					<a href="${ctx}/zqwcase/caseCustomer/delete?id=${caseCustomer.id}" onclick="return confirmx('确认要删除该案件客户信息吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>