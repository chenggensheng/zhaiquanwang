<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>北京中安德信科技有限公司</title>
    <meta name="keywords" content="债全网,北京中安德信科技有限公司">
    <meta name="description" content="北京中安德信科技有限公司。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
    <script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>


    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bootstrap.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/animate.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/ysemm.css">--%>

    <style type="text/css">
        .icon {
            display: inline-block;
            background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
            background-repeat:no-repeat;
        }

        .icon-more{
            width:8px;
            height:7px;
            background-position: 0 -5px;
            vertical-align:  middle;
            margin-left:  3px;
            margin-top: -2px;
        }
    </style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
    <div class="layui-container">
        <!-- 公司logo -->
        <%--<div class="nav-logo">--%>
            <%--<a href="https://www.layui-inc.com/">--%>
                <%--<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" alt="北京中安德限公司" class="logo">--%>
            <%--</a>--%>
        <%--</div>--%>
        <div class="">
            <a href="https://www.zhaiquanwang.com/" >
                <img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
            </a>
        </div>
        <div class="nav-list">

            <ul class="layui-nav" lay-filter="">
                <li class="layui-nav-item layui-this"><a href="${ctx}/index-1${fns:getUrlSuffix()}">首页</a></li>
                <c:forEach items="${fnc:getMainNavList(site.id)}" var="category" varStatus="status">
                    <li class="layui-nav-item "><a href="${category.url}" target="${category.target}">${category.name}</a></li>
                </c:forEach>

                <span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
        </div>
    </div>
</div>

<div  style="width: 100%; height: 100px;"></div>
<!-- banner部分 -->
<%--<div class="main-banner">--%>
    <%--<div class="layui-carousel" id="banner" lay-anim="fade" lay-indicator="inside" lay-arrow="hover" style="width: 100%; height: 660px;">--%>
        <%--<div carousel-item="">--%>

            <%--<div style="background-image: url(${ctxStatic}/res/static/layui-inc/img/banner01.png)" class="layui-this">--%>
                <%--<div class="layui-container">--%>
                    <%--<div class="layinc-panel">--%>
                        <%--<div class="layinc-position">--%>
                            <%--<!--<p class="title">携<span style="color: #209ABD">法律</span>利器，助你疾风前行</p>--%>
                            <%--<p>抛下一切债权，轻装奔涌</p>--%>
                            <%---->--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div style="background-image: url(${ctxStatic}/res/static/layui-inc/img/banner01-a.png)" class="">--%>
                <%--<div class="layui-container">--%>
                    <%--<div class="layinc-panel">--%>
                        <%--<div class="layinc-position">--%>
                            <%--<!----%>
                            <%--<p class="title" style="color: rgba(255,255,255,.8)">指尖之术</p>--%>
                            <%--<p style="color: rgba(255,255,255,.6)">可造琼楼玉宇</p>-->--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div style="background-image: url(${ctxStatic}/res/static/layui-inc/img/banner3.jpg)" class="">--%>
                <%--<div class="layui-container">--%>
                    <%--<div class="layinc-panel">--%>
                        <%--<div class="layinc-position" style="width: 100%; text-align: center;">--%>
                            <%--<p class="title">应天地万物</p>--%>
                            <%--<p>类友而待</p>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div style="background-image: url(${ctxStatic}/res/static/layui-inc/img/banner2.jpg)" class="">--%>
                <%--<div class="layui-container">--%>
                    <%--<div class="layinc-panel">--%>
                        <%--<div class="layinc-position" style="width: 100%; text-align: center;">--%>
                            <%--<p class="title">应天地万物</p>--%>
                            <%--<p>类友而待</p>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>

        <%--</div>--%>
        <%--<div class="layui-carousel-ind">--%>
            <%--<ul><li class=""></li>--%>
                <%--<li class="layui-this"></li>--%>
                <%--<li></li>--%>
            <%--</ul></div>--%>
        <%--<button class="layui-icon layui-carousel-arrow" lay-type="sub"></button>--%>
        <%--<button class="layui-icon layui-carousel-arrow" lay-type="add"></button>--%>
    <%--</div>--%>
<%--</div>--%>


<%--<div class="main-product">--%>
    <%--<div class="layui-container">--%>
        <%--<div class="layui-row layui-col-space25">--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon01.png"></div>--%>
                    <%--<p class="label">金融类金融</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon02.png"></div>--%>
                    <%--<p class="label">设备制造</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon03.png"></div>--%>
                    <%--<p class="label">建筑施工</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon04.png"></div>--%>
                    <%--<p class="label">物业管理</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon12.png"></div>--%>
                    <%--<p class="label">软件信息</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon05.png"></div>--%>
                    <%--<p class="label">节能环保</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon06.png"></div>--%>
                    <%--<p class="label">海外催收</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon07.png"></div>--%>
                    <%--<p class="label">融资租</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon08.png"></div>--%>
                    <%--<p class="label">人防工程</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon09.png"></div>--%>
                    <%--<p class="label">电线电缆</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon10.png"></div>--%>
                    <%--<p class="label">知识产权</p>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class=" layui-col-md2">--%>
                <%--<div class="content">--%>
                    <%--<div><img src="${ctxStatic}/res/static/layui-inc/img/zqw-icon11.png"></div>--%>
                    <%--<p class="label">更多</p>--%>
                <%--</div>--%>
            <%--</div>--%>


        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>

<div class="main-about">
    <div class="layui-container">
        <div class="layui-row">
            <div class="tabJob layui-show">
                <div class="content">
                    <p class="title">债全网在行动</p>
                    <div style="line-height: 26px; color: #999;">
                        <p style="text-indent: 2em">说明：债全网，执行难的终结者！集结全国万名债权律师，为您提供一包到底的专属服务，全网联合行动，定期工作报告，处理流程化、透明化，真正解决您的债权执行难题。交给债全网，下一个有进展的就是您的案件！
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- main部分 -->
<div class="main-product">
    <div class="layui-container">
        <div class="layui-row layui-col-space25">
            <div>
                <div class="content">
                    <table class="layui-hide" id="zxd"></table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



</div>

</div>



<!-- footer部分 -->
<div class="footer">
    <div class="layui-container">

        <div class="layui-row footer-contact">
            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
                <p class="contact-bottom" style="padding-left: 10px;">官方微信</p>

            </div>

            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
                <p class="contact-bottom" style="padding-left: 30px;">帅凯</p>
            </div>

            <div class="layui-col-sm4 layui-col-lg4">
                <ul class="site-dir layui-layer-wrap">
                    <li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2018，让债全网团队真正加速成长</a></li>
                </ul>
            </div>


            <div class="layui-col-sm4 layui-col-lg4">
                <p class="contact-top"><span class="right">© 中安德信科技有限公司</span></p>
                <p class="contact-bottom"><span class="right"> 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
                <p class="contact-bottom"><span class="right"> 客服电话： 400-005-8810</span></p>
                <p class="contact-bottom"><span class="right"> 官网邮箱： service@andsense.cn</span></p>

            </div>

        </div>
    </div>
</div>
<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    layui.config({
        base: '${ctxStatic}/res/static/layui-inc/js/'
        ,version: '1535185020009'
    }).use('firm');

    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#zxd'
            ,url:'${ctx}/listAtionJSON/'
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {field:'debtType', width:80, title: '类型',align: 'center', templet: '#debtTypeTpl'}
                ,{field:'debtNum', width:180, title: '债权编号',align: 'center'}
                ,{field:'debtOwernName', width:180, title: '债权人名称',align: 'center'}
                ,{field:'debtPrice', title: '未履行金额（元）', width:187,align: 'center'}
                ,{field:'debtActionTime', width:137, title: '行动时间',align: 'center'}
                ,{field:'debtInfo', title: '行动结果',align: 'left'}
            ]]
            ,page: true
        });
    });


</script>


<script type="text/html" id="debtTypeTpl">
    {{#  if(d.debtType === '0'){ }}
    自然
    {{#  } else { }}
    法定
    {{#  } }}
</script>

<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b8fd883578db629b5f88e5d9512ba18b";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>