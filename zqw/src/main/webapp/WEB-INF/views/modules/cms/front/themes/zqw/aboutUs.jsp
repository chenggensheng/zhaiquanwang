<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>北京中安德信科技有限公司</title>
    <meta name="keywords" content="债全网,北京中安德信科技有限公司">
    <meta name="description" content="北京中安德信科技有限公司。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
    <script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>


    <link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/animate.css">
    <link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/ysemm.css">

    <style type="text/css">
        .icon {
            display: inline-block;
            background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
            background-repeat:no-repeat;
        }

        .icon-more{
            width:8px;
            height:7px;
            background-position: 0 -5px;
            vertical-align:  middle;
            margin-left:  3px;
            margin-top: -2px;
        }
    </style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
    <div class="layui-container">
        <!-- 公司logo -->
        <%--<div class="nav-logo">--%>
            <%--<a href="https://www.layui-inc.com/">--%>
                <%--<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" alt="北京中安德限公司" class="logo">--%>
            <%--</a>--%>
        <%--</div>--%>

        <div class="">
            <a href="https://www.zhaiquanwang.com/" >
                <img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
            </a>
        </div>
        <div class="nav-list">
            <button>
                <span></span><span></span><span></span>
            </button>
            <ul class="layui-nav" lay-filter="">

                <li class="layui-nav-item "><a href="${ctx}/index-1${fns:getUrlSuffix()}"><span>${site.id eq '1'?'首页':'返回主站'}</span></a></li>
                <c:forEach items="${fnc:getMainNavList(site.id)}" var="categoryTemp" varStatus="status">
                    <li class=" layui-nav-item <c:if test="${categoryTemp.id eq category.id}"> layui-this </c:if> "><a href="${categoryTemp.url}" target="${categoryTemp.target}">${categoryTemp.name}</a></li>
                </c:forEach>

                <span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
        </div>
    </div>
</div>

<!-- banner部分 -->
<div class="banner product">
    <div class="title active">
        <p style="font-family: 'Open Sans', sans-serif">关于我们</p>
        <p class="en" style="font-family: 'Open Sans', sans-serif">About Us</p>
    </div>
</div>



<!-- main部分 -->
<div class="main-product">
    <div class="layui-container">
        <p class="title" style="font-family: 'Open Sans', sans-serif">债全网介绍</p>
        <p style="text-indent: 2em;">作为覆盖全国范围的自营式债权（应收账款）解决互联网平台，由中恩信律师事务所、北京中安德信科技有限公司经营管理，通过整合各类线上数据及线下人脉资源，采用一包到底、内部分包、集中管控的自营式平台模式，彻底颠覆传统的律师催收方式，利用“互联网+ERP”实现标准化流程的质量管控体系，建立全国专业律师服务网络、走专业化法律技术路线，专注于解决债权流转的核心问题。

        </p>
    </div>
</div>
</div>


<div class="main-product">
    <div class="layui-container">
        <p class="title"style="font-family: 'Open Sans', sans-serif">解决客户核心问题</p>
        <p style="text-align: left; justify-content: inherit ;">
            1、 难以找到专业机构的问题</p>
        <p style="text-indent: 2em;">
            我们是公司化管理的律师事务所，同时又是一家互联网化的科技公司，自2011年以来始终专注债权解决方案，坚持以法律技术为核心、以金融手段为支撑、以互联网为平台，只做债权，所以我们更专业。</p>
        <p style="text-align: left; justify-content: inherit;">
        2、债权实现途径是否多样性的问题</p>
        <p style="text-indent: 2em;">
        我们精通债权（应收账款）生命周期的特征，从事前、事中、事后，从定性评估、非诉调解、诉讼执行到抵偿、融资、处置等，我们均有成熟、专业的解决方案和丰富经验。</p>
        <p style="text-align: left; justify-content: inherit;">
        3、法律诉讼成本高、周期长的问题</p>
        <p style="text-indent: 2em;">
        我们采用标准化、流程化、集成化管理，收费合理、高效快捷，专业、安全。</p>
        <p style="text-align: left; justify-content: inherit;">
        4、批量处理案件能力的问题</p>
        <p style="text-indent: 2em;">
        正是因为我们采用创新型的平台化业务模式，采用流程化管理，所以具备大批量案件的处理能力，无论是金融机构的各类消费贷、小微贷等逾期贷款，还是制造商遇到大量终端客户的拖欠货款，我们均可以保证快速、高效的处理。</p>
        <p style="text-align: left; justify-content: inherit;">
        5、合法安全性的问题</p>
        <p style="text-indent: 2em;">
        我们主要承办人员均是执业律师，依法依规是我们的基本工作准则，所有案件承办过程均确保符合国家法律法规的规定，合理合法。</p>
        <p style="text-align: left; justify-content: inherit;">
        6、区域性限制的问题</p>
        <p style="text-indent: 2em;">
        我们采用平台化业务模式，全国已发展超过1万名债权律师，确保服务覆盖全国各地无死角。</p>
        <p style="text-align: left; justify-content: inherit;">
        7、信息沟通不畅的问题</p>
        <p style="text-indent: 2em;">
        我们采用一对一管家级服务，客户只需要联系我们的客服人员，所有的沟通问题都可以解决，并且我们保证24小时全天候响应，同时开通400电话和服务投诉热线。
        </p></p>
       </p>
    </div>
</div>
</div>


<div class="main-product">
    <div class="layui-container">
        <p class="title" style="font-family: 'Open Sans', sans-serif">债全网在哪里？</p>
        <div class="layui-row layui-col-space25">
                <div class="row changde-contactus-info">
                    <div class="col-lg-4 col-xs-4 contact-box">
                        <h2 style="height: 45px;line-height: 45px;font-size:18px;text-align:left;text-indent: 0.8em;font-weight: 600;letter-spacing: 1px;" >运营中心</h2>
                        <p style="text-align: left;min-height: 40px;line-height: 40px;font-size: 16px;letter-spacing: 1px;color: black"><i class="icon-addr"></i>北京：中关村梦想实验室七楼</p>
                        <p style="text-align: left;min-height: 40px;line-height: 40px;font-size: 16px;letter-spacing: 1px;color: black"><i></i>H座11层</p>
                        <p style="text-align: left;min-height: 40px;line-height: 40px;font-size: 16px;letter-spacing: 1px;color: black"><i class="icon-addr"></i>上海：浦东新区东方路738号裕安大厦</p>
                        <p style="text-align: left;min-height: 40px;line-height: 40px;font-size: 16px;letter-spacing: 1px;color: black"><i></i>3楼Funwork办公区</p>
                        <p style="text-align: left;min-height: 40px;line-height: 40px;font-size: 16px;letter-spacing: 1px;color: black"><i class="icon-addr"></i>南昌：红谷滩新区红谷中大道1368号</p>
                        <p style="text-align: left;min-height: 40px;line-height: 40px;font-size: 16px;letter-spacing: 1px;color: black"><i></i>鼎峰中央C座14楼</p>
                    </div>

                    <div class="col-lg-8 col-xs-8" id="map" style="overflow: hidden; position: relative; z-index: 0;  color: rgb(0, 0, 0); text-align: left;">
                </div>
            </div>
        </div>
    </div>
</div>
</div>



<!-- footer部分 -->
<div class="footer">
    <div class="layui-container">

        <div class="layui-row footer-contact">
            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
                <p class="contact-bottom" style="font-weight: 400;line-height: 35px!important;padding-left: 10px;font-size: 16px; font-family: 'Open Sans',san-serif; font-stretch: 100%">官方微信</p>

            </div>

            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
                <p class="contact-bottom" style="font-weight: 400;line-height: 35px!important;padding-left: 30px;font-size: 16px;" >帅凯</p>
            </div>

            <div class="layui-col-sm4 layui-col-lg4">
                <ul class="site-dir layui-layer-wrap" style="font-size: 16px;">
                    <li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2017，让律师团队真正加速成长</a></li>
                </ul>
            </div>


            <div class="layui-col-sm4 layui-col-lg4">
                <p class="contact-top" style="font-size: 16px; "><span class="right" style="color: white;">© 中安德信科技有限公司</span></p>
                <p class="contact-bottom" style="font-size: 16px;"><span class="right" style="color: white;" > 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
                <p class="contact-bottom" style="font-size: 16px;" ><span class="right" style="color: white;" > 客户电话： 400-005-8810</span></p>
                <p class="contact-bottom" style="font-size: 16px;" ><span class="right" style="color: white;"> 官网邮箱： service@andsense.cn</span></p>

            </div>

        </div>
    </div>
</div>
<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    layui.config({
        base: '${ctxStatic}/res/static/layui-inc/js/'
        ,version: '1535185020009'
    }).use('firm');





</script>


<script type="text/html" id="deptTypeTpl">
    {{#  if(d.deptType === '0'){ }}
     自然
    {{#  } else { }}
    法定
    {{#  } }}
</script>

<script type="text/html" id="debtTypeTpl">
    {{#  if(d.debtType === '0'){ }}
    自然
    {{#  } else { }}
    法定
    {{#  } }}
</script>

<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b8fd883578db629b5f88e5d9512ba18b";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();


</script>

<script type="text/javascript" src="${ctxStatic}/res/baidu/api"></script>
<script type="text/javascript" src="http://api.map.baidu.com/getscript?v=2.0&ak=Kpjp7jddqVUhWK5VkrfNt3YNezY88NtR&services=&t=20170517145936"></script>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bmap.css">
<script type="text/javascript" src="${ctxStatic}/res/baidu/baiduMap.js"></script>
<script type="text/javascript">

    (function($) {
        $(function(){
//           alert(1234567890);,
            baiduMap('map','116.321815','39.989253','北京中安德信科技有限公司','中关村梦想实验室七楼');
        });
    })(jQuery);
    // 百度地图API功能



</script>

<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>