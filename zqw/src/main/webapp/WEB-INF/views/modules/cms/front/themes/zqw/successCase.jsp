<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>北京中安德信科技有限公司</title>
    <meta name="keywords" content="债全网,北京中安德信科技有限公司">
    <meta name="description" content="北京中安德信科技有限公司。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
    <script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>


    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bootstrap.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/animate.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/ysemm.css">--%>

    <style type="text/css">
        .icon {
            display: inline-block;
            background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
            background-repeat:no-repeat;
        }

        .icon-more{
            width:8px;
            height:7px;
            background-position: 0 -5px;
            vertical-align:  middle;
            margin-left:  3px;
            margin-top: -2px;
        }
    </style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
    <div class="layui-container">
        <!-- 公司logo -->
        <%--<div class="nav-logo">--%>
            <%--<a href="https://www.layui-inc.com/">--%>
                <%--<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" alt="北京中安德限公司" class="logo">--%>
            <%--</a>--%>
        <%--</div>--%>
        <div class="">
            <a href="https://www.zhaiquanwang.com/" >
                <img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
            </a>
        </div>
        <div class="nav-list">
            <button>
                <span></span><span></span><span></span>
            </button>
            <ul class="layui-nav" lay-filter="">

                <li class="layui-nav-item "><a href="${ctx}/index-1${fns:getUrlSuffix()}"><span>${site.id eq '1'?'首页':'返回主站'}</span></a></li>
                <c:forEach items="${fnc:getMainNavList(site.id)}" var="categoryTemp" varStatus="status">
                    <li class=" layui-nav-item <c:if test="${categoryTemp.id eq category.id}"> layui-this </c:if> "><a href="${categoryTemp.url}" target="${categoryTemp.target}">${categoryTemp.name}</a></li>
                </c:forEach>

                <span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
        </div>
    </div>
</div>

<!-- banner部分 -->
<div class="banner product">
    <div class="title active">
        <p>成功案例</p>
        <p class="en">Success Case</p>
    </div>
</div>



<div class="main-about">
    <div class="layui-container">
        <div class="layui-row">
            <div class="tabJob layui-show">
                <div class="content">
                    <p class="title">案例一：揭开公司有限责任面纱，突破性地把一人公司股东加入共同清偿责任，案件圆满解决</p>
                    <div style="line-height: 26px; color: #999;">
                        <p style="text-indent: 2em">某世界著名制造商集团下属某机械有限公司（以下简称“A公司”）诉昆山某B公司（以下简称“B公司”）买卖合同纠纷执行案，经过我方深度调查发现B公司与股东朱某之间存在财产混同的现象，遂多方收集股东朱某代B公司向第三方支付费用的证据，并就成功就一人公司股东其配偶卢某追加为卢某为被告，经法院判决B公司、朱某、卢某共同承担清偿责任，并顺利执行到位。
                        </p>
                    </div>
                </div>


                <div class="content">
                    <p class="title">案例二：利用追究职务侵占手段，成功将员工侵占的数百万资金追回</p>
                    <div style="line-height: 26px; color: #999;">
                        <p style="text-indent: 2em">国公司国内分公司宁夏地区销售经理刘某，私自设立与公司经营范围相同的B公司，并通过该公司向客户销售货物并索取佣金，造成公司数百万的经济损失，我方接受委托后，经过分析决定同时从刑事与民事方面切入，在多次实地收集证据材料后，我方向公安机关控告刘某涉嫌职务侵占罪、侵犯商业秘密罪，经我方多次与公安部门沟通，现公安机关已对刘某立案侦查，并成功追回钱款。
                        </p>
                    </div>
                </div>

                <div class="content">
                    <p class="title">案例三：公司股东长达9年怠于清算，被追究清偿公司债务</p>
                    <div style="line-height: 26px; color: #999;">
                        <p style="text-indent: 2em">福建某A机械有限公司（以下简称“A公司”）对济南某B设备有限公司（以下简称“B公司”）享有一笔债权，A公司多次向B公司催讨但未果，并且B公司正在进行清算、注销。我方接受委托后，通过调查发现B公司在吊销营业执照后，公司股东未在规定的期限内组织清算导致会计账簿等清算资料部分遗失，于是我方遂以B公司股东为被告提起诉讼，通过我方积极与法院沟通，最终，法院支持了我方的诉请，并成功执行了四位股东的财产，为委托人追回数百万元。
                        </p>
                    </div>
                </div>

                <div class="content">
                    <p class="title">案例四：公司注销后，追加股东清偿公司债务</p>
                    <div style="line-height: 26px; color:  #999;">
                        <p style="text-indent: 2em">世界五百强某A公司（以下简称“A公司”）对赣州某B机械有限公司（以下简称“B公司”）享有一笔债权，A公司多次向B公司催讨未果后将本案委托至我方。我方接受委托后发现B公司已被公司股东注销，经过调查后发现B公司股东在未通知A公司的情况下召开股东会，将B公司注销，注销程序属违法。随后，我方以赖某、董某为被告提起诉讼，要求其承担清偿货款的责任，通过我方积极与法院沟通，最终，法院支持了我方的诉请，并顺利结案。                    </div>
                </p>
                </div>

                <div class="content">
                    <p class="title">案例六：协调上级法院提级执行，高效执行了近3亿元的疑难执行案</p>
                    <div style="line-height: 26px; color: #999;">
                        <p style="text-indent: 2em">江西某A置业有限公司名下一块土地涉及到两宗纠纷，一宗纠纷在青云谱区法院执行，一宗纠纷在南昌市中级人民法院执行，由于涉及到两个不同法院的执行案件使得涉案土地迟迟不能评估、拍卖。我方接受委托后，经过分析决定向江西省高级人民法院申请将两宗纠纷提级执行，通过我方积极与法院沟通，江西省高级人民法院裁定提级执行，最终，该执行案件顺利解决，执行金额达数亿元。</p>
                    </div>
                </div>

                <div class="content">
                    <p class="title">案例七：利用债务重组，成功处理了某银行1.4亿执行案件</p>
                    <div style="line-height: 26px; color: #999;">
                        <p style="text-indent: 2em">某世界著名制造商集团下属某机械有限公司（以下简称“A公司”）诉昆山某B公司（以下简称“B公司”）买卖合同纠纷执行案，经过我方深度调查发现B公司与股东朱某之间存在财产混同的现象，遂多方收集股东朱某代B公司向第三方支付费用的证据，并就成功就一人公司股东其配偶卢某追加为卢某为被告，经法院判决B公司、朱某、卢某共同承担清偿责任，并顺利执行到位。
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


</div>


</div>



<!-- footer部分 -->
<div class="footer">
    <div class="layui-container">

        <div class="layui-row footer-contact">
            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
                <p class="contact-bottom" style="padding-left: 10px;">官方微信</p>

            </div>

            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
                <p class="contact-bottom" style="padding-left: 30px;">帅凯</p>
            </div>
            <div class="layui-col-sm4 layui-col-lg4">
                <ul class="site-dir layui-layer-wrap">
                    <li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2018，让债全网团队真正加速成长</a></li>
                </ul>
            </div>


            <div class="layui-col-sm4 layui-col-lg4">
                <p class="contact-top"><span class="right">© 中安德信科技有限公司</span></p>
                <p class="contact-bottom"><span class="right"> 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
                <p class="contact-bottom"><span class="right"> 客服电话： 400-005-8810</span></p>
                <p class="contact-bottom"><span class="right"> 官网邮箱： service@andsense.cn</span></p>

            </div>

        </div>
    </div>
</div>
<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    layui.config({
        base: '${ctxStatic}/res/static/layui-inc/js/'
        ,version: '1535185020009'
    }).use('firm');





</script>


<script type="text/html" id="deptTypeTpl">

<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b8fd883578db629b5f88e5d9512ba18b";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();


</script>

<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>