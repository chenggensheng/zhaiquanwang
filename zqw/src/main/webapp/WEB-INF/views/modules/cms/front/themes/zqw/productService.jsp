<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>北京中安德信科技有限公司</title>
    <meta name="keywords" content="债全网,北京中安德信科技有限公司">
    <meta name="description" content="北京中安德信科技有限公司。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
    <script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>


    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bootstrap.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/animate.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/ysemm.css">--%>

    <style type="text/css">
        .icon {
            display: inline-block;
            background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
            background-repeat:no-repeat;
        }

        .icon-more{
            width:8px;
            height:7px;
            background-position: 0 -5px;
            vertical-align:  middle;
            margin-left:  3px;
            margin-top: -2px;
        }
    </style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
    <div class="layui-container">
        <!-- 公司logo -->
        <%--<div class="nav-logo">--%>
            <%--<a href="https://www.layui-inc.com/">--%>
                <%--<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" alt="北京中安德限公司" class="logo">--%>
            <%--</a>--%>
        <%--</div>--%>
        <div class="">
            <a href="https://www.zhaiquanwang.com/" >
                <img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
            </a>
        </div>
        <div class="nav-list">
            <button>
                <span></span><span></span><span></span>
            </button>
            <ul class="layui-nav" lay-filter="">

                <li class="layui-nav-item "><a href="${ctx}/index-1${fns:getUrlSuffix()}"><span>${site.id eq '1'?'首页':'返回主站'}</span></a></li>
                <c:forEach items="${fnc:getMainNavList(site.id)}" var="categoryTemp" varStatus="status">
                    <li class=" layui-nav-item <c:if test="${categoryTemp.id eq category.id}"> layui-this </c:if> "><a href="${categoryTemp.url}" target="${categoryTemp.target}">${categoryTemp.name}</a></li>
                </c:forEach>

                <span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
        </div>
    </div>
</div>

<!-- banner部分 -->
<div class="banner product">
    <div class="title active">
        <p>产品展示</p>
        <p class="en">Product Display</p>
    </div>
</div>


<div class="main product">
    <div class="layui-container">

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure11.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">知识产权</p>
                <p class="detail">针对商标假冒、专利侵权、版权盗版等侵害知识产权的行为，提供一包到底的法律服务，服务内容包括但不限于协商解决、民事诉讼、刑事追究、行政查处等；服务覆盖全国2800多个县级行政区域，打击侵权不花钱、专业律师快准狠、覆盖全国无死角、一包到底债全网。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                    <img src="${ctxStatic}/res/zqw/figure04.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">异地协查</p>
                <p class="detail">调查全国各地客户、经销商、合作伙伴及债务人等的资信情况，协查内容：已判决案卷档案资料、工商注册登记信息、股东/法定代表人/高管等身份信息、本人及家庭户籍信息、婚姻登记信息、不动产登记信息、车辆登记信息、知识产权、商标注册信息等，服务区域覆盖全国、收费合理、内容规范、高效快捷。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                    <img src="${ctxStatic}/res/zqw/figure02.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">非诉催收</p>
                <p class="detail">接受客户的委托，利用我们特有的数据排查渠道，结合落地的催收资源，针对不同类别的债权（应收账款）问题，运用合法的非诉讼类的催收手段和方式，为企业及时实现资金回笼，降低企业资金的坏账率，从而有效维护企业财务现金流。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure03.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">诉讼代理</p>
                <p class="detail">接受客户的委托，代为向债务人、担保人、第三人等进行诉讼/仲裁的行为，可根据客户需求及时采取诉前财产保全、诉讼财产保全等措施，属于通过诉讼/仲裁的司法手段，达成调解或者对债权的确权，避免因诉讼时效届满或证据丢失等导致债权损失的情况。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure07.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">执行代理</p>
                <p class="detail">接受客户的委托，将已经生效的判决、调解或仲裁向法院申请执行或申请恢复执行，并利用特有的数据排查渠道、专业法律技术、异地协查途径等手段，加速案件的执行终结，以实现债权。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure06.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">债权托管</p>
                <p class="detail">在穷尽所有现有手段，尚未挖掘到债务人的财产线索、并无法实现债权的情况下，经客户的委托授权，我们将案件转为托管形式，采取定期或不定期的监控手段，一旦发现有价值的财产线索或者有收购意向，我们即刻开展相应的债权处置服务。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure09.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">债权报告</p>
                <p class="detail">作为债权解决专家，我们针对存在各类问题的债权进行深度评估，评估范围：基本案情、涉及的法律问题存在的风险及不确定性、焦点与难点、债务人履行能力评估、催收处置方案建议、实现可能性评估、合理化建议等。</p>
            </div>
        </div>

        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                    <img src="${ctxStatic}/res/zqw/figure08.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">专项服务</p>
                <p class="detail">根据客户的债务处置需要，提供专项的尽职调查、债务重组、债权评估、应收账款风险评估、对渠道代理商的资信评估等的专项服务。</p>
            </div>
        </div>


        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure01.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">法律顾问</p>
                <p class="detail">针对企事业单位对债权（应收账款）的管理需求，提供应收账款管理、体系建设、岗位培训、风控意识养成、日常事务等的培训或咨询。</p>
            </div>
        </div>


        <div class="content layui-row">
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img">
                <img src="${ctxStatic}/res/zqw/figure10.png">
            </div>
            <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
                <p class="label">海外服务</p>
                <p class="detail">服务网络遍布全球 216 个国家和地区，可通过全球征信网络实现信息互换，向欠款客户施以联
                    合惩戒、增强威慑。</p>
            </div>
        </div>



        <!--
        <div class="content layui-row">
          <div class="layui-col-xs12 layui-col-sm6 layui-col-md7 layui-col-lg6 content-img"><img src="//res.layui.com/static/layui-inc/img/Product_img4.jpg"></div>
          <div class="layui-col-xs12 layui-col-sm6 layui-col-md5 layui-col-lg6 right">
            <p class="label">神秘新品</p>
            <p class="detail">我们是来自于一款名为 layer 的弹窗，尽管它曾那样微不足道，却因坚持光耀至今</p>
            <div><a>待揭晓 ></a></div>
          </div>
        </div>
        -->
    </div>
</div>






<!-- footer部分 -->
<div class="footer">
    <div class="layui-container">

        <div class="layui-row footer-contact">
            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
                <p class="contact-bottom" style="padding-left: 10px;">官方微信</p>

            </div>

            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
                <p class="contact-bottom" style="padding-left: 30px;">帅凯</p>
            </div>

            <div class="layui-col-sm4 layui-col-lg4">
                <ul class="site-dir layui-layer-wrap">
                    <li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2018，让债全网团队真正加速成长</a></li>
                </ul>
            </div>


            <div class="layui-col-sm4 layui-col-lg4">
                <p class="contact-top"><span class="right">© 中安德信科技有限公司</span></p>
                <p class="contact-bottom"><span class="right"> 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
                <p class="contact-bottom"><span class="right"> 客服电话： 400-005-8810</span></p>
                <p class="contact-bottom"><span class="right"> 官网邮箱： service@andsense.cn</span></p>

            </div>

        </div>
    </div>
</div>
<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    layui.config({
        base: '${ctxStatic}/res/static/layui-inc/js/'
        ,version: '1535185020009'
    }).use('firm');





</script>



<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b8fd883578db629b5f88e5d9512ba18b";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();


</script>


<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>