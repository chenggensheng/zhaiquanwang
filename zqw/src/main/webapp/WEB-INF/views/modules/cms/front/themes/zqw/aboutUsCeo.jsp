<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>北京中安德信科技有限公司</title>
    <meta name="keywords" content="债全网,北京中安德信科技有限公司">
    <meta name="description" content="北京中安德信科技有限公司。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
    <script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>


    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bootstrap.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/animate.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/ysemm.css">--%>

    <style type="text/css">
        .icon {
            display: inline-block;
            background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
            background-repeat:no-repeat;
        }

        .icon-more{
            width:8px;
            height:7px;
            background-position: 0 -5px;
            vertical-align:  middle;
            margin-left:  3px;
            margin-top: -2px;
        }
    </style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
    <div class="layui-container">
        <!-- 公司logo -->
        <%--<div class="nav-logo">--%>
        <%--<a href="https://www.layui-inc.com/">--%>
        <%--<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" alt="北京中安德限公司" class="logo">--%>
        <%--</a>--%>
        <%--</div>--%>
        <div class="">
            <a href="https://www.zhaiquanwang.com/" >
                <img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
            </a>
        </div>
        <div class="nav-list">
            <button>
                <span></span><span></span><span></span>
            </button>
            <ul class="layui-nav" lay-filter="">

                <li class="layui-nav-item "><a href="${ctx}/index-1${fns:getUrlSuffix()}"><span>首页</span></a></li>
                <c:forEach items="${fnc:getMainNavList(site.id)}" var="categoryTemp" varStatus="status">
                    <li class=" layui-nav-item <c:if test="${categoryTemp.id eq category.id}"> layui-this </c:if> "><a href="${categoryTemp.url}" target="${categoryTemp.target}">${categoryTemp.name}</a></li>
                </c:forEach>

                <span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
        </div>
    </div>
</div>

<div class="banner product">
    <div class="title active">
        <p>关于我们</p>
        <p class="en">About Us</p>
    </div>
</div>



<div class="main-about">
    <div class="layui-container">
        <div class="layui-row">
            <div class="tabJob layui-show">
                <div class="content">
                    <p class="title" style="text-align: center;">痴心追债20年（CEO姜明亮介绍）</p>
                    <div style="line-height: 26px; color: #545454;">
                        <p style="text-indent: 2em">
                            北京2015-11-03(商业电讯)－－ 都四十多岁的人啦，还要创业，你累不累？ 做了十多年的律师，每年收入稳定且生活有滋有味，为何却要带领团队这么折腾自己呢？ 今天我们带大家一起走近债全网CEO姜明亮，听他讲述与债权结缘的二十年。
                        </p>

                        <div align="center">
                        <img style="margin:4px;border:1px solid #000; " width="500"  src="${ctxStatic}/res/zqw/180319324.jpg" width="500" border="1" height="499" alt="讲述：债全网CEO姜明亮痴心追债20年">
                    </div>
                        <p style="text-indent: 2em"><strong>第一份属于自己的工作</strong></p>
                        <p style="text-indent: 2em">&nbsp;</p>
                        <p style="text-indent: 2em">记得是1994年初冬，我刚从部队回地方，干的第一份属于自己的工作，就是“讨债服务”。在江西南昌老福山立交桥附近的电线杆、引桥立柱上，张贴“讨债服务”内容的纸条广告，招引社会上的债权人来洽谈委托。几天后江西工人报以南昌有了一个新行业“讨债公司”给予了报道，当时记得报道后我还请这位记者吃了顿饭，二十一年过去了，一直很想找到那份报纸，找到那位尊敬的记者。</p>

                        <p style="text-indent: 2em">当时借的是江西经纪人事务所的牌子，和另一个人共用一部电话、一个玻璃屋子，追债用的是民间上门催讨方式，有时偶尔也会借助公安的介入，那时候的公安权力大得很。记得有次几位新疆人顺着墙上的广告找过来，是南昌当地的人用“玩炸”的方式骗了她们的饲料原料，希望我们帮助追讨货款。大概是我承揽业务心切，当晚这几位新疆人承诺办理手续后，突然退房不知去向，以为遇上了黑社会。那个年代的环境，人与人之间的信任程度低到无法形容的地步。</p>
                        <p style="text-indent: 2em">这第一份真正属于自己的工作持续了不到半年时间，却在内心里埋下了伏笔，我还会回来的。</p>

                        <div align="center">
                            <img style="margin:4px;border:1px solid #000; " width="500"  src="${ctxStatic}/res/zqw/180319325.jpg" width="500" border="1" height="359" alt="讲述：债全网CEO姜明亮痴心追债20年">
                        </div>

                        <p style="text-indent: 2em"><strong>十年的商场拼搏历程</strong></p>
                        <p style="text-indent: 2em">&nbsp;</p>
                        <p style="text-indent: 2em">年轻的时候，谁不好高骛远？我也一样，再加上拿的是军校毕业证，没专业背景、没从业经验、没高人指点，接下来的十年俨然如过山车一般起伏跌宕，换工作如换衣裳，辗转东莞、深圳、上海、南昌几个地方，从一般职员到部门负责人，从高管到自己创业组建公司，涉及行业不下十余种：装饰工程、化工、信息服务、软件销售、印刷、医药、机械设备、食品批发、服装销售、房地产等，不是耐不住寂寞，就是熬不住那种商场的虚实真假，还有最让人静不下心来的就是那句“不想当将军的士兵不是好士兵”的那句昏话，时刻提醒自己啥时候能当上商场上的将军啊？
                        </p><p style="text-indent: 2em">终于随着我一位老朋友香港陈先生的一次投资失败后，痛定思痛找回自己的人生领悟，我还是需要凭借自己最为真实的一面，发挥自信、担当、内敛与钻研的特长，开启新的人生历程。那一年我34岁。
                       </p>
                        <p style="text-indent: 2em">&nbsp;</p>
                        <p style="text-indent: 2em"><strong>做律师，终于找到了人生感觉</strong></p>
                        <p style="text-indent: 2em">&nbsp;</p>
                        <p style="text-indent: 2em">2003年5月2日，我非常清楚这一天，因为是我翻开司法考试教材第一页的日子，也仿佛是自己重走人生第一步的日子。
                        </p>
                        <p style="text-indent: 2em">第二年考试顺利通过，成为了一名执业律师。</p>
                        <p style="text-indent: 2em">律师的职业，独立自主，很有做将军的感觉，久违的自信又再次回到了脸上。由于有非常丰富的商场经营管理经验，以及波澜起伏、殷实充分的人生阅历，自己快速找到了专业定位，专业从事公司法务领域，为公司企业提供法律顾问、风险防控、股东纠纷、资产并购、诉讼代理执行等服务。
                        </p><p style="text-indent: 2em">毫不夸张地说，只要我愿意，我每天都可以让不同的企业主请吃饭，因为我能明白他们的内心世界，知道他们的难和痛，同时能由浅到深地提供解决方案，所以他们很粘我。
                    </p><p style="text-indent: 2em">可是很快天花板又来了，你一个人能有多少精力，能做多少案子？终于又将自己陷入了“不想当将军的士兵不是好士兵”的思维定式中。一个人的将军，不是真正的将军，将军必须有自己的部队，必须能完成战略部署与战术动作，实现宏伟目标。
                    </p>


                        <div align="center">
                            <img style="margin:4px;border:1px solid #000; " width="500"  src="${ctxStatic}/res/zqw/180319326.jpg" width="500" border="1" height="329" alt="讲述：债全网CEO姜明亮痴心追债20年">
                        </div>

                        <p style="text-indent: 2em"><strong>追债业务，我回来了！</strong></p>
                        <p style="text-indent: 2em">&nbsp;</p>
                        <p style="text-indent: 2em">2011年6月，结束个体户式律师的执业生涯，登记注册了属于自己的律师事务所，组建了自己的律师团队，2012年4月我们非常风光地在滨江宾馆举行了开业仪式，市司法局的领导说这是江西有史以来最为隆重的一次律所开业典礼。
                        </p>
                        <p style="text-indent: 2em">律所团队组建伊始，就设立了“一个中心、四个坚持”的基本原则，以团队为中心、坚持公司化管理、坚持走专业化道路、坚持人才培养、坚持品牌战略。这种布局与模式在当时的江西应该是仅此一家，全国也不多见。
                        </p>
                        <p style="text-indent: 2em">我们很快进入了全国工程机械债权管理领域，为各大制造商、融资租赁公司、代理商提供债权管理咨询、培训、诉讼代理、债权催收处置等全流程系列的专业服务，几年的陪伴服务我们在行业内已小有名气，刊印了我们原创的全国首本《债权管理实务指导手册》，我们俨然成为国内最懂债权的律师团队。
                        </p>
                        <p style="text-indent: 2em">在债权问题最为典型、最为严重的工程机械行业里，我们看到了日益增加的痛点：债权逾期严重、沉淀债权无人问津、债权流转困难等等。据权威估计，目前国内沉淀的法定债权规模至少在5万亿以上，自然债权（应收账款）规模超百万亿，这么庞大体量的市场，过去由于社会法治环境、国家征信体系、司法技术、传统律师代理模式等的限制，一直无法得以解决。现在的机遇来了，在全民创业、万众创新的政策促进下，利用互联网的思维与技术，我们重新开启了追债的模式。于是，债全网作为一种全新的债权综合解决方案，彻底颠覆传统、深度体现互联网+的模式，终于横空出世，为全面、彻底解决执行难、债权流转问题提供了一个可期待的美好未来。
                        </p>
                        <p style="text-indent: 2em">从小，我就爱折腾自己，从来没有让自己闲着，在工厂长大的我，儿时就想当一厂之长，如今虽然没有实现，但心中的将军梦一直都在。我希望自己在国内债权领域，能始终追逐成为将军的梦想。
                        </p>
                        <p style="text-indent: 2em">不忘初心，方得始终！</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>


</div>



<!-- footer部分 -->
<div class="footer">
    <div class="layui-container">

        <div class="layui-row footer-contact">
            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
                <p class="contact-bottom" style="padding-left: 10px;">官方微信</p>

            </div>

            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
                <p class="contact-bottom" style="padding-left: 30px; color: white">帅凯</p>
            </div>
            <div class="layui-col-sm4 layui-col-lg4">
                <ul class="site-dir layui-layer-wrap">
                    <li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2018，让债全网团队真正加速成长</a></li>
                </ul>
            </div>


            <div class="layui-col-sm4 layui-col-lg4">
                <p class="contact-top"><span class="right">© 中安德信科技有限公司</span></p>
                <p class="contact-bottom"><span class="right"> 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
                <p class="contact-bottom"><span class="right"> 客服电话： 400-005-8810</span></p>
                <p class="contact-bottom"><span class="right"> 官网邮箱： service@andsense.cn</span></p>

            </div>

        </div>
    </div>
</div>
<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    layui.config({
        base: '${ctxStatic}/res/static/layui-inc/js/'
        ,version: '1535185020009'
    }).use('firm');





</script>


<script type="text/html" id="deptTypeTpl">

    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?b8fd883578db629b5f88e5d9512ba18b";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();


</script>

<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>