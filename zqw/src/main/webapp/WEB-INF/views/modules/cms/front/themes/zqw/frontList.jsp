
<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>北京中安德信科技有限公司</title>
	<meta name="keywords" content="债全网,北京中安德信科技有限公司">
	<meta name="description" content="北京中安德信科技有限公司">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
	<link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/global1.css" media="all">

	<link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
	<link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
	<script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>
	<script src="${ctxStatic}/res/layer/layer.min.js"></script>


	<style type="text/css">
		.icon {
			display: inline-block;
			background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
			background-repeat:no-repeat;
		}

		.icon-more{
			width:8px;
			height:7px;
			background-position: 0 -5px;
			vertical-align:  middle;
			margin-left:  3px;
			margin-top: -2px;
		}

		ol,li{
			padding: 0;
			margin: 0;
		}
		li.active{
			width: 210px;
			height: 40px;
			list-style-type: none;
			line-height: 40px;
			text-align: center;
			font-size: 16px;
			background-color: #dddddd;
			margin-bottom: 1px;
			color: #fff;
			cursor: pointer;
			list-style-position: inside;
		}



		.re_survey1 ul li{
			list-style-type: none;
			height:50px;
			line-height: 50px;
			border-style: solid;
			border-width: 1px;
			border-color: #fff;
			border-top-color: #e6e5e6;
			text-indent: 0;
		}

		.pagination ul>.willavtive>a:link,
		.pagination ul>.willavtive>a:visited{
			color: black;
			cursor: default;
			background-color: transparent;
		}

		li.activeed{
			width: 210px;
			height: 40px;
			list-style-type: none;
			line-height: 40px;
			text-align: center;
			font-size: 16px;
			background-color: #b3292c;
			margin-bottom: 1px;
			color: #fff;
			cursor: pointer;
			list-style-position: inside;
		}

		.ulClass ol>.activeed>a:visited{
			color: #fff !important;
		}
		.ulClass ol>.activeed>a:link{
			color: #fff !important;
		}

		.pagination ul{
			box-shadow: 0 0px 0px rgba(0,0,0,0);
		}

	</style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
	<div class="layui-container">
		<!-- 公司logo -->
		<div class="">
			<a href="https://www.layui-inc.com/" >
				<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
			</a>
		</div>
		<div class="nav-list">

			<ul class="layui-nav" lay-filter="">
				<li class="layui-nav-item layui-this"><a href="${ctx}/index-1${fns:getUrlSuffix()}">首页</a></li>
				<c:forEach items="${fnc:getMainNavList(site.id)}" var="category" varStatus="status">
					<li class="layui-nav-item "><a href="${category.url}" target="${category.target}">${category.name}</a></li>
				</c:forEach>

				<span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
		</div>
	</div>
</div>


<div class="layui-container" style="margin-top: 30px; ">
	<div class="layui-row layui-col-space15">
		<div class="layui-col-md12">

			<div class="fly-panel" style="height: 508px;">
				<div class="fly-panel-title fly-filter" style="padding: 0px;">
					<p class="title"><span style="color: #ea5404;font-size: 24px">维权打假</span>
					</p>
				</div>
				<ul class="fly-list">
					<c:forEach items="${page.list}" var="article">
						<li>
							<h2>
								<a class="layui-badge">维权</a>
									<%--<a href="" style="color:${article.color}">${fns:abbr(article.title,28)}</a>--%>

								<a href="${article.url}" style="color:${article.color}">${fns:abbr(article.title,28)}</a>
							</h2>
							<div class="fly-list-info">
								<span class="fly-list-nums"><fmt:formatDate value="${article.updateDate}" pattern="yyyy.MM.dd"/>
                               <%--<i class="iconfont icon-pinglun1" title="已阅"></i> ${article.hits}--%>
                             </span>
							</div>
							<div class="fly-list-badge">
							</div>
						</li>
					</c:forEach>
				</ul>


			</div>

			<div class="pagination" >${page}</div>
			<script type="text/javascript">
                function page(n,s){
                    location="${ctx}/list-${category.id}${urlSuffix}?pageNo="+n+"&pageSize="+s;
                }
			</script>

		</div>

	</div>



</div>










	<!-- footer部分 -->
	<div class="footer">
		<div class="layui-container">

			<div class="layui-row footer-contact">
				<div class="layui-col-sm2 layui-col-lg2">
					<img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
					<p class="contact-bottom" style="padding-left: 10px;">官方微信</p>

				</div>

				<div class="layui-col-sm2 layui-col-lg2">
					<img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
					<p class="contact-bottom" style="padding-left: 30px;">帅凯</p>
				</div>

				<div class="layui-col-sm4 layui-col-lg4">
					<ul class="site-dir layui-layer-wrap">
						<li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
						<li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
						<li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
						<li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
						<li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2018，让债全网团队真正加速成长</a></li>
					</ul>
				</div>


				<div class="layui-col-sm4 layui-col-lg4">
					<p class="contact-top"><span class="right">© 中安德信科技有限公司</span></p>
					<p class="contact-bottom"><span class="right"> 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
					<p class="contact-bottom"><span class="right"> 客服电话： 400-005-8810</span></p>
					<p class="contact-bottom"><span class="right"> 官网邮箱： service@andsense.cn</span></p>

				</div>

			</div>
		</div>
	</div>
	<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
	<!--[if lt IE 9]>
	<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
	<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->





	<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>



