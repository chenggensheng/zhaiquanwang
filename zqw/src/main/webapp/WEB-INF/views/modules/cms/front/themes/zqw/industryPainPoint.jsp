<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>北京中安德信科技有限公司</title>
    <meta name="keywords" content="债全网,北京中安德信科技有限公司">
    <meta name="description" content="北京中安德信科技有限公司。">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
    <link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
    <script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>


    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bootstrap.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/animate.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/ysemm.css">--%>

    <style type="text/css">
        .icon {
            display: inline-block;
            background-image: url("${ctxStatic}/res/layui/dist/font/icons_001.png");
            background-repeat:no-repeat;
        }

        .icon-more{
            width:8px;
            height:7px;
            background-position: 0 -5px;
            vertical-align:  middle;
            margin-left:  3px;
            margin-top: -2px;
        }
    </style>
</head>
<body>
<!-- nav部分 -->
<div class="nav index">
    <div class="layui-container">
        <!-- 公司logo -->
        <%--<div class="nav-logo">--%>
            <%--<a href="https://www.layui-inc.com/">--%>
                <%--<img src="${ctxStatic}/res/static/layui-inc/img/logo.png" alt="北京中安德限公司" class="logo">--%>
            <%--</a>--%>
        <%--</div>--%>
        <div class="">
            <a href="https://www.zhaiquanwang.com/" >
                <img src="${ctxStatic}/res/static/layui-inc/img/logo.png" style="padding: 7px;height: 45px;width:117px; margin-top: 12px;"  alt="北京中安德限公司" class="logo">
            </a>
        </div>
        <div class="nav-list">
            <button>
                <span></span><span></span><span></span>
            </button>
            <ul class="layui-nav" lay-filter="">

                <li class="layui-nav-item "><a href="${ctx}/index-1${fns:getUrlSuffix()}"><span>${site.id eq '1'?'首页':'返回主站'}</span></a></li>
                <c:forEach items="${fnc:getMainNavList(site.id)}" var="categoryTemp" varStatus="status">
                    <li class=" layui-nav-item <c:if test="${categoryTemp.id eq category.id}"> layui-this </c:if> "><a href="${categoryTemp.url}" target="${categoryTemp.target}">${categoryTemp.name}</a></li>
                </c:forEach>

                <span class="layui-nav-bar" style="left: 48px; top: 78px; width: 0px; opacity: 0;"></span></ul>
        </div>
    </div>
</div>

<!-- banner部分 -->
<div class="banner industryPainPoint">
    <div class="title active">
        <p>行业痛点</p>
       <p class="en">Industry Pain Point</p>
    </div>
</div>



<div class="main-about">
    <div class="layui-container">
        <div class="layui-row">
            <div class="tabIntro">

                <div class="content">
                    <div class="layui-inline img"><img src="${ctxStatic}/res/zqw/11.png"></div><div class="layui-inline panel">
                    <p>希望找到真正专业、快捷、安全、高效的债权催收处置机构</p>
                </div>


                </div>



                <%--<div class="content">--%>
                    <%--<div class="layui-inline img"><img src="${ctxStatic}/res/zqw/11.png"></div>--%>
                    <%--<div class="layui-inline panel">--%>
                    <%--<p>希望找到真正专业、快捷、安全、高效的债权催收处置机构</p>--%>
                <%--</div>--%>
                <%--</div>--%>
                <div class="content">
                    <div class="layui-inline panel p_block">
                        <p>只需要找到一家机构，即可覆盖全国各地区域，无需繁琐、复杂地进行多地对接</p>
                    </div><div class="layui-inline img"><img src="${ctxStatic}/res/zqw/dq.png"></div>
                    <p class="p_hidden"></p>
                </div>
                <div class="content">
                    <div class="layui-inline img"><img src="${ctxStatic}/res/zqw/dw.jpeg"></div><div class="layui-inline panel">
                    <p>能否同时处理批量案件，案件金额不等、区域不同、类型不同，数量不限</p>
                </div>
                    <div class="content">
                        <div class="layui-inline panel p_block">
                            <p>能否全天候24小时随时响应，确保一对一地有效沟通，提供管家级的企业服务</p>
                        </div><div class="layui-inline img"><img src="${ctxStatic}/res/zqw/qt.jpeg"></div>
                        <p class="p_hidden"></p>
                    </div>
                    <div class="content">
                        <div class="layui-inline img"><img src="${ctxStatic}/res/zqw/zq.jpeg"></div><div class="layui-inline panel">
                        <p>针对各类不同问题的债权（应收账款），有多样性的催收处置手段，包括但不限于：非诉调解、协商、诉讼、执行、债权转让、债权抵偿、债权融资等</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- main部分 -->
<%--<div class="main-about">--%>
    <%--<div class="layui-container">--%>
        <%--<div class="layui-row">--%>
            <%--<div class="tabJob layui-show">--%>
                <%--<div class="content">--%>
                    <%--<p class="title">希望找到真正专业、快捷、安全、高效的债权催收处置机构</p>--%>
                <%--</div>--%>


                <%--<div class="content">--%>
                    <%--<p class="title">只需要找到一家机构，即可覆盖全国各地区域，无需繁琐、复杂地进行多地对接</p>--%>
                <%--</div>--%>

                <%--<div class="content">--%>
                    <%--<p class="title">能否同时处理批量案件，案件金额不等、区域不同、类型不同，数量不限</p>--%>
                <%--</div>--%>

                <%--<div class="content">--%>
                    <%--<p class="title">能否全天候24小时随时响应，确保一对一地有效沟通，提供管家级的企业服务</p>--%>

                <%--</div>--%>

                <%--<div class="content">--%>
                    <%--<p class="title">针对各类不同问题的债权（应收账款），有多样性的催收处置手段，包括但不限于：非诉调解、协商、诉讼、执行、债权转让、债权抵偿、债权融资等</p>--%>
                <%--</div>--%>

            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>



<!-- footer部分 -->
<div class="footer">
    <div class="layui-container">

        <div class="layui-row footer-contact">
            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/static/layui-inc/img/zqw.jpg">
                <p class="contact-bottom" style="padding-left: 10px;">官方微信</p>

            </div>

            <div class="layui-col-sm2 layui-col-lg2">
                <img style="width:86px; height: 86px;" src="${ctxStatic}/res/zqw/lk.jpeg">
                <p class="contact-bottom" style="padding-left: 30px;">帅凯</p>
            </div>

            <div class="layui-col-sm4 layui-col-lg4">
                <ul class="site-dir layui-layer-wrap">
                    <li><a href="${ctx}/aboutUsCeo">痴心追债20年（CEO姜明亮介绍）</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/-a1VV6Y1NDR_NCvV3FRXBA">国家破解执行难：一人欠债，全家诛连</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/Y897ceQcE2DTwQJB3aBlVA">2018年最全！国家限制老赖措施超强汇总！</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/VyD5o-wE0whKp5xvUGJ6jg">失信被执行人没车没房没存款咋办？</a></li>
                    <li><a target="_blank" href="https://mp.weixin.qq.com/s/WI9-B_61b900evFrJbKiJw">2018，让债全网团队真正加速成长</a></li>
                </ul>
            </div>

            <div class="layui-col-sm4 layui-col-lg4">
                <p class="contact-top"><span class="right">© 中安德信科技有限公司</span></p>
                <p class="contact-bottom"><span class="right"> 2018&nbsp;&nbsp; <a href="http://www.zhaiquanwang.com/" target="_blank" rel="nofollow">京ICP备14050766号-2</a></span></p>
                <p class="contact-bottom"><span class="right"> 客服电话： 400-005-8810</span></p>
                <p class="contact-bottom"><span class="right"> 官网邮箱： service@andsense.cn</span></p>

            </div>

        </div>
    </div>
</div>
<script src="${ctxStatic}/res/layui/dist/layui.js" charset="utf-8"></script>
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    layui.config({
        base: '${ctxStatic}/res/static/layui-inc/js/'
        ,version: '1535185020009'
    }).use('firm');





</script>


<script type="text/html" id="deptTypeTpl">
    {{#  if(d.deptType === '0'){ }}
     自然
    {{#  } else { }}
    法定
    {{#  } }}
</script>

<script type="text/html" id="debtTypeTpl">
    {{#  if(d.debtType === '0'){ }}
    自然
    {{#  } else { }}
    法定
    {{#  } }}
</script>

<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b8fd883578db629b5f88e5d9512ba18b";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();


</script>

<script type="text/javascript" src="${ctxStatic}/res/baidu/api"></script>
<script type="text/javascript" src="http://api.map.baidu.com/getscript?v=2.0&ak=Kpjp7jddqVUhWK5VkrfNt3YNezY88NtR&services=&t=20170517145936"></script>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/res/baidu/bmap.css">
<script type="text/javascript" src="${ctxStatic}/res/baidu/baiduMap.js"></script>
<script type="text/javascript">

    (function($) {
        $(function(){
//           alert(1234567890);
            baiduMap('map','116.322056','39.988878','北京中安德信科技有限公司','淀区中关村大街1号海龙大厦H座11层');
        });
    })(jQuery);
    // 百度地图API功能



</script>

<ul class="layui-fixbar"><li class="layui-icon layui-fixbar-top" lay-type="top" style="display: none;"></li></ul></body></html>