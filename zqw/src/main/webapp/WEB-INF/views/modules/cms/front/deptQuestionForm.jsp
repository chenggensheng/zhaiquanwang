<%--
  Created by IntelliJ IDEA.
  User: codercheng
  Date: 2018/10/16
  Time: 下午9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>

<!DOCTYPE html>
<!-- saved from url=(0026)https://www.layui-inc.com/ -->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>北京中安德信科技有限公司</title>
	<meta name="keywords" content="债全网,北京中安德信科技有限公司">
	<meta name="description" content="北京中安德信科技有限公司。">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="${ctxStatic}/res/layui/dist/css/layui.css" media="all">
	<link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/index.css" media="all">
	<link rel="stylesheet" href="${ctxStatic}/res/static/layui-inc/css/style.css" media="all">
	<script type="text/javascript" src="${ctxStatic}/res/static/layui-inc/js/jquery.min.js"></script>
</head>
<body>

	<form:form id="inputForm" modelAttribute="deptQuestion" action="${ctx}/deptQuestion/save" method="post" class="layui-form">
		<form:hidden path="deptType" />
		<div class="layui-form-item" style="margin-top: 15px;">
			<label class="layui-form-label">姓名：</label>
			<div class="layui-input-block" style="width: 80%">

				<form:input path="name" htmlEscape="false"   lay-verify="title" autocomplete="off" maxlength="255"   class="layui-input" placeholder="请输姓名" />

			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">电话：</label>
			<div class="layui-input-block " style="width: 80%">

				<form:input path="phone" htmlEscape="false"   lay-verify="title" autocomplete="off" maxlength="255"   class="layui-input" placeholder="请输电话"  />

			</div>
		</div>


		<div class="layui-form-item">
			<label class="layui-form-label">问题描述：</label>
			<div class="layui-input-block" style="width: 80%">
				<form:textarea path="remarks" htmlEscape="false" placeholder="请输描述需要咨询的问题" rows="4" class="layui-textarea"/>
			</div>
		</div>


		<div class="layui-form-item">
			<label class="layui-form-label">律师姓名：</label>
			<div class="layui-input-block" style="width: 80%">
				<input type="layerName" name="layerName" lay-verify="title" autocomplete="off" placeholder="刘律师" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label" >律师电话：</label>
			<div class="layui-input-block" style="width: 80%">
				<input type="layerPhone" name="layerPhone" lay-verify="title" autocomplete="off" placeholder="15011143693" class="layui-input">
			</div>
		</div>




		<div class="layui-form-item" style="text-align: center">
			<c:if test="${deptQuestion.id eq null || deptQuestion.id eq ''}">
			<input id="btnSubmit" class="layui-btn layui-ban-primary" type="submit" value="提交"/>&nbsp;
			</c:if>
			<c:if test="${deptQuestion.id ne null && deptQuestion.id ne ''}">
				<span style="color:red;">提交成功，刘律师将会联系您！</span>
			</c:if>

		</div>
	</form:form>
</body>
</html>