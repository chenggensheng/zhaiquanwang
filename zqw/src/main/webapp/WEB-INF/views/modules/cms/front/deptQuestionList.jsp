<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>咨询方案管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/cms/front/deptQuestion/">咨询方案列表</a></li>
		<shiro:hasPermission name="cms:front:deptQuestion:edit"><li><a href="${ctx}/cms/front/deptQuestion/form">咨询方案添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="deptQuestion" action="${ctx}/cms/front/deptQuestion/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>咨询人姓名：</label>
				<form:input path="name" htmlEscape="false" maxlength="256" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>咨询人姓名</th>
				<th>咨询类别</th>
				<th>咨询人电话</th>
				<th>问题描述</th>
				<shiro:hasPermission name="cms:front:deptQuestion:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="deptQuestion">
			<tr>
				<td><a href="${ctx}/cms/front/deptQuestion/form?id=${deptQuestion.id}">
					${deptQuestion.name}
				</a></td>
				<td>
					<c:if test="${deptQuestion.deptType eq '1'}">债权催收</c:if>
					<c:if test="${deptQuestion.deptType eq '2'}">维权打假</c:if>
						${deptQuestion.deptType}
				</td>
				<td>
						${deptQuestion.phone}
				</td>
				<td>
					${deptQuestion.remarks}
				</td>
				<shiro:hasPermission name="cms:front:deptQuestion:edit"><td>
    				<a href="${ctx}/cms/front/deptQuestion/form?id=${deptQuestion.id}">修改</a>
					<a href="${ctx}/cms/front/deptQuestion/delete?id=${deptQuestion.id}" onclick="return confirmx('确认要删除该咨询方案吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>