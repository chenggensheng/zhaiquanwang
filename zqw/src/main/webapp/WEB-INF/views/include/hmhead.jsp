<%@ page contentType="text/html;charset=UTF-8" %><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><meta name="author" content="http://jeesite.com/"/>
<meta name="renderer" content="webkit"><meta http-equiv="X-UA-Compatible" content="IE=8,IE=9,IE=10" />
<meta http-equiv="Expires" content="0"><meta http-equiv="Cache-Control" content="no-cache"><meta http-equiv="Cache-Control" content="no-store">

<script src="${ctxStatic}/hAdmin/js/jquery.min.js?v=2.1.4"></script>

<link rel="shortcut icon" href="favicon.ico"> <link href="${ctxStatic}/hAdmin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${ctxStatic}/hAdmin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="${ctxStatic}/hAdmin/css/animate.css" rel="stylesheet">
<link href="${ctxStatic}/hAdmin/css/style.css?v=4.1.0" rel="stylesheet">

<link href="${ctxStatic}/bootstrap/2.3.1/awesome/font-awesome.min.css" type="text/css" rel="stylesheet" />

<!-- 全局js -->
<script src="${ctxStatic}/hAdmin/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${ctxStatic}/hAdmin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="${ctxStatic}/hAdmin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- 自定义js -->
<script src="${ctxStatic}/hAdmin/js/hAdmin.js?v=4.1.0"></script>
<script type="text/javascript" src="${ctxStatic}/hAdmin/js/index.js"></script>
<!-- 第三方插件 -->
<script src="${ctxStatic}/hAdmin/js/plugins/pace/pace.min.js"></script>
<link href="${ctxStatic}/common/case.css" type="text/css" rel="stylesheet" />
<link href="${ctxStatic}/common/jeesite.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/common/jeesite.js" type="text/javascript"></script>

<link href="${ctxStatic}/jquery-select2/3.4/select2.min.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-select2/3.4/select2.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="${ctxStatic}/bootstrap-table-master/src/bootstrap-table.css">
<script src="${ctxStatic}/bootstrap-table-master/src/bootstrap-table.js"></script>
<script src="${ctxStatic}/bootstrap-table-master/src/locale/bootstrap-table-zh-CN.js"></script>
<link rel="stylesheet" href="${ctxStatic}/bootstrap-table-master/src/bootstrap-editable.css">
<script src="${ctxStatic}/bootstrap-table-master/src/bootstrap-editable.js"></script>
<script src="${ctxStatic}/bootstrap-table-master/src/bootstrap-table-editable.js"></script>
<script src="${ctxStatic}/bootstrap-table-master/src/tableExport.js"></script>
<script src="${ctxStatic}/bootstrap-table-master/src/bootstrap-table-export.js"></script>
<script src="${ctxStatic}/hAdmin/js/plugins/layer/layer.min.js"></script>


<script src="${ctxStatic}/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

<script type="text/javascript">var ctx = '${ctx}', ctxStatic='${ctxStatic}';</script>