/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 合作律师Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwCaseLayer extends DataEntity<ZqwCaseLayer> {
	
	private static final long serialVersionUID = 1L;
	private String caseId;		// case_id
	private String layerId;		// layer_id
	
	public ZqwCaseLayer() {
		super();
	}

	public ZqwCaseLayer(String id){
		super(id);
	}

	@Length(min=0, max=255, message="case_id长度必须介于 0 和 255 之间")
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	@Length(min=0, max=255, message="layer_id长度必须介于 0 和 255 之间")
	public String getLayerId() {
		return layerId;
	}

	public void setLayerId(String layerId) {
		this.layerId = layerId;
	}
	
}