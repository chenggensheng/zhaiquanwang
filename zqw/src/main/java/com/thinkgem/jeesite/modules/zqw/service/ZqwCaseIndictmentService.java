/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseIndictment;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCaseIndictmentDao;

/**
 * 民事起诉状Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCaseIndictmentService extends CrudService<ZqwCaseIndictmentDao, ZqwCaseIndictment> {

	public ZqwCaseIndictment get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCaseIndictment> findList(ZqwCaseIndictment zqwCaseIndictment) {
		return super.findList(zqwCaseIndictment);
	}
	
	public Page<ZqwCaseIndictment> findPage(Page<ZqwCaseIndictment> page, ZqwCaseIndictment zqwCaseIndictment) {
		return super.findPage(page, zqwCaseIndictment);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCaseIndictment zqwCaseIndictment) {
		super.save(zqwCaseIndictment);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCaseIndictment zqwCaseIndictment) {
		super.delete(zqwCaseIndictment);
	}
	
}