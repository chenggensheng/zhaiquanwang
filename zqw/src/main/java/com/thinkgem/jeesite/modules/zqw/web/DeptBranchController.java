/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.DeptBranch;
import com.thinkgem.jeesite.modules.zqw.service.DeptBranchService;

/**
 * 曝光台Controller
 * @author coder_cheng@126.com
 * @version 2018-10-22
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/deptBranch")
public class DeptBranchController extends BaseController {

	@Autowired
	private DeptBranchService deptBranchService;
	
	@ModelAttribute
	public DeptBranch get(@RequestParam(required=false) String id) {
		DeptBranch entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = deptBranchService.get(id);
		}
		if (entity == null){
			entity = new DeptBranch();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:deptBranch:view")
	@RequestMapping(value = {"list", ""})
	public String list(DeptBranch deptBranch, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<DeptBranch> page = deptBranchService.findPage(new Page<DeptBranch>(request, response), deptBranch); 
		model.addAttribute("page", page);
		return "modules/zqw/deptBranchList";
	}

	@RequiresPermissions("zqw:deptBranch:view")
	@RequestMapping(value = "form")
	public String form(DeptBranch deptBranch, Model model) {
		model.addAttribute("deptBranch", deptBranch);
		return "modules/zqw/deptBranchForm";
	}

	@RequiresPermissions("zqw:deptBranch:edit")
	@RequestMapping(value = "save")
	public String save(DeptBranch deptBranch, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, deptBranch)){
			return form(deptBranch, model);
		}
		deptBranchService.save(deptBranch);
		addMessage(redirectAttributes, "保存曝光台成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/deptBranch/?repage";
	}
	
	@RequiresPermissions("zqw:deptBranch:edit")
	@RequestMapping(value = "delete")
	public String delete(DeptBranch deptBranch, RedirectAttributes redirectAttributes) {
		deptBranchService.delete(deptBranch);
		addMessage(redirectAttributes, "删除曝光台成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/deptBranch/?repage";
	}

}