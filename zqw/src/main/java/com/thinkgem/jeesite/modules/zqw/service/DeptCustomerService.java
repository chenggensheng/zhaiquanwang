/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.DeptCustomer;
import com.thinkgem.jeesite.modules.zqw.dao.DeptCustomerDao;

/**
 * 客户管理Service
 * @author coder_cheng@126.com
 * @version 2018-10-23
 */
@Service
@Transactional(readOnly = true)
public class DeptCustomerService extends CrudService<DeptCustomerDao, DeptCustomer> {

	public DeptCustomer get(String id) {
		return super.get(id);
	}
	
	public List<DeptCustomer> findList(DeptCustomer deptCustomer) {
		return super.findList(deptCustomer);
	}
	
	public Page<DeptCustomer> findPage(Page<DeptCustomer> page, DeptCustomer deptCustomer) {
		return super.findPage(page, deptCustomer);
	}
	
	@Transactional(readOnly = false)
	public void save(DeptCustomer deptCustomer) {
		super.save(deptCustomer);
	}
	
	@Transactional(readOnly = false)
	public void delete(DeptCustomer deptCustomer) {
		super.delete(deptCustomer);
	}
	
}