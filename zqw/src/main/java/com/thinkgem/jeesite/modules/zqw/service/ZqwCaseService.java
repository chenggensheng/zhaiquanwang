/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCase;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCaseDao;

/**
 * 案件管理Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCaseService extends CrudService<ZqwCaseDao, ZqwCase> {

	public ZqwCase get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCase> findList(ZqwCase zqwCase) {
		return super.findList(zqwCase);
	}
	
	public Page<ZqwCase> findPage(Page<ZqwCase> page, ZqwCase zqwCase) {
		return super.findPage(page, zqwCase);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCase zqwCase) {
		super.save(zqwCase);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCase zqwCase) {
		super.delete(zqwCase);
	}
	
}