/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 客户管理Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwCustom extends DataEntity<ZqwCustom> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 客户简称
	private String fullName;		// 客户名称
	private String contactsName;		// 联系人
	private String contactsPhone;		// 联系电话
	private String customerNature;		// 客户性质
	private String customerStatus;		// 客户状态
	private String customerType;		// 客户类型
	
	public ZqwCustom() {
		super();
	}

	public ZqwCustom(String id){
		super(id);
	}

	@Length(min=0, max=1024, message="客户简称长度必须介于 0 和 1024 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=1024, message="客户名称长度必须介于 0 和 1024 之间")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Length(min=0, max=255, message="联系人长度必须介于 0 和 255 之间")
	public String getContactsName() {
		return contactsName;
	}

	public void setContactsName(String contactsName) {
		this.contactsName = contactsName;
	}
	
	@Length(min=0, max=255, message="联系电话长度必须介于 0 和 255 之间")
	public String getContactsPhone() {
		return contactsPhone;
	}

	public void setContactsPhone(String contactsPhone) {
		this.contactsPhone = contactsPhone;
	}
	
	@Length(min=0, max=1, message="客户性质长度必须介于 0 和 1 之间")
	public String getCustomerNature() {
		return customerNature;
	}

	public void setCustomerNature(String customerNature) {
		this.customerNature = customerNature;
	}
	
	@Length(min=0, max=1, message="客户状态长度必须介于 0 和 1 之间")
	public String getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	
	@Length(min=0, max=1, message="客户类型长度必须介于 0 和 1 之间")
	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
}