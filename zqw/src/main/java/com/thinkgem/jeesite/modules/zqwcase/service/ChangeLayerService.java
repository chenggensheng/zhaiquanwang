/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.ChangeLayer;
import com.thinkgem.jeesite.modules.zqwcase.dao.ChangeLayerDao;

/**
 * 生成律师律所Service
 * @author coder_cheng@126.com
 * @version 2019-09-14
 */
@Service
@Transactional(readOnly = true)
public class ChangeLayerService extends CrudService<ChangeLayerDao, ChangeLayer> {

	public ChangeLayer get(String id) {
		return super.get(id);
	}
	
	public List<ChangeLayer> findList(ChangeLayer changeLayer) {
		return super.findList(changeLayer);
	}
	
	public Page<ChangeLayer> findPage(Page<ChangeLayer> page, ChangeLayer changeLayer) {
		return super.findPage(page, changeLayer);
	}
	
	@Transactional(readOnly = false)
	public void save(ChangeLayer changeLayer) {
		super.save(changeLayer);
	}
	
	@Transactional(readOnly = false)
	public void delete(ChangeLayer changeLayer) {
		super.delete(changeLayer);
	}

	@Transactional(readOnly = false)
	public void changeLayer(List<ChangeLayer> changeLayers){
//		for (ChangeLayer changeLayer:changeLayers){
//			String [] layerTmeps = changeLayer.getContext().split("\n");
//			for (String temp:layerTmeps){
//				if (temp.contains("代理人")&& temp.contains("实习")){
//					//String [] tringTemps = temp.split("：");
//					//if (tringTemps.length>1){
//					//String [] strings = tringTemps[1].split("，");
//					ChangeLayer changeLayerTemp = new ChangeLayer();
//					changeLayerTemp.setLayerName(temp);
////					for (String layerTemp : strings){
////						if (layerTemp.contains("律师")){
////							changeLayerTemp.setLayerPlace(layerTemp);
////						}
////						if (layerTemp.contains("实习")){
////							changeLayerTemp.setLayerType("实习");
////						}
////					}
//					save(changeLayerTemp);
//					}
//				}
//			}

		for (ChangeLayer changeLayer:changeLayers){
			String [] layerTmeps = changeLayer.getContext().split("，");
			if (layerTmeps.length>1){
				ChangeLayer changeLayerTemp = new ChangeLayer();
				changeLayerTemp.setLayerName(layerTmeps[0]);
				for (String t:layerTmeps){
					if (t.contains("实习")){
						changeLayerTemp.setLayerPlace(t);
						break;
					}
				}
               save(changeLayerTemp);
			}

			String [] layerTmepsTow =changeLayer.getContext().split(",");
			if (layerTmepsTow.length>1){
				ChangeLayer changeLayerTemp = new ChangeLayer();
				changeLayerTemp.setLayerName(layerTmeps[0]);
				for (String t:layerTmepsTow){
					if (t.contains("实习")){
						changeLayerTemp.setLayerPlace(t);
						break;
					}
				}
				save(changeLayerTemp);
			}
		}






		}
	
}