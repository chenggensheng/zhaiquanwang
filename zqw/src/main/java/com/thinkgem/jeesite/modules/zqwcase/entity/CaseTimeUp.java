/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 肖邦邦同步管理Entity
 * @author coder_cheng@126.com
 * @version 2019-07-20
 */
public class CaseTimeUp extends DataEntity<CaseTimeUp> {
	
	private static final long serialVersionUID = 1L;
	private String lastDate;		// last_date
	private String updateType;
	private Date updateDateTime;
	
	public CaseTimeUp() {
		super();
	}

	public CaseTimeUp(String id){
		super(id);
	}

	@Length(min=0, max=64, message="last_date长度必须介于 0 和 64 之间")
	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
}