/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 肖邦邦案件管理Entity
 * @author coder_cheng@126.com
 * @version 2019-07-20
 */
public class CaseXbbInsurance extends DataEntity<CaseXbbInsurance> {
	
	private static final long serialVersionUID = 1L;
	private String caseInsuranceId;		// 案件信息
	private String limitTime;		// 举证期限
	private String accidentPlace;		// 事故发生地事故发生地
	private String accidentDate;		// 事故发生日期
	private String maturityDate;		// 到期日期
	private String cooperativeLawyer;		// 合作律师信息
	private String commercialClause;		// 合同商务条款
	private String contractStatus;		// 合同状态
	private String contractType;		// 合同类型
	private String contractAmount;		// 合同金额
	private String outSourcing;		// 委外情况
	private String outSourcingDate;		// 委外日期
	private String caseCustomerId;		// 客户ID
	private String caseCustomerName;		// 客户名称
	private String caseCustmoerContract;		// 客户联系人
	private String caseCustomerDemand;		// 客户需求
	private String openingTime;		// 开庭日期
	private String undertakingTeam;		// 承办团队
	private String undertakingLawyer;		// 承办律师
	private String undertakingStatus;		// 承办状态
	private String reportNum;		// 报案号
	private String chargingMethod;		// 收费方式收费方式
	private String itFiled;		// 是否归档
	private String upTime;		// 更新时间
	private Date createTime;  //创建日期
	private String listMaterials;		// 材料清单
	private String caseName;		// 案件名称
	private String caseStar;		// 案件星级
	private String caseType;		// 案件类型
	private String caseNumber;		// 案件编号
	private String caseIntroduction;		// 案情简介
	private String caseCreditorNub;		// 正式债权编号
	private String claimDate;		// 理赔日期
	private String executionDate;		// 申请执行日期
	private String currentStage;		// 目前阶段
	private String signPersone;		// 签订人
	private String signingDate;		// 签订日期
	private String jurisdictionalCourt;		// 管辖法院
	private String refrigerationDate;		// 续封续冻日期
	private String limitationDate;		   // 诉讼/执行时效届满日
	private String lawsuitFillingDateString;	// 诉讼立案日期


	private Date lawsuitFillingDate;	// 更新日期
	private String compensatorName;		// 赔偿人名称
	private String compensatorContact;		// 赔偿人联系方式
	private String compernsatorAmount;		// 赔偿总金额
	private String createPerson;

	private String nonProsecutio; //非诉开始时间  字符串

	private Date nonPresecutioDate; //非诉开始时间
	private Date lawsuitStartDate;	// 诉讼开始日期
	private Date executionStratDate; // 执行开始时间

	private String lawyerLetter;      //律师函
	private String repaymentIntention;//还款意向






	//跟进记录中的时间字段
	private Date sInspectionEffect;//S验诉效
	private Date sInstant;//S立时  17
	private Date sTurnOnTime;//S开时 18
    private Date sJudgment;//S判达 19
	private Date sJudgmentTime;//S判生

	private Date swTimeShift;//SW移时 21
	private Date swEndTime;//SW完时 22
    private Date swInstant; //SW立时26
	private Date swOpenTime;//SW开时27
	private Date swJudgment;//SW判达
	private Date swJudgmentTime;//SW判生

	private Date zTestEffect;//Z验执效
	private Date zFollowUp;  //Z跟进
	private Date zShenShi;  //Z申时
	private Date zEndTime;  //Z终时

	private Date zwTimeShift;//ZW移时
	private Date zwFinishTime;//ZW完时
	private Date zwShenShi;  //ZW申时
	private Date zwEndTime;  //ZW终时

	private String sourceStauts; //资料情况



	public CaseXbbInsurance() {
		super();
	}

	public CaseXbbInsurance(String id){
		super(id);
	}

	public Date getLawsuitStartDate() {
		return lawsuitStartDate;
	}

	public void setLawsuitStartDate(Date lawsuitStartDate) {
		this.lawsuitStartDate = lawsuitStartDate;
	}

	public Date getExecutionStratDate() {
		return executionStratDate;
	}

	public void setExecutionStratDate(Date executionStratDate) {
		this.executionStratDate = executionStratDate;
	}

	@Length(min=0, max=64, message="举证期限长度必须介于 0 和 64 之间")
	public String getLimitTime() {
		return limitTime;
	}

	public void setLimitTime(String limitTime) {
		this.limitTime = limitTime;
	}
	
	@Length(min=0, max=10240, message="事故发生地事故发生地长度必须介于 0 和 10240 之间")
	public String getAccidentPlace() {
		return accidentPlace;
	}

	public void setAccidentPlace(String accidentPlace) {
		this.accidentPlace = accidentPlace;
	}
	
	@Length(min=0, max=64, message="事故发生日期长度必须介于 0 和 64 之间")
	public String getAccidentDate() {
		return accidentDate;
	}

	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}
	
	@Length(min=0, max=64, message="到期日期长度必须介于 0 和 64 之间")
	public String getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	
	@Length(min=0, max=1024, message="合作律师信息长度必须介于 0 和 1024 之间")
	public String getCooperativeLawyer() {
		return cooperativeLawyer;
	}

	public void setCooperativeLawyer(String cooperativeLawyer) {
		this.cooperativeLawyer = cooperativeLawyer;
	}
	
	@Length(min=0, max=1024, message="合同商务条款长度必须介于 0 和 1024 之间")
	public String getCommercialClause() {
		return commercialClause;
	}

	public void setCommercialClause(String commercialClause) {
		this.commercialClause = commercialClause;
	}
	
	@Length(min=0, max=64, message="合同状态长度必须介于 0 和 64 之间")
	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	
	@Length(min=0, max=64, message="合同类型长度必须介于 0 和 64 之间")
	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	@Length(min=0, max=11, message="合同金额长度必须介于 0 和 11 之间")
	public String getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(String contractAmount) {
		this.contractAmount = contractAmount;
	}
	
	@Length(min=0, max=64, message="委外情况长度必须介于 0 和 64 之间")
	public String getOutSourcing() {
		return outSourcing;
	}

	public void setOutSourcing(String outSourcing) {
		this.outSourcing = outSourcing;
	}
	
	@Length(min=0, max=11, message="委外日期长度必须介于 0 和 11 之间")
	public String getOutSourcingDate() {
		return outSourcingDate;
	}

	public void setOutSourcingDate(String outSourcingDate) {
		this.outSourcingDate = outSourcingDate;
	}
	
	@Length(min=0, max=64, message="客户ID长度必须介于 0 和 64 之间")
	public String getCaseCustomerId() {
		return caseCustomerId;
	}

	public void setCaseCustomerId(String caseCustomerId) {
		this.caseCustomerId = caseCustomerId;
	}
	
	@Length(min=0, max=1024, message="客户名称长度必须介于 0 和 1024 之间")
	public String getCaseCustomerName() {
		return caseCustomerName;
	}

	public void setCaseCustomerName(String caseCustomerName) {
		this.caseCustomerName = caseCustomerName;
	}
	
	@Length(min=0, max=64, message="客户联系人长度必须介于 0 和 64 之间")
	public String getCaseCustmoerContract() {
		return caseCustmoerContract;
	}

	public void setCaseCustmoerContract(String caseCustmoerContract) {
		this.caseCustmoerContract = caseCustmoerContract;
	}
	
	@Length(min=0, max=64, message="客户需求长度必须介于 0 和 64 之间")
	public String getCaseCustomerDemand() {
		return caseCustomerDemand;
	}

	public void setCaseCustomerDemand(String caseCustomerDemand) {
		this.caseCustomerDemand = caseCustomerDemand;
	}
	
	@Length(min=0, max=64, message="开庭日期长度必须介于 0 和 64 之间")
	public String getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}
	
	@Length(min=0, max=64, message="承办团队长度必须介于 0 和 64 之间")
	public String getUndertakingTeam() {
		return undertakingTeam;
	}

	public void setUndertakingTeam(String undertakingTeam) {
		this.undertakingTeam = undertakingTeam;
	}
	
	@Length(min=0, max=64, message="承办律师长度必须介于 0 和 64 之间")
	public String getUndertakingLawyer() {
		return undertakingLawyer;
	}

	public void setUndertakingLawyer(String undertakingLawyer) {
		this.undertakingLawyer = undertakingLawyer;
	}
	
	@Length(min=0, max=64, message="承办状态长度必须介于 0 和 64 之间")
	public String getUndertakingStatus() {
		return undertakingStatus;
	}

	public void setUndertakingStatus(String undertakingStatus) {
		this.undertakingStatus = undertakingStatus;
	}
	
	@Length(min=0, max=64, message="report_num长度必须介于 0 和 64 之间")
	public String getReportNum() {
		return reportNum;
	}

	public void setReportNum(String reportNum) {
		this.reportNum = reportNum;
	}
	
	@Length(min=0, max=255, message="收费方式收费方式长度必须介于 0 和 255 之间")
	public String getChargingMethod() {
		return chargingMethod;
	}

	public void setChargingMethod(String chargingMethod) {
		this.chargingMethod = chargingMethod;
	}
	
	@Length(min=0, max=255, message="是否归档长度必须介于 0 和 255 之间")
	public String getItFiled() {
		return itFiled;
	}

	public void setItFiled(String itFiled) {
		this.itFiled = itFiled;
	}
	
	@Length(min=0, max=64, message="更新时间长度必须介于 0 和 64 之间")
	public String getUpTime() {
		return upTime;
	}

	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
	
	@Length(min=0, max=1024, message="材料清单长度必须介于 0 和 1024 之间")
	public String getListMaterials() {
		return listMaterials;
	}

	public void setListMaterials(String listMaterials) {
		this.listMaterials = listMaterials;
	}
	
	@Length(min=0, max=1024, message="案件名称长度必须介于 0 和 1024 之间")
	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	
	@Length(min=0, max=64, message="案件星级长度必须介于 0 和 64 之间")
	public String getCaseStar() {
		return caseStar;
	}

	public void setCaseStar(String caseStar) {
		this.caseStar = caseStar;
	}
	
	@Length(min=0, max=64, message="案件类型长度必须介于 0 和 64 之间")
	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	
	@Length(min=0, max=64, message="案件类型长度必须介于 0 和 64 之间")
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public String getCaseIntroduction() {
		return caseIntroduction;
	}

	public void setCaseIntroduction(String caseIntroduction) {
		this.caseIntroduction = caseIntroduction;
	}
	
	@Length(min=0, max=64, message="正式债权编号长度必须介于 0 和 64 之间")
	public String getCaseCreditorNub() {
		return caseCreditorNub;
	}

	public void setCaseCreditorNub(String caseCreditorNub) {
		this.caseCreditorNub = caseCreditorNub;
	}
	
	@Length(min=0, max=64, message="理赔日期长度必须介于 0 和 64 之间")
	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}
	
	@Length(min=0, max=64, message="申请执行日期长度必须介于 0 和 64 之间")
	public String getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(String executionDate) {
		this.executionDate = executionDate;
	}
	
	@Length(min=0, max=64, message="目前阶段长度必须介于 0 和 64 之间")
	public String getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(String currentStage) {
		this.currentStage = currentStage;
	}
	
	@Length(min=0, max=64, message="签订人长度必须介于 0 和 64 之间")
	public String getSignPersone() {
		return signPersone;
	}

	public void setSignPersone(String signPersone) {
		this.signPersone = signPersone;
	}
	
	@Length(min=0, max=64, message="签订日期长度必须介于 0 和 64 之间")
	public String getSigningDate() {
		return signingDate;
	}

	public void setSigningDate(String signingDate) {
		this.signingDate = signingDate;
	}
	
	@Length(min=0, max=1024, message="管辖法院长度必须介于 0 和 1024 之间")
	public String getJurisdictionalCourt() {
		return jurisdictionalCourt;
	}

	public void setJurisdictionalCourt(String jurisdictionalCourt) {
		this.jurisdictionalCourt = jurisdictionalCourt;
	}
	
	@Length(min=0, max=64, message="续封续冻日期长度必须介于 0 和 64 之间")
	public String getRefrigerationDate() {
		return refrigerationDate;
	}

	public void setRefrigerationDate(String refrigerationDate) {
		this.refrigerationDate = refrigerationDate;
	}
	
	@Length(min=0, max=64, message="诉讼/执行时效届满日长度必须介于 0 和 64 之间")
	public String getLimitationDate() {
		return limitationDate;
	}

	public void setLimitationDate(String limitationDate) {
		this.limitationDate = limitationDate;
	}

	public Date getLawsuitFillingDate() {
		return lawsuitFillingDate;
	}

	public void setLawsuitFillingDate(Date lawsuitFillingDate) {
		this.lawsuitFillingDate = lawsuitFillingDate;
	}

	@Length(min=0, max=1024, message="赔偿人名称长度必须介于 0 和 1024 之间")
	public String getCompensatorName() {
		return compensatorName;
	}

	public void setCompensatorName(String compensatorName) {
		this.compensatorName = compensatorName;
	}
	
	@Length(min=0, max=64, message="赔偿人联系方式长度必须介于 0 和 64 之间")
	public String getCompensatorContact() {
		return compensatorContact;
	}

	public void setCompensatorContact(String compensatorContact) {
		this.compensatorContact = compensatorContact;
	}
	
	@Length(min=0, max=64, message="赔偿总金额长度必须介于 0 和 64 之间")
	public String getCompernsatorAmount() {
		return compernsatorAmount;
	}

	public void setCompernsatorAmount(String compernsatorAmount) {
		this.compernsatorAmount = compernsatorAmount;
	}

	public String getCaseInsuranceId() {
		return caseInsuranceId;
	}

	public void setCaseInsuranceId(String caseInsuranceId) {
		this.caseInsuranceId = caseInsuranceId;
	}

	public String getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getNonProsecutio() {
		return nonProsecutio;
	}

	public void setNonProsecutio(String nonProsecutio) {
		this.nonProsecutio = nonProsecutio;
	}

	public Date getNonPresecutioDate() {
		return nonPresecutioDate;
	}

	public void setNonPresecutioDate(Date nonPresecutioDate) {
		this.nonPresecutioDate = nonPresecutioDate;
	}

	public String getLawsuitFillingDateString() {
		return lawsuitFillingDateString;
	}

	public void setLawsuitFillingDateString(String lawsuitFillingDateString) {
		this.lawsuitFillingDateString = lawsuitFillingDateString;
	}

	public Date getsInspectionEffect() {
		return sInspectionEffect;
	}

	public void setsInspectionEffect(Date sInspectionEffect) {
		this.sInspectionEffect = sInspectionEffect;
	}

	public Date getsInstant() {
		return sInstant;
	}

	public void setsInstant(Date sInstant) {
		this.sInstant = sInstant;
	}

	public Date getsTurnOnTime() {
		return sTurnOnTime;
	}

	public void setsTurnOnTime(Date sTurnOnTime) {
		this.sTurnOnTime = sTurnOnTime;
	}

	public Date getsJudgment() {
		return sJudgment;
	}

	public void setsJudgment(Date sJudgment) {
		this.sJudgment = sJudgment;
	}

	public Date getsJudgmentTime() {
		return sJudgmentTime;
	}

	public void setsJudgmentTime(Date sJudgmentTime) {
		this.sJudgmentTime = sJudgmentTime;
	}

	public Date getSwTimeShift() {
		return swTimeShift;
	}

	public void setSwTimeShift(Date swTimeShift) {
		this.swTimeShift = swTimeShift;
	}

	public Date getSwEndTime() {
		return swEndTime;
	}

	public void setSwEndTime(Date swEndTime) {
		this.swEndTime = swEndTime;
	}

	public Date getSwInstant() {
		return swInstant;
	}

	public void setSwInstant(Date swInstant) {
		this.swInstant = swInstant;
	}

	public Date getSwOpenTime() {
		return swOpenTime;
	}

	public void setSwOpenTime(Date swOpenTime) {
		this.swOpenTime = swOpenTime;
	}

	public Date getSwJudgment() {
		return swJudgment;
	}

	public void setSwJudgment(Date swJudgment) {
		this.swJudgment = swJudgment;
	}

	public Date getSwJudgmentTime() {
		return swJudgmentTime;
	}

	public void setSwJudgmentTime(Date swJudgmentTime) {
		this.swJudgmentTime = swJudgmentTime;
	}

	public Date getzTestEffect() {
		return zTestEffect;
	}

	public void setzTestEffect(Date zTestEffect) {
		this.zTestEffect = zTestEffect;
	}

	public Date getzFollowUp() {
		return zFollowUp;
	}

	public void setzFollowUp(Date zFollowUp) {
		this.zFollowUp = zFollowUp;
	}

	public Date getzShenShi() {
		return zShenShi;
	}

	public void setzShenShi(Date zShenShi) {
		this.zShenShi = zShenShi;
	}

	public Date getzEndTime() {
		return zEndTime;
	}

	public void setzEndTime(Date zEndTime) {
		this.zEndTime = zEndTime;
	}

	public Date getZwTimeShift() {
		return zwTimeShift;
	}

	public void setZwTimeShift(Date zwTimeShift) {
		this.zwTimeShift = zwTimeShift;
	}

	public Date getZwFinishTime() {
		return zwFinishTime;
	}

	public void setZwFinishTime(Date zwFinishTime) {
		this.zwFinishTime = zwFinishTime;
	}

	public Date getZwShenShi() {
		return zwShenShi;
	}

	public void setZwShenShi(Date zwShenShi) {
		this.zwShenShi = zwShenShi;
	}

	public Date getZwEndTime() {
		return zwEndTime;
	}

	public void setZwEndTime(Date zwEndTime) {
		this.zwEndTime = zwEndTime;
	}

	public String getSourceStauts() {
		return sourceStauts;
	}

	public void setSourceStauts(String sourceStauts) {
		this.sourceStauts = sourceStauts;
	}

	public String getLawyerLetter() {
		return lawyerLetter;
	}

	public void setLawyerLetter(String lawyerLetter) {
		this.lawyerLetter = lawyerLetter;
	}

	public String getRepaymentIntention() {
		return repaymentIntention;
	}

	public void setRepaymentIntention(String repaymentIntention) {
		this.repaymentIntention = repaymentIntention;
	}



}