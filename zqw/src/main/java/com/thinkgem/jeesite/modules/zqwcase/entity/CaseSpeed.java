/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 案件进展信息Entity
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
public class CaseSpeed extends DataEntity<CaseSpeed> {
	
	private static final long serialVersionUID = 1L;
	private String caseInsuranceId;		// 案件信息
	private String speedIntroduction;		// 进展描述
	private String attorneyLitigation;		// 诉讼部承办律师
	private String jurisdictionalCourt;		// 管辖法院
	private Date judgmentDate;		// 本案判决书出具日期
	private String acceptingNum;		// 立案受理案号
	private String judgmentAmount;		// 本案判决金额
	private String judgmentContent;		// 判决内容
	private String judgmentReason;		// 事实和理由
	private String courtOfExecution;		// 执行法院
	private String executionNum;		// 执行案号
	private String unexecutedContent;		// 未执行内容
	private String attorneyLitigationName;  //诉讼部承办律师姓名
	private String attorneyLitigationTelphone;// 诉讼部承办律师电话
 	private String investmentInquiries;//投资查询情况


	//非PO字段
	private String caseCustomerId;    //
	
	public CaseSpeed() {
		super();
	}

	public CaseSpeed(String id){
		super(id);
	}

	@Length(min=0, max=64, message="案件信息长度必须介于 0 和 64 之间")
	public String getCaseInsuranceId() {
		return caseInsuranceId;
	}

	public void setCaseInsuranceId(String caseInsuranceId) {
		this.caseInsuranceId = caseInsuranceId;
	}
	
	public String getSpeedIntroduction() {
		return speedIntroduction;
	}

	public void setSpeedIntroduction(String speedIntroduction) {
		this.speedIntroduction = speedIntroduction;
	}
	
	@Length(min=0, max=64, message="诉讼部承办律师长度必须介于 0 和 64 之间")
	public String getAttorneyLitigation() {
		return attorneyLitigation;
	}

	public void setAttorneyLitigation(String attorneyLitigation) {
		this.attorneyLitigation = attorneyLitigation;
	}
	
	@Length(min=0, max=655, message="管辖法院长度必须介于 0 和 655 之间")
	public String getJurisdictionalCourt() {
		return jurisdictionalCourt;
	}

	public void setJurisdictionalCourt(String jurisdictionalCourt) {
		this.jurisdictionalCourt = jurisdictionalCourt;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getJudgmentDate() {
		return judgmentDate;
	}

	public void setJudgmentDate(Date judgmentDate) {
		this.judgmentDate = judgmentDate;
	}
	
	@Length(min=0, max=64, message="立案受理案号长度必须介于 0 和 64 之间")
	public String getAcceptingNum() {
		return acceptingNum;
	}

	public void setAcceptingNum(String acceptingNum) {
		this.acceptingNum = acceptingNum;
	}
	
	@Length(min=0, max=255, message="本案判决金额长度必须介于 0 和 255 之间")
	public String getJudgmentAmount() {
		return judgmentAmount;
	}

	public void setJudgmentAmount(String judgmentAmount) {
		this.judgmentAmount = judgmentAmount;
	}
	
	public String getJudgmentContent() {
		return judgmentContent;
	}

	public void setJudgmentContent(String judgmentContent) {
		this.judgmentContent = judgmentContent;
	}
	
	public String getJudgmentReason() {
		return judgmentReason;
	}

	public void setJudgmentReason(String judgmentReason) {
		this.judgmentReason = judgmentReason;
	}
	
	@Length(min=0, max=255, message="执行法院长度必须介于 0 和 255 之间")
	public String getCourtOfExecution() {
		return courtOfExecution;
	}

	public void setCourtOfExecution(String courtOfExecution) {
		this.courtOfExecution = courtOfExecution;
	}
	
	@Length(min=0, max=225, message="执行案号长度必须介于 0 和 225 之间")
	public String getExecutionNum() {
		return executionNum;
	}

	public void setExecutionNum(String executionNum) {
		this.executionNum = executionNum;
	}
	
	public String getUnexecutedContent() {
		return unexecutedContent;
	}

	public void setUnexecutedContent(String unexecutedContent) {
		this.unexecutedContent = unexecutedContent;
	}

	public String getCaseCustomerId() {
		return caseCustomerId;
	}

	public void setCaseCustomerId(String caseCustomerId) {
		this.caseCustomerId = caseCustomerId;
	}

	public String getAttorneyLitigationName() {
		return attorneyLitigationName;
	}

	public void setAttorneyLitigationName(String attorneyLitigationName) {
		this.attorneyLitigationName = attorneyLitigationName;
	}

	public String getAttorneyLitigationTelphone() {
		return attorneyLitigationTelphone;
	}

	public void setAttorneyLitigationTelphone(String attorneyLitigationTelphone) {
		this.attorneyLitigationTelphone = attorneyLitigationTelphone;
	}

	public String getInvestmentInquiries() {
		return investmentInquiries;
	}

	public void setInvestmentInquiries(String investmentInquiries) {
		this.investmentInquiries = investmentInquiries;
	}
}