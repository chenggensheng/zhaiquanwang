/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 民事起诉状Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwCaseIndictment extends DataEntity<ZqwCaseIndictment> {
	
	private static final long serialVersionUID = 1L;
	private String caseId;		// case_id
	private Date sendTime;		// send_time
	private String sendContent;		// send_content
	
	public ZqwCaseIndictment() {
		super();
	}

	public ZqwCaseIndictment(String id){
		super(id);
	}

	@Length(min=0, max=64, message="case_id长度必须介于 0 和 64 之间")
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	
	public String getSendContent() {
		return sendContent;
	}

	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}
	
}