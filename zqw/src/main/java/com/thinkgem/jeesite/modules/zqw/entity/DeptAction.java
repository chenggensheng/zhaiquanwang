/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 债行动Entity
 * @author coder_cheng@126.com
 * @version 2018-10-20
 */
public class DeptAction extends DataEntity<DeptAction> {
	
	private static final long serialVersionUID = 1L;
	private String debtType;		// 债权类型
	private String debtNum;		// 债权编号
	private String debtOwernName;		// 债权人名称
	private String debtPrice;		// 债权金额（元）
	private String debtInfo;		// 行动结果
	private Date debtActionTime;		// 行动时间
	
	public DeptAction() {
		super();
	}

	public DeptAction(String id){
		super(id);
	}

	@Length(min=0, max=6, message="债权类型长度必须介于 0 和 6 之间")
	public String getDebtType() {
		return debtType;
	}

	public void setDebtType(String debtType) {
		this.debtType = debtType;
	}
	
	@Length(min=0, max=64, message="债权编号长度必须介于 0 和 64 之间")
	public String getDebtNum() {
		return debtNum;
	}

	public void setDebtNum(String debtNum) {
		this.debtNum = debtNum;
	}
	
	@Length(min=0, max=1024, message="债权人名称长度必须介于 0 和 64 之间")
	public String getDebtOwernName() {
		return debtOwernName;
	}

	public void setDebtOwernName(String debtOwernName) {
		this.debtOwernName = debtOwernName;
	}
	
	@Length(min=0, max=64, message="债权金额（元）长度必须介于 0 和 64 之间")
	public String getDebtPrice() {
		return debtPrice;
	}

	public void setDebtPrice(String debtPrice) {
		this.debtPrice = debtPrice;
	}
	
	public String getDebtInfo() {
		return debtInfo;
	}

	public void setDebtInfo(String debtInfo) {
		this.debtInfo = debtInfo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getDebtActionTime() {
		return debtActionTime;
	}

	public void setDebtActionTime(Date debtActionTime) {
		this.debtActionTime = debtActionTime;
	}
	
}