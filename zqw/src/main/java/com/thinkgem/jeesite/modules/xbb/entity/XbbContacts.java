/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 联系人管理Entity
 * @author coder_cheng@126.com
 * @version 2018-08-31
 */
public class XbbContacts extends DataEntity<XbbContacts> {
	
	private static final long serialVersionUID = 1L;
	private String sort1;		// sort1
	private String sort2;		// sort2
	private String sort3;		// sort3
	private String customerid;		// 关联客户
	private String jcgx;		// 决策关系
	private String xq;		// 区/县
	private String dz;		// 地址
	private String bz;		// 备注
	private String xm;		// 姓名
	private String sjx;		// 市
	private String xb;		// 性别
	private String sj;		// 手机
	private String sjxx;	// 省
	private String jb;		// 级别
	private String zw;		// 职务
	private String bm;		// 部门
	private String zycd;		// 重要程度
	
	public XbbContacts() {
		super();
	}

	public XbbContacts(String id){
		super(id);
	}

	@Length(min=0, max=255, message="sort1长度必须介于 0 和 255 之间")
	public String getSort1() {
		return sort1;
	}

	public void setSort1(String sort1) {
		this.sort1 = sort1;
	}
	
	@Length(min=0, max=255, message="sort2长度必须介于 0 和 255 之间")
	public String getSort2() {
		return sort2;
	}

	public void setSort2(String sort2) {
		this.sort2 = sort2;
	}
	
	@Length(min=0, max=255, message="sort3长度必须介于 0 和 255 之间")
	public String getSort3() {
		return sort3;
	}

	public void setSort3(String sort3) {
		this.sort3 = sort3;
	}
	
	@Length(min=0, max=255, message="关联客户长度必须介于 0 和 255 之间")
	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
	@Length(min=0, max=255, message="决策关系长度必须介于 0 和 255 之间")
	public String getJcgx() {
		return jcgx;
	}

	public void setJcgx(String jcgx) {
		this.jcgx = jcgx;
	}
	
	@Length(min=0, max=255, message="区/县长度必须介于 0 和 255 之间")
	public String getXq() {
		return xq;
	}

	public void setXq(String xq) {
		this.xq = xq;
	}
	
	@Length(min=0, max=255, message="地址长度必须介于 0 和 255 之间")
	public String getDz() {
		return dz;
	}

	public void setDz(String dz) {
		this.dz = dz;
	}
	
	@Length(min=0, max=255, message="备注长度必须介于 0 和 255 之间")
	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}
	
	@Length(min=0, max=255, message="姓名长度必须介于 0 和 255 之间")
	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}
	
	@Length(min=0, max=255, message="市长度必须介于 0 和 255 之间")
	public String getSjx() {
		return sjx;
	}

	public void setSjx(String sjx) {
		this.sjx = sjx;
	}
	
	@Length(min=0, max=255, message="性别长度必须介于 0 和 255 之间")
	public String getXb() {
		return xb;
	}

	public void setXb(String xb) {
		this.xb = xb;
	}
	
	@Length(min=0, max=255, message="手机长度必须介于 0 和 255 之间")
	public String getSj() {
		return sj;
	}

	public void setSj(String sj) {
		this.sj = sj;
	}
	
	@Length(min=0, max=255, message="省长度必须介于 0 和 255 之间")
	public String getSjxx() {
		return sjxx;
	}

	public void setSjxx(String sjxx) {
		this.sjxx = sjxx;
	}
	
	@Length(min=0, max=255, message="级别长度必须介于 0 和 255 之间")
	public String getJb() {
		return jb;
	}

	public void setJb(String jb) {
		this.jb = jb;
	}
	
	@Length(min=0, max=255, message="职务长度必须介于 0 和 255 之间")
	public String getZw() {
		return zw;
	}

	public void setZw(String zw) {
		this.zw = zw;
	}
	
	@Length(min=0, max=255, message="部门长度必须介于 0 和 255 之间")
	public String getBm() {
		return bm;
	}

	public void setBm(String bm) {
		this.bm = bm;
	}
	
	@Length(min=0, max=255, message="重要程度长度必须介于 0 和 255 之间")
	public String getZycd() {
		return zycd;
	}

	public void setZycd(String zycd) {
		this.zycd = zycd;
	}
	
}