/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * 邮件接收人Entity
 * @author Huang
 * @version 2018-04-27
 */
public class OaMailPerson extends DataEntity<OaMailPerson> {
	
	private static final long serialVersionUID = 1L;
	private String mailId;		// 邮件
	private String type;		// 发送类型
	private String status;		// 状态
	private String readFlag;		// 阅读标志
	private String dustbinFlag;		// 垃圾标志
	private String importantFlag;		// 重要标志
	private User   user = new User();
	

	private Date readDate;		// 阅读时间
	
	public OaMailPerson() {
		super();
	}

	public OaMailPerson(String id){
		super(id);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getImportantFlag() {
		return importantFlag;
	}

	public void setImportantFlag(String importantFlag) {
		this.importantFlag = importantFlag;
	}

	@Length(min=1, max=64, message="邮件长度必须介于 1 和 64 之间")
	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	
	@Length(min=0, max=64, message="发送类型长度必须介于 0 和 64 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public String getDustbinFlag() {
		return dustbinFlag;
	}

	public void setDustbinFlag(String dustbinFlag) {
		this.dustbinFlag = dustbinFlag;
	}
	@Length(min=0, max=64, message="状态长度必须介于 0 和 64 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="阅读标志长度必须介于 0 和 64 之间")
	public String getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(String readFlag) {
		this.readFlag = readFlag;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	
}