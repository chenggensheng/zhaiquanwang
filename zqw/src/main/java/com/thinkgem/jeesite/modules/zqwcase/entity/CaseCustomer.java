/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 案件客户信息Entity
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
public class CaseCustomer extends DataEntity<CaseCustomer> {
	
	private static final long serialVersionUID = 1L;
	private String creditor;		// 债权人
	private String crmCreditor;
	private String creditorDomicile;		// 债权人住所地
	private String creditorIdcart;		// 债权人信用代码
	private String creditorCharge;		// 债权负责人
	private String creditorChargePositon;		// 债权人负责人职位
	private String undertakingLawyer;		// 承办律师
	private String undertakingLawyerName;   // 承办律师姓名
	private String undertakingLawyerPhone;  // 承办律师电话
	private String undertakingLawyerTyep;
	private String crmId;


	private  String accountNameInformation; //开户名信息
	private  String openingBankInformation; //开户行信息
	private  String accountInformation;     //账号信息

	
	public CaseCustomer() {
		super();
	}

	public CaseCustomer(String id){
		super(id);
	}

	@Length(min=0, max=255, message="债权人长度必须介于 0 和 255 之间")
	public String getCreditor() {
		return creditor;
	}

	public void setCreditor(String creditor) {
		this.creditor = creditor;
	}
	
	@Length(min=0, max=1024, message="债权人住所地长度必须介于 0 和 1024 之间")
	public String getCreditorDomicile() {
		return creditorDomicile;
	}

	public void setCreditorDomicile(String creditorDomicile) {
		this.creditorDomicile = creditorDomicile;
	}
	
	@Length(min=0, max=255, message="债权人信用代码长度必须介于 0 和 255 之间")
	public String getCreditorIdcart() {
		return creditorIdcart;
	}

	public void setCreditorIdcart(String creditorIdcart) {
		this.creditorIdcart = creditorIdcart;
	}
	
	@Length(min=0, max=255, message="债权负责人长度必须介于 0 和 255 之间")
	public String getCreditorCharge() {
		return creditorCharge;
	}

	public void setCreditorCharge(String creditorCharge) {
		this.creditorCharge = creditorCharge;
	}
	
	@Length(min=0, max=255, message="债权人负责人职位长度必须介于 0 和 255 之间")
	public String getCreditorChargePositon() {
		return creditorChargePositon;
	}

	public void setCreditorChargePositon(String creditorChargePositon) {
		this.creditorChargePositon = creditorChargePositon;
	}
	
	@Length(min=0, max=255, message="承办律师长度必须介于 0 和 255 之间")
	public String getUndertakingLawyer() {
		return undertakingLawyer;
	}

	public void setUndertakingLawyer(String undertakingLawyer) {
		this.undertakingLawyer = undertakingLawyer;
	}


	public String getUndertakingLawyerName() {
		return undertakingLawyerName;
	}

	public void setUndertakingLawyerName(String undertakingLawyerName) {
		this.undertakingLawyerName = undertakingLawyerName;
	}

	public String getUndertakingLawyerPhone() {
		return undertakingLawyerPhone;
	}

	public void setUndertakingLawyerPhone(String undertakingLawyerPhone) {
		this.undertakingLawyerPhone = undertakingLawyerPhone;
	}

	public String getUndertakingLawyerTyep() {
		return undertakingLawyerTyep;
	}

	public void setUndertakingLawyerTyep(String undertakingLawyerTyep) {
		this.undertakingLawyerTyep = undertakingLawyerTyep;
	}

	public String getAccountNameInformation() {
		return accountNameInformation;
	}

	public void setAccountNameInformation(String accountNameInformation) {
		this.accountNameInformation = accountNameInformation;
	}

	public String getOpeningBankInformation() {
		return openingBankInformation;
	}

	public void setOpeningBankInformation(String openingBankInformation) {
		this.openingBankInformation = openingBankInformation;
	}

	public String getAccountInformation() {
		return accountInformation;
	}

	public String getCrmCreditor() {
		return crmCreditor;
	}

	public void setCrmCreditor(String crmCreditor) {
		this.crmCreditor = crmCreditor;
	}

	public void setAccountInformation(String accountInformation) {
		this.accountInformation = accountInformation;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}
}