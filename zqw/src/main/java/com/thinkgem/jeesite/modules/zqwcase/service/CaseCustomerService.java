/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseCustomer;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseCustomerDao;

/**
 * 案件客户信息Service
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@Service
@Transactional(readOnly = true)
public class CaseCustomerService extends CrudService<CaseCustomerDao, CaseCustomer> {

	public CaseCustomer get(String id) {
		return super.get(id);
	}
	
	public List<CaseCustomer> findList(CaseCustomer caseCustomer) {
		return super.findList(caseCustomer);
	}
	
	public Page<CaseCustomer> findPage(Page<CaseCustomer> page, CaseCustomer caseCustomer) {
		return super.findPage(page, caseCustomer);
	}
	
	@Transactional(readOnly = false)
	public void save(CaseCustomer caseCustomer) {
		super.save(caseCustomer);
	}

	@Transactional(readOnly = false)
	public void saveInsert(CaseCustomer caseCustomer) {
		caseCustomer.preOnlyInsert();
		dao.insert(caseCustomer);
	}


	@Transactional(readOnly = false)
	public void delete(CaseCustomer caseCustomer) {
		super.delete(caseCustomer);
	}




}