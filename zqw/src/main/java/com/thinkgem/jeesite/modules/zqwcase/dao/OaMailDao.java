/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.OaMail;

import java.util.List;

/**
 * 内部邮件DAO接口
 * @author Huang
 * @version 2018-04-27
 */
@MyBatisDao
public interface OaMailDao extends CrudDao<OaMail> {
	public List<OaMail> findReceiveList(OaMail oaMail);
	public List<OaMail> findSendList(OaMail oaMail);


	public List<OaMail> findLimitFive(OaMail oaMail);
}