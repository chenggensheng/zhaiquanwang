/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.DeptCustomer;
import com.thinkgem.jeesite.modules.zqw.service.DeptCustomerService;

/**
 * 客户管理Controller
 * @author coder_cheng@126.com
 * @version 2018-10-23
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/deptCustomer")
public class DeptCustomerController extends BaseController {

	@Autowired
	private DeptCustomerService deptCustomerService;
	
	@ModelAttribute
	public DeptCustomer get(@RequestParam(required=false) String id) {
		DeptCustomer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = deptCustomerService.get(id);
		}
		if (entity == null){
			entity = new DeptCustomer();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:deptCustomer:view")
	@RequestMapping(value = {"list", ""})
	public String list(DeptCustomer deptCustomer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<DeptCustomer> page = deptCustomerService.findPage(new Page<DeptCustomer>(request, response), deptCustomer); 
		model.addAttribute("page", page);
		return "modules/zqw/deptCustomerList";
	}

	@RequiresPermissions("zqw:deptCustomer:view")
	@RequestMapping(value = "form")
	public String form(DeptCustomer deptCustomer, Model model) {
		model.addAttribute("deptCustomer", deptCustomer);
		return "modules/zqw/deptCustomerForm";
	}

	@RequiresPermissions("zqw:deptCustomer:edit")
	@RequestMapping(value = "save")
	public String save(DeptCustomer deptCustomer, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, deptCustomer)){
			return form(deptCustomer, model);
		}
		deptCustomerService.save(deptCustomer);
		addMessage(redirectAttributes, "保存客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/deptCustomer/?repage";
	}
	
	@RequiresPermissions("zqw:deptCustomer:edit")
	@RequestMapping(value = "delete")
	public String delete(DeptCustomer deptCustomer, RedirectAttributes redirectAttributes) {
		deptCustomerService.delete(deptCustomer);
		addMessage(redirectAttributes, "删除客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/deptCustomer/?repage";
	}

}