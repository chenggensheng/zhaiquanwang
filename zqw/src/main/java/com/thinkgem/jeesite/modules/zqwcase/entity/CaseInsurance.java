/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 案件信息Entity
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
public class CaseInsurance extends DataEntity<CaseInsurance> {
	
	private static final long serialVersionUID = 1L;
	private String caseCustomerId;		// case_customer_id
	private String caseType;		// 案件类别
	private String insured;		// 被保险人
	private String insuredDomicile;		// 被保险人住所地
	private String insuredIdcard;		// 被保险人信用代码
	private String insuredPersonCharge;		// 被保险人负责人
	private String insuredPersonChargePosition;		// 被保险人负责人职位
	private String insuredContactTelephone;		// 被保险人联系电话
	private String insuredLicenseNumber;		// 被保险人车牌号
	private Date accidentDate;		// 事故日期
	private String insuredPeople;		// 被保险人民族
	private String insuredIdNumber;		// 被保险人身份证号
	private String insuredSex;		// 被保险人性别
	private String insuredBith;		// 被保险人生日
	private String insuredVehicleDriver;		// 被保险人车辆驾驶人
	private String insuredVehicleDriverHousehlod;		// 被保险人车辆驾驶人户籍地
	private String insuredVehicleDriverPeople;		// 被保险人车辆驾驶人民族
	private String insuredVehicleDriverNumber;		// 被保险人车辆驾驶人身份证号
	private String insuredVehicleDriverTelephone;		// 被保险人车辆驾驶人联系电话
	private String insuredVehicleDriverLiability;		// 被保险人车辆驾驶人责任
	private String vehicleDriverInsuredParty;		// 受害方车辆驾驶人
	private String vehicleDriverInsuredLicense;		// 受害方车辆车牌号
	private String vehicleDriverInsuredResponsibility;		// 受害方车辆驾驶人责任
	private String vehicleInsuredVictim;		// 受害方车辆被保险人
	private Date accidentJudgmentDate;		// 交通事故判决书出具
	private String courtOfJudgment;		// 交通事故判决法院
	private String judgmentNumber;		// 交通事故判决案号
	private String judgmentAmount;		// 交通事故判决金额
	private String recoveryCasesAmount;		// 追偿案件诉讼费金额
	private String reasonsRecourse;		// 追偿原因
	private String caseIntroduction;		// 案情简介
	private String amountPaidBack;		// 已还金额
	private Date insuranceCompanyTime;		// 保险公司理赔时间
	private String claimAmountInsuranceCompany;		// 保险公司理赔金额

	private String victimVehicleOwner;		// 致害人车辆车主
	private String victimVehicleOwenrIsCompany; //致害人车主是否公司
	private String victimVehicleOwnerHousehlod;		// 致害人车辆车主住所地
	private String victimVehicleOwnerIdcard;		// 致害人车辆车主信用代码
	private String victimVehicleOwnerResponsiblity;		// 致害人车辆车主负责人
	private String victimVehicleOwnerDuty;		// 致害人车辆车主负责人职位
	private String victimVehicleOwnerTelephone;		// 致害人车辆车主联系电话
	private String victimVehicleOwnerLicense;		// 致害人车牌号
	private String victimVehicleOwnerPeople;		// 致害人车辆车主民族
	private String victimVehicleOwnerNumber;		// 致害人车辆车主身份证号
	private String victimVehicleOwnerSex;		// 致害人车辆车主性别
	private String victimVehicleOwnerBith;		// 致害人车辆车主生日
	private String victimDriverHarmful;		// 致害人车辆驾驶人
	private String victimDriverHarmfulPeople;		// 致害人车辆驾驶人民族
	private String victimDriverHarmfulHousehlod;		// 致害人车辆驾驶人户籍地
	private String victimDriverHarmfulIdcard;		// 致害人车辆驾驶人身份证号
	private String victimDriverHarmfulTelephone;		// 致害人车辆驾驶人联系电话

	private String victimDriverHarmfulSex;      //致害人车辆驾驶人性别
	private String victimDriverHarmfulBith;     //致害人车辆驾驶人生日
	private String insuredVehicleDriverSex;      //被保险人车辆驾驶人性别
	private String insuredVehicleDriverBith;     //被保险人车辆驾驶人生日
	private String isCompany;                    //是否公司
	private String injuriousInsuranceCompany;    //致害人保险公司
	private String injuriousCompanyTelephone;  //致害人保险公司联系电话
	private String insuranceClaimsContractors; //保险理赔承办人员
	private String insuranceClaimsTelephone;   //保险理赔承办人员联系电话
	private String creditorAmount;             //债权金额
	private String victimDriverHarmfulResponsibility; // 致害人车辆驾驶人责人
	private String recoveryAmount;           //追偿立案诉讼标的金额
	private String insuredCommercialInsurance; // 投保商业险名称

	private String civilIndictment;//民事起诉状
	private String grantAuthorization;//授权
	private String catalogueEvidence;//证据目录


	private String specialDebtorInformation; //债务人信息
	private String specialCssreditorAmount;    //债权金额
	private String specialCaseIntroduction;		// 案情简介
	private String specialAmountPaidBack;  // 已还金额

	private String acceptanceNumebr; //立案受理号
	private String acceptanceNumebrOld;//立案受理号
	private int layerNumber;//律师函号
	private String layerNumberOne; // 律师函号一
	private String layerNumberTow; // 律师函号二


	//非PO字段
	private String creditor;
	private String caseTypeSeache;
	private String deptSearch;
	private String ckeckAllFlag;
	private String insuranceSubrogation;
	private String ratioShow;
	private CaseCustomer caseCustomer;
	private CaseSpeed caseSpeed;
	private CaseXbbInsurance caseXbbInsurance;
	private String reportNumSearch;
	private String caseNumberSearch;
	private String caseNameSearch;



	private String creditorSearch;


	public CaseInsurance() {
		super();
	}

	public CaseInsurance(String id){
		super(id);
	}

	@Length(min=0, max=64, message="case_customer_id长度必须介于 0 和 64 之间")
	public String getCaseCustomerId() {
		return caseCustomerId;
	}

	public void setCaseCustomerId(String caseCustomerId) {
		this.caseCustomerId = caseCustomerId;
	}
	
//	@Length(min=0, max=255, message="案件类别长度必须介于 0 和 255 之间")
	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	
//	@Length(min=0, max=255, message="被保险人长度必须介于 0 和 255 之间")
	public String getInsured() {
		return insured;
	}

	public void setInsured(String insured) {
		this.insured = insured;
	}
	
//	@Length(min=0, max=1024, message="被保险人住所地长度必须介于 0 和 1024 之间")
	public String getInsuredDomicile() {
		return insuredDomicile;
	}

	public void setInsuredDomicile(String insuredDomicile) {
		this.insuredDomicile = insuredDomicile;
	}
	
//	@Length(min=0, max=255, message="被保险人信用代码长度必须介于 0 和 255 之间")
	public String getInsuredIdcard() {
		return insuredIdcard;
	}

	public void setInsuredIdcard(String insuredIdcard) {
		this.insuredIdcard = insuredIdcard;
	}
	
//	@Length(min=0, max=255, message="被保险人负责人长度必须介于 0 和 255 之间")
	public String getInsuredPersonCharge() {
		return insuredPersonCharge;
	}

	public void setInsuredPersonCharge(String insuredPersonCharge) {
		this.insuredPersonCharge = insuredPersonCharge;
	}
	
//	@Length(min=0, max=1024, message="被保险人负责人职位长度必须介于 0 和 1024 之间")
	public String getInsuredPersonChargePosition() {
		return insuredPersonChargePosition;
	}

	public void setInsuredPersonChargePosition(String insuredPersonChargePosition) {
		this.insuredPersonChargePosition = insuredPersonChargePosition;
	}
	
//	@Length(min=0, max=255, message="被保险人联系电话长度必须介于 0 和 255 之间")
	public String getInsuredContactTelephone() {
		return insuredContactTelephone;
	}

	public void setInsuredContactTelephone(String insuredContactTelephone) {
		this.insuredContactTelephone = insuredContactTelephone;
	}
	
//	@Length(min=0, max=11, message="被保险人车牌号长度必须介于 0 和 11 之间")
	public String getInsuredLicenseNumber() {
		return insuredLicenseNumber;
	}

	public void setInsuredLicenseNumber(String insuredLicenseNumber) {
		this.insuredLicenseNumber = insuredLicenseNumber;
	}
	
	@JsonFormat(pattern = "yyyy年MM月dd日")
	public Date getAccidentDate() {
		return accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}
	
//	@Length(min=0, max=255, message="被保险人民族长度必须介于 0 和 255 之间")
	public String getInsuredPeople() {
		return insuredPeople;
	}

	public void setInsuredPeople(String insuredPeople) {
		this.insuredPeople = insuredPeople;
	}
	
//	@Length(min=0, max=25, message="被保险人身份证号长度必须介于 0 和 25 之间")
	public String getInsuredIdNumber() {
		return insuredIdNumber;
	}

	public void setInsuredIdNumber(String insuredIdNumber) {
		this.insuredIdNumber = insuredIdNumber;
	}
	
//	@Length(min=0, max=1, message="被保险人性别长度必须介于 0 和 1 之间")
	public String getInsuredSex() {
		return insuredSex;
	}

	public void setInsuredSex(String insuredSex) {
		this.insuredSex = insuredSex;
	}
	
//	@Length(min=0, max=64, message="被保险人生日长度必须介于 0 和 64 之间")
	public String getInsuredBith() {
		return insuredBith;
	}

	public void setInsuredBith(String insuredBith) {
		this.insuredBith = insuredBith;
	}
	
//	@Length(min=0, max=255, message="被保险人车辆驾驶人长度必须介于 0 和 255 之间")
	public String getInsuredVehicleDriver() {
		return insuredVehicleDriver;
	}

	public void setInsuredVehicleDriver(String insuredVehicleDriver) {
		this.insuredVehicleDriver = insuredVehicleDriver;
	}
	
//	@Length(min=0, max=655, message="被保险人车辆驾驶人户籍地长度必须介于 0 和 655 之间")
	public String getInsuredVehicleDriverHousehlod() {
		return insuredVehicleDriverHousehlod;
	}

	public void setInsuredVehicleDriverHousehlod(String insuredVehicleDriverHousehlod) {
		this.insuredVehicleDriverHousehlod = insuredVehicleDriverHousehlod;
	}
	
//	@Length(min=0, max=255, message="被保险人车辆驾驶人民族长度必须介于 0 和 255 之间")
	public String getInsuredVehicleDriverPeople() {
		return insuredVehicleDriverPeople;
	}

	public void setInsuredVehicleDriverPeople(String insuredVehicleDriverPeople) {
		this.insuredVehicleDriverPeople = insuredVehicleDriverPeople;
	}
	
//	@Length(min=0, max=25, message="被保险人车辆驾驶人身份证号长度必须介于 0 和 25 之间")
	public String getInsuredVehicleDriverNumber() {
		return insuredVehicleDriverNumber;
	}

	public void setInsuredVehicleDriverNumber(String insuredVehicleDriverNumber) {
		this.insuredVehicleDriverNumber = insuredVehicleDriverNumber;
	}
	
//	@Length(min=0, max=255, message="被保险人车辆驾驶人联系电话长度必须介于 0 和 255 之间")
	public String getInsuredVehicleDriverTelephone() {
		return insuredVehicleDriverTelephone;
	}

	public void setInsuredVehicleDriverTelephone(String insuredVehicleDriverTelephone) {
		this.insuredVehicleDriverTelephone = insuredVehicleDriverTelephone;
	}
	
//	@Length(min=0, max=255, message="被保险人车辆驾驶人责任长度必须介于 0 和 255 之间")
	public String getInsuredVehicleDriverLiability() {
		return insuredVehicleDriverLiability;
	}

	public void setInsuredVehicleDriverLiability(String insuredVehicleDriverLiability) {
		this.insuredVehicleDriverLiability = insuredVehicleDriverLiability;
	}
	
//	@Length(min=0, max=255, message="受害方车辆驾驶人长度必须介于 0 和 255 之间")
	public String getVehicleDriverInsuredParty() {
		return vehicleDriverInsuredParty;
	}

	public void setVehicleDriverInsuredParty(String vehicleDriverInsuredParty) {
		this.vehicleDriverInsuredParty = vehicleDriverInsuredParty;
	}
	
//	@Length(min=0, max=11, message="受害方车辆车牌号长度必须介于 0 和 11 之间")
	public String getVehicleDriverInsuredLicense() {
		return vehicleDriverInsuredLicense;
	}

	public void setVehicleDriverInsuredLicense(String vehicleDriverInsuredLicense) {
		this.vehicleDriverInsuredLicense = vehicleDriverInsuredLicense;
	}
	
//	@Length(min=0, max=255, message="受害方车辆驾驶人责任长度必须介于 0 和 255 之间")
	public String getVehicleDriverInsuredResponsibility() {
		return vehicleDriverInsuredResponsibility;
	}

	public void setVehicleDriverInsuredResponsibility(String vehicleDriverInsuredResponsibility) {
		this.vehicleDriverInsuredResponsibility = vehicleDriverInsuredResponsibility;
	}
	
//	@Length(min=0, max=255, message="受害方车辆被保险人长度必须介于 0 和 255 之间")
	public String getVehicleInsuredVictim() {
		return vehicleInsuredVictim;
	}

	public void setVehicleInsuredVictim(String vehicleInsuredVictim) {
		this.vehicleInsuredVictim = vehicleInsuredVictim;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getAccidentJudgmentDate() {
		return accidentJudgmentDate;
	}

	public void setAccidentJudgmentDate(Date accidentJudgmentDate) {
		this.accidentJudgmentDate = accidentJudgmentDate;
	}
	
//	@Length(min=0, max=255, message="交通事故判决法院长度必须介于 0 和 255 之间")
	public String getCourtOfJudgment() {
		return courtOfJudgment;
	}

	public void setCourtOfJudgment(String courtOfJudgment) {
		this.courtOfJudgment = courtOfJudgment;
	}
	
//	@Length(min=0, max=255, message="交通事故判决案号长度必须介于 0 和 255 之间")
	public String getJudgmentNumber() {
		return judgmentNumber;
	}

	public void setJudgmentNumber(String judgmentNumber) {
		this.judgmentNumber = judgmentNumber;
	}
	
//	@Length(min=0, max=255, message="交通事故判决金额长度必须介于 0 和 255 之间")
	public String getJudgmentAmount() {
		return judgmentAmount;
	}

	public void setJudgmentAmount(String judgmentAmount) {
		this.judgmentAmount = judgmentAmount;
	}
	
//	@Length(min=0, max=255, message="追偿案件诉讼费金额长度必须介于 0 和 255 之间")
	public String getRecoveryCasesAmount() {
		return recoveryCasesAmount;
	}

	public void setRecoveryCasesAmount(String recoveryCasesAmount) {
		this.recoveryCasesAmount = recoveryCasesAmount;
	}
	
	public String getReasonsRecourse() {
		return reasonsRecourse;
	}

	public void setReasonsRecourse(String reasonsRecourse) {
		this.reasonsRecourse = reasonsRecourse;
	}
	
	public String getCaseIntroduction() {
		return caseIntroduction;
	}

	public void setCaseIntroduction(String caseIntroduction) {
		this.caseIntroduction = caseIntroduction;
	}
	
//	@Length(min=0, max=10, message="已还金额长度必须介于 0 和 10 之间")
	public String getAmountPaidBack() {
		return amountPaidBack;
	}

	public void setAmountPaidBack(String amountPaidBack) {
		this.amountPaidBack = amountPaidBack;
	}
	
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getInsuranceCompanyTime() {
		return insuranceCompanyTime;
	}

	public void setInsuranceCompanyTime(Date insuranceCompanyTime) {
		this.insuranceCompanyTime = insuranceCompanyTime;
	}
	
//	@Length(min=0, max=255, message="保险公司理赔金额长度必须介于 0 和 255 之间")
	public String getClaimAmountInsuranceCompany() {
		return claimAmountInsuranceCompany;
	}

	public void setClaimAmountInsuranceCompany(String claimAmountInsuranceCompany) {
		this.claimAmountInsuranceCompany = claimAmountInsuranceCompany;
	}

//	@Length(min=0, max=255, message="致害人车辆车主长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwner() {
		return victimVehicleOwner;
	}

	public void setVictimVehicleOwner(String victimVehicleOwner) {
		this.victimVehicleOwner = victimVehicleOwner;
	}

//	@Length(min=0, max=655, message="致害人车辆车主住所地长度必须介于 0 和 655 之间")
	public String getVictimVehicleOwnerHousehlod() {
		return victimVehicleOwnerHousehlod;
	}

	public void setVictimVehicleOwnerHousehlod(String victimVehicleOwnerHousehlod) {
		this.victimVehicleOwnerHousehlod = victimVehicleOwnerHousehlod;
	}

//	@Length(min=0, max=255, message="致害人车辆车主信用代码长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerIdcard() {
		return victimVehicleOwnerIdcard;
	}

	public void setVictimVehicleOwnerIdcard(String victimVehicleOwnerIdcard) {
		this.victimVehicleOwnerIdcard = victimVehicleOwnerIdcard;
	}

//	@Length(min=0, max=255, message="致害人车辆车主负责人长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerResponsiblity() {
		return victimVehicleOwnerResponsiblity;
	}

	public void setVictimVehicleOwnerResponsiblity(String victimVehicleOwnerResponsiblity) {
		this.victimVehicleOwnerResponsiblity = victimVehicleOwnerResponsiblity;
	}

//	@Length(min=0, max=255, message="致害人车辆车主负责人职位长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerDuty() {
		return victimVehicleOwnerDuty;
	}

	public void setVictimVehicleOwnerDuty(String victimVehicleOwnerDuty) {
		this.victimVehicleOwnerDuty = victimVehicleOwnerDuty;
	}

//	@Length(min=0, max=255, message="致害人车辆车主联系电话长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerTelephone() {
		return victimVehicleOwnerTelephone;
	}

	public void setVictimVehicleOwnerTelephone(String victimVehicleOwnerTelephone) {
		this.victimVehicleOwnerTelephone = victimVehicleOwnerTelephone;
	}

//	@Length(min=0, max=255, message="致害人车牌号长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerLicense() {
		return victimVehicleOwnerLicense;
	}

	public void setVictimVehicleOwnerLicense(String victimVehicleOwnerLicense) {
		this.victimVehicleOwnerLicense = victimVehicleOwnerLicense;
	}

//	@Length(min=0, max=255, message="致害人车辆车主民族长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerPeople() {
		return victimVehicleOwnerPeople;
	}

	public void setVictimVehicleOwnerPeople(String victimVehicleOwnerPeople) {
		this.victimVehicleOwnerPeople = victimVehicleOwnerPeople;
	}

//	@Length(min=0, max=255, message="致害人车辆车主身份证号长度必须介于 0 和 255 之间")
	public String getVictimVehicleOwnerNumber() {
		return victimVehicleOwnerNumber;
	}

	public void setVictimVehicleOwnerNumber(String victimVehicleOwnerNumber) {
		this.victimVehicleOwnerNumber = victimVehicleOwnerNumber;
	}

//	@Length(min=0, max=1, message="致害人车辆车主性别长度必须介于 0 和 1 之间")
	public String getVictimVehicleOwnerSex() {
		return victimVehicleOwnerSex;
	}

	public void setVictimVehicleOwnerSex(String victimVehicleOwnerSex) {
		this.victimVehicleOwnerSex = victimVehicleOwnerSex;
	}

//	@Length(min=0, max=25, message="致害人车辆车主生日长度必须介于 0 和 25 之间")
	public String getVictimVehicleOwnerBith() {
		return victimVehicleOwnerBith;
	}

	public void setVictimVehicleOwnerBith(String victimVehicleOwnerBith) {
		this.victimVehicleOwnerBith = victimVehicleOwnerBith;
	}

//	@Length(min=0, max=255, message="致害人车辆驾驶人长度必须介于 0 和 255 之间")
	public String getVictimDriverHarmful() {
		return victimDriverHarmful;
	}

	public void setVictimDriverHarmful(String victimDriverHarmful) {
		this.victimDriverHarmful = victimDriverHarmful;
	}

//	@Length(min=0, max=255, message="致害人车辆驾驶人民族长度必须介于 0 和 255 之间")
	public String getVictimDriverHarmfulPeople() {
		return victimDriverHarmfulPeople;
	}

	public void setVictimDriverHarmfulPeople(String victimDriverHarmfulPeople) {
		this.victimDriverHarmfulPeople = victimDriverHarmfulPeople;
	}

//	@Length(min=0, max=1024, message="致害人车辆驾驶人户籍地长度必须介于 0 和 1024 之间")
	public String getVictimDriverHarmfulHousehlod() {
		return victimDriverHarmfulHousehlod;
	}

	public void setVictimDriverHarmfulHousehlod(String victimDriverHarmfulHousehlod) {
		this.victimDriverHarmfulHousehlod = victimDriverHarmfulHousehlod;
	}

//	@Length(min=0, max=255, message="致害人车辆驾驶人身份证号长度必须介于 0 和 255 之间")
	public String getVictimDriverHarmfulIdcard() {
		return victimDriverHarmfulIdcard;
	}

	public void setVictimDriverHarmfulIdcard(String victimDriverHarmfulIdcard) {
		this.victimDriverHarmfulIdcard = victimDriverHarmfulIdcard;
	}

//	@Length(min=0, max=255, message="致害人车辆驾驶人联系电话长度必须介于 0 和 255 之间")
	public String getVictimDriverHarmfulTelephone() {
		return victimDriverHarmfulTelephone;
	}

	public void setVictimDriverHarmfulTelephone(String victimDriverHarmfulTelephone) {
		this.victimDriverHarmfulTelephone = victimDriverHarmfulTelephone;
	}

	public String getCreditor() {
		return creditor;
	}

	public void setCreditor(String creditor) {
		this.creditor = creditor;
	}

	public String getDeptSearch() {
		return deptSearch;
	}

	public void setDeptSearch(String deptSearch) {
		this.deptSearch = deptSearch;
	}

	public String getCaseTypeSeache() {
		return caseTypeSeache;
	}

	public void setCaseTypeSeache(String caseTypeSeache) {
		this.caseTypeSeache = caseTypeSeache;
	}

	public String getCkeckAllFlag() {
		return ckeckAllFlag;
	}

	public void setCkeckAllFlag(String ckeckAllFlag) {
		this.ckeckAllFlag = ckeckAllFlag;
	}

	public String getInsuranceSubrogation() {
		return insuranceSubrogation;
	}

	public void setInsuranceSubrogation(String insuranceSubrogation) {
		this.insuranceSubrogation = insuranceSubrogation;
	}

	public CaseCustomer getCaseCustomer() {
		return caseCustomer;
	}

	public void setCaseCustomer(CaseCustomer caseCustomer) {
		this.caseCustomer = caseCustomer;
	}

	public CaseSpeed getCaseSpeed() {
		return caseSpeed;
	}

	public void setCaseSpeed(CaseSpeed caseSpeed) {
		this.caseSpeed = caseSpeed;
	}

	public String getCivilIndictment() {
		return civilIndictment;
	}

	public void setCivilIndictment(String civilIndictment) {
		this.civilIndictment = civilIndictment;
	}

	public String getGrantAuthorization() {
		return grantAuthorization;
	}

	public void setGrantAuthorization(String grantAuthorization) {
		this.grantAuthorization = grantAuthorization;
	}

	public String getCatalogueEvidence() {
		return catalogueEvidence;
	}

	public void setCatalogueEvidence(String catalogueEvidence) {
		this.catalogueEvidence = catalogueEvidence;
	}

	public String getInsuredVehicleDriverSex() {
		return insuredVehicleDriverSex;
	}

	public void setInsuredVehicleDriverSex(String insuredVehicleDriverSex) {
		this.insuredVehicleDriverSex = insuredVehicleDriverSex;
	}

	public String getInsuredVehicleDriverBith() {
		return insuredVehicleDriverBith;
	}

	public void setInsuredVehicleDriverBith(String insuredVehicleDriverBith) {
		this.insuredVehicleDriverBith = insuredVehicleDriverBith;
	}

	public String getIsCompany() {
		return isCompany;
	}

	public void setIsCompany(String isCompany) {
		this.isCompany = isCompany;
	}

	public String getInjuriousInsuranceCompany() {
		return injuriousInsuranceCompany;
	}

	public void setInjuriousInsuranceCompany(String injuriousInsuranceCompany) {
		this.injuriousInsuranceCompany = injuriousInsuranceCompany;
	}

	public String getInjuriousCompanyTelephone() {
		return injuriousCompanyTelephone;
	}

	public void setInjuriousCompanyTelephone(String injuriousCompanyTelephone) {
		this.injuriousCompanyTelephone = injuriousCompanyTelephone;
	}

	public String getInsuranceClaimsContractors() {
		return insuranceClaimsContractors;
	}

	public void setInsuranceClaimsContractors(String insuranceClaimsContractors) {
		this.insuranceClaimsContractors = insuranceClaimsContractors;
	}

	public String getInsuranceClaimsTelephone() {
		return insuranceClaimsTelephone;
	}

	public void setInsuranceClaimsTelephone(String insuranceClaimsTelephone) {
		this.insuranceClaimsTelephone = insuranceClaimsTelephone;
	}

	public String getCreditorAmount() {
		return creditorAmount;
	}

	public void setCreditorAmount(String creditorAmount) {
		this.creditorAmount = creditorAmount;
	}

	public String getVictimDriverHarmfulResponsibility() {
		return victimDriverHarmfulResponsibility;
	}

	public void setVictimDriverHarmfulResponsibility(String victimDriverHarmfulResponsibility) {
		this.victimDriverHarmfulResponsibility = victimDriverHarmfulResponsibility;
	}

	public String getRecoveryAmount() {
		return recoveryAmount;
	}

	public void setRecoveryAmount(String recoveryAmount) {
		this.recoveryAmount = recoveryAmount;
	}


	public String getVictimDriverHarmfulSex() {
		return victimDriverHarmfulSex;
	}

	public void setVictimDriverHarmfulSex(String victimDriverHarmfulSex) {
		this.victimDriverHarmfulSex = victimDriverHarmfulSex;
	}

	public String getVictimDriverHarmfulBith() {
		return victimDriverHarmfulBith;
	}

	public void setVictimDriverHarmfulBith(String victimDriverHarmfulBith) {
		this.victimDriverHarmfulBith = victimDriverHarmfulBith;
	}

	public String getRatioShow() {
		return ratioShow;
	}

	public void setRatioShow(String ratioShow) {
		this.ratioShow = ratioShow;
	}

	public String getInsuredCommercialInsurance() {
		return insuredCommercialInsurance;
	}

	public void setInsuredCommercialInsurance(String insuredCommercialInsurance) {
		this.insuredCommercialInsurance = insuredCommercialInsurance;
	}

	public String getSpecialDebtorInformation() {
		return specialDebtorInformation;
	}

	public void setSpecialDebtorInformation(String specialDebtorInformation) {
		this.specialDebtorInformation = specialDebtorInformation;
	}

	public String getSpecialCssreditorAmount() {
		return specialCssreditorAmount;
	}

	public void setSpecialCssreditorAmount(String specialCssreditorAmount) {
		this.specialCssreditorAmount = specialCssreditorAmount;
	}

	public String getSpecialCaseIntroduction() {
		return specialCaseIntroduction;
	}

	public void setSpecialCaseIntroduction(String specialCaseIntroduction) {
		this.specialCaseIntroduction = specialCaseIntroduction;
	}

	public String getSpecialAmountPaidBack() {
		return specialAmountPaidBack;
	}

	public void setSpecialAmountPaidBack(String specialAmountPaidBack) {
		this.specialAmountPaidBack = specialAmountPaidBack;
	}

	public String getAcceptanceNumebr() {
		return acceptanceNumebr;
	}

	public void setAcceptanceNumebr(String acceptanceNumebr) {
		this.acceptanceNumebr = acceptanceNumebr;
	}

	public CaseXbbInsurance getCaseXbbInsurance() {
		return caseXbbInsurance;
	}

	public void setCaseXbbInsurance(CaseXbbInsurance caseXbbInsurance) {
		this.caseXbbInsurance = caseXbbInsurance;
	}

	public String getCaseNumberSearch() {
		return caseNumberSearch;
	}

	public void setCaseNumberSearch(String caseNumberSearch) {
		this.caseNumberSearch = caseNumberSearch;
	}


	public String getReportNumSearch() {
		return reportNumSearch;
	}

	public void setReportNumSearch(String reportNumSearch) {
		this.reportNumSearch = reportNumSearch;
	}

	public String getVictimVehicleOwenrIsCompany() {
		return victimVehicleOwenrIsCompany;
	}

	public void setVictimVehicleOwenrIsCompany(String victimVehicleOwenrIsCompany) {
		this.victimVehicleOwenrIsCompany = victimVehicleOwenrIsCompany;
	}

	public int getLayerNumber() {
		return layerNumber;
	}

	public void setLayerNumber(int layerNumber) {
		this.layerNumber = layerNumber;
	}

	public String getCaseNameSearch() {
		return caseNameSearch;
	}

	public void setCaseNameSearch(String caseNameSearch) {
		this.caseNameSearch = caseNameSearch;
	}

	public String getLayerNumberOne() {
		return layerNumberOne;
	}

	public void setLayerNumberOne(String layerNumberOne) {
		this.layerNumberOne = layerNumberOne;
	}

	public String getLayerNumberTow() {
		return layerNumberTow;
	}

	public void setLayerNumberTow(String layerNumberTow) {
		this.layerNumberTow = layerNumberTow;
	}

	public String getAcceptanceNumebrOld() {
		return acceptanceNumebrOld;
	}

	public void setAcceptanceNumebrOld(String acceptanceNumebrOld) {
		this.acceptanceNumebrOld = acceptanceNumebrOld;
	}


	public String getCreditorSearch() {
		return creditorSearch;
	}

	public void setCreditorSearch(String creditorSearch) {
		this.creditorSearch = creditorSearch;
	}
}