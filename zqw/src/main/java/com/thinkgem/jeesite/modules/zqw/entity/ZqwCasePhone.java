/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 电话催收Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwCasePhone extends DataEntity<ZqwCasePhone> {
	
	private static final long serialVersionUID = 1L;
	private String caseId;		// case_id
	private String phoneContent;		// phone_content
	private String phoneTime;		// phone_time
	
	public ZqwCasePhone() {
		super();
	}

	public ZqwCasePhone(String id){
		super(id);
	}

	@Length(min=0, max=64, message="case_id长度必须介于 0 和 64 之间")
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	public String getPhoneContent() {
		return phoneContent;
	}

	public void setPhoneContent(String phoneContent) {
		this.phoneContent = phoneContent;
	}
	
	@Length(min=0, max=64, message="phone_time长度必须介于 0 和 64 之间")
	public String getPhoneTime() {
		return phoneTime;
	}

	public void setPhoneTime(String phoneTime) {
		this.phoneTime = phoneTime;
	}
	
}