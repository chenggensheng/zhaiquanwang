/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.List;

import com.thinkgem.jeesite.modules.zqwcase.entity.CaseXbbInsurance;
import com.thinkgem.jeesite.modules.zqwcase.entity.XbbCommunicate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.CasePayment;
import com.thinkgem.jeesite.modules.zqwcase.dao.CasePaymentDao;

/**
 * 肖邦邦合同回款管理Service
 * @author coder_cheng@126.com
 * @version 2019-09-03
 */
@Service
@Transactional(readOnly = true)
public class CasePaymentService extends CrudService<CasePaymentDao, CasePayment> {

	public CasePayment get(String id) {
		return super.get(id);
	}
	
	public List<CasePayment> findList(CasePayment casePayment) {
		return super.findList(casePayment);
	}
	
	public Page<CasePayment> findPage(Page<CasePayment> page, CasePayment casePayment) {
		return super.findPage(page, casePayment);
	}
	
	@Transactional(readOnly = false)
	public void save(CasePayment casePayment) {
		super.save(casePayment);
	}
	
	@Transactional(readOnly = false)
	public void delete(CasePayment casePayment) {
		super.delete(casePayment);
	}



	@Transactional(readOnly = false)
	public void saveFromCrm(List<CaseXbbInsurance> caseXbbInsurances, List<CasePayment> casePayments){
		for (CasePayment casePayment: casePayments){
			for (CaseXbbInsurance caseXbbInsurance : caseXbbInsurances){
				if (casePayment.getContractId().equals(caseXbbInsurance.getId())){
					CasePayment casePaymentTemp = dao.get(casePayment.getId());
					if (casePaymentTemp!=null){
						save(casePayment);
					}else {
						saveInsert(casePayment);
					}
				}
			}
		}

	}

	@Transactional(readOnly = false)
	public void saveInsert(CasePayment casePayment) {
		dao.insert(casePayment);
	}
	
}