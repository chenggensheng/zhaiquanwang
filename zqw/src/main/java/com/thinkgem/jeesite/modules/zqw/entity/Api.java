package com.thinkgem.jeesite.modules.zqw.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class Api  extends DataEntity<DeptAction> {

    private static final long serialVersionUID = 1L;
    private String apiType;		// 债权类型
    private String idCard;  //身份证
    private String name;//姓名
    private String licensePlateType;
    private String carNum;
    private String cellphone;//手机号
    private String cycle;

    private String bankcard; //银行卡


    public String getBankcard() {
        return bankcard;
    }

    public void setBankcard(String bankcard) {
        this.bankcard = bankcard;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getLicensePlateType() {
        return licensePlateType;
    }

    public void setLicensePlateType(String licensePlateType) {
        this.licensePlateType = licensePlateType;
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getApiType() {
        return apiType;
    }

    public void setApiType(String apiType) {
        this.apiType = apiType;
    }
}
