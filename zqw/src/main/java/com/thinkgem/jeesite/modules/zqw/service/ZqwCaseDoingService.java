/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseDoing;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCaseDoingDao;

/**
 * 案件进展Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCaseDoingService extends CrudService<ZqwCaseDoingDao, ZqwCaseDoing> {

	public ZqwCaseDoing get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCaseDoing> findList(ZqwCaseDoing zqwCaseDoing) {
		return super.findList(zqwCaseDoing);
	}
	
	public Page<ZqwCaseDoing> findPage(Page<ZqwCaseDoing> page, ZqwCaseDoing zqwCaseDoing) {
		return super.findPage(page, zqwCaseDoing);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCaseDoing zqwCaseDoing) {
		super.save(zqwCaseDoing);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCaseDoing zqwCaseDoing) {
		super.delete(zqwCaseDoing);
	}
	
}