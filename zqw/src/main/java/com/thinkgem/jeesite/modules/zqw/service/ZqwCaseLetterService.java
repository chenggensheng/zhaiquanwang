/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseLetter;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCaseLetterDao;

/**
 * 律师函发送及反馈Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCaseLetterService extends CrudService<ZqwCaseLetterDao, ZqwCaseLetter> {

	public ZqwCaseLetter get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCaseLetter> findList(ZqwCaseLetter zqwCaseLetter) {
		return super.findList(zqwCaseLetter);
	}
	
	public Page<ZqwCaseLetter> findPage(Page<ZqwCaseLetter> page, ZqwCaseLetter zqwCaseLetter) {
		return super.findPage(page, zqwCaseLetter);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCaseLetter zqwCaseLetter) {
		super.save(zqwCaseLetter);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCaseLetter zqwCaseLetter) {
		super.delete(zqwCaseLetter);
	}
	
}