/**
/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.common.xbb.helper.XbbException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseXbbInsurance;
import com.thinkgem.jeesite.modules.zqwcase.service.CaseXbbInsuranceService;

/**
 * 肖邦邦案件管理Controller
 * @author coder_cheng@126.com
 * @version 2019-07-20
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/caseXbbInsurance")
public class CaseXbbInsuranceController extends BaseController {

	@Autowired
	private CaseXbbInsuranceService caseXbbInsuranceService;
	
	@ModelAttribute
	public CaseXbbInsurance get(@RequestParam(required=false) String id) {
		CaseXbbInsurance entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = caseXbbInsuranceService.get(id);
		}
		if (entity == null){
			entity = new CaseXbbInsurance();
		}
		return entity;
	}
	
	@RequiresPermissions("zqwcase:caseXbbInsurance:view")
	@RequestMapping(value = {"list", ""})
	public String list(CaseXbbInsurance caseXbbInsurance, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CaseXbbInsurance> page = caseXbbInsuranceService.findPage(new Page<CaseXbbInsurance>(request, response), caseXbbInsurance); 
		model.addAttribute("page", page);
		return "modules/zqwcase/caseXbbInsuranceList";
	}

	@RequiresPermissions("zqwcase:caseXbbInsurance:view")
	@RequestMapping(value = "form")
	public String form(CaseXbbInsurance caseXbbInsurance, Model model) {
		model.addAttribute("caseXbbInsurance", caseXbbInsurance);
		return "modules/zqwcase/caseXbbInsuranceForm";
	}

	@RequiresPermissions("zqwcase:caseXbbInsurance:edit")
	@RequestMapping(value = "save")
	public String save(CaseXbbInsurance caseXbbInsurance, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, caseXbbInsurance)){
			return form(caseXbbInsurance, model);
		}
		caseXbbInsuranceService.save(caseXbbInsurance);
		addMessage(redirectAttributes, "保存肖邦邦案件管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseXbbInsurance/?repage";
	}
	
	@RequiresPermissions("zqwcase:caseXbbInsurance:edit")
	@RequestMapping(value = "delete")
	public String delete(CaseXbbInsurance caseXbbInsurance, RedirectAttributes redirectAttributes) {
		caseXbbInsuranceService.delete(caseXbbInsurance);
		addMessage(redirectAttributes, "删除肖邦邦案件管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseXbbInsurance/?repage";
	}


	@ResponseBody
	@RequestMapping(value = "syncCrmData")
	public String syncCrmData(CaseXbbInsurance caseXbbInsurance) throws XbbException {
		caseXbbInsuranceService.save(caseXbbInsurance);

		String retrunString = CustomerApi.updateContranct(ConfigConstant.corpid,ConfigConstant.token,caseXbbInsurance, ConfigConstant.UPDATE_CONTACTS);
	    if (retrunString.contains("获取成功")||retrunString.contains("操作成功")){
	    	return "true";
		}else return "false";

	}

}