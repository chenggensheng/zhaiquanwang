/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.xbb.entity.XbbOpportunity;
import com.thinkgem.jeesite.modules.xbb.service.XbbOpportunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 肖帮帮销售计划管理Controller
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
@Controller
@RequestMapping(value = "${adminPath}/xbb/xbbOpportunity")
public class XbbOpportunityController extends BaseController {

	@Autowired
	private XbbOpportunityService xbbOpportunityService;
	
	@ModelAttribute
	public XbbOpportunity get(@RequestParam(required=false) String id) {
		XbbOpportunity entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xbbOpportunityService.get(id);
		}
		if (entity == null){
			entity = new XbbOpportunity();
		}
		return entity;
	}


//	@RequiresPermissions("xbb:xbbOpportunity:view")
	@RequestMapping(value = {"list", ""})
	public String list(XbbOpportunity xbbOpportunity, HttpServletRequest request, HttpServletResponse response, Model model) {
	    Page<XbbOpportunity> page = xbbOpportunityService.findPage(new Page<XbbOpportunity>(request, response), xbbOpportunity);
		model.addAttribute("page", page);
		return "modules/xbb/xbbOpportunityList";

	}

    @ResponseBody
//	@RequiresPermissions("xbb:xbbOpportunity:view")
	@RequestMapping(value = "listJSON")
	public Map<String, Object> listJSON(XbbOpportunity xbbOpportunity, HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		Page<XbbOpportunity> page = xbbOpportunityService.findPage(new Page<XbbOpportunity>(request, response), xbbOpportunity);
		model.addAttribute("page", page);
		returnMap.put("total", page.getCount());
		returnMap.put("rows", page.getList());
		return returnMap;
	}

//	@RequiresPermissions("xbb:xbbOpportunity:view")
	@RequestMapping(value = "form")
	public String form(XbbOpportunity xbbOpportunity, Model model) {
		model.addAttribute("xbbOpportunity", xbbOpportunity);
		return "modules/xbb/xbbOpportunityForm";
	}

//	@RequiresPermissions("xbb:xbbOpportunity:edit")
	@RequestMapping(value = "save")
	public String save(XbbOpportunity xbbOpportunity, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, xbbOpportunity)){
			return form(xbbOpportunity, model);
		}
		xbbOpportunityService.save(xbbOpportunity);
		addMessage(redirectAttributes, "保存肖帮帮销售计划管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbOpportunity/?repage";
	}
	
//	@RequiresPermissions("xbb:xbbOpportunity:edit")
	@RequestMapping(value = "delete")
	public String delete(XbbOpportunity xbbOpportunity, RedirectAttributes redirectAttributes) {
		xbbOpportunityService.delete(xbbOpportunity);
		addMessage(redirectAttributes, "删除肖帮帮销售计划管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbOpportunity/?repage";
	}

}