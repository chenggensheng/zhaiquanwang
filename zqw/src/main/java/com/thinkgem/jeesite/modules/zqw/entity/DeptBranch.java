/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 曝光台Entity
 * @author coder_cheng@126.com
 * @version 2018-10-22
 */
public class DeptBranch extends DataEntity<DeptBranch> {
	
	private static final long serialVersionUID = 1L;
	private String deptType;		// 债权类型
	private String deptName;		// 债务人名称
	private String deptPrice;		// 债权金额
	private Date flluTime;		// 发布时间
	private String deptNum;		// 债权编号
	
	public DeptBranch() {
		super();
	}

	public DeptBranch(String id){
		super(id);
	}

	@Length(min=0, max=1, message="债权类型长度必须介于 0 和 1 之间")
	public String getDeptType() {
		return deptType;
	}

	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}
	
	@Length(min=0, max=10240, message="债务人名称长度必须介于 0 和 10240 之间")
	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
	@Length(min=0, max=1024, message="债权金额长度必须介于 0 和 1024 之间")
	public String getDeptPrice() {
		return deptPrice;
	}

	public void setDeptPrice(String deptPrice) {
		this.deptPrice = deptPrice;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getFlluTime() {
		return flluTime;
	}

	public void setFlluTime(Date flluTime) {
		this.flluTime = flluTime;
	}
	
	@Length(min=0, max=64, message="债权编号长度必须介于 0 和 64 之间")
	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}
	
}