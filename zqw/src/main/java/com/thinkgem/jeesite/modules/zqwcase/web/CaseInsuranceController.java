/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.Encodes;
import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.common.xbb.helper.XbbException;
import com.thinkgem.jeesite.modules.zqw.utils.WordUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.*;
import com.thinkgem.jeesite.modules.zqwcase.service.*;
import com.thinkgem.jeesite.modules.zqwcase.utils.ExcleUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 案件信息Controller
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/caseInsurance")
public class CaseInsuranceController extends BaseController {

	@Autowired
	private CaseInsuranceService caseInsuranceService;
	@Autowired
	private CaseCustomerService caseCustomerService;
	@Autowired
	private CaseSpeedService caseSpeedService;

	@Autowired
	private CaseXbbInsuranceService caseXbbInsuranceService;

	@Autowired
	private CaseTimeUpService caseTimeUpService;

	@Autowired
	private XbbCommunicateService xbbCommunicateService;

	@Autowired
	private CasePaymentService casePaymentService;


	@Autowired
	private ChangeLayerService changeLayerService;



	
	@ModelAttribute
	public CaseInsurance get(@RequestParam(required=false) String id) {
		CaseInsurance entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = caseInsuranceService.get(id);
		}
		if (entity == null){
			entity = new CaseInsurance();
		}
		return entity;
	}
	
//	@RequiresPermissions("zqwcase:caseInsurance:view")
	@RequestMapping(value = {"list", ""})
	public String list(CaseInsurance caseInsurance, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CaseInsurance> page = caseInsuranceService.findPage(new Page<CaseInsurance>(request, response), caseInsurance); 
		model.addAttribute("page", page);
		return "modules/zqwcase/caseInsuranceList";
	}


	@RequestMapping(value = "showTypeList")
	public String showTypeList(String showType,HttpServletRequest request,HttpServletResponse response,Model model){
		List<CaseXbbInsurance> caseXbbInsurances = caseInsuranceService.findTypeList(showType);
		model.addAttribute("caseXbbInsurances",caseXbbInsurances);
		return "modules/zqwcase/caseInsuranceShowTypeList";

	}


//	@RequiresPermissions("zqwcase:caseInsurance:view")
	@RequestMapping(value = "form")
	public String form(CaseInsurance caseInsurance, Model model,HttpServletRequest request) {

		if (StringUtils.isBlank(caseInsurance.getId())){
			caseInsurance.setCaseType("0");
		}

		String pageSize = request.getParameter("pageSize");
		String  pageNo = request.getParameter("pageNo");

		String caseCustomerId = caseInsurance.getCaseCustomerId();
		if (StringUtils.isNotBlank(caseCustomerId)){
			CaseCustomer caseCustomer = caseCustomerService.get(caseCustomerId);
			model.addAttribute("caseCustomer",caseCustomer);
			caseInsurance.setCaseCustomer(caseCustomer);
		}
		model.addAttribute("caseInsurance", caseInsurance);
		model.addAttribute("pageSize",pageSize);
		model.addAttribute("pageNo",pageNo);
		return "modules/zqwcase/caseInsuranceForm";
	}

	@RequestMapping(value = "view")
	public String view(CaseInsurance caseInsurance, Model model) {

		if (StringUtils.isBlank(caseInsurance.getId())){
			caseInsurance.setCaseType("0");
		}

		String caseCustomerId = caseInsurance.getCaseCustomerId();
		if (StringUtils.isNotBlank(caseCustomerId)){
			CaseCustomer caseCustomer = caseCustomerService.get(caseCustomerId);
			model.addAttribute("caseCustomer",caseCustomer);
			caseInsurance.setCaseCustomer(caseCustomer);
		}
		model.addAttribute("caseInsurance", caseInsurance);
		return "modules/zqwcase/caseInsuranceViewForm";
	}

	@RequestMapping(value = "exportForm")
	public String exportForm(CaseInsurance caseInsurance, Model model) {
		return "modules/zqwcase/exportForm";
	}

//	@RequiresPermissions("zqwcase:caseInsurance:edit")R125105282018809041
	@RequestMapping(value = "save")
	public String save(CaseInsurance caseInsurance, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request,HttpServletResponse response) {
		if (!beanValidator(model, caseInsurance)){
			return form(caseInsurance, model,request);
		}
		String saveFlag = request.getParameter("saveFlag");
		String pageNo = request.getParameter("pageNo");
		String pageSize = request.getParameter("pageSize");


		String creditorSearch = request.getParameter("creditorSearch");
		String caseTypeSeache = request.getParameter("caseTypeSeache");
		String deptSearch = request.getParameter("deptSearch");
		String caseNameSearch = request.getParameter("caseNameSearch");
		String reportNumSearch = request.getParameter("reportNumSearch");
		String caseNumberSearch = request.getParameter("caseNumberSearch");

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("pageNo=").append(pageNo).append("&pageSize=").append(pageSize);
		if (StringUtils.isNotBlank(caseInsurance.getCreditorSearch())){
			stringBuilder.append("&creditor=").append(Encodes.urlEncode(caseInsurance.getCreditorSearch()));
		}
		if (StringUtils.isNotBlank(caseInsurance.getCaseTypeSeache())){
			stringBuilder.append("&caseTypeSeache=").append(caseInsurance.getCaseTypeSeache());
		}
		if (StringUtils.isNotBlank(caseInsurance.getDeptSearch())){
			stringBuilder.append("&deptSearch=").append(caseInsurance.getDeptSearch());
		}
		if (StringUtils.isNotBlank(caseInsurance.getCaseNameSearch())){
			stringBuilder.append("&caseNameSearch=").append(Encodes.urlEncode(caseInsurance.getCaseNameSearch()));
		}
		if (StringUtils.isNotBlank(caseInsurance.getReportNumSearch())){
			stringBuilder.append("&reportNumSearch=").append(caseInsurance.getReportNumSearch());
		}

		if (StringUtils.isNotBlank(caseInsurance.getCaseNumberSearch())){
			stringBuilder.append("&caseNumberSearch=").append(caseInsurance.getCaseNumberSearch());
		}

		caseInsuranceService.save(caseInsurance);
		addMessage(redirectAttributes, "保存案件信息成功");


		if (saveFlag.equals("0")){
			caseCustomerService.save(caseInsurance.getCaseCustomer());
			return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/?"+stringBuilder.toString();
		}
		else if (saveFlag.equals("3")){
			caseCustomerService.save(caseInsurance.getCaseCustomer());
			try {
				CaseCustomer caseCustomer = caseCustomerService.get(caseInsurance.getCaseCustomerId());
				CaseSpeed caseSpeed = new CaseSpeed();
				caseSpeed.setCaseInsuranceId(caseInsurance.getId());
				List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
				if (caseSpeeds !=null && caseSpeeds.size()>0){
					caseSpeed = caseSpeeds.get(0);
				}
				caseInsurance.setCaseCustomer(caseCustomer);
				caseInsurance.setCaseSpeed(caseSpeed);
				WordUtils.downloadZip(caseInsurance,response);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
//			return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/form?id="+caseInsurance.getId();

		}

		else {

			caseCustomerService.save(caseInsurance.getCaseCustomer());
			return "redirect:"+Global.getAdminPath()+"/zqwcase/caseSpeed/form?caseInsuranceId="+caseInsurance.getId()+"&caseCustomerId="+caseInsurance.getCaseCustomerId();
		}
	}

	@RequestMapping(value = "saveOther")
	public String saveOther(CaseInsurance caseInsurance, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request,HttpServletResponse response) {
		if (!beanValidator(model, caseInsurance)){
			return form(caseInsurance, model,request);
		}

			try {
				CaseCustomer caseCustomer = caseCustomerService.get(caseInsurance.getCaseCustomerId());
				CaseSpeed caseSpeed = new CaseSpeed();
				caseSpeed.setCaseInsuranceId(caseInsurance.getId());
				List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
				if (caseSpeeds !=null && caseSpeeds.size()>0){
					caseSpeed = caseSpeeds.get(0);
				}
				caseInsurance.setCaseCustomer(caseCustomer);
				caseInsurance.setCaseSpeed(caseSpeed);
				WordUtils.downloadZip(caseInsurance,response);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
//			return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/form?id="+caseInsurance.getId();
	}

	@RequestMapping(value = "exportLayerNum")
	public String exportLayerNum(CaseInsurance caseInsurance,HttpServletResponse response){

		List<CaseInsurance> caseInsurances = caseInsuranceService.findNumList(caseInsurance);
		try {
			ExcleUtils.exportExcle("律师函编号",caseInsurances,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "exprotCaseInsurance")
	public String exprotCaseInsurance(CaseInsurance caseInsurance,HttpServletResponse response){
		List<CaseInsurance> caseInsurances = caseInsuranceService.findList(caseInsurance);
		List<CaseInsurance> caseInsuranceList = Lists.newArrayList();
		for (CaseInsurance caseInsuranceTmep :caseInsurances){
			caseInsuranceTmep.setCaseCustomer(caseCustomerService.get(caseInsuranceTmep.getCaseCustomerId()));
			CaseSpeed caseSpeed = new CaseSpeed();
			caseSpeed.setCaseInsuranceId(caseInsuranceTmep.getId());
			List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
			if (caseSpeeds != null && caseSpeeds.size()>0){
				caseInsuranceTmep.setCaseSpeed(caseSpeeds.get(0));
			}
			caseInsuranceList.add(caseInsuranceTmep);
		}
		try {
			ExcleUtils.exportCaseExcle("保险案件",caseInsuranceList,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value = "exportCaseMaterial")
	public  String exportCaseMaterial(HttpServletResponse response){
		CaseInsuranceBoard caseInsuranceBoard = caseInsuranceService.caseMaterialBoard();

		String yearString = DateUtils.getYear();
		String mouthString = "";
		String dayString =  "";
		if (DateUtils.getDay().equals("01")){
			mouthString = DateUtils.getMonth();
			dayString = DateUtils.getDay();
		}else {
			mouthString =  DateUtils.getMonth();
			dayString = DateUtils.getLastDay(DateUtils.getNextDay(new Date()));
		}


		try {
			ExcleUtils.exportMaterialExcle("保险案件资料",yearString,mouthString,dayString,caseInsuranceBoard.getCaseInsurancePersonBoards(),response);
		} catch (Exception e) {
			e.printStackTrace();
		}

       return null;
	}

	//R125105282018809041
	//R125105282018809041
	@RequestMapping(value = "exportZip")
	public String exportZip(CaseInsurance caseInsurance, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request,HttpServletResponse response) {

		String ckeckAllFlag = request.getParameter("ckeckAllFlag");
		String insuranceSubrogations = request.getParameter("insuranceSubrogations");

		List<CaseInsurance> caseInsurances = Lists.newArrayList();

		List<CaseInsurance> exportsCaseInsurances = Lists.newArrayList();

		if (StringUtils.isNotBlank(ckeckAllFlag) && ckeckAllFlag.equals("all")){
			 caseInsurances = caseInsuranceService.findList(caseInsurance);
			 if (caseInsurances !=null && caseInsurances.size()>0){
				 for (CaseInsurance caseInsuranceTemp: caseInsurances){
					 CaseCustomer caseCustomer = caseCustomerService.get(caseInsuranceTemp.getCaseCustomerId());
					 CaseSpeed caseSpeed = new CaseSpeed();
					 caseSpeed.setCaseInsuranceId(caseInsuranceTemp.getId());
					 List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
					 if (caseSpeeds !=null && caseSpeeds.size()>0){
						 caseSpeed = caseSpeeds.get(0);
					 }
					 caseInsuranceTemp.setCaseCustomer(caseCustomer);
					 caseInsuranceTemp.setCaseSpeed(caseSpeed);
					 exportsCaseInsurances.add(caseInsuranceTemp);
				 }

			 }
		}else {
			String [] exprotsIds = request.getParameter("exprotIds").split(",");

			for (String id : exprotsIds){
				caseInsurance = caseInsuranceService.get(id);
				CaseCustomer caseCustomer = caseCustomerService.get(caseInsurance.getCaseCustomerId());
				CaseSpeed caseSpeed = new CaseSpeed();
				caseSpeed.setCaseInsuranceId(caseInsurance.getId());
				List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
				if (caseSpeeds !=null && caseSpeeds.size()>0){
					caseSpeed = caseSpeeds.get(0);
				}
				caseInsurance.setCaseCustomer(caseCustomer);
				caseInsurance.setCaseSpeed(caseSpeed);
				exportsCaseInsurances.add(caseInsurance);
			}
		}
			try {

				WordUtils.downloadZip(exportsCaseInsurances,insuranceSubrogations,response);
			} catch (Exception e) {
				e.printStackTrace();
			}
//			return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/form?id="+caseInsurance.getId();
             return null;
	}

	
//	@RequiresPermissions("zqwcase:caseInsurance:edit")
	@RequestMapping(value = "delete")
	public String delete(CaseInsurance caseInsurance, RedirectAttributes redirectAttributes) {
		caseInsuranceService.delete(caseInsurance);
		addMessage(redirectAttributes, "删除案件信息成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/?repage";
	}

	@RequestMapping(value = "caseSuperviseboard")
	public String caseSuperviseboard(CaseInsurance caseInsurance,Model model,RedirectAttributes redirectAttributes){
		CaseInsuranceBoard caseInsuranceBoard = caseInsuranceService.caseSuperviseboard();
		model.addAttribute("caseInsuranceBoard",caseInsuranceBoard);
		return "modules/zqwcase/caseSuperviseBoard";
	}

	@RequestMapping(value = "caseMaterialBoard")
	public String caseMaterialBoard(CaseInsurance caseInsurance, Model model,RedirectAttributes redirectAttributes){
		CaseInsuranceBoard caseInsuranceBoard = caseInsuranceService.caseMaterialBoard();
		model.addAttribute("caseInsuranceBoard",caseInsuranceBoard);
		return "modules/zqwcase/caseMaterialBoard";
	}



	@ResponseBody
	@RequestMapping(value = "allSync")
	public String allSync() throws XbbException {

		//1）查询所有CRM案件信息
		//2) 判断批处理系统中是否有相同的案件
		  //2.1) 有只创建xbb案件信息，
		  //2.2) 没有
		  // 2.2.1：创建客户信息
		  // 2.2.2：创建案件信息
		  // 2.2.3：创建xbb案件信息
		Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.CONTACT_LIST,ConfigConstant.contactListKey,null,null);
       List<CaseXbbInsurance> caseXbbInsurances = (List<CaseXbbInsurance>) returnMap.get("rows");
		int total = Integer.valueOf((String) returnMap.get("total"));
        int allPage = ((int)Math.ceil((double)total/(double)100));
        List<CaseInsurance> caseInsurances = caseInsuranceService.findList(new CaseInsurance());

		caseXbbInsuranceService.saveFromCrm(caseXbbInsurances,caseInsurances);


		for (int i =2 ;i<= allPage;i++){
			Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.CONTACT_LIST,ConfigConstant.contactListKey,null,null);
			List<CaseXbbInsurance> caseXbbInsuranceTemps = (List<CaseXbbInsurance>) returnMapTemp.get("rows");
			caseXbbInsuranceService.saveFromCrm(caseXbbInsuranceTemps,caseInsurances);
		}


		List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(new CaseTimeUp());
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			CaseTimeUp lastTime = caseTimeUps.get(0);
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}else {
			CaseTimeUp lastTime = new CaseTimeUp();
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}

		return "true";
	}
	@ResponseBody
	@RequestMapping(value = "incrementSyncAll")
	public String incrementSyncAll() throws Exception{




       if(true) {
		   String lastTimeString = "";
		   CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		   caseTimeUpSearch.setUpdateType("1");
		   List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		   if (caseTimeUps != null && caseTimeUps.size() > 0) {
			   lastTimeString = caseTimeUps.get(0).getLastDate();
		   } else {
			   lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
		   }
		   Map<String, Object> returnMap = CustomerApi.returnMap(ConfigConstant.corpid, ConfigConstant.token, 1, 100, ConfigConstant.CONTACT_LIST, ConfigConstant.contactListKey, null, lastTimeString);
		   List<CaseXbbInsurance> caseXbbInsurances = (List<CaseXbbInsurance>) returnMap.get("rows");
		   int total = Integer.valueOf((String) returnMap.get("total"));
		   int allPage = ((int) Math.ceil((double) total / (double) 100));
		   List<CaseInsurance> caseInsurances = caseInsuranceService.findList(new CaseInsurance());
		   caseXbbInsuranceService.saveFromCrm(caseXbbInsurances, caseInsurances);
		   for (int i = 2; i <= allPage; i++) {
			   Map<String, Object> returnMapTemp = CustomerApi.returnMap(ConfigConstant.corpid, ConfigConstant.token, i, 100, ConfigConstant.CONTACT_LIST, ConfigConstant.contactListKey, null, lastTimeString);
			   List<CaseXbbInsurance> caseXbbInsuranceTemps = (List<CaseXbbInsurance>) returnMapTemp.get("rows");
			   caseXbbInsuranceService.saveFromCrm(caseXbbInsuranceTemps, caseInsurances);
		   }
		   if (caseTimeUps != null && caseTimeUps.size() > 0) {
			   CaseTimeUp lastTime = caseTimeUps.get(0);
			   lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			   caseTimeUpService.save(lastTime);
		   } else {
			   CaseTimeUp lastTime = new CaseTimeUp();
			   lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			   caseTimeUpService.save(lastTime);
		   }
	   }

	   if (true){
		   String lastTimeString ="";
		   CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		   caseTimeUpSearch.setUpdateType("2");
		   List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		   if (caseTimeUps != null && caseTimeUps.size()>0) {
			   lastTimeString = caseTimeUps.get(0).getLastDate();
		   }else {
			   lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
		   }

		   Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.COMMUNICATE_LIST,ConfigConstant.communicateListKey,null,lastTimeString);
		   List<XbbCommunicate> xbbCommunicates = (List<XbbCommunicate>) returnMap.get("rows");
		   int total = Integer.valueOf((String) returnMap.get("total"));
		   int allPage = ((int)Math.ceil((double)total/(double)100));
		   xbbCommunicateService.saveFromCrm(xbbCommunicates);
		   for (int i =2 ;i<= allPage;i++){
			   Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.COMMUNICATE_LIST,ConfigConstant.communicateListKey,null,lastTimeString);
			   List<XbbCommunicate> xbbCommunicatesTemps = (List<XbbCommunicate>) returnMapTemp.get("rows");
			   xbbCommunicateService.saveFromCrm(xbbCommunicatesTemps);
		   }

		   if (caseTimeUps != null && caseTimeUps.size()>0) {
			   CaseTimeUp lastTime = caseTimeUps.get(0);
			   lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			   caseTimeUpService.save(lastTime);
		   }else {
			   CaseTimeUp lastTime = new CaseTimeUp();
			   lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			   caseTimeUpService.save(lastTime);
		   }
	   }

		if (true){
			String lastTimeString ="";
			CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
			caseTimeUpSearch.setUpdateType("3");
			List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
			if (caseTimeUps != null && caseTimeUps.size()>0) {
				lastTimeString = caseTimeUps.get(0).getLastDate();
			}else {
				lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
			}

			List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceService.findList(new CaseXbbInsurance());

			Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.PAYMENT_LIST,ConfigConstant.paymentListKey,null,lastTimeString);
			List<CasePayment> casePayments = (List<CasePayment>) returnMap.get("rows");
			int total = Integer.valueOf((String) returnMap.get("total"));
			int allPage = ((int)Math.ceil((double)total/(double)100));
			casePaymentService.saveFromCrm(caseXbbInsurances,casePayments);
			for (int i =2 ;i<= allPage;i++){
				Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.PAYMENT_LIST,ConfigConstant.paymentListKey,null,lastTimeString);
				List<CasePayment> casePaymentsTemps = (List<CasePayment>) returnMapTemp.get("rows");
				casePaymentService.saveFromCrm(caseXbbInsurances,casePaymentsTemps);
			}

			if (caseTimeUps != null && caseTimeUps.size()>0) {
				CaseTimeUp lastTime = caseTimeUps.get(0);
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}else {
				CaseTimeUp lastTime = new CaseTimeUp();
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}
		}

		return "true";
	}


	@ResponseBody
	@RequestMapping(value = "incrementSync")
	public String incrementSync()throws XbbException{
		//1）查询所有CRM案件信息
		//2) 判断批处理系统中是否有相同的案件
		//2.1) 有只创建xbb案件信息，
		//2.2) 没有
		// 2.2.1：创建客户信息
		// 2.2.2：创建案件信息
		// 2.2.3：创建xbb案件信息
		String lastTimeString ="";
		CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		caseTimeUpSearch.setUpdateType("1");
		List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			lastTimeString = caseTimeUps.get(0).getLastDate();
		}else {
			lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
		}
		Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.CONTACT_LIST,ConfigConstant.contactListKey,null,lastTimeString);
		List<CaseXbbInsurance> caseXbbInsurances = (List<CaseXbbInsurance>) returnMap.get("rows");
		int total = Integer.valueOf((String) returnMap.get("total"));
		int allPage = ((int)Math.ceil((double)total/(double)100));
		List<CaseInsurance> caseInsurances = caseInsuranceService.findList(new CaseInsurance());
		caseXbbInsuranceService.saveFromCrm(caseXbbInsurances,caseInsurances);
		for (int i =2 ;i<= allPage;i++){
			Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.CONTACT_LIST,ConfigConstant.contactListKey,null,lastTimeString);
			List<CaseXbbInsurance> caseXbbInsuranceTemps = (List<CaseXbbInsurance>) returnMapTemp.get("rows");
			caseXbbInsuranceService.saveFromCrm(caseXbbInsuranceTemps,caseInsurances);
		}
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			CaseTimeUp lastTime = caseTimeUps.get(0);
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}else {
			CaseTimeUp lastTime = new CaseTimeUp();
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}
		return "true";
	}



	@ResponseBody
	@RequestMapping(value = "communicateSync")
	public String communicateSync()throws XbbException{
		//2) 循环获取跟进记录
		//2.1) 处理跟进记录需要更新的时间，及保存跟进记录
		String lastTimeString ="";
		CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		caseTimeUpSearch.setUpdateType("2");
		List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			lastTimeString = caseTimeUps.get(0).getLastDate();
		}else {
			lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
		}

			Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.COMMUNICATE_LIST,ConfigConstant.communicateListKey,null,lastTimeString);
			List<XbbCommunicate> xbbCommunicates = (List<XbbCommunicate>) returnMap.get("rows");
			int total = Integer.valueOf((String) returnMap.get("total"));
			int allPage = ((int)Math.ceil((double)total/(double)100));
			xbbCommunicateService.saveFromCrm(xbbCommunicates);
			for (int i =2 ;i<= allPage;i++){
				Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.COMMUNICATE_LIST,ConfigConstant.communicateListKey,null,lastTimeString);
				List<XbbCommunicate> xbbCommunicatesTemps = (List<XbbCommunicate>) returnMapTemp.get("rows");
				xbbCommunicateService.saveFromCrm(xbbCommunicatesTemps);
			}

		if (caseTimeUps != null && caseTimeUps.size()>0) {
			CaseTimeUp lastTime = caseTimeUps.get(0);
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}else {
			CaseTimeUp lastTime = new CaseTimeUp();
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}
		return "true";
	}




	@ResponseBody
	@RequestMapping(value = "paymentSync")
	public String paymentSync()throws XbbException{
		//2) 循环获取跟进记录
		//2.1) 处理跟进记录需要更新的时间，及保存跟进记录
		String lastTimeString ="";
		CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		caseTimeUpSearch.setUpdateType("3");
		List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			lastTimeString = caseTimeUps.get(0).getLastDate();
		}else {
			lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
		}

		List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceService.findList(new CaseXbbInsurance());

		Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.PAYMENT_LIST,ConfigConstant.paymentListKey,null,lastTimeString);
		List<CasePayment> casePayments = (List<CasePayment>) returnMap.get("rows");
		int total = Integer.valueOf((String) returnMap.get("total"));
		int allPage = ((int)Math.ceil((double)total/(double)100));
		casePaymentService.saveFromCrm(caseXbbInsurances,casePayments);
		for (int i =2 ;i<= allPage;i++){
			Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.PAYMENT_LIST,ConfigConstant.paymentListKey,null,lastTimeString);
			List<CasePayment> casePaymentsTemps = (List<CasePayment>) returnMapTemp.get("rows");
			casePaymentService.saveFromCrm(caseXbbInsurances,casePaymentsTemps);
		}

		if (caseTimeUps != null && caseTimeUps.size()>0) {
			CaseTimeUp lastTime = caseTimeUps.get(0);
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}else {
			CaseTimeUp lastTime = new CaseTimeUp();
			lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
			caseTimeUpService.save(lastTime);
		}
		return "true";
	}



	@ResponseBody
	@RequestMapping(value = "changeLayers")
	public String changeLayers()throws XbbException{
		List<ChangeLayer> changeLayers = changeLayerService.findList(new ChangeLayer());
		changeLayerService.changeLayer(changeLayers);
		return "true";
	}


	@ResponseBody
	@RequestMapping(value = "incrementSaveAll")
	public String incrementSaveAll() throws  XbbException{


		Date lastTimeString =null;
		CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		caseTimeUpSearch.setUpdateType("4");
		List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			lastTimeString = caseTimeUps.get(0).getUpdateDateTime();
		}else {
			lastTimeString = new Date();
		}
		CaseXbbInsurance caseXbbInsurance = new CaseXbbInsurance();
		caseXbbInsurance.setUpdateDate(lastTimeString);
		List<CaseXbbInsurance> caseXbbInsurances = caseXbbInsuranceService.findNewList(caseXbbInsurance);
		caseInsuranceService.caseSuperviseboardSave(caseXbbInsurances);

		for (CaseXbbInsurance xbbInsurance:caseXbbInsurances){
			String retrunString = CustomerApi.updateContranct(ConfigConstant.corpid,ConfigConstant.token,xbbInsurance, ConfigConstant.UPDATE_CONTACTS);
		}


		if (caseTimeUps != null && caseTimeUps.size()>0) {
			CaseTimeUp lastTime = caseTimeUps.get(0);
			lastTime.setUpdateDateTime(new Date());
			caseTimeUpService.save(lastTime);
		}else {
			CaseTimeUp lastTime = new CaseTimeUp();
			lastTime.setUpdateDateTime(new Date());
			lastTime.setUpdateType("4");
			caseTimeUpService.save(lastTime);
		}
		return "true";
	}
}