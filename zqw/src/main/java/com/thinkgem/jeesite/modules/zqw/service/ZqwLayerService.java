/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwLayer;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwLayerDao;

/**
 * 律师管理Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwLayerService extends CrudService<ZqwLayerDao, ZqwLayer> {

	public ZqwLayer get(String id) {
		return super.get(id);
	}
	
	public List<ZqwLayer> findList(ZqwLayer zqwLayer) {
		return super.findList(zqwLayer);
	}
	
	public Page<ZqwLayer> findPage(Page<ZqwLayer> page, ZqwLayer zqwLayer) {
		return super.findPage(page, zqwLayer);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwLayer zqwLayer) {
		super.save(zqwLayer);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwLayer zqwLayer) {
		super.delete(zqwLayer);
	}
	
}