/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.ChangeLayer;
import com.thinkgem.jeesite.modules.zqwcase.service.ChangeLayerService;

/**
 * 生成律师律所Controller
 * @author coder_cheng@126.com
 * @version 2019-09-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/changeLayer")
public class ChangeLayerController extends BaseController {

	@Autowired
	private ChangeLayerService changeLayerService;
	
	@ModelAttribute
	public ChangeLayer get(@RequestParam(required=false) String id) {
		ChangeLayer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = changeLayerService.get(id);
		}
		if (entity == null){
			entity = new ChangeLayer();
		}
		return entity;
	}
	//@RequiresPermissions("zqwcase:changeLayer:view")
	@RequestMapping(value = {"list", ""})
	public String list(ChangeLayer changeLayer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ChangeLayer> page = changeLayerService.findPage(new Page<ChangeLayer>(request, response), changeLayer); 
		model.addAttribute("page", page);
		return "modules/zqwcase/changeLayerList";
	}

	//@RequiresPermissions("zqwcase:changeLayer:view")
	@RequestMapping(value = "form")
	public String form(ChangeLayer changeLayer, Model model) {
		model.addAttribute("changeLayer", changeLayer);
		return "modules/zqwcase/changeLayerForm";
	}

	//@RequiresPermissions("zqwcase:changeLayer:edit")
	@RequestMapping(value = "save")
	public String save(ChangeLayer changeLayer, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, changeLayer)){
			return form(changeLayer, model);
		}
		changeLayerService.save(changeLayer);
		addMessage(redirectAttributes, "保存生成律师律所成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/changeLayer/?repage";
	}

	//@RequiresPermissions("zqwcase:changeLayer:edit")
	@RequestMapping(value = "delete")
	public String delete(ChangeLayer changeLayer, RedirectAttributes redirectAttributes) {
		changeLayerService.delete(changeLayer);
		addMessage(redirectAttributes, "删除生成律师律所成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/changeLayer/?repage";
	}

}