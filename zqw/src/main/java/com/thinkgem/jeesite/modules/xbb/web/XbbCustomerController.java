/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.modules.xbb.entity.XbbCustomer;
import com.thinkgem.jeesite.modules.xbb.service.XbbCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 肖帮帮客户管理Controller
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
@Controller
@RequestMapping(value = "${adminPath}/xbb/xbbCustomer")
public class XbbCustomerController extends BaseController {

	@Autowired
	private XbbCustomerService xbbCustomerService;
	
	@ModelAttribute
	public XbbCustomer get(@RequestParam(required=false) String id) {
		XbbCustomer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xbbCustomerService.get(id);
		}
		if (entity == null){
			entity = new XbbCustomer();
		}
		return entity;
	}


//	@RequiresPermissions("xbb:xbbCustomer:view")
	@RequestMapping(value = {"list", ""})
	public String list(XbbCustomer xbbCustomer, HttpServletRequest request, HttpServletResponse response, Model model) {
	    Page<XbbCustomer> page = xbbCustomerService.findPage(new Page<XbbCustomer>(request, response), xbbCustomer);
		model.addAttribute("page", page);
		return "modules/xbb/xbbCustomerList";

	}

    @ResponseBody
//	@RequiresPermissions("xbb:xbbCustomer:view")
	@RequestMapping(value = "listJSON")
	public Map<String, Object> listJSON(XbbCustomer xbbCustomer, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		Page<XbbCustomer> page = new Page<XbbCustomer>(request, response);
		Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,page.getPageNo(),page.getPageSize(), ConfigConstant.CUSTOMER_LIST,ConfigConstant.customerKey,null,null);
		model.addAttribute("page",page);
		return returnMap;
	}

//	@RequiresPermissions("xbb:xbbCustomer:view")
	@RequestMapping(value = "form")
	public String form(XbbCustomer xbbCustomer, Model model) {
		model.addAttribute("xbbCustomer", xbbCustomer);
		return "modules/xbb/xbbCustomerForm";
	}

//	@RequiresPermissions("xbb:xbbCustomer:edit")
	@RequestMapping(value = "save")
	public String save(XbbCustomer xbbCustomer, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, xbbCustomer)){
			return form(xbbCustomer, model);
		}
		xbbCustomerService.save(xbbCustomer);
		addMessage(redirectAttributes, "保存肖帮帮客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbCustomer/?repage";
	}
	
//	@RequiresPermissions("xbb:xbbCustomer:edit")
	@RequestMapping(value = "delete")
	public String delete(XbbCustomer xbbCustomer, RedirectAttributes redirectAttributes) {
		xbbCustomerService.delete(xbbCustomer);
		addMessage(redirectAttributes, "删除肖帮帮客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbCustomer/?repage";
	}

}