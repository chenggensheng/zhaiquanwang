/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseLayer;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCaseLayerDao;

/**
 * 合作律师Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCaseLayerService extends CrudService<ZqwCaseLayerDao, ZqwCaseLayer> {

	public ZqwCaseLayer get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCaseLayer> findList(ZqwCaseLayer zqwCaseLayer) {
		return super.findList(zqwCaseLayer);
	}
	
	public Page<ZqwCaseLayer> findPage(Page<ZqwCaseLayer> page, ZqwCaseLayer zqwCaseLayer) {
		return super.findPage(page, zqwCaseLayer);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCaseLayer zqwCaseLayer) {
		super.save(zqwCaseLayer);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCaseLayer zqwCaseLayer) {
		super.delete(zqwCaseLayer);
	}
	
}