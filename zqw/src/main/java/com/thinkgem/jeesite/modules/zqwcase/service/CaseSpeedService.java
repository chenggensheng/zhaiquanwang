/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseSpeed;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseSpeedDao;

/**
 * 案件进展信息Service
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@Service
@Transactional(readOnly = true)
public class CaseSpeedService extends CrudService<CaseSpeedDao, CaseSpeed> {

	public CaseSpeed get(String id) {
		return super.get(id);
	}
	
	public List<CaseSpeed> findList(CaseSpeed caseSpeed) {
		return super.findList(caseSpeed);
	}
	
	public Page<CaseSpeed> findPage(Page<CaseSpeed> page, CaseSpeed caseSpeed) {
		return super.findPage(page, caseSpeed);
	}
	
	@Transactional(readOnly = false)
	public void save(CaseSpeed caseSpeed) {
		super.save(caseSpeed);
	}
	
	@Transactional(readOnly = false)
	public void delete(CaseSpeed caseSpeed) {
		super.delete(caseSpeed);
	}
	
}