/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.xbb.entity.XbbContacts;
import com.thinkgem.jeesite.modules.xbb.service.XbbContactsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


/**
 * 联系人管理Controller
 * @author coder_cheng@126.com
 * @version 2018-08-31
 */
@Controller
@RequestMapping(value = "${adminPath}/xbb/xbbContacts")
public class XbbContactsController extends BaseController {

	@Autowired
	private XbbContactsService xbbContactsService;
	
	@ModelAttribute
	public XbbContacts get(@RequestParam(required=false) String id) {
		XbbContacts entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xbbContactsService.get(id);
		}
		if (entity == null){
			entity = new XbbContacts();
		}
		return entity;
	}


//	@RequiresPermissions("xbb:xbbContacts:view")
	@RequestMapping(value = {"list", ""})
	public String list(XbbContacts xbbContacts, HttpServletRequest request, HttpServletResponse response, Model model) {
	    Page<XbbContacts> page = xbbContactsService.findPage(new Page<XbbContacts>(request, response), xbbContacts);
		model.addAttribute("page", page);
		return "modules/xbb/xbbContactsList";

	}

    @ResponseBody
//	@RequiresPermissions("xbb:xbbContacts:view")
	@RequestMapping(value = "listJSON")
	public Map<String, Object> listJSON(XbbContacts xbbContacts, HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		Page<XbbContacts> page = xbbContactsService.findPage(new Page<XbbContacts>(request, response), xbbContacts);
		model.addAttribute("page", page);
		returnMap.put("total", page.getCount());
		returnMap.put("rows", page.getList());
		return returnMap;
	}

//	@RequiresPermissions("xbb:xbbContacts:view")
	@RequestMapping(value = "form")
	public String form(XbbContacts xbbContacts, Model model) {
		model.addAttribute("xbbContacts", xbbContacts);
		return "modules/xbb/xbbContactsForm";
	}

//	@RequiresPermissions("xbb:xbbContacts:edit")
	@RequestMapping(value = "save")
	public String save(XbbContacts xbbContacts, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, xbbContacts)){
			return form(xbbContacts, model);
		}
		xbbContactsService.save(xbbContacts);
		addMessage(redirectAttributes, "保存联系人管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbContacts/?repage";
	}
	
//	@RequiresPermissions("xbb:xbbContacts:edit")
	@RequestMapping(value = "delete")
	public String delete(XbbContacts xbbContacts, RedirectAttributes redirectAttributes) {
		xbbContactsService.delete(xbbContacts);
		addMessage(redirectAttributes, "删除联系人管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbContacts/?repage";
	}

}