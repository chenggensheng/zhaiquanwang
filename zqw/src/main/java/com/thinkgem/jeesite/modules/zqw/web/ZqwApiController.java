package com.thinkgem.jeesite.modules.zqw.web;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.modules.zqw.entity.Api;
import com.thinkgem.jeesite.modules.zqw.entity.DeptCustomer;
import com.thinkgem.jeesite.modules.zqw.utils.WebUtil;
import net.sf.json.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "${adminPath}/zqw/api")
public class ZqwApiController {

   public static String andsense =  "andsenseapi";

    @RequestMapping(value = {"list", ""})
    public String list(Api api, Model model) {
        api.setApiType("0");
        model.addAttribute("api",api);
        return "modules/zqw/zqwApiList";
    }


    @RequestMapping(value = "searchResult")
    public String searchResult(Api api,Model model){
        String baseUrl = "https://api.acedata.com.cn:2443";
        if (api.getApiType().equals("0")){//个人投资核查
            String url =baseUrl+"/oreo/personal/investment/check";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("idcard",api.getIdCard());
            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrtzhc";
        }else if (api.getApiType().equals("1")){//个人收入能力
            String url = baseUrl +"/oreo/personal/performance/ability";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("idcard",api.getIdCard());
            params.put("name",api.getName());
            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrsrnl";

        }else if (api.getApiType().equals("2")){//乘车人出行记录
            String url = baseUrl +"/oreo/travel/portrait/basic";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("idcard",api.getIdCard());
            params.put("type","all");
            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrccrcxjl";

        }else if (api.getApiType().equals("3")){//新-车辆档案
            String url = baseUrl +"/oreo/verbose/vehicle/archive";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("name",api.getName());
            params.put("licensePlateNo",api.getCarNum());
            params.put("licensePlateType",api.getLicensePlateType());

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwcldan";

        }else if (api.getApiType().equals("4")){//多头借贷全量核查
            String url = baseUrl +"/oreo/verbose/personal/detail/creditInfoAll";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("cellphone",api.getCellphone());
            params.put("cycle",api.getCycle());

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwdtdhc";

        }else if (api.getApiType().equals("5")){//银联消费报告2.0
            String url = baseUrl +"/oreo/verbose/personal/consume/report";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("name",api.getName());
            params.put("idcard",api.getIdCard());
            params.put("bankcard",api.getBankcard());
            params.put("cellphone",api.getCellphone());
            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrrlxfbg";

        }else if (api.getApiType().equals("6")){//个人银联消费画像.0
            String url = baseUrl +"/oreo/personal/consume/portrait";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("name",api.getName());
            params.put("bankcard",api.getBankcard());

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrylxfhx";

        }else if (api.getApiType().equals("7")){//综合风险验证
            String url = baseUrl +"/oreo/personal/comprehensive/risk/check";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("cellphone",api.getCellphone());
            params.put("name",api.getName());
            params.put("idcard",api.getIdCard());

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrzhfxyz";

        }else if (api.getApiType().equals("8")){//刑事案底核查
            String url = baseUrl +"/oreo/personal/crimeInfo";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("name",api.getName());
            params.put("idcard",api.getIdCard());

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrxsadhc";

        }else if (api.getApiType().equals("9")){//姓名+手机号+身份证
            // String url = baseUrl +"/oreo/verbose/personal/name/idcard/cellphone/detail";
            String url = baseUrl +"/oreo/personal/validation/name/idcard/cellphone";

            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("name",api.getName());
            params.put("idcard",api.getIdCard());
            params.put("cellphone",api.getCellphone());

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrxmsjhsfz";

        }else if (api.getApiType().equals("10")){//个人整体涉诉
            String url = baseUrl +"/oreo/verbose/personal/overall/complaint/query";
            Map<String,String> params = new HashMap<String,String>();
            params.put("account",andsense);
            params.put("name",api.getName());
            params.put("idcard",api.getIdCard());
            params.put("type","all");

            String temp =  WebUtil.doPost(url,params,"UTF-8");
            Map mapReturn = JSONObject.fromObject(temp);
            model.addAttribute("mapReturn",mapReturn);
            model.addAttribute("api",api);
            return "modules/zqw/zqwgrztss";

        }
        else {

            return "modules/zqw/zqwSearchResult";
        }

    }






}
