/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.cms.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.cms.entity.DeptQuestion;

/**
 * 咨询方案DAO接口
 * @author coder_cheng@126.com
 * @version 2018-12-17
 */
@MyBatisDao
public interface DeptQuestionDao extends CrudDao<DeptQuestion> {
	
}