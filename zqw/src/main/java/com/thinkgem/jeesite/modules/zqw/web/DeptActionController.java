/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.DeptAction;
import com.thinkgem.jeesite.modules.zqw.service.DeptActionService;

/**
 * 债行动Controller
 * @author coder_cheng@126.com
 * @version 2018-10-20
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/deptAction")
public class DeptActionController extends BaseController {

	@Autowired
	private DeptActionService deptActionService;
	
	@ModelAttribute
	public DeptAction get(@RequestParam(required=false) String id) {
		DeptAction entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = deptActionService.get(id);
		}
		if (entity == null){
			entity = new DeptAction();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:deptAction:view")
	@RequestMapping(value = {"list", ""})
	public String list(DeptAction deptAction, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<DeptAction> page = deptActionService.findPage(new Page<DeptAction>(request, response), deptAction); 
		model.addAttribute("page", page);
		return "modules/zqw/deptActionList";
	}

	@RequiresPermissions("zqw:deptAction:view")
	@RequestMapping(value = "form")
	public String form(DeptAction deptAction, Model model) {
		model.addAttribute("deptAction", deptAction);
		return "modules/zqw/deptActionForm";
	}

	@RequiresPermissions("zqw:deptAction:edit")
	@RequestMapping(value = "save")
	public String save(DeptAction deptAction, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, deptAction)){
			return form(deptAction, model);
		}
		deptActionService.save(deptAction);
		addMessage(redirectAttributes, "保存债行动成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/deptAction/?repage";
	}
	
	@RequiresPermissions("zqw:deptAction:edit")
	@RequestMapping(value = "delete")
	public String delete(DeptAction deptAction, RedirectAttributes redirectAttributes) {
		deptActionService.delete(deptAction);
		addMessage(redirectAttributes, "删除债行动成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/deptAction/?repage";
	}

}