/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.modules.xbb.entity.XbbUser;
import com.thinkgem.jeesite.modules.xbb.service.XbbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 肖帮帮用户管理Controller
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
@Controller
@RequestMapping(value = "${adminPath}/xbb/xbbUser")
public class XbbUserController extends BaseController {

	@Autowired
	private XbbUserService xbbUserService;
	
	@ModelAttribute
	public XbbUser get(@RequestParam(required=false) String id) {
		XbbUser entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xbbUserService.get(id);
		}
		if (entity == null){
			entity = new XbbUser();
		}
		return entity;
	}


//	@RequiresPermissions("xbb:xbbUser:view")
	@RequestMapping(value = {"list", ""})
	public String list(XbbUser xbbUser, HttpServletRequest request, HttpServletResponse response, Model model) {
	    Page<XbbUser> page = xbbUserService.findPage(new Page<XbbUser>(request, response), xbbUser);
		model.addAttribute("page", page);
		return "modules/xbb/xbbUserList";

	}

    @ResponseBody
//	@RequiresPermissions("xbb:xbbUser:view")
	@RequestMapping(value = "listJSON")
	public Map<String, Object> listJSON(XbbUser xbbUser, HttpServletRequest request, HttpServletResponse response, Model model)throws Exception {
		Page<XbbUser> page = new Page<XbbUser>(request, response);                                                                                         //ConfigConstant.USER_ALL_LIST //userKey
		Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,page.getPageNo(),page.getPageSize(), ConfigConstant.USER_ALL_LIST,ConfigConstant.userKey,null,null);
		model.addAttribute("page",page);
		return returnMap;
	}

//	@RequiresPermissions("xbb:xbbUser:view")
	@RequestMapping(value = "form")
	public String form(XbbUser xbbUser, Model model) {
		model.addAttribute("xbbUser", xbbUser);
		return "modules/xbb/xbbUserForm";
	}

//	@RequiresPermissions("xbb:xbbUser:edit")
	@RequestMapping(value = "save")
	public String save(XbbUser xbbUser, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, xbbUser)){
			return form(xbbUser, model);
		}
		xbbUserService.save(xbbUser);
		addMessage(redirectAttributes, "保存肖帮帮用户管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbUser/?repage";
	}
	
//	@RequiresPermissions("xbb:xbbUser:edit")
	@RequestMapping(value = "delete")
	public String delete(XbbUser xbbUser, RedirectAttributes redirectAttributes) {
		xbbUserService.delete(xbbUser);
		addMessage(redirectAttributes, "删除肖帮帮用户管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbUser/?repage";
	}

}