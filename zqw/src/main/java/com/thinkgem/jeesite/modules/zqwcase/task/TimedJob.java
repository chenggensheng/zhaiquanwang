package com.thinkgem.jeesite.modules.zqwcase.task;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.modules.zqwcase.entity.*;
import com.thinkgem.jeesite.modules.zqwcase.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 定时任务
 * @author coder_cheng@126.com
 *
 */
@Service
@Lazy(false)
@Transactional
public class TimedJob {
	
	private static Logger logger = LoggerFactory.getLogger(TimedJob.class);

	@Autowired
	CaseTimeUpService caseTimeUpService;

	@Autowired
	CaseXbbInsuranceService caseXbbInsuranceService;

	@Autowired
	CaseInsuranceService caseInsuranceService;

	@Autowired
	XbbCommunicateService xbbCommunicateService;

	@Autowired
	CasePaymentService casePaymentService;


	/**
	 * 发送祝福
	 */
	@Scheduled(cron = "0 01 23 * * ?")
	public synchronized void syncDate() throws Exception {

		if(true) {
			String lastTimeString = "";
			CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
			caseTimeUpSearch.setUpdateType("1");
			List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
			if (caseTimeUps != null && caseTimeUps.size() > 0) {
				lastTimeString = caseTimeUps.get(0).getLastDate();
			} else {
				lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
			}
			Map<String, Object> returnMap = CustomerApi.returnMap(ConfigConstant.corpid, ConfigConstant.token, 1, 100, ConfigConstant.CONTACT_LIST, ConfigConstant.contactListKey, null, lastTimeString);
			List<CaseXbbInsurance> caseXbbInsurances = (List<CaseXbbInsurance>) returnMap.get("rows");
			int total = Integer.valueOf((String) returnMap.get("total"));
			int allPage = ((int) Math.ceil((double) total / (double) 100));
			List<CaseInsurance> caseInsurances = caseInsuranceService.findList(new CaseInsurance());
			caseXbbInsuranceService.saveFromCrm(caseXbbInsurances, caseInsurances);
			for (int i = 2; i <= allPage; i++) {
				Map<String, Object> returnMapTemp = CustomerApi.returnMap(ConfigConstant.corpid, ConfigConstant.token, i, 100, ConfigConstant.CONTACT_LIST, ConfigConstant.contactListKey, null, lastTimeString);
				List<CaseXbbInsurance> caseXbbInsuranceTemps = (List<CaseXbbInsurance>) returnMapTemp.get("rows");
				caseXbbInsuranceService.saveFromCrm(caseXbbInsuranceTemps, caseInsurances);
			}
			if (caseTimeUps != null && caseTimeUps.size() > 0) {
				CaseTimeUp lastTime = caseTimeUps.get(0);
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			} else {
				CaseTimeUp lastTime = new CaseTimeUp();
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}
		}

		if (true){
			String lastTimeString ="";
			CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
			caseTimeUpSearch.setUpdateType("2");
			List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
			if (caseTimeUps != null && caseTimeUps.size()>0) {
				lastTimeString = caseTimeUps.get(0).getLastDate();
			}else {
				lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
			}

			Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.COMMUNICATE_LIST,ConfigConstant.communicateListKey,null,lastTimeString);
			List<XbbCommunicate> xbbCommunicates = (List<XbbCommunicate>) returnMap.get("rows");
			int total = Integer.valueOf((String) returnMap.get("total"));
			int allPage = ((int)Math.ceil((double)total/(double)100));
			xbbCommunicateService.saveFromCrm(xbbCommunicates);
			for (int i =2 ;i<= allPage;i++){
				Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.COMMUNICATE_LIST,ConfigConstant.communicateListKey,null,lastTimeString);
				List<XbbCommunicate> xbbCommunicatesTemps = (List<XbbCommunicate>) returnMapTemp.get("rows");
				xbbCommunicateService.saveFromCrm(xbbCommunicatesTemps);
			}

			if (caseTimeUps != null && caseTimeUps.size()>0) {
				CaseTimeUp lastTime = caseTimeUps.get(0);
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}else {
				CaseTimeUp lastTime = new CaseTimeUp();
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}
		}

		if (true){
			String lastTimeString ="";
			CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
			caseTimeUpSearch.setUpdateType("3");
			List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
			if (caseTimeUps != null && caseTimeUps.size()>0) {
				lastTimeString = caseTimeUps.get(0).getLastDate();
			}else {
				lastTimeString = DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString();
			}

			List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceService.findList(new CaseXbbInsurance());

			Map<String, Object> returnMap =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,1,100, ConfigConstant.PAYMENT_LIST,ConfigConstant.paymentListKey,null,lastTimeString);
			List<CasePayment> casePayments = (List<CasePayment>) returnMap.get("rows");
			int total = Integer.valueOf((String) returnMap.get("total"));
			int allPage = ((int)Math.ceil((double)total/(double)100));
			casePaymentService.saveFromCrm(caseXbbInsurances,casePayments);
			for (int i =2 ;i<= allPage;i++){
				Map<String, Object> returnMapTemp =  CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,i,100, ConfigConstant.PAYMENT_LIST,ConfigConstant.paymentListKey,null,lastTimeString);
				List<CasePayment> casePaymentsTemps = (List<CasePayment>) returnMapTemp.get("rows");
				casePaymentService.saveFromCrm(caseXbbInsurances,casePaymentsTemps);
			}

			if (caseTimeUps != null && caseTimeUps.size()>0) {
				CaseTimeUp lastTime = caseTimeUps.get(0);
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}else {
				CaseTimeUp lastTime = new CaseTimeUp();
				lastTime.setLastDate(DateUtils.StringToTimestamp(DateUtils.getDateTime()).toString());
				caseTimeUpService.save(lastTime);
			}
		}

		Date lastTimeString =null;
		CaseTimeUp caseTimeUpSearch = new CaseTimeUp();
		caseTimeUpSearch.setUpdateType("4");
		List<CaseTimeUp> caseTimeUps = caseTimeUpService.findList(caseTimeUpSearch);
		if (caseTimeUps != null && caseTimeUps.size()>0) {
			lastTimeString = caseTimeUps.get(0).getUpdateDateTime();
		}else {
			lastTimeString = new Date();
		}
		CaseXbbInsurance caseXbbInsurance = new CaseXbbInsurance();
		caseXbbInsurance.setUpdateDate(lastTimeString);
		List<CaseXbbInsurance> caseXbbInsurances = caseXbbInsuranceService.findNewList(caseXbbInsurance);
		caseInsuranceService.caseSuperviseboardSave(caseXbbInsurances);

		for (CaseXbbInsurance xbbInsurance:caseXbbInsurances){
			try {
				 CustomerApi.updateContranct(ConfigConstant.corpid,ConfigConstant.token,xbbInsurance, ConfigConstant.UPDATE_CONTACTS);

			}catch (Exception e){

			}
		}

		if (caseTimeUps != null && caseTimeUps.size()>0) {
			CaseTimeUp lastTime = caseTimeUps.get(0);
			lastTime.setUpdateDateTime(new Date());
			caseTimeUpService.save(lastTime);
		}else {
			CaseTimeUp lastTime = new CaseTimeUp();
			lastTime.setUpdateDateTime(new Date());
			lastTime.setUpdateType("4");
			caseTimeUpService.save(lastTime);
		}
	}
	
}
