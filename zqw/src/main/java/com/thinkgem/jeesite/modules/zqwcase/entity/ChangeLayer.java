/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 生成律师律所Entity
 * @author coder_cheng@126.com
 * @version 2019-09-14
 */
public class ChangeLayer extends DataEntity<ChangeLayer> {
	
	private static final long serialVersionUID = 1L;
	private String context;		// context
	private String layerName;		// layer_name
	private String layerPlace;		// layer_place
	private String layerType;
	
	public ChangeLayer() {
		super();
	}

	public ChangeLayer(String id){
		super(id);
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
	
	public String getLayerName() {
		return layerName;
	}

	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}
	
	@Length(min=0, max=10240, message="layer_place长度必须介于 0 和 10240 之间")
	public String getLayerPlace() {
		return layerPlace;
	}

	public void setLayerPlace(String layerPlace) {
		this.layerPlace = layerPlace;
	}

	public String getLayerType() {
		return layerType;
	}

	public void setLayerType(String layerType) {
		this.layerType = layerType;
	}
}