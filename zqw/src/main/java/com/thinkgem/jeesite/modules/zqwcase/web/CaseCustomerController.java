/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseCustomer;
import com.thinkgem.jeesite.modules.zqwcase.service.CaseCustomerService;

/**
 * 案件客户信息Controller
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/caseCustomer")
public class CaseCustomerController extends BaseController {

	@Autowired
	private CaseCustomerService caseCustomerService;
	
	@ModelAttribute
	public CaseCustomer get(@RequestParam(required=false) String id) {
		CaseCustomer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = caseCustomerService.get(id);
		}
		if (entity == null){
			entity = new CaseCustomer();
		}
		return entity;
	}
	
//	@RequiresPermissions("zqwcase:caseCustomer:view")
	@RequestMapping(value = {"list", ""})
	public String list(CaseCustomer caseCustomer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CaseCustomer> page = caseCustomerService.findPage(new Page<CaseCustomer>(request, response), caseCustomer); 
		model.addAttribute("page", page);
		return "modules/zqwcase/caseCustomerList";
	}

//	@RequiresPermissions("zqwcase:caseCustomer:view")
	@RequestMapping(value = "form")
	public String form(CaseCustomer caseCustomer, Model model) {
		model.addAttribute("caseCustomer", caseCustomer);
		return "modules/zqwcase/caseCustomerForm";
	}

//	@RequiresPermissions("zqwcase:caseCustomer:edit")
	@RequestMapping(value = "save")
	public String save(CaseCustomer caseCustomer, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
		if (!beanValidator(model, caseCustomer)){
			return form(caseCustomer, model);
		}
		String saveFlag = request.getParameter("saveFlag");
		caseCustomerService.save(caseCustomer);
		addMessage(redirectAttributes, "保存案件客户信息成功");
		// 1 保存并添加案件
		if (saveFlag.equals("1"))
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/form?caseCustomerId="+caseCustomer.getId();
		// 0 保存
		else
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseCustomer/?repage";
	}

//	@RequiresPermissions("zqwcase:caseCustomer:edit")
	@RequestMapping(value = "delete")
	public String delete(CaseCustomer caseCustomer, RedirectAttributes redirectAttributes) {
		caseCustomerService.delete(caseCustomer);
		addMessage(redirectAttributes, "删除案件客户信息成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseCustomer/?repage";
	}

}