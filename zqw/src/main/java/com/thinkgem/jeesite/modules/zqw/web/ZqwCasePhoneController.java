/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCasePhone;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCasePhoneService;

/**
 * 电话催收Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCasePhone")
public class ZqwCasePhoneController extends BaseController {

	@Autowired
	private ZqwCasePhoneService zqwCasePhoneService;
	
	@ModelAttribute
	public ZqwCasePhone get(@RequestParam(required=false) String id) {
		ZqwCasePhone entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCasePhoneService.get(id);
		}
		if (entity == null){
			entity = new ZqwCasePhone();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCasePhone:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCasePhone zqwCasePhone, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCasePhone> page = zqwCasePhoneService.findPage(new Page<ZqwCasePhone>(request, response), zqwCasePhone); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCasePhoneList";
	}

	@RequiresPermissions("zqw:zqwCasePhone:view")
	@RequestMapping(value = "form")
	public String form(ZqwCasePhone zqwCasePhone, Model model) {
		model.addAttribute("zqwCasePhone", zqwCasePhone);
		return "modules/zqw/zqwCasePhoneForm";
	}

	@RequiresPermissions("zqw:zqwCasePhone:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCasePhone zqwCasePhone, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwCasePhone)){
			return form(zqwCasePhone, model);
		}
		zqwCasePhoneService.save(zqwCasePhone);
		addMessage(redirectAttributes, "保存电话催收成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCasePhone/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCasePhone:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCasePhone zqwCasePhone, RedirectAttributes redirectAttributes) {
		zqwCasePhoneService.delete(zqwCasePhone);
		addMessage(redirectAttributes, "删除电话催收成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCasePhone/?repage";
	}

}