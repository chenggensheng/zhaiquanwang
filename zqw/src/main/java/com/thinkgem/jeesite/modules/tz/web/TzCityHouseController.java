/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.tz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.tz.entity.TzCityHouse;
import com.thinkgem.jeesite.modules.tz.service.TzCityHouseService;

/**
 * 城市管理Controller
 * @author coder_cheng@126.com
 * @version 2019-06-11
 */
@Controller
@RequestMapping(value = "${adminPath}/tz/tzCityHouse")
public class TzCityHouseController extends BaseController {

	@Autowired
	private TzCityHouseService tzCityHouseService;
	
	@ModelAttribute
	public TzCityHouse get(@RequestParam(required=false) String id) {
		TzCityHouse entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tzCityHouseService.get(id);
		}
		if (entity == null){
			entity = new TzCityHouse();
		}
		return entity;
	}
	
	@RequiresPermissions("tz:tzCityHouse:view")
	@RequestMapping(value = {"list", ""})
	public String list(TzCityHouse tzCityHouse, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TzCityHouse> page = tzCityHouseService.findPage(new Page<TzCityHouse>(request, response), tzCityHouse); 
		model.addAttribute("page", page);
		return "modules/tz/tzCityHouseList";
	}

	@RequiresPermissions("tz:tzCityHouse:view")
	@RequestMapping(value = "form")
	public String form(TzCityHouse tzCityHouse, Model model) {
		model.addAttribute("tzCityHouse", tzCityHouse);
		return "modules/tz/tzCityHouseForm";
	}

	@RequiresPermissions("tz:tzCityHouse:edit")
	@RequestMapping(value = "save")
	public String save(TzCityHouse tzCityHouse, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tzCityHouse)){
			return form(tzCityHouse, model);
		}
		tzCityHouseService.save(tzCityHouse);
		addMessage(redirectAttributes, "保存城市管理成功");
		return "redirect:"+Global.getAdminPath()+"/tz/tzCityHouse/?repage";
	}
	
	@RequiresPermissions("tz:tzCityHouse:edit")
	@RequestMapping(value = "delete")
	public String delete(TzCityHouse tzCityHouse, RedirectAttributes redirectAttributes) {
		tzCityHouseService.delete(tzCityHouse);
		addMessage(redirectAttributes, "删除城市管理成功");
		return "redirect:"+Global.getAdminPath()+"/tz/tzCityHouse/?repage";
	}

}