/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseTimeUp;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseTimeUpDao;

/**
 * 肖邦邦同步管理Service
 * @author coder_cheng@126.com
 * @version 2019-07-20
 */
@Service
@Transactional(readOnly = true)
public class CaseTimeUpService extends CrudService<CaseTimeUpDao, CaseTimeUp> {

	public CaseTimeUp get(String id) {
		return super.get(id);
	}
	
	public List<CaseTimeUp> findList(CaseTimeUp caseTimeUp) {
		return super.findList(caseTimeUp);
	}
	
	public Page<CaseTimeUp> findPage(Page<CaseTimeUp> page, CaseTimeUp caseTimeUp) {
		return super.findPage(page, caseTimeUp);
	}
	
	@Transactional(readOnly = false)
	public void save(CaseTimeUp caseTimeUp) {
		super.save(caseTimeUp);
	}
	
	@Transactional(readOnly = false)
	public void delete(CaseTimeUp caseTimeUp) {
		super.delete(caseTimeUp);
	}
	
}