/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.zqwcase.entity.CaseCustomer;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseInsurance;
import com.thinkgem.jeesite.modules.zqwcase.service.CaseCustomerService;
import com.thinkgem.jeesite.modules.zqwcase.service.CaseInsuranceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseSpeed;
import com.thinkgem.jeesite.modules.zqwcase.service.CaseSpeedService;

import java.util.List;

/**
 * 案件进展信息Controller
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/caseSpeed")
public class CaseSpeedController extends BaseController {

	@Autowired
	private CaseSpeedService caseSpeedService;

	@Autowired
	private CaseCustomerService caseCustomerService;

	@Autowired
	private CaseInsuranceService caseInsuranceService;
	
	@ModelAttribute
	public CaseSpeed get(@RequestParam(required=false) String id) {
		CaseSpeed entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = caseSpeedService.get(id);
		}
		if (entity == null){
			entity = new CaseSpeed();
		}
		return entity;
	}
	
//	@RequiresPermissions("zqwcase:caseSpeed:view")
	@RequestMapping(value = {"list", ""})
	public String list(CaseSpeed caseSpeed, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CaseSpeed> page = caseSpeedService.findPage(new Page<CaseSpeed>(request, response), caseSpeed); 
		model.addAttribute("page", page);
		return "modules/zqwcase/caseSpeedList";
	}

//	@RequiresPermissions("zqwcase:caseSpeed:view")
	@RequestMapping(value = "form")
	public String form(CaseSpeed caseSpeed, Model model) {
		model.addAttribute("caseSpeed", caseSpeed);
		CaseCustomer caseCustomer = caseCustomerService.get(caseSpeed.getCaseCustomerId());
		CaseInsurance caseInsurance = caseInsuranceService.get(caseSpeed.getCaseInsuranceId());
		List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
		if (caseSpeeds !=null && caseSpeeds.size()>0){
			caseSpeed = caseSpeeds.get(0);
		}
		model.addAttribute("caseSpeed",caseSpeed);
		model.addAttribute("caseCustomer",caseCustomer);
		model.addAttribute("caseInsurance",caseInsurance);
		return "modules/zqwcase/caseSpeedForm";
	}


	@RequestMapping(value = "view")
	public String view(CaseSpeed caseSpeed, Model model) {
		model.addAttribute("caseSpeed", caseSpeed);
		CaseCustomer caseCustomer = caseCustomerService.get(caseSpeed.getCaseCustomerId());
		CaseInsurance caseInsurance = caseInsuranceService.get(caseSpeed.getCaseInsuranceId());
		List<CaseSpeed> caseSpeeds = caseSpeedService.findList(caseSpeed);
		if (caseSpeeds !=null && caseSpeeds.size()>0){
			caseSpeed = caseSpeeds.get(0);
		}
		model.addAttribute("caseSpeed",caseSpeed);
		model.addAttribute("caseCustomer",caseCustomer);
		model.addAttribute("caseInsurance",caseInsurance);
		return "modules/zqwcase/caseSpeedViewForm";
	}

//	@RequiresPermissions("zqwcase:caseSpeed:edit")
	@RequestMapping(value = "save")
	public String save(CaseSpeed caseSpeed, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, caseSpeed)){
			return form(caseSpeed, model);
		}
		caseSpeedService.save(caseSpeed);
		addMessage(redirectAttributes, "保存案件进展信息成功");

		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseInsurance/form?id="+caseSpeed.getCaseInsuranceId();


//		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseSpeed/?repage";
	}
	
//	@RequiresPermissions("zqwcase:caseSpeed:edit")
	@RequestMapping(value = "delete")
	public String delete(CaseSpeed caseSpeed, RedirectAttributes redirectAttributes) {
		caseSpeedService.delete(caseSpeed);
		addMessage(redirectAttributes, "删除案件进展信息成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/caseSpeed/?repage";
	}

}