/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCasePhone;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCasePhoneDao;

/**
 * 电话催收Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCasePhoneService extends CrudService<ZqwCasePhoneDao, ZqwCasePhone> {

	public ZqwCasePhone get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCasePhone> findList(ZqwCasePhone zqwCasePhone) {
		return super.findList(zqwCasePhone);
	}
	
	public Page<ZqwCasePhone> findPage(Page<ZqwCasePhone> page, ZqwCasePhone zqwCasePhone) {
		return super.findPage(page, zqwCasePhone);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCasePhone zqwCasePhone) {
		super.save(zqwCasePhone);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCasePhone zqwCasePhone) {
		super.delete(zqwCasePhone);
	}
	
}