/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseLayer;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwLayer;
import com.thinkgem.jeesite.modules.zqw.service.ZqwLayerService;

/**
 * 律师管理Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwLayer")
public class ZqwLayerController extends BaseController {

	@Autowired
	private ZqwLayerService zqwLayerService;
	
	@ModelAttribute
	public ZqwLayer get(@RequestParam(required=false) String id) {
		ZqwLayer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwLayerService.get(id);
		}
		if (entity == null){
			entity = new ZqwLayer();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwLayer:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwLayer zqwLayer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwLayer> page = zqwLayerService.findPage(new Page<ZqwLayer>(request, response), zqwLayer); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwLayerList";
	}

	@RequiresPermissions("zqw:zqwLayer:view")
	@RequestMapping(value = "form")
	public String form(ZqwLayer zqwLayer, Model model) {
		model.addAttribute("zqwLayer", zqwLayer);
		return "modules/zqw/zqwLayerForm";
	}

	@RequiresPermissions("zqw:zqwLayer:edit")
	@RequestMapping(value = "save")
	public String save(ZqwLayer zqwLayer, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwLayer)){
			return form(zqwLayer, model);
		}
		zqwLayerService.save(zqwLayer);
		addMessage(redirectAttributes, "保存律师管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwLayer/?repage";
	}
	
	@RequiresPermissions("zqw:zqwLayer:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwLayer zqwLayer, RedirectAttributes redirectAttributes) {
		zqwLayerService.delete(zqwLayer);
		addMessage(redirectAttributes, "删除律师管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwLayer/?repage";
	}

	@ResponseBody
	@RequestMapping(value = "changePhone")
	public String changePhone(ZqwLayer zqwLayer)  {
		return zqwLayer.getLayerPhone();
	}

}