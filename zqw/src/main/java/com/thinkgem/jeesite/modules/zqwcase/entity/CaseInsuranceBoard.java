package com.thinkgem.jeesite.modules.zqwcase.entity;

import java.util.List;

public class CaseInsuranceBoard {

    private String allCount;//案件总数
    private String doingCount; //在办案件
    private String didCount;//完结案件
    private String otherCount;//其他案件

    private String advanceProjectsCount;//预立项
    private String doingCountTow;//进行中
    private String nonComplaintCount;// 非诉催收
    private String litigationsCount;//诉讼阶段
    private String implementsCount;//执行阶段
    private String selfLitigationsCount;// 自办诉讼阶段
    private String outLitigationsCount;// 委外诉讼阶段
    private String selfImplementsCount;// 自办执行阶段
    private String outImplementsCount;// 委外诉讼阶段




    private String todayCount; //今天新增

    private String allMoneyCount;// 合同总金额
    private String planMoneyCount;// 计划收款金额
    private String overMonthCount;//超一个月合同订单金额
    private String backRaio;//回款率
    private String dateString;//今天日期


    List<CaseInsuranceRisk> caseInsuranceRisks;                 //风险数据
    List<CaseInsurancePersonBoard> caseInsurancePersonBoards ;  //个人数据


    private String yearString;
    private String monthString;
    private String dayString;


    public String getAllCount() {
        return allCount;
    }

    public void setAllCount(String allCount) {
        this.allCount = allCount;
    }

    public String getDoingCount() {
        return doingCount;
    }

    public void setDoingCount(String doingCount) {
        this.doingCount = doingCount;
    }

    public String getDidCount() {
        return didCount;
    }

    public void setDidCount(String didCount) {
        this.didCount = didCount;
    }

    public String getOtherCount() {
        return otherCount;
    }

    public void setOtherCount(String otherCount) {
        this.otherCount = otherCount;
    }

    public String getAdvanceProjectsCount() {
        return advanceProjectsCount;
    }

    public void setAdvanceProjectsCount(String advanceProjectsCount) {
        this.advanceProjectsCount = advanceProjectsCount;
    }

    public String getNonComplaintCount() {
        return nonComplaintCount;
    }

    public void setNonComplaintCount(String nonComplaintCount) {
        this.nonComplaintCount = nonComplaintCount;
    }

    public String getLitigationsCount() {
        return litigationsCount;
    }

    public void setLitigationsCount(String litigationsCount) {
        this.litigationsCount = litigationsCount;
    }

    public String getImplementsCount() {
        return implementsCount;
    }

    public void setImplementsCount(String implementsCount) {
        this.implementsCount = implementsCount;
    }

    public String getAllMoneyCount() {
        return allMoneyCount;
    }

    public void setAllMoneyCount(String allMoneyCount) {
        this.allMoneyCount = allMoneyCount;
    }

    public String getPlanMoneyCount() {
        return planMoneyCount;
    }

    public void setPlanMoneyCount(String planMoneyCount) {
        this.planMoneyCount = planMoneyCount;
    }

    public String getOverMonthCount() {
        return overMonthCount;
    }

    public void setOverMonthCount(String overMonthCount) {
        this.overMonthCount = overMonthCount;
    }

    public String getBackRaio() {
        return backRaio;
    }

    public void setBackRaio(String backRaio) {
        this.backRaio = backRaio;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getTodayCount() {
        return todayCount;
    }

    public void setTodayCount(String todayCount) {
        this.todayCount = todayCount;
    }

    public List<CaseInsurancePersonBoard> getCaseInsurancePersonBoards() {
        return caseInsurancePersonBoards;
    }

    public void setCaseInsurancePersonBoards(List<CaseInsurancePersonBoard> caseInsurancePersonBoards) {
        this.caseInsurancePersonBoards = caseInsurancePersonBoards;
    }

    public List<CaseInsuranceRisk> getCaseInsuranceRisks() {
        return caseInsuranceRisks;
    }

    public void setCaseInsuranceRisks(List<CaseInsuranceRisk> caseInsuranceRisks) {
        this.caseInsuranceRisks = caseInsuranceRisks;
    }

    public String getDoingCountTow() {
        return doingCountTow;
    }

    public void setDoingCountTow(String doingCountTow) {
        this.doingCountTow = doingCountTow;
    }

    public String getSelfLitigationsCount() {
        return selfLitigationsCount;
    }

    public void setSelfLitigationsCount(String selfLitigationsCount) {
        this.selfLitigationsCount = selfLitigationsCount;
    }

    public String getOutLitigationsCount() {
        return outLitigationsCount;
    }

    public void setOutLitigationsCount(String outLitigationsCount) {
        this.outLitigationsCount = outLitigationsCount;
    }

    public String getSelfImplementsCount() {
        return selfImplementsCount;
    }

    public void setSelfImplementsCount(String selfImplementsCount) {
        this.selfImplementsCount = selfImplementsCount;
    }

    public String getOutImplementsCount() {
        return outImplementsCount;
    }

    public void setOutImplementsCount(String outImplementsCount) {
        this.outImplementsCount = outImplementsCount;
    }

    public String getYearString() {
        return yearString;
    }

    public void setYearString(String yearString) {
        this.yearString = yearString;
    }

    public String getMonthString() {
        return monthString;
    }

    public void setMonthString(String monthString) {
        this.monthString = monthString;
    }

    public String getDayString() {
        return dayString;
    }

    public void setDayString(String dayString) {
        this.dayString = dayString;
    }
}
