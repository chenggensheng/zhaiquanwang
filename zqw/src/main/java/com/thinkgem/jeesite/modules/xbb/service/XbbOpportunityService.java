/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xbb.dao.XbbOpportunityDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbOpportunity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 肖帮帮销售计划管理Service
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
@Service
@Transactional(readOnly = true)
public class XbbOpportunityService extends CrudService<XbbOpportunityDao, XbbOpportunity> {

	public XbbOpportunity get(String id) {
		return super.get(id);
	}
	
	public List<XbbOpportunity> findList(XbbOpportunity xbbOpportunity) {
		return super.findList(xbbOpportunity);
	}
	
	public Page<XbbOpportunity> findPage(Page<XbbOpportunity> page, XbbOpportunity xbbOpportunity) {
		return super.findPage(page, xbbOpportunity);
	}
	
	@Transactional(readOnly = false)
	public void save(XbbOpportunity xbbOpportunity) {
		super.save(xbbOpportunity);
	}
	
	@Transactional(readOnly = false)
	public void delete(XbbOpportunity xbbOpportunity) {
		super.delete(xbbOpportunity);
	}
	
}