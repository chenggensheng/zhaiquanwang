/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.DeptBranch;
import com.thinkgem.jeesite.modules.zqw.dao.DeptBranchDao;

/**
 * 曝光台Service
 * @author coder_cheng@126.com
 * @version 2018-10-22
 */
@Service
@Transactional(readOnly = true)
public class DeptBranchService extends CrudService<DeptBranchDao, DeptBranch> {

	public DeptBranch get(String id) {
		return super.get(id);
	}
	
	public List<DeptBranch> findList(DeptBranch deptBranch) {
		return super.findList(deptBranch);
	}

	public List<DeptBranch> findListLimit(){ return dao.findListLimit();}
	
	public Page<DeptBranch> findPage(Page<DeptBranch> page, DeptBranch deptBranch) {
		return super.findPage(page, deptBranch);
	}
	
	@Transactional(readOnly = false)
	public void save(DeptBranch deptBranch) {
		super.save(deptBranch);
	}
	
	@Transactional(readOnly = false)
	public void delete(DeptBranch deptBranch) {
		super.delete(deptBranch);
	}
	
}