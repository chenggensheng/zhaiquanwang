/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xbb.dao.XbbUserDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 肖帮帮用户管理Service
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
@Service
@Transactional(readOnly = true)
public class XbbUserService extends CrudService<XbbUserDao, XbbUser> {

	public XbbUser get(String id) {
		return super.get(id);
	}
	
	public List<XbbUser> findList(XbbUser xbbUser) {
		return super.findList(xbbUser);
	}
	
	public Page<XbbUser> findPage(Page<XbbUser> page, XbbUser xbbUser) {
		return super.findPage(page, xbbUser);
	}
	
	@Transactional(readOnly = false)
	public void save(XbbUser xbbUser) {
		super.save(xbbUser);
	}
	
	@Transactional(readOnly = false)
	public void delete(XbbUser xbbUser) {
		super.delete(xbbUser);
	}
	
}