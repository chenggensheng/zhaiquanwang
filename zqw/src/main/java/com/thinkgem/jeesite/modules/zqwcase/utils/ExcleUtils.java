package com.thinkgem.jeesite.modules.zqwcase.utils;

import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseCustomer;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseInsurance;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseInsurancePersonBoard;
import org.apache.poi.hssf.usermodel.*;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import com.google.common.collect.Lists;
import org.apache.poi.ss.util.CellRangeAddress;


import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.io.OutputStream;
import java.net.URLEncoder;

public class ExcleUtils {



    public static void exportExcle(String fileName, List<CaseInsurance> caseInsurances, HttpServletResponse response)throws  Exception{

        // 第一步
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheetOne = wb.createSheet("律师函编号");

        HSSFRow rowOne  = sheetOne.createRow((int) 0);
        List<String> headerList = Lists.newArrayList();
        headerList.add("被保险人");
        headerList.add("立案受理号");
        headerList.add("律师函号");
        for (int i=0;i<headerList.size();i++){
            HSSFCell cell = rowOne.createCell(i);
            cell.setCellValue(headerList.get(i));
        }
		List<String> dataRowList = Lists.newArrayList();

        List<CaseInsurance> caseInsuranceList = changeList(caseInsurances);

		for (int i =0; i<caseInsuranceList.size();i++){
		    HSSFRow rowTow = sheetOne.createRow((int)i+1);
		    HSSFCell datacell = null;
		       for (int j=0;j<headerList.size();j++){
                   datacell = rowTow.createCell(j);
                   if (j==0){
                       if (StringUtils.isNotBlank(caseInsuranceList.get(i).getId())){
                           if (caseInsuranceList.get(i).getCaseType().equals("0")){
                               datacell.setCellValue(caseInsuranceList.get(i).getInsured());
                           }else if(caseInsuranceList.get(i).getCaseType().equals("1")){
                               datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwner());
                           }
                       }else {
                           if (caseInsuranceList.get(i).getCaseType().equals("0")){
                               datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriver());
                           }else if(caseInsuranceList.get(i).getCaseType().equals("1")){
                               datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmful());
                           }
                       }
                   }else if(j==1){
                       datacell.setCellValue(caseInsuranceList.get(i).getAcceptanceNumebr());
                   }else if (j==2){
                       if (StringUtils.isNotBlank(caseInsuranceList.get(i).getId())){
                           datacell.setCellValue(caseInsuranceList.get(i).getLayerNumber()+caseInsuranceList.get(i).getLayerNumberOne());

                       }else {
                           datacell.setCellValue(caseInsuranceList.get(i).getLayerNumber()+caseInsuranceList.get(i).getLayerNumberTow());
                       }


                   }
               }
        }

        // 第二步，将文件存到浏览器设置的下载位置
        String filename = fileName + ".xls";
        response.setContentType("application/ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                .concat(String.valueOf(URLEncoder.encode(filename, "UTF-8"))));
        OutputStream out = response.getOutputStream();
        try {
            wb.write(out);// 将数据写出去
            String str = "导出" + fileName + "成功！";
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
            String str1 = "导出" + fileName + "失败！";
            System.out.println(str1);
        } finally {
            out.close();
        }
    }


    public static void exportMaterialExcle(String fileName,String yearString ,String mouthString, String dayString,List<CaseInsurancePersonBoard> caseInsurancePersonBoards, HttpServletResponse response)throws  Exception{

        // 第一步
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheetOne = wb.createSheet("案件资料缺失情况表");

        HSSFRow rowZero  = sheetOne.createRow((int) 0);

        rowZero.setHeightInPoints(50);

        //第三步创建标题的单位格样式style以及字体央视的headerFont
        HSSFCellStyle style = wb.createCellStyle();

        HSSFFont headerFont = (HSSFFont) wb.createFont(); // 创建字体样式
        headerFont.setFontName("方正小标宋简体"); // 设置字体类型
        headerFont.setFontHeightInPoints((short) 20); // 设置字体大小
        style.setFont(headerFont); // 为标题样式设置字体样式
        HSSFCell cellSummaryOne = rowZero.createCell(0);
        //合并列标题
        sheetOne.addMergedRegion(new CellRangeAddress(0,0,0,8));
        cellSummaryOne.setCellValue(yearString+"年"+mouthString+"月"+dayString+"日保险案件资料缺补最新情况"); //设置标题值
        cellSummaryOne.setCellStyle(style);



        HSSFRow rowOne  = sheetOne.createRow((int) 1);
        List<String> headerList = Lists.newArrayList();
        headerList.add("序号");
        headerList.add("案件名称");
        headerList.add("承办人员");
        headerList.add("F资缺");
        headerList.add("F资补");
        headerList.add("S资缺");
        headerList.add("S资补");
        headerList.add("SW资缺");
        headerList.add("SW资补");
        for (int i=0;i<headerList.size();i++){
            HSSFCell cell = rowOne.createCell(i);
            cell.setCellValue(headerList.get(i));
        }
        List<String> dataRowList = Lists.newArrayList();


        for (int i =1; i<caseInsurancePersonBoards.size();i++){
            HSSFRow rowTow = sheetOne.createRow((int)i+1);
            HSSFCell datacell = null;
            for (int j=0;j<headerList.size();j++){
                datacell = rowTow.createCell(j);
                if (j==0){
                    datacell.setCellValue(j+1);
                }else if(j==1){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getCaseInsuranceName());
                }else if (j==2){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getName());
                }else if (j==3){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getfLackFunds());
                }else if (j==4){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getfSupplement());
                }else if (j==5){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getsShortage());
                }else if (j==6){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getsSupplement());
                }else if (j==7){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getSwShortage());
                }else if (j==8){
                    datacell.setCellValue(caseInsurancePersonBoards.get(i).getsSupplement());
                }

            }
        }

        // 第二步，将文件存到浏览器设置的下载位置
        String filename = fileName + ".xls";
        response.setContentType("application/ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                .concat(String.valueOf(URLEncoder.encode(filename, "UTF-8"))));
        OutputStream out = response.getOutputStream();
        try {
            wb.write(out);// 将数据写出去
            String str = "导出" + fileName + "成功！";
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
            String str1 = "导出" + fileName + "失败！";
            System.out.println(str1);
        } finally {
            out.close();
        }
    }


    public static void exportCaseExcle(String fileName, List<CaseInsurance> caseInsuranceList, HttpServletResponse response)throws  Exception{

        // 第一步
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheetOne = wb.createSheet("保险案件");

        HSSFRow rowOne  = sheetOne.createRow((int) 0);
        List<String> headerList = Lists.newArrayList();
        headerList.add("债权人");
        headerList.add("债权人住所地");
        headerList.add("债权人信用代码");
        headerList.add("债权人负责人");
        headerList.add("债权人负责人职位");


        headerList.add("被保险人");
        headerList.add("被保险人住所地");
        headerList.add("被保险人信用代码");
        headerList.add("被保险人负责人");
        headerList.add("被保险人负责人职位");
        headerList.add("被保险人性别");
        headerList.add("被保险人民族");
        headerList.add("被保险人出生日期");
        headerList.add("被保险人身份证号");
        headerList.add("被保险人联系电话");
        headerList.add("被保险人车牌号");
        headerList.add("被保险人车辆驾驶人");
        headerList.add("被保险人车辆驾驶人性别");
        headerList.add("被保险人车辆驾驶人民族");
        headerList.add("被保险人车辆驾驶人出生日期");
        headerList.add("被保险人车辆驾驶人户籍地");
        headerList.add("被保险人车辆驾驶人身份证号");
        headerList.add("被保险人车辆驾驶人联系电话");
        headerList.add("被保险人车辆驾驶人责任");

        headerList.add("受害方车辆驾驶人");
        headerList.add("受害方车辆车牌号");
        headerList.add("受害方车辆驾驶人责任");
        headerList.add("受害方车辆被保险人");
        headerList.add("交通事故判决书出具日期");
        headerList.add("交通事故判决法院");
        headerList.add("交通事故判决案号");
        headerList.add("交通事故判决金额");
        headerList.add("追偿案件诉讼费金额");
        headerList.add("追偿原因");


        headerList.add("致害人车辆车主");
        headerList.add("致害人车辆车主住所地");
        headerList.add("致害人车辆车主信用代码");
        headerList.add("致害人车辆车主负责人");
        headerList.add("致害人车辆车主负责人职位");
        headerList.add("致害人车辆车主性别");
        headerList.add("致害人车辆车主民族");
        headerList.add("致害人车辆车主出生日期");
        headerList.add("致害人车辆车主身份证号");
        headerList.add("致害人车辆车主联系电话");
        headerList.add("致害人车牌号");
        headerList.add("致害人车辆投保保险公司");
        headerList.add("致害人车辆投保保险公司联系电话");
        headerList.add("致害人车辆投保商业险名称");
        headerList.add("致害人车辆驾驶人");
        headerList.add("致害人车辆驾驶人性别");
        headerList.add("致害人车辆驾驶人民族");
        headerList.add("致害人车辆驾驶人出生日期");
        headerList.add("致害人车辆驾驶人户籍地");
        headerList.add("致害人车辆驾驶人身份证号");
        headerList.add("致害人车辆驾驶人联系电话");
        headerList.add("致害人车辆驾驶人责任");
        headerList.add("致害人车辆驾驶人责任比例");
        headerList.add("事故日期");
        headerList.add("债权金额");

        headerList.add("保险理赔承办人员");
        headerList.add("保险理赔承办人员联系电话");
        headerList.add("承办律师");
        headerList.add("承办律师联系电话");
        headerList.add("诉讼部承办律师");
        headerList.add("诉讼部承办律师联系电话");
        headerList.add("开户名信息");
        headerList.add("开户行信息");
        headerList.add("账号信息");
        headerList.add("立案受理案号");
        headerList.add("代位立案诉讼标的金额");
        headerList.add("保险公司理赔金额");
        headerList.add("已偿还金额");
        headerList.add("追偿立案诉讼标的金额");
        headerList.add("投资查询情况");
        headerList.add("特殊案件");
        headerList.add("代理费率");
        headerList.add("案情简介");
        headerList.add("保险理赔承办地址");
        headerList.add("律师函编号");
        headerList.add("律师函编号车主");
        headerList.add("已偿还金额取负数");
        headerList.add("案件类型");



        for (int i=0;i<headerList.size();i++){
            HSSFCell cell = rowOne.createCell(i);
            cell.setCellValue(headerList.get(i));
        }
        List<String> dataRowList = Lists.newArrayList();

        for (int i =0; i<caseInsuranceList.size();i++){

            HSSFRow rowTow = sheetOne.createRow((int)i+1);
            HSSFCell datacell = null;
            for (int j=0;j<headerList.size();j++){
                datacell = rowTow.createCell(j);
                if (j==0){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getCreditor());
                }else if(j==1){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getCreditorDomicile());
                }else if (j==2){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getCreditorIdcart());
                }else if (j==3){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getCreditorCharge());
                }else if (j==4){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getCreditorChargePositon());
                }else if (j==5){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsured());
                }else if (j==6){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredDomicile());
                }else if (j==7){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredIdcard());
                }else if (j==8){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredPersonCharge());
                }else if (j==9){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredPersonChargePosition());
                }else if (j==10){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredSex());
                }else if (j==11){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredPeople());
                }else if (j==12){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredBith());
                }else if (j==13){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredIdNumber());
                }else if (j==14){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredContactTelephone());
                }else if (j==15){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredLicenseNumber());
                }else if (j==16){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriver());
                }else if (j==17){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriverSex());
                }else if (j==17){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriverPeople());
                }else if (j==19){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriverBith());
                }else if (j==20){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriverHousehlod());
                }else if (j==21){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriverNumber());
                }else if (j==22){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredVehicleDriverTelephone());
                }else if (j==23){
                    datacell.setCellValue(DictUtils.getDictLabel(caseInsuranceList.get(i).getInsuredVehicleDriverLiability(),"driver_liability",""));
                }else if (j==24){
                    datacell.setCellValue(caseInsuranceList.get(i).getVehicleDriverInsuredParty());
                }else if (j==25){
                    datacell.setCellValue(caseInsuranceList.get(i).getVehicleDriverInsuredLicense());
                }else if (j==26){
                    datacell.setCellValue(caseInsuranceList.get(i).getVehicleDriverInsuredResponsibility());
                }else if (j==27){
                    datacell.setCellValue(caseInsuranceList.get(i).getVehicleInsuredVictim());
                }else if (j==28){
                    datacell.setCellValue(DateUtils.formatDate(caseInsuranceList.get(i).getAccidentJudgmentDate()));
                }else if (j==29){
                    datacell.setCellValue(caseInsuranceList.get(i).getCourtOfJudgment());
                }else if (j==30){
                    datacell.setCellValue(caseInsuranceList.get(i).getJudgmentNumber());
                }else if (j==31){
                    datacell.setCellValue(caseInsuranceList.get(i).getJudgmentAmount());
                }else if (j==32){
                    datacell.setCellValue(caseInsuranceList.get(i).getRecoveryCasesAmount());
                }else if (j==33){
                    datacell.setCellValue(caseInsuranceList.get(i).getReasonsRecourse());
                }else if (j==34){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwner());
                }else if (j==35){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerHousehlod());
                }else if (j==36){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerIdcard());
                }else if (j==37){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerResponsiblity());
                }else if (j==38){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerDuty());
                }else if (j==39){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulSex());
                }else if (j==40){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerPeople());
                }else if (j==41){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulBith());
                }else if (j==42){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerNumber());
                }else if (j==43){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerTelephone());
                }else if (j==44){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimVehicleOwnerLicense());
                }else if (j==45){
                    datacell.setCellValue(caseInsuranceList.get(i).getInjuriousInsuranceCompany());
                }else if (j==46){
                    datacell.setCellValue(caseInsuranceList.get(i).getInjuriousCompanyTelephone());
                }else if (j==47){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuredCommercialInsurance());
                }else if (j==48){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmful());
                }else if (j==49){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulSex());
                }else if (j==50){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulPeople());
                }else if (j==51){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulBith());
                }else if (j==52){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulHousehlod());
                }else if (j==53){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulIdcard());
                }else if (j==54){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulTelephone());
                }else if (j==55){
                    datacell.setCellValue(caseInsuranceList.get(i).getVictimDriverHarmfulResponsibility());
                }else if (j==56){
                    if (caseInsuranceList.get(i).getVictimDriverHarmfulResponsibility().equals("全部")){
                        datacell.setCellValue("100%");
                    }else if(caseInsuranceList.get(i).getVictimDriverHarmfulResponsibility().equals("主要")){
                        datacell.setCellValue("70%");
                    }else if(caseInsuranceList.get(i).getVictimDriverHarmfulResponsibility().equals("次要")){
                        datacell.setCellValue("30%");
                    }else if(caseInsuranceList.get(i).getVictimDriverHarmfulResponsibility().equals("同等")){
                        datacell.setCellValue("50%");
                    }else {
                        datacell.setCellValue("0");
                    }
                }else if (j==57){
                    datacell.setCellValue(DateUtils.formatDate(caseInsuranceList.get(i).getAccidentDate()));
                }else if (j==58){
                    datacell.setCellValue(caseInsuranceList.get(i).getCreditorAmount());
                }else if (j==59){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuranceClaimsContractors());
                }else if (j==60){
                    datacell.setCellValue(caseInsuranceList.get(i).getInsuranceClaimsTelephone());
                }else if (j==61){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getUndertakingLawyerName());
                }else if (j==62){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getUndertakingLawyerPhone());
                }else if (j==63){
                    if (caseInsuranceList.get(i).getCaseSpeed()!=null)
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseSpeed().getAttorneyLitigationName());
                }else if (j==64){
                    if (caseInsuranceList.get(i).getCaseSpeed()!=null)
                        datacell.setCellValue(caseInsuranceList.get(i).getCaseSpeed().getAttorneyLitigationTelphone());
                }else if (j==65){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getAccountNameInformation());
                }else if (j==66){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getOpeningBankInformation());
                }else if (j==67){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseCustomer().getAccountInformation());
                }else if (j==68){
                    datacell.setCellValue(caseInsuranceList.get(i).getAcceptanceNumebrOld());
                }else if (j==69){
                    datacell.setCellValue(caseInsuranceList.get(i).getRecoveryAmount());
                }else if (j==70){
                    datacell.setCellValue(caseInsuranceList.get(i).getClaimAmountInsuranceCompany());
                } else if (j==71){
                    datacell.setCellValue(caseInsuranceList.get(i).getAmountPaidBack());
                }else if (j==72){
                    if (caseInsuranceList.get(i).getCaseSpeed()!=null)
                        datacell.setCellValue(caseInsuranceList.get(i).getCaseSpeed().getInvestmentInquiries());
                }else if (j==73){
                    if (caseInsuranceList.get(i).getCaseSpeed()!=null)
                        datacell.setCellValue(caseInsuranceList.get(i).getCaseSpeed().getInvestmentInquiries());
                }
                else if (j==74){ //特殊案件
                    datacell.setCellValue("");
                }
                else if (j==75){//代理费率
                    datacell.setCellValue("");
                }
                else if (j==76){
                    datacell.setCellValue(caseInsuranceList.get(i).getCaseIntroduction());
                }else if (j==77){
                    datacell.setCellValue(caseInsuranceList.get(i).getLayerNumber()+caseInsuranceList.get(i).getLayerNumberOne());
                }else if (j==78){
                    datacell.setCellValue(caseInsuranceList.get(i).getLayerNumber()+caseInsuranceList.get(i).getLayerNumberTow());
                }else if (j==79){
                    datacell.setCellValue("");
                }else if (j==79){
                    if (caseInsuranceList.get(i).getCaseType().equals("0")){
                        datacell.setCellValue("追偿");
                    }else if (caseInsuranceList.get(i).getCaseType().equals("1")){
                        datacell.setCellValue("代位");
                    }else if (caseInsuranceList.get(i).getCaseType().equals("1")){
                        datacell.setCellValue("特殊");
                    }

                }



            }
        }

        // 第二步，将文件存到浏览器设置的下载位置
        String filename = fileName + ".xls";
        response.setContentType("application/ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                .concat(String.valueOf(URLEncoder.encode(filename, "UTF-8"))));
        OutputStream out = response.getOutputStream();
        try {
            wb.write(out);// 将数据写出去
            String str = "导出" + fileName + "成功！";
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
            String str1 = "导出" + fileName + "失败！";
            System.out.println(str1);
        } finally {
            out.close();
        }
    }

    public static List<CaseInsurance> changeList(List<CaseInsurance> caseInsurances){
        List <CaseInsurance> returnList = Lists.newArrayList();
        for (CaseInsurance caseInsurance :caseInsurances){
            if (StringUtils.isNotBlank(caseInsurance.getLayerNumberTow())) {
                returnList.add(caseInsurance);
                CaseInsurance caseInsuranceTemp = new CaseInsurance();
                if (caseInsurance.getCaseType().equals("0")){ //追偿
                    caseInsuranceTemp.setCaseType(caseInsurance.getCaseType());
                    caseInsuranceTemp.setInsuredVehicleDriver(caseInsurance.getInsuredVehicleDriver());
                    caseInsuranceTemp.setLayerNumberTow(caseInsurance.getLayerNumberTow());
                }else {
                    caseInsuranceTemp.setCaseType(caseInsurance.getCaseType());
                    caseInsuranceTemp.setVictimDriverHarmful(caseInsurance.getVictimDriverHarmful());
                    caseInsuranceTemp.setLayerNumberTow(caseInsurance.getLayerNumberTow());
                }
                returnList.add(caseInsuranceTemp);
            }else {
                returnList.add(caseInsurance);
            }

        }
        return returnList;
    }





}
