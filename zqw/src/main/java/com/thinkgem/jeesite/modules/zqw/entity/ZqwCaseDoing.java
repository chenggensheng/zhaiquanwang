/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 案件进展Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwCaseDoing extends DataEntity<ZqwCaseDoing> {
	
	private static final long serialVersionUID = 1L;
	private String caseId;		// case_id
	private String caseContent;		// case_content
	
	public ZqwCaseDoing() {
		super();
	}

	public ZqwCaseDoing(String id){
		super(id);
	}

	@Length(min=0, max=64, message="case_id长度必须介于 0 和 64 之间")
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	@Length(min=0, max=255, message="case_content长度必须介于 0 和 255 之间")
	public String getCaseContent() {
		return caseContent;
	}

	public void setCaseContent(String caseContent) {
		this.caseContent = caseContent;
	}
	
}