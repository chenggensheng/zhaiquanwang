/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.dao.CasePaymentDao;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseXbbInsuranceDao;
import com.thinkgem.jeesite.modules.zqwcase.dao.XbbCommunicateDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseInsuranceDao;

/**
 * 案件信息Service
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@Service
@Transactional(readOnly = true)
public class CaseInsuranceService extends CrudService<CaseInsuranceDao, CaseInsurance> {


	@Autowired
	CaseXbbInsuranceDao caseXbbInsuranceDao;

	@Autowired
	CasePaymentDao casePaymentDao;


	@Autowired
	XbbCommunicateDao xbbCommunicateDao;

	public CaseInsurance get(String id) {
		return super.get(id);
	}
	
	public List<CaseInsurance> findList(CaseInsurance caseInsurance) {
		return super.findList(caseInsurance);
	}
	
	public Page<CaseInsurance> findPage(Page<CaseInsurance> page, CaseInsurance caseInsurance) {
		return super.findPage(page, caseInsurance);
	}


	public Page<CaseInsurance> findTypePage(Page<CaseInsurance> page,CaseInsurance caseInsurance){
		caseInsurance.setPage(page);
		page.setList(dao.findTypeList(caseInsurance));
		return page;

	}
	
	@Transactional(readOnly = false)
	public void save(CaseInsurance caseInsurance) {
		super.save(caseInsurance);
	}
	
	@Transactional(readOnly = false)
	public void delete(CaseInsurance caseInsurance) {
		super.delete(caseInsurance);
	}


	public List<CaseInsurance> findNumList(CaseInsurance caseInsurance){
      return    dao.findNumList(caseInsurance);
	}
	@Transactional(readOnly = false)
	public  void  caseSuperviseboardSave(List<CaseXbbInsurance> caseXbbInsurances) {

//		List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceDao.findCountList();
		for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
			if ((caseXbbInsurance.getNonPresecutioDate() !=null
					|| caseXbbInsurance.getLawsuitStartDate()!=null
					|| caseXbbInsurance.getExecutionStratDate()!=null)
					&&( caseXbbInsurance.getzEndTime()==null && caseXbbInsurance.getzEndTime()==null)){

				caseXbbInsurance.setUndertakingStatus("进行中");
				caseXbbInsuranceDao.update(caseXbbInsurance);

			}


			if ((caseXbbInsurance.getNonPresecutioDate() !=null
					|| caseXbbInsurance.getLawsuitStartDate()!=null
					|| caseXbbInsurance.getExecutionStratDate()!=null)
					&&( caseXbbInsurance.getzEndTime()!=null || caseXbbInsurance.getzEndTime()!=null)){

				caseXbbInsurance.setUndertakingStatus("已完结");
				caseXbbInsuranceDao.update(caseXbbInsurance);

			}


			if (caseXbbInsurance.getNonPresecutioDate()!=null
					&&caseXbbInsurance.getLawsuitStartDate()==null
					&&caseXbbInsurance.getExecutionStratDate()==null
					&&caseXbbInsurance.getzEndTime()==null
					&&caseXbbInsurance.getZwEndTime()==null
					){
				caseXbbInsurance.setCurrentStage("非诉催收");
				caseXbbInsuranceDao.update(caseXbbInsurance);
			}


			if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					&& caseXbbInsurance.getExecutionStratDate()==null){

				caseXbbInsurance.setCurrentStage("诉讼");
				caseXbbInsuranceDao.update(caseXbbInsurance);

			}else if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					&& caseXbbInsurance.getExecutionStratDate()==null){

				caseXbbInsurance.setCurrentStage("诉讼");
				caseXbbInsuranceDao.update(caseXbbInsurance);
			}

			if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
					&& caseXbbInsurance.getOutSourcing().equals("自行执行")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					){

				caseXbbInsurance.setCurrentStage("执行");
				caseXbbInsuranceDao.update(caseXbbInsurance);

			}else if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
					&& caseXbbInsurance.getOutSourcing().equals("委外执行")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null) {

				caseXbbInsurance.setCurrentStage("执行");
				caseXbbInsuranceDao.update(caseXbbInsurance);

			}

			if (caseXbbInsurance.getzEndTime()!=null ||
					caseXbbInsurance.getZwEndTime() !=null){  //========
				caseXbbInsurance.setCurrentStage("已终结");
				caseXbbInsuranceDao.update(caseXbbInsurance);
			}

		}




	}

    public CaseInsuranceBoard caseSuperviseboard(){
		CaseInsuranceBoard caseInsuranceBoard = new CaseInsuranceBoard();
		List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceDao.findCountList();
		List<CasePayment> casePayments = casePaymentDao.findList(new CasePayment());
		List<XbbCommunicate> xbbCommunicates = xbbCommunicateDao.findList(new XbbCommunicate());
		caseInsuranceBoard.setAllCount(Integer.toString(caseXbbInsurances.size()));
        int doingCount=0;
        int didCount = 0;
        int otherCount=0;
        int advanceProjectsCount=0;
		int nonComplaintCount =0 ;// 非诉催收
		int litigationsCount  =0 ;//诉讼阶段
		int implementsCount  =0 ;//执行阶段

		int selfLitigationsCount=0;// 自办诉讼阶段
		int outLitigationsCount =0;// 委外诉讼阶段
		int selfImplementsCount =0;// 自办执行阶段
		int outImplementsCount  =0;// 委外执行阶段
		int todayCount=0; //今天新增
		float allMoneyCount = 0f;// 合同总金额
		float planMoneyCount= 0f;;// 计划收款金额
		float overMonthCount= 0f;;//超一个月合同订单金额
		float backRaio= 0f;;//回款率

		Set<String> nameKeys = new HashSet<String>();

		List<CaseInsurancePersonBoard> caseInsurancePersonBoards = Lists.newArrayList();

        for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
        	if(StringUtils.isNotBlank(caseXbbInsurance.getUndertakingLawyer()) && !caseXbbInsurance.getUndertakingLawyer().equals("无") &&!caseXbbInsurance.getUndertakingLawyer().contains("暂无") && !caseXbbInsurance.getUndertakingLawyer().contains("暂未") && !caseXbbInsurance.getUndertakingLawyer().contains("分配") ){
			nameKeys.add(caseXbbInsurance.getUndertakingLawyer());
        	}
        	if (caseXbbInsurance.getUndertakingStatus().equals("进行中")){
				doingCount++;
			}else if (caseXbbInsurance.getUndertakingStatus().equals("已完结")){
				didCount++;
			}else {
				if (caseXbbInsurance.getUndertakingStatus().equals("预立项")){
					advanceProjectsCount++;
				}
				otherCount++;
			}

			if (caseXbbInsurance.getCreateTime() !=null){
			if (DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getCreateTime(),new Date())>30.0){
				overMonthCount ++;
			}
			}


			if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
					&& caseXbbInsurance.getOutSourcing().equals("自行执行")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					){
					selfImplementsCount++;
					implementsCount++;
			}else if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
					&& caseXbbInsurance.getOutSourcing().equals("委外执行")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null) {
					outImplementsCount++;
					implementsCount++;
			}


			if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					&& caseXbbInsurance.getExecutionStratDate()==null){
					selfLitigationsCount++;
					litigationsCount++;
				}else if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					&& caseXbbInsurance.getExecutionStratDate()==null){
					outLitigationsCount++;
					litigationsCount++;
			}



			if (caseXbbInsurance.getNonPresecutioDate()!=null
				      &&caseXbbInsurance.getLawsuitStartDate()==null
					  &&caseXbbInsurance.getExecutionStratDate()==null
					  &&caseXbbInsurance.getzEndTime()==null
					  &&caseXbbInsurance.getZwEndTime()==null
					){
				nonComplaintCount++;
			}

			if (StringUtils.isNotBlank(caseXbbInsurance.getContractAmount())){

				if (caseXbbInsurance.getUndertakingStatus().equals("进行中")||
						caseXbbInsurance.getUndertakingStatus().equals("预立项")||
						caseXbbInsurance.getUndertakingStatus().equals("已完结")){
				allMoneyCount += Float.valueOf(caseXbbInsurance.getContractAmount());
			   }
			}

			if (StringUtils.isNotBlank(caseXbbInsurance.getContractAmount())){
				if (caseXbbInsurance.getNonPresecutioDate()!=null && DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getCreateTime(),new Date())>=30){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}else if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()!=null && DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())>=30){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}else if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()==null
						&&  caseXbbInsurance.getExecutionStratDate()!=null
						&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getExecutionStratDate(),new Date())>=30){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}
			}

			if (caseXbbInsurance.getNonPresecutioDate()!=null &&
						DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"yyyy").equals(DateUtils.getYear())
						&&DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"MM").equals(DateUtils.getMonth())
						&&DateUtils.getIntDay(DateUtils.parseDate(caseXbbInsurance.getSigningDate()))==DateUtils.getIntDay()-1){
					todayCount++;
				}else if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()!=null &&
						DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"yyyy").equals(DateUtils.getYear())
						&&DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"MM").equals(DateUtils.getMonth())
						&&DateUtils.getIntDay(DateUtils.parseDate(caseXbbInsurance.getSigningDate()))==DateUtils.getIntDay()-1){
					todayCount++;
				}else if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()==null
						&&  caseXbbInsurance.getExecutionStratDate()!=null
						&&DateUtils.formatDate(caseXbbInsurance.getCreateDate(),"yyyy").equals(DateUtils.getYear())
						&&DateUtils.formatDate(caseXbbInsurance.getCreateDate(),"MM").equals(DateUtils.getMonth())
						&&DateUtils.getIntDay(caseXbbInsurance.getCreateDate())==DateUtils.getIntDay()-1){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}
			}



		for (String nameKey:nameKeys){

        	if(!nameKey.equals("胡鑫")){
        	CaseInsurancePersonBoard caseInsurancePersonBoard = new CaseInsurancePersonBoard();
			caseInsurancePersonBoard.setName(nameKey);

			int nonComplaintCountPerson =0 ;// 非诉催收
			int nonOverOneComplatinCountPerson =0;//超过一个月
			int  nonOverTowComplatinCountPerson=0;//超过二个月
			int qqCount= 0; //缺
			int bCount= 0;  //补

			int litigationsCountPerson  =0 ;//诉讼阶段
			int selfLitigationsCountPerson =0;//自办总数
			int selfOverOneCount=0;
			int selfOverTowCount=0;
			int selflhwkCount =0;
			int selfykwpCount =0;
			int selfypwsCount =0;
			int selfpjsxCount=0;
			int selfqsCount=0;
			int selfpcCount=0;




			int outLitigationsCountPerson =0;//委外


			int outOverfiveCount =0;//委外超15天
			int outOverthreeCount=0;//委外超30天
			int outOverOneCount=0;// 超1未立
			int outOverTowCount=0;//超2未立
			int outlhwkCount=0;//立后无开
			int outykwpCount=0;//已开未判
			int outypwsCount=0;//已判未生
			int outpjsxCount=0;//判决生效
			int outqsCount=0;//缺
			int outpcCount=0;//补充




			int implementsCountPerson =0;//执行阶段
			int selfImplementsCountPerson =0;//自办
			int selfOverOneImplementsCountPerson =0;//超1未申立
			int selfOverTowImplementsCountPerson =0;//超2未申立
			int selfOverThreeImplementsCountPerson =0;//超3未终
			int selfOverSixImplementsCountPerson =0;//超6未终

			int outImplementsCountPerson =0;//委外
			int outFiveImplementsCountPerson =0;//委外15
			int outThreeImplementsCountPerson =0;//委外30
			int outOverOneImplementsCountPerson =0;//超1未申立
			int outOverTowImplementsCountPerson =0;//超2未申立
			int outOverThreeImplementsCountPerson =0;//超3未终
			int outOverSixImplementsCountPerson =0;//超6未终


			int todayFollowAllCaseCount =0;
			int todayFollowCaseCount=0;


			List<CaseXbbInsurance> nonComplaintPersons = Lists.newArrayList();// 个人非诉催收
			List<CaseXbbInsurance> litigationsPersons = Lists.newArrayList();//诉讼阶段
			List<CaseXbbInsurance> implementsPersons = Lists.newArrayList();//执行阶段

			List<XbbCommunicate> nonComplaintXbbCommunicates = Lists.newArrayList();
			List<CaseXbbInsurance> selfLitigationPersons = Lists.newArrayList();// 自行诉讼
			List<CaseXbbInsurance> outLitigationPersons = Lists.newArrayList();//委外诉讼

			List<XbbCommunicate> litigationsXbbCommunicates = Lists.newArrayList();
			List<XbbCommunicate> implementsXbbCommunicates = Lists.newArrayList();

			List<CaseXbbInsurance> selfImplementsPersons = Lists.newArrayList();
			List<CaseXbbInsurance> outImplementsPersons = Lists.newArrayList();


			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
					if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行执行")
							&& caseXbbInsurance.getzEndTime() ==null && caseXbbInsurance.getZwEndTime() ==null && caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){
						selfImplementsCountPerson++;
						implementsCountPerson++;
						selfImplementsPersons.add(caseXbbInsurance);

					}else if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("委外执行")
							&& caseXbbInsurance.getzEndTime() ==null && caseXbbInsurance.getZwEndTime() ==null&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)) {
						outImplementsCountPerson++;
						implementsCountPerson++;
						outImplementsPersons.add(caseXbbInsurance);

					}



					if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){
						if (caseXbbInsurance.getzEndTime() == null && caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfLitigationsCountPerson++;
							litigationsCountPerson++;
							selfLitigationPersons.add(caseXbbInsurance);
						}
						if (caseXbbInsurance.getLawsuitStartDate()!=null
								&& (DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())>=30.0
								    &&DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())<60.0 )
								&& caseXbbInsurance.getzEndTime() == null
						        && caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							 selfOverOneCount++;
						}
						if (caseXbbInsurance.getLawsuitStartDate()!=null
								&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())>=60.0
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							 selfOverTowCount++;
						}


						if (caseXbbInsurance.getsInstant()!=null
								&& caseXbbInsurance.getsTurnOnTime() ==null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selflhwkCount++;
						}


						if (caseXbbInsurance.getsTurnOnTime() !=null
								&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getsTurnOnTime(),new Date())>=0
								&& caseXbbInsurance.getsJudgment()==null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfykwpCount++;
						}

						if (caseXbbInsurance.getsJudgment()!=null && caseXbbInsurance.getsJudgmentTime()==null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfypwsCount++;
						}
						if (caseXbbInsurance.getsJudgmentTime()!=null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfpjsxCount++;
						}


						if (StringUtils.isNotBlank(caseXbbInsurance.getSourceStauts())&&caseXbbInsurance.getSourceStauts().equals("0")
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null
								){
							selfqsCount++;
						}else if (StringUtils.isNotBlank(caseXbbInsurance.getSourceStauts())&&caseXbbInsurance.getSourceStauts().equals("1")
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfpcCount++;
						}

					}else if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){

						if (caseXbbInsurance.getLawsuitStartDate()!=null
//								&& caseXbbInsurance.getSwOpenTime()==null
								&& caseXbbInsurance.getzEndTime() ==null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							outLitigationPersons.add(caseXbbInsurance);
							outLitigationsCountPerson++;
							litigationsCountPerson++;
						}
					}

				if (
						caseXbbInsurance.getNonPresecutioDate()!=null
						&&caseXbbInsurance.getLawsuitStartDate()==null
						&&caseXbbInsurance.getExecutionStratDate()==null
						&&caseXbbInsurance.getzEndTime()==null
						&&caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){
					nonComplaintCountPerson++;
					nonComplaintPersons.add(caseXbbInsurance);
				}


				if (caseXbbInsurance.getNonPresecutioDate()!=null
								&&caseXbbInsurance.getLawsuitStartDate()==null
								&&caseXbbInsurance.getExecutionStratDate()==null
								&&caseXbbInsurance.getzEndTime()==null
								&&caseXbbInsurance.getZwEndTime()==null
								&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)
						&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getNonPresecutioDate(),new Date())>=30
						&&DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getNonPresecutioDate(),new Date())<60
						){
					nonOverOneComplatinCountPerson++;
				}

				if (caseXbbInsurance.getNonPresecutioDate()!=null
						&&caseXbbInsurance.getLawsuitStartDate()==null
						&&caseXbbInsurance.getExecutionStratDate()==null
						&&caseXbbInsurance.getzEndTime()==null
						&&caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)
						&&DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getNonPresecutioDate(),new Date())>=60
						){
					nonOverTowComplatinCountPerson++;
				}


			}

			for (XbbCommunicate xbbCommunicate:xbbCommunicates){
				if (xbbCommunicate.getVistPerson().equals(nameKey)){
					if (DateUtils.formatDate(DateUtils.parseDate(xbbCommunicate.getCreateTimes()),"yyyy").equals(DateUtils.getYear())
							&&DateUtils.formatDate(DateUtils.parseDate(xbbCommunicate.getCreateTimes()),"MM").equals(DateUtils.getMonth())
							&&DateUtils.getIntDay(DateUtils.parseDate(xbbCommunicate.getCreateTimes()))==DateUtils.getIntDay()
							){
						litigationsXbbCommunicates.add(xbbCommunicate);
						todayFollowAllCaseCount++;
					}
				}
			}

			List<XbbCommunicate> caseXbbCommunicateTemps = Lists.newArrayList();
			for(XbbCommunicate xbbCommunicate : litigationsXbbCommunicates){
				if (caseXbbCommunicateTemps.size()==0){
					caseXbbCommunicateTemps.add(xbbCommunicate);
				}else {
					boolean addFlag= true;
					for (XbbCommunicate temp:caseXbbCommunicateTemps){
						if (temp.getXbbInsuranceId().equals(xbbCommunicate.getXbbInsuranceId())){
							addFlag = false;
						}
					}

					if (addFlag){
						caseXbbCommunicateTemps.add(xbbCommunicate);
					}
				}


			}


			for (CaseXbbInsurance temp: outLitigationPersons){
				if (temp.getSwTimeShift()!=null&&
						DateUtils.getDistanceOfTwoDate(temp.getSwTimeShift(),new Date())>=15
						&&DateUtils.getDistanceOfTwoDate(temp.getSwTimeShift(),new Date())<30
						&& temp.getSwEndTime() == null){
					outOverfiveCount++;

				}
				if (temp.getSwTimeShift()!=null
						&&DateUtils.getDistanceOfTwoDate(temp.getSwTimeShift(),new Date())>=30
						&& temp.getSwEndTime() == null){
					outOverthreeCount++;
				}
				if (temp.getSwEndTime()!=null
						&&DateUtils.getDistanceOfTwoDate(temp.getSwEndTime(),new Date())>=30
						&&DateUtils.getDistanceOfTwoDate(temp.getSwEndTime(),new Date())<60
						&& temp.getSwInstant() == null){
					outOverOneCount++;// 超1未立
				}
				if (temp.getSwEndTime()!=null
						&&DateUtils.getDistanceOfTwoDate(temp.getSwEndTime(),new Date())>=60
						&& temp.getSwInstant() == null){
					outOverTowCount++;//超2未立

				}
				if (StringUtils.isNotBlank(temp.getSourceStauts())&&temp.getSourceStauts().equals("0")){
					outqsCount++;//缺
				}else if (StringUtils.isNotBlank(temp.getSourceStauts())&&temp.getSourceStauts().equals("1")){
					outpcCount++;//补充
				}
				if (temp.getSwInstant() !=null && temp.getSwOpenTime() ==null){
					outlhwkCount++;
				}
				if (temp.getSwOpenTime() !=null
						&&DateUtils.getDistanceOfTwoDate(temp.getSwInstant(),new Date())>0
						&& temp.getSwJudgment()==null){
					outykwpCount++;
				}
				if (temp.getSwJudgment() !=null
						&& temp.getSwJudgmentTime()==null){
					outypwsCount++;
				}
				if (temp.getSwJudgmentTime()!=null){
					outpjsxCount++;
				}
			}


			if (selfImplementsPersons!=null && selfImplementsPersons.size()>0){
				for (CaseXbbInsurance temp:selfImplementsPersons){
					if (temp.getExecutionStratDate()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())>=30
							&&temp.getzShenShi() == null
							){
						selfOverOneImplementsCountPerson++;
					}
					if (temp.getExecutionStratDate()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getzShenShi(),new Date())>=90
							&&DateUtils.getDistanceOfTwoDate(temp.getzShenShi(),new Date())<180
							&&temp.getzEndTime()==null){
						selfOverThreeImplementsCountPerson++;
					}
					if (temp.getExecutionStratDate()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getzShenShi(),new Date())>=180
							&&temp.getzEndTime()==null){
						selfOverSixImplementsCountPerson++;
					}
				}
			}

			if (outImplementsPersons!=null && outImplementsPersons.size()>0){
				for (CaseXbbInsurance temp:outImplementsPersons){
					if (temp.getExecutionStratDate()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())>=15
							&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())<30
							&&temp.getZwShenShi()==null){
						outFiveImplementsCountPerson++;
					}

					if (temp.getExecutionStratDate()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())>=30
							&&temp.getZwShenShi()==null
							){
						outThreeImplementsCountPerson++;
					}


					if (temp.getExecutionStratDate()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getZwFinishTime(),new Date())>=30
							&&temp.getZwShenShi()==null
							){
						outOverOneImplementsCountPerson++;
					}

					if (temp.getZwShenShi()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getZwShenShi(),new Date())>=90
							&&DateUtils.getDistanceOfTwoDate(temp.getZwShenShi(),new Date())<180
							&&temp.getzEndTime()==null
							){
						outOverThreeImplementsCountPerson++;
					}

					if (temp.getZwShenShi()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getZwShenShi(),new Date())>=180
							&&temp.getzEndTime()==null
							){
						outOverSixImplementsCountPerson++;
					}

				}
			}


			for (CaseXbbInsurance caseXbbInsuranceTemp : nonComplaintPersons){

				if (StringUtils.isNotBlank(caseXbbInsuranceTemp.getSourceStauts())&&caseXbbInsuranceTemp.getSourceStauts().equals("0")){
					qqCount++;
				}else if (StringUtils.isNotBlank(caseXbbInsuranceTemp.getSourceStauts())&&caseXbbInsuranceTemp.getSourceStauts().equals("1")){
					bCount++;
				}

			}

			for (XbbCommunicate xbbCommunicate:xbbCommunicates){
				if (xbbCommunicate.getVistPerson().equals(nameKey)){
					nonComplaintXbbCommunicates.add(xbbCommunicate);
				}
			}
			List<XbbCommunicate> nowNonComplaintXbbCommunicates = Lists.newArrayList();
			for (XbbCommunicate xbbCommunicate:nonComplaintXbbCommunicates){
				boolean addFlag = false;
				for (CaseXbbInsurance caseXbbInsuranceTemp : nonComplaintPersons){
					if (caseXbbInsuranceTemp.getId().equals(xbbCommunicate.getXbbInsuranceId())){
						addFlag = true;
					}
				}
				if (addFlag){
					nowNonComplaintXbbCommunicates.add(xbbCommunicate);
				}


			}

			List<XbbCommunicate> xbbCommunicatesY= Lists.newArrayList();
			List<XbbCommunicate> xbbCommunicatesE= Lists.newArrayList();
			List<XbbCommunicate> xbbCommunicatesS= Lists.newArrayList();
			List<XbbCommunicate> xbbCommunicatesX= Lists.newArrayList();
			List<XbbCommunicate> xbbCommunicatesW= Lists.newArrayList();
			List<XbbCommunicate> xbbCommunicatesQ= Lists.newArrayList();

			for (XbbCommunicate xbbCommunicate :nowNonComplaintXbbCommunicates){
				if (xbbCommunicate.getVistWay().equals("F意还")){
					if (xbbCommunicatesY.size()==0){
						xbbCommunicatesY.add(xbbCommunicate);
					}else {
						boolean addFlag = true;
						for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesY){

							if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
								addFlag = false;
								if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

								}else {
									xbbCommunicatesY.remove(xbbCommunicateTemp);
									xbbCommunicatesY.add(xbbCommunicate);
								}
							}
						}

						if (addFlag){
							xbbCommunicatesY.add(xbbCommunicate);
						}
					}

				}else if (xbbCommunicate.getVistWay().equals("F恶拖")){
					if (xbbCommunicatesE.size()==0){
						xbbCommunicatesE.add(xbbCommunicate);
					}else {
						boolean addFlag = true;
						for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesE){

							if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
								addFlag = false;
								if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

								}else {
									xbbCommunicatesE.remove(xbbCommunicateTemp);
									xbbCommunicatesE.add(xbbCommunicate);
								}
							}
						}
						if (addFlag){
							xbbCommunicatesE.add(xbbCommunicate);
						}
					}

				}else if (xbbCommunicate.getVistWay().equals("F失联")){

					if (xbbCommunicatesS.size()==0){
						xbbCommunicatesS.add(xbbCommunicate);
					}else {
						boolean addFlag = true;
						for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesS){

							if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
								addFlag = false;
								if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

								}else {
									xbbCommunicatesS.remove(xbbCommunicateTemp);
									xbbCommunicatesS.add(xbbCommunicate);
								}
							}
						}
						if (addFlag){
							xbbCommunicatesS.add(xbbCommunicate);
						}
					}

				}else if (xbbCommunicate.getVistWay().equals("F失修")){
					if (xbbCommunicatesX.size()==0){
						xbbCommunicatesX.add(xbbCommunicate);
					}else {
						boolean addFlag = true;
						for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesX){

							if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
								addFlag = false;
								if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

								}else {
									xbbCommunicatesX.remove(xbbCommunicateTemp);
									xbbCommunicatesX.add(xbbCommunicate);
								}
							}
						}
						if (addFlag){
							xbbCommunicatesX.add(xbbCommunicate);
						}
					}


				}else if (xbbCommunicate.getVistWay().equals("F无听")){
					if (xbbCommunicatesW.size()==0){
						xbbCommunicatesW.add(xbbCommunicate);
					}else {
						boolean addFlag = true;
						for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesW){

							if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
								addFlag = false;
								if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

								}else {
									xbbCommunicatesW.remove(xbbCommunicateTemp);
									xbbCommunicatesW.add(xbbCommunicate);
								}
							}
						}
						if (addFlag){
							xbbCommunicatesW.add(xbbCommunicate);
						}
					}

				}
				else if (xbbCommunicate.getVistWay().equals("F其它")){

					if (xbbCommunicatesQ.size()==0){
						xbbCommunicatesQ.add(xbbCommunicate);
					}else {
						boolean addFlag = true;
						for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesQ){

							if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
								addFlag = false;
								if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

								}else {
									xbbCommunicatesQ.remove(xbbCommunicateTemp);
									xbbCommunicatesQ.add(xbbCommunicate);
								}
							}
						}
						if (addFlag){
							xbbCommunicatesQ.add(xbbCommunicate);
						}
					}
				}
			}

			//=============非诉阶段开始===============//
			caseInsurancePersonBoard.setNonComplaintCount(StringUtils.changeIntNuns(nonComplaintCountPerson));
			caseInsurancePersonBoard.setNonOverOneComplatinCount(StringUtils.changeIntNuns(nonOverOneComplatinCountPerson));
			caseInsurancePersonBoard.setNonOverTowComplatinCount(StringUtils.changeIntNuns(nonOverTowComplatinCountPerson));

			caseInsurancePersonBoard.setyCount(Integer.toString(xbbCommunicatesY.size()));
			caseInsurancePersonBoard.seteCount(Integer.toString(xbbCommunicatesE.size()));
			caseInsurancePersonBoard.setsCount(Integer.toString(xbbCommunicatesS.size()));
			caseInsurancePersonBoard.setxCount(Integer.toString(xbbCommunicatesX.size()));
			caseInsurancePersonBoard.setwCount(Integer.toString(xbbCommunicatesW.size()));
			caseInsurancePersonBoard.setqCount(Integer.toString(xbbCommunicatesQ.size()));
			caseInsurancePersonBoard.setbCount(Integer.toString(bCount));
			caseInsurancePersonBoard.setQqCount(Integer.toString(qqCount));
			//=============非诉阶段结束===============//



            //=============诉讼阶段开始================//
			//=====自行诉讼开始====//
			caseInsurancePersonBoard.setLitigationsCount(StringUtils.changeIntNuns(litigationsCountPerson));
			caseInsurancePersonBoard.setSelfLitigationsCount(StringUtils.changeIntNuns(selfLitigationsCountPerson));
			caseInsurancePersonBoard.setSelfOverOneCount(StringUtils.changeIntNuns(selfOverOneCount));
			caseInsurancePersonBoard.setSelfOverTowCount(StringUtils.changeIntNuns(selfOverTowCount));
			caseInsurancePersonBoard.setSelflhwkCount(StringUtils.changeIntNuns(selflhwkCount));
			caseInsurancePersonBoard.setSelfykwpCount(StringUtils.changeIntNuns(selfykwpCount));
			caseInsurancePersonBoard.setSelfypwsCount(StringUtils.changeIntNuns(selfypwsCount));
			caseInsurancePersonBoard.setSelfpjsxCount(StringUtils.changeIntNuns(selfpjsxCount));
			caseInsurancePersonBoard.setSelfqsCount(StringUtils.changeIntNuns(selfqsCount));
			caseInsurancePersonBoard.setSelfpcCount(StringUtils.changeIntNuns(selfpcCount));


			//=====自行诉讼结束====//

			//=====委外诉讼开始====//
			caseInsurancePersonBoard.setOutLitigationsCount(StringUtils.changeIntNuns(outLitigationsCountPerson));
			caseInsurancePersonBoard.setOutOverfiveCount(StringUtils.changeIntNuns(outOverfiveCount));
			caseInsurancePersonBoard.setOutOverthreeCount(StringUtils.changeIntNuns(outOverthreeCount));
			caseInsurancePersonBoard.setOutOverOneCount(StringUtils.changeIntNuns(outOverOneCount));
			caseInsurancePersonBoard.setOutOverTowCount(StringUtils.changeIntNuns(outOverTowCount));
			caseInsurancePersonBoard.setOutlhwkCount(StringUtils.changeIntNuns(outlhwkCount));
			caseInsurancePersonBoard.setOutykwpCount(StringUtils.changeIntNuns(outykwpCount));
			caseInsurancePersonBoard.setOutypwsCount(StringUtils.changeIntNuns(outypwsCount));
			caseInsurancePersonBoard.setOutpjsxCount(StringUtils.changeIntNuns(outpjsxCount));
			caseInsurancePersonBoard.setOutqsCount(StringUtils.changeIntNuns(outqsCount));
			caseInsurancePersonBoard.setOutpcCount(StringUtils.changeIntNuns(outpcCount));

			//=====委外诉讼结束====//
			//=============诉讼阶段结束================//

            //=============执行阶段开始================//
			//=====自行诉讼开始====//
			caseInsurancePersonBoard.setImplementsCount(StringUtils.changeIntNuns(implementsCountPerson));
			caseInsurancePersonBoard.setSelfImplementsCount(StringUtils.changeIntNuns(selfImplementsCountPerson));
			caseInsurancePersonBoard.setSelfOverOneImplementsCount(StringUtils.changeIntNuns(selfOverOneImplementsCountPerson));
			caseInsurancePersonBoard.setSelfOverTowImplementsCount(StringUtils.changeIntNuns(selfOverTowImplementsCountPerson));
			caseInsurancePersonBoard.setSelfOverThreeImplementsCount(StringUtils.changeIntNuns(selfOverThreeImplementsCountPerson));
			caseInsurancePersonBoard.setSelfOverSixImplementsCount(StringUtils.changeIntNuns(selfOverSixImplementsCountPerson));
			//=====自行诉讼结束====//

			//=====委外诉讼开始====//
			caseInsurancePersonBoard.setOutImplementsCount(StringUtils.changeIntNuns(outImplementsCountPerson));
            caseInsurancePersonBoard.setOutFiveImplementsCount(StringUtils.changeIntNuns(outFiveImplementsCountPerson));
            caseInsurancePersonBoard.setOutThreeImplementsCount(StringUtils.changeIntNuns(outThreeImplementsCountPerson));
            caseInsurancePersonBoard.setOutOverOneImplementsCount(StringUtils.changeIntNuns(outOverOneImplementsCountPerson));
            caseInsurancePersonBoard.setOutOverTowImplementsCount(StringUtils.changeIntNuns(outOverTowImplementsCountPerson));
            caseInsurancePersonBoard.setOutOverThreeImplementsCount(StringUtils.changeIntNuns(outOverThreeImplementsCountPerson));
            caseInsurancePersonBoard.setOutOverSixImplementsCount(StringUtils.changeIntNuns(outOverSixImplementsCountPerson));
			//=====委外诉讼结束====//
			//=============执行阶段结束================//


			caseInsurancePersonBoard.setTodayFollowAllCaseCount(StringUtils.changeIntNuns(todayFollowAllCaseCount));
			caseInsurancePersonBoard.setTodayFollowCaseCount(StringUtils.changeIntNuns(caseXbbCommunicateTemps.size()));
			caseInsurancePersonBoards.add(caseInsurancePersonBoard);
        	}
		}

		List<CaseXbbInsurance> implementsLists = Lists.newArrayList();
		List<CaseXbbInsurance> litigationsLists = Lists.newArrayList();
		List<CaseXbbInsurance> nonComplaintLists = Lists.newArrayList();

	    for(CaseXbbInsurance caseXbbInsurance:caseXbbInsurances){
			if (caseXbbInsurance.getCurrentStage().equals("执行")){
				implementsLists.add(caseXbbInsurance);
			}else if (caseXbbInsurance.getCurrentStage().equals("诉讼")){
				litigationsLists.add(caseXbbInsurance);
			}else if (caseXbbInsurance.getCurrentStage().equals("非诉催收")){
				nonComplaintLists.add(caseXbbInsurance);
			}
		}

		Integer implementsOneMonth =0;
		Integer implementsTowMonth =0;
		Integer implementsThreeMonth =0;


		Integer litigationsOneMonth =0;
		Integer litigationsTowMonth =0;
		Integer litigationsThreeMonth =0;

		Integer nonComplaintOneMonth =0;
		Integer nonComplaintTowMonth =0;
		Integer nonComplaintThreeMonth =0;


		for (XbbCommunicate xbbCommunicate : xbbCommunicates){
	    	for (CaseXbbInsurance  implementsTemp : implementsLists) {
	    		if (xbbCommunicate.getXbbInsuranceId().equals(implementsTemp.getId())){
	    			if (xbbCommunicate.getVistWay().equals("Z验执效")){
	    			String dateString = xbbCommunicate.getRemarks().split("#")[0];
	    			if (StringUtils.isNotBlank(dateString)){
	    				if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))>0&&
	    						DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<30.0){
							implementsOneMonth ++;
						}else if (30.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<60.0){
							implementsTowMonth++;
						}else if (60.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<90.0){
							implementsThreeMonth++;
						}
					}
	    			}
				}
			}

			for (CaseXbbInsurance  litigationsTemp : litigationsLists) {
				if (xbbCommunicate.getXbbInsuranceId().equals(litigationsTemp.getId())) {
					if (xbbCommunicate.getVistWay().equals("S验诉效")) {
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						if (StringUtils.isNotBlank(dateString)) {
							if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) > 0
									&& DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) < 30.0) {
								litigationsOneMonth++;
							} else if (30.0 <= DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) && DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) < 60.0) {
								litigationsTowMonth++;
							} else if (60.0 <= DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) && DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) < 90.0) {
								litigationsThreeMonth++;
							}
						}
					}
				}

			}

			for (CaseXbbInsurance  nonComplaintTemp : nonComplaintLists) {
				if (xbbCommunicate.getXbbInsuranceId().equals(nonComplaintTemp.getId())) {
					if (xbbCommunicate.getVistWay().equals("F验诉效")){
					String dateString = xbbCommunicate.getRemarks().split("#")[0];
					if (StringUtils.isNotBlank(dateString)){
						if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))>0 &&
								DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<30.0){
							nonComplaintOneMonth ++;
						}else if (30.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<60.0){
							nonComplaintTowMonth++;
						}else if (60.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<90.0){
							nonComplaintThreeMonth++;
						}
					}
				}
				}
			}

		}


		CaseInsuranceRisk implementsRisk = new CaseInsuranceRisk();
		CaseInsuranceRisk litigationsRisk = new CaseInsuranceRisk();
		CaseInsuranceRisk nonComplaintRisk = new CaseInsuranceRisk();

		implementsRisk.setTypeName("执行阶段");
		implementsRisk.setType("0");
		implementsRisk.setOneMounthCount(Integer.toString(implementsOneMonth));
		implementsRisk.setTowMounthCount(Integer.toString(implementsTowMonth));
		implementsRisk.setThreeMounthCount(Integer.toString(implementsThreeMonth));


		nonComplaintRisk.setTypeName("非诉阶段");
		nonComplaintRisk.setType("1");
		nonComplaintRisk.setOneMounthCount(Integer.toString(nonComplaintOneMonth));
		nonComplaintRisk.setTowMounthCount(Integer.toString(nonComplaintTowMonth));
		nonComplaintRisk.setThreeMounthCount(Integer.toString(nonComplaintThreeMonth));

		litigationsRisk.setTypeName("诉讼阶段");
		litigationsRisk.setType("2");
		litigationsRisk.setOneMounthCount(Integer.toString(litigationsOneMonth));
		litigationsRisk.setTowMounthCount(Integer.toString(litigationsTowMonth));
		litigationsRisk.setThreeMounthCount(Integer.toString(litigationsThreeMonth));


		List<CaseInsuranceRisk> caseInsuranceRisks  = Lists.newArrayList();
		caseInsuranceRisks.add(nonComplaintRisk);
		caseInsuranceRisks.add(litigationsRisk);
		caseInsuranceRisks.add(implementsRisk);


		for (CasePayment casePayment : casePayments){
			if (StringUtils.isNotBlank(casePayment.getAmount())){
				planMoneyCount += Float.valueOf(casePayment.getAmount());
			}
		}

        caseInsuranceBoard.setCaseInsuranceRisks(caseInsuranceRisks);
        caseInsuranceBoard.setDoingCount(StringUtils.changeIntNuns(doingCount));
        caseInsuranceBoard.setDidCount(StringUtils.changeIntNuns(didCount));
        caseInsuranceBoard.setOtherCount(StringUtils.changeIntNuns(otherCount));

		caseInsuranceBoard.setAdvanceProjectsCount(StringUtils.changeIntNuns(advanceProjectsCount));
		caseInsuranceBoard.setNonComplaintCount(StringUtils.changeIntNuns(nonComplaintCount));
		caseInsuranceBoard.setLitigationsCount(StringUtils.changeIntNuns(litigationsCount));
		caseInsuranceBoard.setImplementsCount(StringUtils.changeIntNuns(implementsCount));

		caseInsuranceBoard.setAllMoneyCount(StringUtils.changeNuns(allMoneyCount));
		caseInsuranceBoard.setPlanMoneyCount(StringUtils.changeNuns(planMoneyCount));
		caseInsuranceBoard.setOverMonthCount(StringUtils.changeNuns(overMonthCount));
		caseInsuranceBoard.setBackRaio(Float.toString(planMoneyCount/overMonthCount*100));

		caseInsuranceBoard.setSelfImplementsCount(StringUtils.changeIntNuns(selfImplementsCount));
		caseInsuranceBoard.setOutImplementsCount(StringUtils.changeIntNuns(outImplementsCount));
        caseInsuranceBoard.setSelfLitigationsCount(StringUtils.changeIntNuns(selfLitigationsCount));
		caseInsuranceBoard.setOutLitigationsCount(StringUtils.changeIntNuns(outLitigationsCount));

		caseInsuranceBoard.setDateString(DateUtils.getDate());
		caseInsuranceBoard.setCaseInsurancePersonBoards(caseInsurancePersonBoards);
		caseInsuranceBoard.setTodayCount(StringUtils.changeIntNuns(todayCount));

		return caseInsuranceBoard;
	}


	public CaseInsuranceBoard caseMaterialBoard(){
		CaseInsuranceBoard caseInsuranceBoard = new CaseInsuranceBoard();
		List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceDao.findCountList();
		List<XbbCommunicate> xbbCommunicates = xbbCommunicateDao.findList(new XbbCommunicate());


		Set<String> nameKeys = new HashSet<String>();

		List<CaseInsurancePersonBoard> caseInsurancePersonBoards = Lists.newArrayList();

		for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
			if(StringUtils.isNotBlank(caseXbbInsurance.getUndertakingLawyer())
					&& !caseXbbInsurance.getUndertakingLawyer().equals("无")
					&&!caseXbbInsurance.getUndertakingLawyer().contains("暂无")
					&& !caseXbbInsurance.getUndertakingLawyer().contains("暂未")
					&& !caseXbbInsurance.getUndertakingLawyer().contains("分配")
					&& caseXbbInsurance.getzEndTime() == null
					&& caseXbbInsurance.getZwEndTime() == null
					&& caseXbbInsurance.getsJudgmentTime() == null
					&& caseXbbInsurance.getSwJudgmentTime() ==null
					){


				StringBuilder fLackFunds = new StringBuilder();  //F资缺
				StringBuilder fSupplement = new StringBuilder(); //F资补
				StringBuilder sShortage = new StringBuilder();   //S资缺
				StringBuilder sSupplement = new StringBuilder(); //S资补
				StringBuilder swShortage = new StringBuilder(); //SW资缺
				StringBuilder swSupplement= new StringBuilder();//SW资补

				if(!caseXbbInsurance.getUndertakingLawyer().equals("胡鑫")){
					CaseInsurancePersonBoard caseInsurancePersonBoard = new CaseInsurancePersonBoard();
					caseInsurancePersonBoard.setCaseInsuranceName(caseXbbInsurance.getCaseName());
					caseInsurancePersonBoard.setName(caseXbbInsurance.getUndertakingLawyer());
					for (XbbCommunicate xbbCommunicate:xbbCommunicates){
						 if(caseXbbInsurance.getId().equals(xbbCommunicate.getXbbInsuranceId())){
							if (xbbCommunicate.getVistWay().equals("F资缺")){
								fLackFunds.append(xbbCommunicate.getUpdateTimes()).append(",").append(xbbCommunicate.getRemarks()).append("<br/>");
							}else if(xbbCommunicate.getVistWay().equals("F资补")){
								fSupplement.append(xbbCommunicate.getUpdateTimes()).append(",").append(xbbCommunicate.getRemarks()).append("<br/>");
							}else if(xbbCommunicate.getVistWay().equals("S资缺")){
								sShortage.append(xbbCommunicate.getUpdateTimes()).append(",").append(xbbCommunicate.getRemarks()).append("<br/>");
							}else if(xbbCommunicate.getVistWay().equals("S资补")){
								sSupplement.append(xbbCommunicate.getUpdateTimes()).append(",").append(xbbCommunicate.getRemarks()).append("<br/>");
							}else if(xbbCommunicate.getVistWay().equals("SW资缺")){
								swShortage.append(xbbCommunicate.getUpdateTimes()).append(",").append(xbbCommunicate.getRemarks()).append("<br/>");
							}else if(xbbCommunicate.getVistWay().equals("SW资补")){
								swSupplement.append(xbbCommunicate.getUpdateTimes()).append(",").append(xbbCommunicate.getRemarks()).append("<br/>");
							}
						 }
					}

					caseInsurancePersonBoard.setfLackFunds(fLackFunds.toString());
					caseInsurancePersonBoard.setfSupplement(fSupplement.toString());
					caseInsurancePersonBoard.setsShortage(sShortage.toString());
					caseInsurancePersonBoard.setsSupplement(sSupplement.toString());
					caseInsurancePersonBoard.setSwShortage(swShortage.toString());
					caseInsurancePersonBoard.setSwSupplement(swSupplement.toString());

					if (StringUtils.isNotBlank(caseInsurancePersonBoard.getfLackFunds())
							||StringUtils.isNotBlank(caseInsurancePersonBoard.getfSupplement())
							||StringUtils.isNotBlank(caseInsurancePersonBoard.getsShortage())
							||StringUtils.isNotBlank(caseInsurancePersonBoard.getsSupplement())
							||StringUtils.isNotBlank(caseInsurancePersonBoard.getSwShortage())
							||StringUtils.isNotBlank(caseInsurancePersonBoard.getSwSupplement())){
						caseInsurancePersonBoards.add(caseInsurancePersonBoard);
					}
				}
			}
		}
		caseInsuranceBoard.setYearString(DateUtils.getYear());
		if (DateUtils.getDay().equals("01")){
			caseInsuranceBoard.setMonthString(DateUtils.getMonth());
			caseInsuranceBoard.setDayString(DateUtils.getDay());
		}else {
			caseInsuranceBoard.setMonthString(DateUtils.getMonth());
			caseInsuranceBoard.setDayString(DateUtils.getLastDay(DateUtils.getNextDay(new Date())));
		}
		caseInsuranceBoard.setCaseInsurancePersonBoards(caseInsurancePersonBoards);

		return caseInsuranceBoard;
	}


	public List<CaseXbbInsurance> findTypeList(String showType){

		CaseInsuranceBoard caseInsuranceBoard = new CaseInsuranceBoard();
		List<CaseXbbInsurance> caseXbbInsurances =  caseXbbInsuranceDao.findCountList();
		List<CasePayment> casePayments = casePaymentDao.findList(new CasePayment());

		List<CaseXbbInsurance> returnCaseXbbInsurances = Lists.newArrayList();
		if(showType.equals("didCount")){// 完结事件
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				 if (caseXbbInsurance.getUndertakingStatus().equals("已完结")){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
          return  returnCaseXbbInsurances;
		}else if(showType.equals("otherCount")){//其他案件
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				if (caseXbbInsurance.getUndertakingStatus().equals("进行中")){
				}else if (caseXbbInsurance.getUndertakingStatus().equals("已完结")){
				}else {
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;

		}else if(showType.equals("advanceProjectsCount")){//预立案


			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances) {

				if (caseXbbInsurance.getUndertakingStatus().equals("进行中")) {

				} else if (caseXbbInsurance.getUndertakingStatus().equals("已完结")) {
				} else {
					if (caseXbbInsurance.getUndertakingStatus().equals("预立项")) {
						returnCaseXbbInsurances.add(caseXbbInsurance);
					}
				}
			}
			return  returnCaseXbbInsurances;

			}else if(showType.equals("todayCount")){//昨日新增

			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){


				if (caseXbbInsurance.getNonPresecutioDate()!=null &&
						DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"yyyy").equals(DateUtils.getYear())
						&&DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"MM").equals(DateUtils.getMonth())
						&&DateUtils.getIntDay(DateUtils.parseDate(caseXbbInsurance.getSigningDate()))==DateUtils.getIntDay()-1){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}

				if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()!=null &&
						DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"yyyy").equals(DateUtils.getYear())
						&&DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"MM").equals(DateUtils.getMonth())
						&&DateUtils.getIntDay(DateUtils.parseDate(caseXbbInsurance.getSigningDate()))==DateUtils.getIntDay()-1){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;


		}else if(showType.equals("nonComplaintCount")){//非诉阶段
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				if (caseXbbInsurance.getNonPresecutioDate()!=null
						&&caseXbbInsurance.getLawsuitStartDate()==null
						&&caseXbbInsurance.getExecutionStratDate()==null
						&&caseXbbInsurance.getzEndTime()==null
						&&caseXbbInsurance.getZwEndTime()==null
						){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;

		}else if(showType.equals("litigationsCount")){//诉讼阶段
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null
						&& caseXbbInsurance.getExecutionStratDate()==null){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}else if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null
						&& caseXbbInsurance.getExecutionStratDate()==null){
					    returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;
		}else if(showType.equals("implementsCount")){//执行阶段

			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){

				if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
						&& caseXbbInsurance.getOutSourcing().equals("自行执行")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null
						){

					returnCaseXbbInsurances.add(caseXbbInsurance);

				}else if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
						&& caseXbbInsurance.getOutSourcing().equals("委外执行")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null) {
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;


		}else if(showType.equals("selfLitigationsCount")){//自办诉讼阶段

			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null
						&& caseXbbInsurance.getExecutionStratDate()==null){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;
		}else if(showType.equals("selfImplementsCount")){//自办执行阶段

			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
						&& caseXbbInsurance.getOutSourcing().equals("自行执行")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null
						){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;

		}else if(showType.equals("outLitigationsCount")){//委外诉讼阶段
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				 if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null
						&& caseXbbInsurance.getExecutionStratDate()==null){
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;
		}else if(showType.equals("outImplementsCount")){//委外执行阶段
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){

				 if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
						&& caseXbbInsurance.getOutSourcing().equals("委外执行")
						&& caseXbbInsurance.getZwEndTime()==null
						&& caseXbbInsurance.getzEndTime()==null) {
					returnCaseXbbInsurances.add(caseXbbInsurance);
				}
			}
			return  returnCaseXbbInsurances;


		}else if(showType.equals("oneMounthImplementsRisk")){//非诉阶段不足一个月


        	List<XbbCommunicate> xbbCommunicates = xbbCommunicateDao.findList(new XbbCommunicate());

			List<CaseXbbInsurance> implementsLists = Lists.newArrayList();
			List<CaseXbbInsurance> litigationsLists = Lists.newArrayList();
			List<CaseXbbInsurance> nonComplaintLists = Lists.newArrayList();

			for(CaseXbbInsurance caseXbbInsurance:caseXbbInsurances){
				if (caseXbbInsurance.getCurrentStage().equals("执行")){
					implementsLists.add(caseXbbInsurance);
				}else if (caseXbbInsurance.getCurrentStage().equals("诉讼")){
					litigationsLists.add(caseXbbInsurance);
				}else if (caseXbbInsurance.getCurrentStage().equals("非诉催收")){
					nonComplaintLists.add(caseXbbInsurance);
				}
			}

			Integer implementsOneMonth =0;
			Integer implementsTowMonth =0;
			Integer implementsThreeMonth =0;


			Integer litigationsOneMonth =0;
			Integer litigationsTowMonth =0;
			Integer litigationsThreeMonth =0;

			Integer nonComplaintOneMonth =0;
			Integer nonComplaintTowMonth =0;
			Integer nonComplaintThreeMonth =0;


			for (XbbCommunicate xbbCommunicate : xbbCommunicates){
				for (CaseXbbInsurance  implementsTemp : implementsLists) {
					if (xbbCommunicate.getXbbInsuranceId().equals(implementsTemp.getId())){
						if (xbbCommunicate.getVistWay().equals("Z验执效")){
							String dateString = xbbCommunicate.getRemarks().split("#")[0];
							if (StringUtils.isNotBlank(dateString)){
								if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))>0&&
										DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<30.0){
									implementsOneMonth ++;
								}else if (30.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<60.0){
									implementsTowMonth++;
								}else if (60.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<90.0){
									implementsThreeMonth++;
								}
							}
						}
					}
				}

				for (CaseXbbInsurance  litigationsTemp : litigationsLists) {
					if (xbbCommunicate.getXbbInsuranceId().equals(litigationsTemp.getId())) {
						if (xbbCommunicate.getVistWay().equals("S验诉效")) {
							String dateString = xbbCommunicate.getRemarks().split("#")[0];
							if (StringUtils.isNotBlank(dateString)) {
								if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) > 0
										&& DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) < 30.0) {
									litigationsOneMonth++;
								} else if (30.0 <= DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) && DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) < 60.0) {
									litigationsTowMonth++;
								} else if (60.0 <= DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) && DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) < 90.0) {
									litigationsThreeMonth++;
								}
							}
						}
					}

				}

				for (CaseXbbInsurance  nonComplaintTemp : nonComplaintLists) {
					if (xbbCommunicate.getXbbInsuranceId().equals(nonComplaintTemp.getId())) {
						if (xbbCommunicate.getVistWay().equals("F验诉效")){
							String dateString = xbbCommunicate.getRemarks().split("#")[0];
							if (StringUtils.isNotBlank(dateString)){
								if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))>0 &&
										DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<30.0){
									nonComplaintOneMonth ++;
								}else if (30.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<60.0){
									nonComplaintTowMonth++;
								}else if (60.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<90.0){
									nonComplaintThreeMonth++;
								}
							}
						}
					}
				}

			}


		}else if(showType.equals("oneMounthNonComplaintRisk")){//诉讼阶段不足一个月

		}else if(showType.equals("oneMounthLitigationsRisk")){//执行阶段不足一个月

		}else if(showType.equals("towMounthImplementsRisk")){//非诉阶段不足二个月

		}else if(showType.equals("towMounthNonComplaintRisk")){//诉讼阶段不足二个月

		}else if(showType.equals("towMounthLitigationsRisk")){//执行阶段不足二个月

		}else if(showType.equals("threeMounthImplementsRisk")){//非诉阶段不足三个月

		}else if(showType.equals("threeMounthNonComplaintRisk")){//诉讼阶段不足二个月

		}else if(showType.equals("threeMounthLitigationsRisk")){//执行阶段不足三个月

		}





		List<CaseXbbInsurance> didCountList = Lists.newArrayList();

		List<CaseXbbInsurance> otherCountList = Lists.newArrayList();

		int advanceProjectsCount=0;
		int nonComplaintCount =0 ;// 非诉催收
		int litigationsCount  =0 ;//诉讼阶段
		int implementsCount  =0 ;//执行阶段

		int selfLitigationsCount=0;// 自办诉讼阶段
		int outLitigationsCount =0;// 委外诉讼阶段
		int selfImplementsCount =0;// 自办执行阶段
		int outImplementsCount  =0;// 委外执行阶段
		int todayCount=0; //今天新增
		float allMoneyCount = 0f;// 合同总金额
		float planMoneyCount= 0f;;// 计划收款金额
		float overMonthCount= 0f;;//超一个月合同订单金额
		float backRaio= 0f;;//回款率

		Set<String> nameKeys = new HashSet<String>();

		List<CaseInsurancePersonBoard> caseInsurancePersonBoards = Lists.newArrayList();

		for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
			if(StringUtils.isNotBlank(caseXbbInsurance.getUndertakingLawyer()) && !caseXbbInsurance.getUndertakingLawyer().equals("无") &&!caseXbbInsurance.getUndertakingLawyer().contains("暂无") && !caseXbbInsurance.getUndertakingLawyer().contains("暂未") && !caseXbbInsurance.getUndertakingLawyer().contains("分配") ){
				nameKeys.add(caseXbbInsurance.getUndertakingLawyer());
			}
			if (caseXbbInsurance.getUndertakingStatus().equals("进行中")){

			}else if (caseXbbInsurance.getUndertakingStatus().equals("已完结")){
				didCountList.add(caseXbbInsurance);
			}else {
				if (caseXbbInsurance.getUndertakingStatus().equals("预立项")){
					advanceProjectsCount++;
				}
				otherCountList.add(caseXbbInsurance);
			}

			if (caseXbbInsurance.getCreateTime() !=null){
				if (DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getCreateTime(),new Date())>30.0){
					overMonthCount ++;
				}
			}


			if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
					&& caseXbbInsurance.getOutSourcing().equals("自行执行")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					){
				selfImplementsCount++;
				implementsCount++;
			}else if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())
					&& caseXbbInsurance.getOutSourcing().equals("委外执行")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null) {
				outImplementsCount++;
				implementsCount++;
			}


			if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					&& caseXbbInsurance.getExecutionStratDate()==null){
				selfLitigationsCount++;
				litigationsCount++;
			}else if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")
					&& caseXbbInsurance.getZwEndTime()==null
					&& caseXbbInsurance.getzEndTime()==null
					&& caseXbbInsurance.getExecutionStratDate()==null){
				outLitigationsCount++;
				litigationsCount++;
			}



			if (caseXbbInsurance.getNonPresecutioDate()!=null
					&&caseXbbInsurance.getLawsuitStartDate()==null
					&&caseXbbInsurance.getExecutionStratDate()==null
					&&caseXbbInsurance.getzEndTime()==null
					&&caseXbbInsurance.getZwEndTime()==null
					){
				nonComplaintCount++;
			}

			if (StringUtils.isNotBlank(caseXbbInsurance.getContractAmount())){

				if (caseXbbInsurance.getUndertakingStatus().equals("进行中")||
						caseXbbInsurance.getUndertakingStatus().equals("预立项")||
						caseXbbInsurance.getUndertakingStatus().equals("已完结")){
					allMoneyCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}
			}

			if (StringUtils.isNotBlank(caseXbbInsurance.getContractAmount())){
				if (caseXbbInsurance.getNonPresecutioDate()!=null && DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getCreateTime(),new Date())>=30){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}else if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()!=null && DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())>=30){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}else if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()==null
						&&  caseXbbInsurance.getExecutionStratDate()!=null
						&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getExecutionStratDate(),new Date())>=30){
					overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
				}
			}

			if (caseXbbInsurance.getNonPresecutioDate()!=null &&
					DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"yyyy").equals(DateUtils.getYear())
					&&DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"MM").equals(DateUtils.getMonth())
					&&DateUtils.getIntDay(DateUtils.parseDate(caseXbbInsurance.getSigningDate()))==DateUtils.getIntDay()-1){
				todayCount++;
			}

			if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()!=null &&
					DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"yyyy").equals(DateUtils.getYear())
					&&DateUtils.formatDate(DateUtils.parseDate(caseXbbInsurance.getSigningDate()),"MM").equals(DateUtils.getMonth())
					&&DateUtils.getIntDay(DateUtils.parseDate(caseXbbInsurance.getSigningDate()))==DateUtils.getIntDay()-1){
				todayCount++;
			}

			if (caseXbbInsurance.getNonPresecutioDate()==null &&  caseXbbInsurance.getLawsuitStartDate()==null
					&&  caseXbbInsurance.getExecutionStratDate()!=null
					&&DateUtils.formatDate(caseXbbInsurance.getCreateDate(),"yyyy").equals(DateUtils.getYear())
					&&DateUtils.formatDate(caseXbbInsurance.getCreateDate(),"MM").equals(DateUtils.getMonth())
					&&DateUtils.getIntDay(caseXbbInsurance.getCreateDate())==DateUtils.getIntDay()-1){
				overMonthCount += Float.valueOf(caseXbbInsurance.getContractAmount());
			}
		}



		for (String nameKey:nameKeys){

			if(!nameKey.equals("胡鑫")){
				CaseInsurancePersonBoard caseInsurancePersonBoard = new CaseInsurancePersonBoard();
				caseInsurancePersonBoard.setName(nameKey);

				int nonComplaintCountPerson =0 ;// 非诉催收
				int nonOverOneComplatinCountPerson =0;//超过一个月
				int  nonOverTowComplatinCountPerson=0;//超过二个月
				int qqCount= 0; //缺
				int bCount= 0;  //补

				int litigationsCountPerson  =0 ;//诉讼阶段
				int selfLitigationsCountPerson =0;//自办总数
				int selfOverOneCount=0;
				int selfOverTowCount=0;
				int selflhwkCount =0;
				int selfykwpCount =0;
				int selfypwsCount =0;
				int selfpjsxCount=0;
				int selfqsCount=0;
				int selfpcCount=0;




				int outLitigationsCountPerson =0;//委外


				int outOverfiveCount =0;//委外超15天
				int outOverthreeCount=0;//委外超30天
				int outOverOneCount=0;// 超1未立
				int outOverTowCount=0;//超2未立
				int outlhwkCount=0;//立后无开
				int outykwpCount=0;//已开未判
				int outypwsCount=0;//已判未生
				int outpjsxCount=0;//判决生效
				int outqsCount=0;//缺
				int outpcCount=0;//补充




				int implementsCountPerson =0;//执行阶段
				int selfImplementsCountPerson =0;//自办
				int selfOverOneImplementsCountPerson =0;//超1未申立
				int selfOverTowImplementsCountPerson =0;//超2未申立
				int selfOverThreeImplementsCountPerson =0;//超3未终
				int selfOverSixImplementsCountPerson =0;//超6未终

				int outImplementsCountPerson =0;//委外
				int outFiveImplementsCountPerson =0;//委外15
				int outThreeImplementsCountPerson =0;//委外30
				int outOverOneImplementsCountPerson =0;//超1未申立
				int outOverTowImplementsCountPerson =0;//超2未申立
				int outOverThreeImplementsCountPerson =0;//超3未终
				int outOverSixImplementsCountPerson =0;//超6未终


				int todayFollowAllCaseCount =0;
				int todayFollowCaseCount=0;


				List<CaseXbbInsurance> nonComplaintPersons = Lists.newArrayList();// 个人非诉催收
				List<CaseXbbInsurance> litigationsPersons = Lists.newArrayList();//诉讼阶段
				List<CaseXbbInsurance> implementsPersons = Lists.newArrayList();//执行阶段

				List<XbbCommunicate> nonComplaintXbbCommunicates = Lists.newArrayList();
				List<CaseXbbInsurance> selfLitigationPersons = Lists.newArrayList();// 自行诉讼
				List<CaseXbbInsurance> outLitigationPersons = Lists.newArrayList();//委外诉讼

				List<XbbCommunicate> litigationsXbbCommunicates = Lists.newArrayList();
				List<XbbCommunicate> implementsXbbCommunicates = Lists.newArrayList();

				List<CaseXbbInsurance> selfImplementsPersons = Lists.newArrayList();
				List<CaseXbbInsurance> outImplementsPersons = Lists.newArrayList();


				for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
					if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行执行")
							&& caseXbbInsurance.getzEndTime() ==null && caseXbbInsurance.getZwEndTime() ==null && caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){
						selfImplementsCountPerson++;
						implementsCountPerson++;
						selfImplementsPersons.add(caseXbbInsurance);

					}else if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("委外执行")
							&& caseXbbInsurance.getzEndTime() ==null && caseXbbInsurance.getZwEndTime() ==null&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)) {
						outImplementsCountPerson++;
						implementsCountPerson++;
						outImplementsPersons.add(caseXbbInsurance);

					}



					if(StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing()) && caseXbbInsurance.getOutSourcing().equals("自行诉讼")&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){
						if (caseXbbInsurance.getzEndTime() == null && caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfLitigationsCountPerson++;
							litigationsCountPerson++;
							selfLitigationPersons.add(caseXbbInsurance);
						}
						if (caseXbbInsurance.getLawsuitStartDate()!=null
								&& (DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())>=30.0
								&&DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())<60.0 )
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfOverOneCount++;
						}
						if (caseXbbInsurance.getLawsuitStartDate()!=null
								&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getLawsuitStartDate(),new Date())>=60.0
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfOverTowCount++;
						}


						if (caseXbbInsurance.getsInstant()!=null
								&& caseXbbInsurance.getsTurnOnTime() ==null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selflhwkCount++;
						}


						if (caseXbbInsurance.getsTurnOnTime() !=null
								&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getsTurnOnTime(),new Date())>=0
								&& caseXbbInsurance.getsJudgment()==null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfykwpCount++;
						}

						if (caseXbbInsurance.getsJudgment()!=null && caseXbbInsurance.getsJudgmentTime()==null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfypwsCount++;
						}
						if (caseXbbInsurance.getsJudgmentTime()!=null
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfpjsxCount++;
						}


						if (StringUtils.isNotBlank(caseXbbInsurance.getSourceStauts())&&caseXbbInsurance.getSourceStauts().equals("0")
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null
								){
							selfqsCount++;
						}else if (StringUtils.isNotBlank(caseXbbInsurance.getSourceStauts())&&caseXbbInsurance.getSourceStauts().equals("1")
								&& caseXbbInsurance.getzEndTime() == null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							selfpcCount++;
						}

					}else if (StringUtils.isNotBlank(caseXbbInsurance.getOutSourcing())&& caseXbbInsurance.getOutSourcing().equals("委外诉讼")&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){

						if (caseXbbInsurance.getLawsuitStartDate()!=null
								&& caseXbbInsurance.getSwOpenTime()==null
								&& caseXbbInsurance.getzEndTime() ==null
								&& caseXbbInsurance.getZwEndTime() == null
								&& caseXbbInsurance.getExecutionStratDate() ==null){
							outLitigationPersons.add(caseXbbInsurance);
							outLitigationsCountPerson++;
							litigationsCountPerson++;
						}
					}

					if (
							caseXbbInsurance.getNonPresecutioDate()!=null
									&&caseXbbInsurance.getLawsuitStartDate()==null
									&&caseXbbInsurance.getExecutionStratDate()==null
									&&caseXbbInsurance.getzEndTime()==null
									&&caseXbbInsurance.getZwEndTime()==null
									&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)){
						nonComplaintCountPerson++;
						nonComplaintPersons.add(caseXbbInsurance);
					}


					if (caseXbbInsurance.getNonPresecutioDate()!=null
							&&caseXbbInsurance.getLawsuitStartDate()==null
							&&caseXbbInsurance.getExecutionStratDate()==null
							&&caseXbbInsurance.getzEndTime()==null
							&&caseXbbInsurance.getZwEndTime()==null
							&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)
							&& DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getNonPresecutioDate(),new Date())>=30
							&&DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getNonPresecutioDate(),new Date())<60
							){
						nonOverOneComplatinCountPerson++;
					}

					if (caseXbbInsurance.getNonPresecutioDate()!=null
							&&caseXbbInsurance.getLawsuitStartDate()==null
							&&caseXbbInsurance.getExecutionStratDate()==null
							&&caseXbbInsurance.getzEndTime()==null
							&&caseXbbInsurance.getZwEndTime()==null
							&& caseXbbInsurance.getUndertakingLawyer().equals(nameKey)
							&&DateUtils.getDistanceOfTwoDate(caseXbbInsurance.getNonPresecutioDate(),new Date())>=60
							){
						nonOverTowComplatinCountPerson++;
					}


				}

//				for (XbbCommunicate xbbCommunicate:xbbCommunicates){
//					if (xbbCommunicate.getVistPerson().equals(nameKey)){
//						if (DateUtils.formatDate(DateUtils.parseDate(xbbCommunicate.getCreateTimes()),"yyyy").equals(DateUtils.getYear())
//								&&DateUtils.formatDate(DateUtils.parseDate(xbbCommunicate.getCreateTimes()),"MM").equals(DateUtils.getMonth())
//								&&DateUtils.getIntDay(DateUtils.parseDate(xbbCommunicate.getCreateTimes()))==DateUtils.getIntDay()
//								){
//							litigationsXbbCommunicates.add(xbbCommunicate);
//							todayFollowAllCaseCount++;
//						}
//					}
//				}

				List<XbbCommunicate> caseXbbCommunicateTemps = Lists.newArrayList();
				for(XbbCommunicate xbbCommunicate : litigationsXbbCommunicates){
					if (caseXbbCommunicateTemps.size()==0){
						caseXbbCommunicateTemps.add(xbbCommunicate);
					}else {
						boolean addFlag= true;
						for (XbbCommunicate temp:caseXbbCommunicateTemps){
							if (temp.getXbbInsuranceId().equals(xbbCommunicate.getXbbInsuranceId())){
								addFlag = false;
							}
						}

						if (addFlag){
							caseXbbCommunicateTemps.add(xbbCommunicate);
						}
					}


				}


				for (CaseXbbInsurance temp: outLitigationPersons){
					if (temp.getSwTimeShift()!=null&&
							DateUtils.getDistanceOfTwoDate(temp.getSwTimeShift(),new Date())>=15
							&&DateUtils.getDistanceOfTwoDate(temp.getSwTimeShift(),new Date())<30
							&& temp.getSwEndTime() == null){
						outOverfiveCount++;

					}
					if (temp.getSwTimeShift()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getSwTimeShift(),new Date())>=30
							&& temp.getSwEndTime() == null){
						outOverthreeCount++;
					}
					if (temp.getSwEndTime()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getSwEndTime(),new Date())>=30
							&&DateUtils.getDistanceOfTwoDate(temp.getSwEndTime(),new Date())<60
							&& temp.getSwInstant() == null){
						outOverOneCount++;// 超1未立
					}
					if (temp.getSwEndTime()!=null
							&&DateUtils.getDistanceOfTwoDate(temp.getSwEndTime(),new Date())>=60
							&& temp.getSwInstant() == null){
						outOverTowCount++;//超2未立

					}
					if (StringUtils.isNotBlank(temp.getSourceStauts())&&temp.getSourceStauts().equals("0")){
						outqsCount++;//缺
					}else if (StringUtils.isNotBlank(temp.getSourceStauts())&&temp.getSourceStauts().equals("1")){
						outpcCount++;//补充
					}
					if (temp.getSwInstant() !=null && temp.getSwOpenTime() ==null){
						outlhwkCount++;
					}
					if (temp.getSwOpenTime() !=null
							&&DateUtils.getDistanceOfTwoDate(temp.getSwInstant(),new Date())>0
							&& temp.getSwJudgment()==null){
						outykwpCount++;
					}
					if (temp.getSwJudgment() !=null
							&& temp.getSwJudgmentTime()==null){
						outypwsCount++;
					}
					if (temp.getSwJudgmentTime()!=null){
						outpjsxCount++;
					}
				}


				if (selfImplementsPersons!=null && selfImplementsPersons.size()>0){
					for (CaseXbbInsurance temp:selfImplementsPersons){
						if (temp.getExecutionStratDate()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())>=30
								&&temp.getzShenShi() == null
								){
							selfOverOneImplementsCountPerson++;
						}
						if (temp.getExecutionStratDate()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getzShenShi(),new Date())>=90
								&&DateUtils.getDistanceOfTwoDate(temp.getzShenShi(),new Date())<180
								&&temp.getzEndTime()==null){
							selfOverThreeImplementsCountPerson++;
						}
						if (temp.getExecutionStratDate()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getzShenShi(),new Date())>=180
								&&temp.getzEndTime()==null){
							selfOverSixImplementsCountPerson++;
						}
					}
				}

				if (outImplementsPersons!=null && outImplementsPersons.size()>0){
					for (CaseXbbInsurance temp:outImplementsPersons){
						if (temp.getExecutionStratDate()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())>=15
								&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())<30
								&&temp.getZwShenShi()==null){
							outFiveImplementsCountPerson++;
						}

						if (temp.getExecutionStratDate()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getExecutionStratDate(),new Date())>=30
								&&temp.getZwShenShi()==null
								){
							outThreeImplementsCountPerson++;
						}


						if (temp.getExecutionStratDate()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getZwFinishTime(),new Date())>=30
								&&temp.getZwShenShi()==null
								){
							outOverOneImplementsCountPerson++;
						}

						if (temp.getZwShenShi()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getZwShenShi(),new Date())>=90
								&&DateUtils.getDistanceOfTwoDate(temp.getZwShenShi(),new Date())<180
								&&temp.getzEndTime()==null
								){
							outOverThreeImplementsCountPerson++;
						}

						if (temp.getZwShenShi()!=null
								&&DateUtils.getDistanceOfTwoDate(temp.getZwShenShi(),new Date())>=180
								&&temp.getzEndTime()==null
								){
							outOverSixImplementsCountPerson++;
						}

					}
				}


				for (CaseXbbInsurance caseXbbInsuranceTemp : nonComplaintPersons){

					if (StringUtils.isNotBlank(caseXbbInsuranceTemp.getSourceStauts())&&caseXbbInsuranceTemp.getSourceStauts().equals("0")){
						qqCount++;
					}else if (StringUtils.isNotBlank(caseXbbInsuranceTemp.getSourceStauts())&&caseXbbInsuranceTemp.getSourceStauts().equals("1")){
						bCount++;
					}

				}


				List<XbbCommunicate> nowNonComplaintXbbCommunicates = Lists.newArrayList();
				for (XbbCommunicate xbbCommunicate:nonComplaintXbbCommunicates){
					boolean addFlag = false;
					for (CaseXbbInsurance caseXbbInsuranceTemp : nonComplaintPersons){
						if (caseXbbInsuranceTemp.getId().equals(xbbCommunicate.getXbbInsuranceId())){
							addFlag = true;
						}
					}
					if (addFlag){
						nowNonComplaintXbbCommunicates.add(xbbCommunicate);
					}


				}

				List<XbbCommunicate> xbbCommunicatesY= Lists.newArrayList();
				List<XbbCommunicate> xbbCommunicatesE= Lists.newArrayList();
				List<XbbCommunicate> xbbCommunicatesS= Lists.newArrayList();
				List<XbbCommunicate> xbbCommunicatesX= Lists.newArrayList();
				List<XbbCommunicate> xbbCommunicatesW= Lists.newArrayList();
				List<XbbCommunicate> xbbCommunicatesQ= Lists.newArrayList();

				for (XbbCommunicate xbbCommunicate :nowNonComplaintXbbCommunicates){
					if (xbbCommunicate.getVistWay().equals("F意还")){
						if (xbbCommunicatesY.size()==0){
							xbbCommunicatesY.add(xbbCommunicate);
						}else {
							boolean addFlag = true;
							for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesY){

								if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
									addFlag = false;
									if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

									}else {
										xbbCommunicatesY.remove(xbbCommunicateTemp);
										xbbCommunicatesY.add(xbbCommunicate);
									}
								}
							}

							if (addFlag){
								xbbCommunicatesY.add(xbbCommunicate);
							}
						}

					}else if (xbbCommunicate.getVistWay().equals("F恶拖")){
						if (xbbCommunicatesE.size()==0){
							xbbCommunicatesE.add(xbbCommunicate);
						}else {
							boolean addFlag = true;
							for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesE){

								if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
									addFlag = false;
									if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

									}else {
										xbbCommunicatesE.remove(xbbCommunicateTemp);
										xbbCommunicatesE.add(xbbCommunicate);
									}
								}
							}
							if (addFlag){
								xbbCommunicatesE.add(xbbCommunicate);
							}
						}

					}else if (xbbCommunicate.getVistWay().equals("F失联")){

						if (xbbCommunicatesS.size()==0){
							xbbCommunicatesS.add(xbbCommunicate);
						}else {
							boolean addFlag = true;
							for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesS){

								if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
									addFlag = false;
									if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

									}else {
										xbbCommunicatesS.remove(xbbCommunicateTemp);
										xbbCommunicatesS.add(xbbCommunicate);
									}
								}
							}
							if (addFlag){
								xbbCommunicatesS.add(xbbCommunicate);
							}
						}

					}else if (xbbCommunicate.getVistWay().equals("F失修")){
						if (xbbCommunicatesX.size()==0){
							xbbCommunicatesX.add(xbbCommunicate);
						}else {
							boolean addFlag = true;
							for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesX){

								if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
									addFlag = false;
									if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

									}else {
										xbbCommunicatesX.remove(xbbCommunicateTemp);
										xbbCommunicatesX.add(xbbCommunicate);
									}
								}
							}
							if (addFlag){
								xbbCommunicatesX.add(xbbCommunicate);
							}
						}


					}else if (xbbCommunicate.getVistWay().equals("F无听")){
						if (xbbCommunicatesW.size()==0){
							xbbCommunicatesW.add(xbbCommunicate);
						}else {
							boolean addFlag = true;
							for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesW){

								if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
									addFlag = false;
									if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

									}else {
										xbbCommunicatesW.remove(xbbCommunicateTemp);
										xbbCommunicatesW.add(xbbCommunicate);
									}
								}
							}
							if (addFlag){
								xbbCommunicatesW.add(xbbCommunicate);
							}
						}

					}
					else if (xbbCommunicate.getVistWay().equals("F其它")){

						if (xbbCommunicatesQ.size()==0){
							xbbCommunicatesQ.add(xbbCommunicate);
						}else {
							boolean addFlag = true;
							for (XbbCommunicate xbbCommunicateTemp : xbbCommunicatesQ){

								if (xbbCommunicate.getVistWay().equals(xbbCommunicateTemp.getVistWay())&&xbbCommunicate.getXbbInsuranceId().equals(xbbCommunicateTemp.getXbbInsuranceId())){
									addFlag = false;
									if (DateUtils.getDistanceOfTwoDate(xbbCommunicate.getUpdateDate(),xbbCommunicateTemp.getCreateDate())>=0.0){

									}else {
										xbbCommunicatesQ.remove(xbbCommunicateTemp);
										xbbCommunicatesQ.add(xbbCommunicate);
									}
								}
							}
							if (addFlag){
								xbbCommunicatesQ.add(xbbCommunicate);
							}
						}
					}
				}

				//=============非诉阶段开始===============//
				caseInsurancePersonBoard.setNonComplaintCount(StringUtils.changeIntNuns(nonComplaintCountPerson));
				caseInsurancePersonBoard.setNonOverOneComplatinCount(StringUtils.changeIntNuns(nonOverOneComplatinCountPerson));
				caseInsurancePersonBoard.setNonOverTowComplatinCount(StringUtils.changeIntNuns(nonOverTowComplatinCountPerson));

				caseInsurancePersonBoard.setyCount(Integer.toString(xbbCommunicatesY.size()));
				caseInsurancePersonBoard.seteCount(Integer.toString(xbbCommunicatesE.size()));
				caseInsurancePersonBoard.setsCount(Integer.toString(xbbCommunicatesS.size()));
				caseInsurancePersonBoard.setxCount(Integer.toString(xbbCommunicatesX.size()));
				caseInsurancePersonBoard.setwCount(Integer.toString(xbbCommunicatesW.size()));
				caseInsurancePersonBoard.setqCount(Integer.toString(xbbCommunicatesQ.size()));
				caseInsurancePersonBoard.setbCount(Integer.toString(bCount));
				caseInsurancePersonBoard.setQqCount(Integer.toString(qqCount));
				//=============非诉阶段结束===============//



				//=============诉讼阶段开始================//
				//=====自行诉讼开始====//
				caseInsurancePersonBoard.setLitigationsCount(StringUtils.changeIntNuns(litigationsCountPerson));
				caseInsurancePersonBoard.setSelfLitigationsCount(StringUtils.changeIntNuns(selfLitigationsCountPerson));
				caseInsurancePersonBoard.setSelfOverOneCount(StringUtils.changeIntNuns(selfOverOneCount));
				caseInsurancePersonBoard.setSelfOverTowCount(StringUtils.changeIntNuns(selfOverTowCount));
				caseInsurancePersonBoard.setSelflhwkCount(StringUtils.changeIntNuns(selflhwkCount));
				caseInsurancePersonBoard.setSelfykwpCount(StringUtils.changeIntNuns(selfykwpCount));
				caseInsurancePersonBoard.setSelfypwsCount(StringUtils.changeIntNuns(selfypwsCount));
				caseInsurancePersonBoard.setSelfpjsxCount(StringUtils.changeIntNuns(selfpjsxCount));
				caseInsurancePersonBoard.setSelfqsCount(StringUtils.changeIntNuns(selfqsCount));
				caseInsurancePersonBoard.setSelfpcCount(StringUtils.changeIntNuns(selfpcCount));


				//=====自行诉讼结束====//

				//=====委外诉讼开始====//
				caseInsurancePersonBoard.setOutLitigationsCount(StringUtils.changeIntNuns(outLitigationsCountPerson));
				caseInsurancePersonBoard.setOutOverfiveCount(StringUtils.changeIntNuns(outOverfiveCount));
				caseInsurancePersonBoard.setOutOverthreeCount(StringUtils.changeIntNuns(outOverthreeCount));
				caseInsurancePersonBoard.setOutOverOneCount(StringUtils.changeIntNuns(outOverOneCount));
				caseInsurancePersonBoard.setOutOverTowCount(StringUtils.changeIntNuns(outOverTowCount));
				caseInsurancePersonBoard.setOutlhwkCount(StringUtils.changeIntNuns(outlhwkCount));
				caseInsurancePersonBoard.setOutykwpCount(StringUtils.changeIntNuns(outykwpCount));
				caseInsurancePersonBoard.setOutypwsCount(StringUtils.changeIntNuns(outypwsCount));
				caseInsurancePersonBoard.setOutpjsxCount(StringUtils.changeIntNuns(outpjsxCount));
				caseInsurancePersonBoard.setOutqsCount(StringUtils.changeIntNuns(outqsCount));
				caseInsurancePersonBoard.setOutpcCount(StringUtils.changeIntNuns(outpcCount));

				//=====委外诉讼结束====//
				//=============诉讼阶段结束================//

				//=============执行阶段开始================//
				//=====自行诉讼开始====//
				caseInsurancePersonBoard.setImplementsCount(StringUtils.changeIntNuns(implementsCountPerson));
				caseInsurancePersonBoard.setSelfImplementsCount(StringUtils.changeIntNuns(selfImplementsCountPerson));
				caseInsurancePersonBoard.setSelfOverOneImplementsCount(StringUtils.changeIntNuns(selfOverOneImplementsCountPerson));
				caseInsurancePersonBoard.setSelfOverTowImplementsCount(StringUtils.changeIntNuns(selfOverTowImplementsCountPerson));
				caseInsurancePersonBoard.setSelfOverThreeImplementsCount(StringUtils.changeIntNuns(selfOverThreeImplementsCountPerson));
				caseInsurancePersonBoard.setSelfOverSixImplementsCount(StringUtils.changeIntNuns(selfOverSixImplementsCountPerson));
				//=====自行诉讼结束====//

				//=====委外诉讼开始====//
				caseInsurancePersonBoard.setOutImplementsCount(StringUtils.changeIntNuns(outImplementsCountPerson));
				caseInsurancePersonBoard.setOutFiveImplementsCount(StringUtils.changeIntNuns(outFiveImplementsCountPerson));
				caseInsurancePersonBoard.setOutThreeImplementsCount(StringUtils.changeIntNuns(outThreeImplementsCountPerson));
				caseInsurancePersonBoard.setOutOverOneImplementsCount(StringUtils.changeIntNuns(outOverOneImplementsCountPerson));
				caseInsurancePersonBoard.setOutOverTowImplementsCount(StringUtils.changeIntNuns(outOverTowImplementsCountPerson));
				caseInsurancePersonBoard.setOutOverThreeImplementsCount(StringUtils.changeIntNuns(outOverThreeImplementsCountPerson));
				caseInsurancePersonBoard.setOutOverSixImplementsCount(StringUtils.changeIntNuns(outOverSixImplementsCountPerson));
				//=====委外诉讼结束====//
				//=============执行阶段结束================//


				caseInsurancePersonBoard.setTodayFollowAllCaseCount(StringUtils.changeIntNuns(todayFollowAllCaseCount));
				caseInsurancePersonBoard.setTodayFollowCaseCount(StringUtils.changeIntNuns(caseXbbCommunicateTemps.size()));
				caseInsurancePersonBoards.add(caseInsurancePersonBoard);
			}
		}

		List<CaseXbbInsurance> implementsLists = Lists.newArrayList();
		List<CaseXbbInsurance> litigationsLists = Lists.newArrayList();
		List<CaseXbbInsurance> nonComplaintLists = Lists.newArrayList();

		for(CaseXbbInsurance caseXbbInsurance:caseXbbInsurances){
			if (caseXbbInsurance.getCurrentStage().equals("执行")){
				implementsLists.add(caseXbbInsurance);
			}else if (caseXbbInsurance.getCurrentStage().equals("诉讼")){
				litigationsLists.add(caseXbbInsurance);
			}else if (caseXbbInsurance.getCurrentStage().equals("非诉催收")){
				nonComplaintLists.add(caseXbbInsurance);
			}
		}

		Integer implementsOneMonth =0;
		Integer implementsTowMonth =0;
		Integer implementsThreeMonth =0;


		Integer litigationsOneMonth =0;
		Integer litigationsTowMonth =0;
		Integer litigationsThreeMonth =0;

		Integer nonComplaintOneMonth =0;
		Integer nonComplaintTowMonth =0;
		Integer nonComplaintThreeMonth =0;


		List<XbbCommunicate>xbbCommunicates = Lists.newArrayList();

		for (XbbCommunicate xbbCommunicate : xbbCommunicates){
			for (CaseXbbInsurance  implementsTemp : implementsLists) {
				if (xbbCommunicate.getXbbInsuranceId().equals(implementsTemp.getId())){
					if (xbbCommunicate.getVistWay().equals("Z验执效")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						if (StringUtils.isNotBlank(dateString)){
							if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))>0&&
									DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<30.0){
								implementsOneMonth ++;
							}else if (30.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<60.0){
								implementsTowMonth++;
							}else if (60.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<90.0){
								implementsThreeMonth++;
							}
						}
					}
				}
			}

			for (CaseXbbInsurance  litigationsTemp : litigationsLists) {
				if (xbbCommunicate.getXbbInsuranceId().equals(litigationsTemp.getId())) {
					if (xbbCommunicate.getVistWay().equals("S验诉效")) {
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						if (StringUtils.isNotBlank(dateString)) {
							if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) > 0
									&& DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) < 30.0) {
								litigationsOneMonth++;
							} else if (30.0 <= DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) && DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) < 60.0) {
								litigationsTowMonth++;
							} else if (60.0 <= DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString) ) && DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) < 90.0) {
								litigationsThreeMonth++;
							}
						}
					}
				}

			}

			for (CaseXbbInsurance  nonComplaintTemp : nonComplaintLists) {
				if (xbbCommunicate.getXbbInsuranceId().equals(nonComplaintTemp.getId())) {
					if (xbbCommunicate.getVistWay().equals("F验诉效")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						if (StringUtils.isNotBlank(dateString)){
							if (DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))>0 &&
									DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<30.0){
								nonComplaintOneMonth ++;
							}else if (30.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<60.0){
								nonComplaintTowMonth++;
							}else if (60.0<=DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString)) &&DateUtils.getDistanceOfTwoDate(new Date(),DateUtils.parseDate(dateString))<90.0){
								nonComplaintThreeMonth++;
							}
						}
					}
				}
			}

		}


		CaseInsuranceRisk implementsRisk = new CaseInsuranceRisk();
		CaseInsuranceRisk litigationsRisk = new CaseInsuranceRisk();
		CaseInsuranceRisk nonComplaintRisk = new CaseInsuranceRisk();

		implementsRisk.setTypeName("执行阶段");
		implementsRisk.setOneMounthCount(Integer.toString(implementsOneMonth));
		implementsRisk.setTowMounthCount(Integer.toString(implementsTowMonth));
		implementsRisk.setThreeMounthCount(Integer.toString(implementsThreeMonth));


		nonComplaintRisk.setTypeName("非诉阶段");
		nonComplaintRisk.setOneMounthCount(Integer.toString(nonComplaintOneMonth));
		nonComplaintRisk.setTowMounthCount(Integer.toString(nonComplaintTowMonth));
		nonComplaintRisk.setThreeMounthCount(Integer.toString(nonComplaintThreeMonth));

		litigationsRisk.setTypeName("诉讼阶段");
		litigationsRisk.setOneMounthCount(Integer.toString(litigationsOneMonth));
		litigationsRisk.setTowMounthCount(Integer.toString(litigationsTowMonth));
		litigationsRisk.setThreeMounthCount(Integer.toString(litigationsThreeMonth));


		List<CaseInsuranceRisk> caseInsuranceRisks  = Lists.newArrayList();
		caseInsuranceRisks.add(nonComplaintRisk);
		caseInsuranceRisks.add(litigationsRisk);
		caseInsuranceRisks.add(implementsRisk);


		for (CasePayment casePayment : casePayments){
			if (StringUtils.isNotBlank(casePayment.getAmount())){
				planMoneyCount += Float.valueOf(casePayment.getAmount());
			}
		}

		caseInsuranceBoard.setCaseInsuranceRisks(caseInsuranceRisks);
		//caseInsuranceBoard.setDoingCount(StringUtils.changeIntNuns(doingCount));
		//caseInsuranceBoard.setDidCount(StringUtils.changeIntNuns(didCount));
		//caseInsuranceBoard.setOtherCount(StringUtils.changeIntNuns(otherCount));

		caseInsuranceBoard.setAdvanceProjectsCount(StringUtils.changeIntNuns(advanceProjectsCount));
		caseInsuranceBoard.setNonComplaintCount(StringUtils.changeIntNuns(nonComplaintCount));
		caseInsuranceBoard.setLitigationsCount(StringUtils.changeIntNuns(litigationsCount));
		caseInsuranceBoard.setImplementsCount(StringUtils.changeIntNuns(implementsCount));

		caseInsuranceBoard.setAllMoneyCount(StringUtils.changeNuns(allMoneyCount));
		caseInsuranceBoard.setPlanMoneyCount(StringUtils.changeNuns(planMoneyCount));
		caseInsuranceBoard.setOverMonthCount(StringUtils.changeNuns(overMonthCount));
		caseInsuranceBoard.setBackRaio(Float.toString(planMoneyCount/overMonthCount*100));

		caseInsuranceBoard.setSelfImplementsCount(StringUtils.changeIntNuns(selfImplementsCount));
		caseInsuranceBoard.setOutImplementsCount(StringUtils.changeIntNuns(outImplementsCount));
		caseInsuranceBoard.setSelfLitigationsCount(StringUtils.changeIntNuns(selfLitigationsCount));
		caseInsuranceBoard.setOutLitigationsCount(StringUtils.changeIntNuns(outLitigationsCount));

		caseInsuranceBoard.setDateString(DateUtils.getDate());
		caseInsuranceBoard.setCaseInsurancePersonBoards(caseInsurancePersonBoards);
		caseInsuranceBoard.setTodayCount(StringUtils.changeIntNuns(todayCount));

    	return null;

	}
}