/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 案件管理Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwCase extends DataEntity<ZqwCase> {
	
	private static final long serialVersionUID = 1L;
	private String customId;		// custom_id
	private String customShortName;		// 客户简称
	private String nameOfInfringer;		// 侵权人姓名
	private String idCard;		// 身份证号码
	private String contactsPhone;		// contacts_phone


	private String householdRegister;		// 户籍地
	private String amountCreditor;		// 债权金额
	private String undertaker;		// 承办人员
	private String isOut;		// 是否委外
	private String causeOfAction;		// 案由
	private String limitationOfAction;		// 诉讼时效
	private String jurisdictionalCourt;		// 管辖法院
	private String insurancePlace;		// 保险事故发生地
	private String insured;		// 被保险人
	private String insuredPhone; //被保险人电话
	private String driver;		// 驾驶人
	private String driverPhone;      //驾驶人电话
	private Date claimDate;		// 理赔日期
	private Date holdDate;		// hold_date
	private String caseList;		// 案件材料领取时间及清单
	private String responsibility;		// 责任承担
	private String otherPenson;		// 其他赔偿主体姓名、身份证号码、联系电话
	private String compulsoryInsurance;		// 交强险
	private String commercialInsurace;		// 商业险
	private String reasonsForArrears;		// 欠款原因
	private String currentStage;		// 目前阶段
	private Date hzLayerTime;		// 确定合作律师时间
	private Date wlLayerTime;		// 委托查找合作律师时间
	private String caseNum;		// 案号
	private String layerName;		// 法官名称
	private String layerTel;		// 法官电话
	private String chargeRatio;		// 收费比例
	private Date closeCaseDate;		// 结案日期
	private Date filingDate;		// 归档日期
	private String filingNum;		// 归档号
	private String recordNum;		// 总案卷号
	private String noticeOfAction;		// 诉讼通告


	//非PO字段
	private String insuranceSubrogation;//保险代位求偿
	private String civilIndictment;//民事起诉状
	private String grantAuthorization;//授权
	private String catalogueEvidence;//证据目录

	private String insuranceRecourse;// 保险追偿



	public ZqwCase() {
		super();
	}

	public ZqwCase(String id){
		super(id);
	}


	public String getInsuredPhone() {
		return insuredPhone;
	}

	public void setInsuredPhone(String insuredPhone) {
		this.insuredPhone = insuredPhone;
	}

	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	@Length(min=0, max=64, message="custom_id长度必须介于 0 和 64 之间")
	public String getCustomId() {
		return customId;
	}

	public void setCustomId(String customId) {
		this.customId = customId;
	}
	
	@Length(min=0, max=1024, message="客户简称长度必须介于 0 和 1024 之间")
	public String getCustomShortName() {
		return customShortName;
	}

	public void setCustomShortName(String customShortName) {
		this.customShortName = customShortName;
	}
	
	@Length(min=0, max=255, message="侵权人姓名长度必须介于 0 和 255 之间")
	public String getNameOfInfringer() {
		return nameOfInfringer;
	}

	public void setNameOfInfringer(String nameOfInfringer) {
		this.nameOfInfringer = nameOfInfringer;
	}
	
	@Length(min=0, max=255, message="身份证号码长度必须介于 0 和 255 之间")
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	@Length(min=0, max=255, message="contacts_phone长度必须介于 0 和 255 之间")
	public String getContactsPhone() {
		return contactsPhone;
	}

	public void setContactsPhone(String contactsPhone) {
		this.contactsPhone = contactsPhone;
	}
	
	@Length(min=0, max=1024, message="户籍地长度必须介于 0 和 1024 之间")
	public String getHouseholdRegister() {
		return householdRegister;
	}

	public void setHouseholdRegister(String householdRegister) {
		this.householdRegister = householdRegister;
	}
	
	@Length(min=0, max=255, message="债权金额长度必须介于 0 和 255 之间")
	public String getAmountCreditor() {
		return amountCreditor;
	}

	public void setAmountCreditor(String amountCreditor) {
		this.amountCreditor = amountCreditor;
	}
	
	@Length(min=0, max=64, message="承办人员长度必须介于 0 和 64 之间")
	public String getUndertaker() {
		return undertaker;
	}

	public void setUndertaker(String undertaker) {
		this.undertaker = undertaker;
	}
	
	@Length(min=0, max=255, message="是否委外长度必须介于 0 和 255 之间")
	public String getIsOut() {
		return isOut;
	}

	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}
	
	@Length(min=0, max=255, message="案由长度必须介于 0 和 255 之间")
	public String getCauseOfAction() {
		return causeOfAction;
	}

	public void setCauseOfAction(String causeOfAction) {
		this.causeOfAction = causeOfAction;
	}
	
	@Length(min=0, max=255, message="诉讼时效长度必须介于 0 和 255 之间")
	public String getLimitationOfAction() {
		return limitationOfAction;
	}

	public void setLimitationOfAction(String limitationOfAction) {
		this.limitationOfAction = limitationOfAction;
	}
	
	@Length(min=0, max=1024, message="管辖法院长度必须介于 0 和 1024 之间")
	public String getJurisdictionalCourt() {
		return jurisdictionalCourt;
	}

	public void setJurisdictionalCourt(String jurisdictionalCourt) {
		this.jurisdictionalCourt = jurisdictionalCourt;
	}
	
	@Length(min=0, max=1024, message="保险事故发生地长度必须介于 0 和 1024 之间")
	public String getInsurancePlace() {
		return insurancePlace;
	}

	public void setInsurancePlace(String insurancePlace) {
		this.insurancePlace = insurancePlace;
	}
	
	@Length(min=0, max=255, message="被保险人长度必须介于 0 和 255 之间")
	public String getInsured() {
		return insured;
	}

	public void setInsured(String insured) {
		this.insured = insured;
	}
	
	@Length(min=0, max=255, message="驾驶人长度必须介于 0 和 255 之间")
	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(Date claimDate) {
		this.claimDate = claimDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getHoldDate() {
		return holdDate;
	}

	public void setHoldDate(Date holdDate) {
		this.holdDate = holdDate;
	}
	
	@Length(min=0, max=255, message="案件材料领取时间及清单长度必须介于 0 和 255 之间")
	public String getCaseList() {
		return caseList;
	}

	public void setCaseList(String caseList) {
		this.caseList = caseList;
	}
	
	@Length(min=0, max=255, message="责任承担长度必须介于 0 和 255 之间")
	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}
	
	public String getOtherPenson() {
		return otherPenson;
	}

	public void setOtherPenson(String otherPenson) {
		this.otherPenson = otherPenson;
	}
	
	@Length(min=0, max=1024, message="交强险长度必须介于 0 和 1024 之间")
	public String getCompulsoryInsurance() {
		return compulsoryInsurance;
	}

	public void setCompulsoryInsurance(String compulsoryInsurance) {
		this.compulsoryInsurance = compulsoryInsurance;
	}
	
	@Length(min=0, max=1024, message="商业险长度必须介于 0 和 1024 之间")
	public String getCommercialInsurace() {
		return commercialInsurace;
	}

	public void setCommercialInsurace(String commercialInsurace) {
		this.commercialInsurace = commercialInsurace;
	}
	
	@Length(min=0, max=255, message="欠款原因长度必须介于 0 和 255 之间")
	public String getReasonsForArrears() {
		return reasonsForArrears;
	}

	public void setReasonsForArrears(String reasonsForArrears) {
		this.reasonsForArrears = reasonsForArrears;
	}
	
	@Length(min=0, max=255, message="目前阶段长度必须介于 0 和 255 之间")
	public String getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(String currentStage) {
		this.currentStage = currentStage;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getHzLayerTime() {
		return hzLayerTime;
	}

	public void setHzLayerTime(Date hzLayerTime) {
		this.hzLayerTime = hzLayerTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getWlLayerTime() {
		return wlLayerTime;
	}

	public void setWlLayerTime(Date wlLayerTime) {
		this.wlLayerTime = wlLayerTime;
	}
	
	@Length(min=0, max=255, message="案号长度必须介于 0 和 255 之间")
	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	@Length(min=0, max=1024, message="法官名称长度必须介于 0 和 1024 之间")
	public String getLayerName() {
		return layerName;
	}

	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}
	
	@Length(min=0, max=255, message="法官电话长度必须介于 0 和 255 之间")
	public String getLayerTel() {
		return layerTel;
	}

	public void setLayerTel(String layerTel) {
		this.layerTel = layerTel;
	}
	
	@Length(min=0, max=1, message="收费比例长度必须介于 0 和 1 之间")
	public String getChargeRatio() {
		return chargeRatio;
	}

	public void setChargeRatio(String chargeRatio) {
		this.chargeRatio = chargeRatio;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCloseCaseDate() {
		return closeCaseDate;
	}

	public void setCloseCaseDate(Date closeCaseDate) {
		this.closeCaseDate = closeCaseDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFilingDate() {
		return filingDate;
	}

	public void setFilingDate(Date filingDate) {
		this.filingDate = filingDate;
	}
	
	@Length(min=0, max=255, message="归档号长度必须介于 0 和 255 之间")
	public String getFilingNum() {
		return filingNum;
	}

	public void setFilingNum(String filingNum) {
		this.filingNum = filingNum;
	}
	
	@Length(min=0, max=255, message="总案卷号长度必须介于 0 和 255 之间")
	public String getRecordNum() {
		return recordNum;
	}

	public void setRecordNum(String recordNum) {
		this.recordNum = recordNum;
	}
	
	public String getNoticeOfAction() {
		return noticeOfAction;
	}

	public void setNoticeOfAction(String noticeOfAction) {
		this.noticeOfAction = noticeOfAction;
	}

	public String getInsuranceSubrogation() {
		return insuranceSubrogation;
	}

	public void setInsuranceSubrogation(String insuranceSubrogation) {
		this.insuranceSubrogation = insuranceSubrogation;
	}

	public String getInsuranceRecourse() {
		return insuranceRecourse;
	}

	public void setInsuranceRecourse(String insuranceRecourse) {
		this.insuranceRecourse = insuranceRecourse;
	}

	public String getCivilIndictment() {
		return civilIndictment;
	}

	public void setCivilIndictment(String civilIndictment) {
		this.civilIndictment = civilIndictment;
	}

	public String getGrantAuthorization() {
		return grantAuthorization;
	}

	public void setGrantAuthorization(String grantAuthorization) {
		this.grantAuthorization = grantAuthorization;
	}

	public String getCatalogueEvidence() {
		return catalogueEvidence;
	}

	public void setCatalogueEvidence(String catalogueEvidence) {
		this.catalogueEvidence = catalogueEvidence;
	}
}