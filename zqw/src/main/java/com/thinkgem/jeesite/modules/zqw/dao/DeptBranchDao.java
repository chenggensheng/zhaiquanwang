/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.dao;
import java.util.List;
import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqw.entity.DeptBranch;

/**
 * 曝光台DAO接口
 * @author coder_cheng@126.com
 * @version 2018-10-22
 */
@MyBatisDao
public interface DeptBranchDao extends CrudDao<DeptBranch> {
    List<DeptBranch> findListLimit();
}