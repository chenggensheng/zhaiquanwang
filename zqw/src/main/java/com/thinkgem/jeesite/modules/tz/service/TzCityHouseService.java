/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.tz.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.tz.entity.TzCityHouse;
import com.thinkgem.jeesite.modules.tz.dao.TzCityHouseDao;

/**
 * 城市管理Service
 * @author coder_cheng@126.com
 * @version 2019-06-11
 */
@Service
@Transactional(readOnly = true)
public class TzCityHouseService extends CrudService<TzCityHouseDao, TzCityHouse> {

	public TzCityHouse get(String id) {
		return super.get(id);
	}
	
	public List<TzCityHouse> findList(TzCityHouse tzCityHouse) {
		return super.findList(tzCityHouse);
	}
	
	public Page<TzCityHouse> findPage(Page<TzCityHouse> page, TzCityHouse tzCityHouse) {
		return super.findPage(page, tzCityHouse);
	}
	
	@Transactional(readOnly = false)
	public void save(TzCityHouse tzCityHouse) {
		super.save(tzCityHouse);
	}
	
	@Transactional(readOnly = false)
	public void delete(TzCityHouse tzCityHouse) {
		super.delete(tzCityHouse);
	}
	
}