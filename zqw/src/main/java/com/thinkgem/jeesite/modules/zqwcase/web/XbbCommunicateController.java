/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.XbbCommunicate;
import com.thinkgem.jeesite.modules.zqwcase.service.XbbCommunicateService;

/**
 * 肖邦邦跟进记录管理Controller
 * @author coder_cheng@126.com
 * @version 2019-09-01
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/xbbCommunicate")
public class XbbCommunicateController extends BaseController {

	@Autowired
	private XbbCommunicateService xbbCommunicateService;
	
	@ModelAttribute
	public XbbCommunicate get(@RequestParam(required=false) String id) {
		XbbCommunicate entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xbbCommunicateService.get(id);
		}
		if (entity == null){
			entity = new XbbCommunicate();
		}
		return entity;
	}
	
	@RequiresPermissions("zqwcase:xbbCommunicate:view")
	@RequestMapping(value = {"list", ""})
	public String list(XbbCommunicate xbbCommunicate, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<XbbCommunicate> page = xbbCommunicateService.findPage(new Page<XbbCommunicate>(request, response), xbbCommunicate); 
		model.addAttribute("page", page);
		return "modules/zqwcase/xbbCommunicateList";
	}

	@RequiresPermissions("zqwcase:xbbCommunicate:view")
	@RequestMapping(value = "form")
	public String form(XbbCommunicate xbbCommunicate, Model model) {
		model.addAttribute("xbbCommunicate", xbbCommunicate);
		return "modules/zqwcase/xbbCommunicateForm";
	}

	@RequiresPermissions("zqwcase:xbbCommunicate:edit")
	@RequestMapping(value = "save")
	public String save(XbbCommunicate xbbCommunicate, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, xbbCommunicate)){
			return form(xbbCommunicate, model);
		}
		xbbCommunicateService.save(xbbCommunicate);
		addMessage(redirectAttributes, "保存肖邦邦跟进记录管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/xbbCommunicate/?repage";
	}
	
	@RequiresPermissions("zqwcase:xbbCommunicate:edit")
	@RequestMapping(value = "delete")
	public String delete(XbbCommunicate xbbCommunicate, RedirectAttributes redirectAttributes) {
		xbbCommunicateService.delete(xbbCommunicate);
		addMessage(redirectAttributes, "删除肖邦邦跟进记录管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/xbbCommunicate/?repage";
	}

}