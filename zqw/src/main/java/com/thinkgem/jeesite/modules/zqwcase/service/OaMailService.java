/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.Collections3;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.dao.OaMailDao;
import com.thinkgem.jeesite.modules.zqwcase.dao.OaMailPersonDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.OaMail;
import com.thinkgem.jeesite.modules.zqwcase.entity.OaMailPerson;
import com.thinkgem.jeesite.modules.sys.entity.User;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 内部邮件Service
 * @author Huang
 * @version 2018-04-27
 */
@Service
@Transactional(readOnly = true)
public class OaMailService extends CrudService<OaMailDao, OaMail> {

	@Autowired
	protected OaMailPersonDao oaMailPersonDao;

	public OaMail get(String id) {
		return super.get(id);
	}


	public List<OaMail> findReceiveList(OaMail oaMail) {
		return dao.findReceiveList(oaMail);
	}

	public List<OaMail> findLimitFive(OaMail oaMail){
		return dao.findLimitFive(oaMail);
	}



	public int findUnReadCount(OaMail oaMail) {
		return oaMailPersonDao.findUnReadCount(oaMail.getMailPerson());
	}

	public List<OaMail> findSendList(OaMail oaMail) {
		return dao.findSendList(oaMail);
	}	


	public Page<OaMail> findReceivePage(Page<OaMail> page, OaMail oaMail) {
		oaMail.setPage(page);
		page.setList(dao.findReceiveList(oaMail));
		return page;
	}


	public Page<OaMail> findSendPage(Page<OaMail> page, OaMail oaMail) {
		oaMail.setPage(page);
		page.setList(dao.findSendList(oaMail));
		return page;
	}


	public Page<OaMail> findPage(Page<OaMail> page, OaMail oaMail) {
		return super.findPage(page, oaMail);
	}


	/**
	 * 保存发送
	 */
	@Transactional(readOnly = false)
	public void save(OaMail oaMail) {
		if (StringUtils.isNotBlank(oaMail.getContent())){
			oaMail.setContent(StringEscapeUtils.unescapeHtml4(oaMail.getContent()));
		}
		if (StringUtils.isBlank(oaMail.getId())){
			oaMail.preInsert();
			if (StringUtils.isBlank(oaMail.getSourceMail())){
				oaMail.setSourceMail(oaMail.getId());
			}
			dao.insert(oaMail);
		}else{
			oaMail.preUpdate();
			dao.update(oaMail);
		}
		if(oaMail.getStatus().equals("1")){
			if (null != oaMail.getToUser() && StringUtils.isNotBlank(oaMail.getToUser().getId())){
				//如果是发送，则创建发送记录表
				for (String s : StringUtils.split(oaMail.getToUser().getId(), ",")) {
					OaMailPerson oaMailPerson = new OaMailPerson();
					oaMailPerson.setStatus("0");
					oaMailPerson.setMailId(oaMail.getId());
					oaMailPerson.setReadFlag("0");
					oaMailPerson.setImportantFlag("0");
					oaMailPerson.setDustbinFlag("0");
					oaMailPerson.setType("to");
					oaMailPerson.setUser(new User(s));
					oaMailPerson.preInsert();
					oaMailPersonDao.insert(oaMailPerson);
				}
			}
			if (null != oaMail.getCcUser() && StringUtils.isNotBlank(oaMail.getCcUser().getId())){ 
				for (String s : StringUtils.split(oaMail.getCcUser().getId(), ",")) {
					OaMailPerson oaMailPerson = new OaMailPerson();
					oaMailPerson.setStatus("0");
					oaMailPerson.setType("cc");
					oaMailPerson.setMailId(oaMail.getId());
					oaMailPerson.setReadFlag("0");
					oaMailPerson.setImportantFlag("0");
					oaMailPerson.setDustbinFlag("0");
					oaMailPerson.setUser(new User(s));
					oaMailPerson.preInsert();
					oaMailPersonDao.insert(oaMailPerson);
				}	
			}
			if (null != oaMail.getBccUser() && StringUtils.isNotBlank(oaMail.getBccUser().getId())){ 
				for (String s : StringUtils.split(oaMail.getBccUser().getId(), ",")) {
					OaMailPerson oaMailPerson = new OaMailPerson();
					oaMailPerson.setStatus("0");
					oaMailPerson.setType("bcc");
					oaMailPerson.setMailId(oaMail.getId());
					oaMailPerson.setReadFlag("0");
					oaMailPerson.setImportantFlag("0");
					oaMailPerson.setDustbinFlag("0");
					oaMailPerson.setUser(new User(s));
					oaMailPerson.preInsert();
					oaMailPersonDao.insert(oaMailPerson);
				}	
			}
		}
	}

	
	/**
	 * 批量标记为重要邮件
	 * @param user
	 * @param idList
	 */
	@Transactional(readOnly = false)
	public void updateImportantFlag(User user,List<String> idList,String importantFlag) {
		if (null == user || StringUtils.isBlank(user.getId())){
			return ;
		}
		if (Collections3.isEmpty(idList)){
			return ;
		}
		if (StringUtils.isBlank(importantFlag)){
			return ;
		}
		for (String mailId : idList) {
			OaMailPerson oaMailPerson = new OaMailPerson();
			oaMailPerson.setMailId(mailId);
			oaMailPerson.setImportantFlag(importantFlag);
			oaMailPerson.setUser(user);
			// 设置邮件不是垃圾邮件
			oaMailPerson.setDustbinFlag("0");
			oaMailPersonDao.updateImportantFlag(oaMailPerson);
		}
	}

	
	/**
	 * 标记为垃圾邮件
	 * @param user
	 * @param idList
	 */
	@Transactional(readOnly = false)
	public void updateDustbinFlag(User user,List<String> idList,String dustbinFlag) {
		if (null == user || StringUtils.isBlank(user.getId())){
			return ;
		}
		if (Collections3.isEmpty(idList)){
			return ;
		}
		if (StringUtils.isBlank(dustbinFlag)){
			return ;
		}
		for (String mailId : idList) {
			OaMailPerson oaMailPerson = new OaMailPerson();
			oaMailPerson.setMailId(mailId);
			// 垃圾邮件不是重要邮件
			oaMailPerson.setImportantFlag("0");
			oaMailPerson.setUser(user);
			oaMailPerson.setDustbinFlag(dustbinFlag);
			oaMailPersonDao.updateDustbinFlag(oaMailPerson);
		}
	}


	/**
	 * 批量标记为已读
	 * @param user
	 * @param idList
	 */
	@Transactional(readOnly = false)
	public void updateReadFlag(User user,List<String> idList) {
		if (null == user || StringUtils.isBlank(user.getId())){
			return ;
		}
		if (Collections3.isEmpty(idList)){
			return ;
		}
		Date readDate = new Date();
		for (String mailId : idList) {
			OaMailPerson mailPerson = new OaMailPerson();
			mailPerson.setMailId(mailId);
			mailPerson.setUser(user);
			mailPerson.setReadDate(readDate);
			mailPerson.setReadFlag("1");
			oaMailPersonDao.updateReadFlag(mailPerson);
		}
	}
	
	
	/**
	 * 批量标记为已读
	 * @param user
	 * @param idList
	 */
	@Transactional(readOnly = false)
	public void updateAllReadFlag(User user) {
		if (null == user || StringUtils.isBlank(user.getId())){
			return ;
		}
		Date readDate = new Date();
		OaMailPerson mailPerson = new OaMailPerson();
		mailPerson.setUser(user);
		mailPerson.setReadDate(readDate);
		mailPerson.setReadFlag("1");
		oaMailPersonDao.updateReadFlag(mailPerson);
	}


	/**
	 * 批量删除
	 * @param idList
	 */
	@Transactional(readOnly = false)
	public void delete(User user,List<String> idList) {
		if (null == user || StringUtils.isBlank(user.getId())){
			return ;
		}
		if (Collections3.isEmpty(idList)){
			return ;
		}
		Date updateDate = new Date();
		for (String id : idList) {
			OaMail mail = new OaMail();
			mail.setId(id);
			mail.setCreateBy(user);
			mail.setUpdateDate(updateDate);
			dao.delete(mail);
		}
	}


	
	public OaMailPerson getOaMailPerson(String mailId, User user) {
		if (StringUtils.isBlank(mailId) || null == user || StringUtils.isBlank(user.getId())){
			return null;
		}
		OaMailPerson oaMailPerson = new OaMailPerson();
		oaMailPerson.setMailId(mailId);
		oaMailPerson.setUser(user);
		List<OaMailPerson> list = oaMailPersonDao.findList(oaMailPerson);
		if (Collections3.isEmpty(list)){
			return null;
		}
		return list.get(0);
	}

	
}