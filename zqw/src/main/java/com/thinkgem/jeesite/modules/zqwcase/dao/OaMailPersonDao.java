/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.OaMailPerson;

/**
 * 邮件接收人DAO接口
 * @author Huang
 * @version 2018-04-27
 */
@MyBatisDao
public interface OaMailPersonDao extends CrudDao<OaMailPerson> {
	
	
	/**
	 * 更新数据
	 * @param entity
	 * @return
	 */
	public int updateReadFlag(OaMailPerson entity);
	public int findUnReadCount(OaMailPerson entity);
	
	public int updateImportantFlag(OaMailPerson entity);
	
	public int updateDustbinFlag(OaMailPerson entity);
}