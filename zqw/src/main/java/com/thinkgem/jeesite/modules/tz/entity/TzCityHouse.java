/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.tz.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 城市管理Entity
 * @author coder_cheng@126.com
 * @version 2019-06-11
 */
public class TzCityHouse extends DataEntity<TzCityHouse> {
	
	private static final long serialVersionUID = 1L;
	private String cityName;		// city_name
	private String areaName;		// area_name
	private String numberOfBuilding;
	private String otherNameOne;		// other_name_one
	private String otherNameTow;		// other_name_tow
	private String flowNum;		// flow_num


	private String searchValue;

	public TzCityHouse() {
		super();
	}

	public TzCityHouse(String id){
		super(id);
	}

	@Length(min=0, max=1024, message="city_name长度必须介于 0 和 1024 之间")
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	@Length(min=0, max=1024, message="area_name长度必须介于 0 和 1024 之间")
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	@Length(min=0, max=1024, message="other_name_one长度必须介于 0 和 1024 之间")
	public String getOtherNameOne() {
		return otherNameOne;
	}

	public void setOtherNameOne(String otherNameOne) {
		this.otherNameOne = otherNameOne;
	}
	
	@Length(min=0, max=1024, message="other_name_tow长度必须介于 0 和 1024 之间")
	public String getOtherNameTow() {
		return otherNameTow;
	}

	public void setOtherNameTow(String otherNameTow) {
		this.otherNameTow = otherNameTow;
	}
	
	@Length(min=0, max=102, message="flow_num长度必须介于 0 和 102 之间")
	public String getFlowNum() {
		return flowNum;
	}

	public void setFlowNum(String flowNum) {
		this.flowNum = flowNum;
	}

	public String getNumberOfBuilding() {
		return numberOfBuilding;
	}

	public void setNumberOfBuilding(String numberOfBuilding) {
		this.numberOfBuilding = numberOfBuilding;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
}