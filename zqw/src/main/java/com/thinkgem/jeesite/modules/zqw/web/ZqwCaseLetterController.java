/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseLetter;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCaseLetterService;

/**
 * 律师函发送及反馈Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCaseLetter")
public class ZqwCaseLetterController extends BaseController {

	@Autowired
	private ZqwCaseLetterService zqwCaseLetterService;
	
	@ModelAttribute
	public ZqwCaseLetter get(@RequestParam(required=false) String id) {
		ZqwCaseLetter entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCaseLetterService.get(id);
		}
		if (entity == null){
			entity = new ZqwCaseLetter();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCaseLetter:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCaseLetter zqwCaseLetter, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCaseLetter> page = zqwCaseLetterService.findPage(new Page<ZqwCaseLetter>(request, response), zqwCaseLetter); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCaseLetterList";
	}

	@RequiresPermissions("zqw:zqwCaseLetter:view")
	@RequestMapping(value = "form")
	public String form(ZqwCaseLetter zqwCaseLetter, Model model) {
		model.addAttribute("zqwCaseLetter", zqwCaseLetter);
		return "modules/zqw/zqwCaseLetterForm";
	}

	@RequiresPermissions("zqw:zqwCaseLetter:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCaseLetter zqwCaseLetter, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwCaseLetter)){
			return form(zqwCaseLetter, model);
		}
		zqwCaseLetterService.save(zqwCaseLetter);
		addMessage(redirectAttributes, "保存律师函发送及反馈成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseLetter/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCaseLetter:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCaseLetter zqwCaseLetter, RedirectAttributes redirectAttributes) {
		zqwCaseLetterService.delete(zqwCaseLetter);
		addMessage(redirectAttributes, "删除律师函发送及反馈成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseLetter/?repage";
	}

}