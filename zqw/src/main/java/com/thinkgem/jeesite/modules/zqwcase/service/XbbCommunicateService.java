/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.List;

import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CasePayment;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseXbbInsurance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.XbbCommunicate;
import com.thinkgem.jeesite.modules.zqwcase.dao.XbbCommunicateDao;

/**
 * 肖邦邦跟进记录管理Service
 * @author coder_cheng@126.com
 * @version 2019-09-01
 */
@Service
@Transactional(readOnly = true)
public class XbbCommunicateService extends CrudService<XbbCommunicateDao, XbbCommunicate> {

     @Autowired
	 CaseXbbInsuranceService caseXbbInsuranceService;

	public XbbCommunicate get(String id) {
		return super.get(id);
	}
	
	public List<XbbCommunicate> findList(XbbCommunicate xbbCommunicate) {
		return super.findList(xbbCommunicate);
	}
	
	public Page<XbbCommunicate> findPage(Page<XbbCommunicate> page, XbbCommunicate xbbCommunicate) {
		return super.findPage(page, xbbCommunicate);
	}
	
	@Transactional(readOnly = false)
	public void save(XbbCommunicate xbbCommunicate) {
		super.save(xbbCommunicate);
	}
	
	@Transactional(readOnly = false)
	public void delete(XbbCommunicate xbbCommunicate) {
		super.delete(xbbCommunicate);
	}


	@Transactional(readOnly = false)
	public void saveFromCrm(List<XbbCommunicate>xbbCommunicates){
		for (XbbCommunicate xbbCommunicate: xbbCommunicates){
			if (StringUtils.isNotBlank(xbbCommunicate.getXbbInsuranceId())){
			XbbCommunicate xbbCommunicateTemp = dao.get(xbbCommunicate.getId());
			if (xbbCommunicateTemp!=null){
				save(xbbCommunicate);
			}else {
				saveInsert(xbbCommunicate);
			}
		 CaseXbbInsurance caseXbbInsurance = caseXbbInsuranceService.get(xbbCommunicate.getXbbInsuranceId());
			if (caseXbbInsurance != null){


				if (xbbCommunicate.getVistWay().equals("F资缺")){
					caseXbbInsurance.setSourceStauts("0");
				}else if (xbbCommunicate.getVistWay().equals("F资补")){
					caseXbbInsurance.setSourceStauts("1");
				}else if (xbbCommunicate.getVistWay().equals("S验诉效")){
					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setsInspectionEffect(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setsInspectionEffect(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("S立时")){
					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setsInstant(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setsInstant(DateUtils.parseDate(dateString));
					}
				}else if (xbbCommunicate.getVistWay().equals("S开时")){
					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setsTurnOnTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setsTurnOnTime(DateUtils.parseDate(dateString));
					}
				}else if (xbbCommunicate.getVistWay().equals("S判达")){

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setsJudgment(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setsJudgment(DateUtils.parseDate(dateString));
					}


				}else if (xbbCommunicate.getVistWay().equals("S判生")){

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setsJudgmentTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setsJudgmentTime(DateUtils.parseDate(dateString));
					}


				}else if (xbbCommunicate.getVistWay().equals("SW移时")){
					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setSwTimeShift(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setSwTimeShift(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("SW完时")){


					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setSwEndTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setSwEndTime(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("SW立时")){
					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setSwInstant(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);
						caseXbbInsurance.setSwInstant(DateUtils.parseDate(dateString));
					}


				}else if (xbbCommunicate.getVistWay().equals("SW开时")){


					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setSwOpenTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setSwOpenTime(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("SW判达")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setSwJudgment(DateUtils.parseDate(dateString));


					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setSwJudgment(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setSwJudgment(DateUtils.parseDate(dateString));
					}


				}else if (xbbCommunicate.getVistWay().equals("SW判生")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setSwJudgmentTime(DateUtils.parseDate(dateString));


					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setSwJudgmentTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setSwJudgmentTime(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("Z验执效")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setzTestEffect(DateUtils.parseDate(dateString));



					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setzTestEffect(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setzTestEffect(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("Z申时")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setzShenShi(DateUtils.parseDate(dateString));

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setzShenShi(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setzShenShi(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("Z终时")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setzEndTime(DateUtils.parseDate(dateString));

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setzEndTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setzEndTime(DateUtils.parseDate(dateString));
					}

				}else if (xbbCommunicate.getVistWay().equals("ZW移时")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setZwTimeShift(DateUtils.parseDate(dateString));

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setZwTimeShift(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setZwTimeShift(DateUtils.parseDate(dateString));
					}
				}else if (xbbCommunicate.getVistWay().equals("ZW完时")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setZwFinishTime(DateUtils.parseDate(dateString));

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setZwFinishTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setZwFinishTime(DateUtils.parseDate(dateString));
					}
				}else if (xbbCommunicate.getVistWay().equals("ZW申时")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setZwShenShi(DateUtils.parseDate(dateString));

					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setZwShenShi(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setZwShenShi(DateUtils.parseDate(dateString));
					}
				}else if (xbbCommunicate.getVistWay().equals("ZW终时")){
//					String dateString = xbbCommunicate.getRemarks().split("#")[0];
//					caseXbbInsurance.setZwEndTime(DateUtils.parseDate(dateString));
					if (xbbCommunicate.getRemarks().contains("#")){
						String dateString = xbbCommunicate.getRemarks().split("#")[0];
						caseXbbInsurance.setZwEndTime(DateUtils.parseDate(dateString));
					}else if (xbbCommunicate.getRemarks().length()>=11){
						String dateString = xbbCommunicate.getRemarks().substring(0,11);;
						caseXbbInsurance.setZwEndTime(DateUtils.parseDate(dateString));
					}
				}else if (xbbCommunicate.getVistWay().equals("F意还")){
					caseXbbInsurance.setRepaymentIntention("意向还款");
				}else if (xbbCommunicate.getVistWay().equals("F恶拖")){
					caseXbbInsurance.setRepaymentIntention("恶意拖欠");
				}else if (xbbCommunicate.getVistWay().equals("F失联")){
					caseXbbInsurance.setRepaymentIntention("失联");
				}else if (xbbCommunicate.getVistWay().equals("F无听")){
					caseXbbInsurance.setRepaymentIntention("无人接听/拒接");
				}else if (xbbCommunicate.getVistWay().equals("F无听")){
					caseXbbInsurance.setRepaymentIntention("无人接听/拒接");
				}else if (xbbCommunicate.getVistWay().equals("F其他")){
					caseXbbInsurance.setRepaymentIntention("其他");
				}else if (xbbCommunicate.getVistWay().equals("LS涵")){
					caseXbbInsurance.setLawyerLetter("已发生");
				}

				caseXbbInsuranceService.save(caseXbbInsurance);
			}
			}
		}
	}



	@Transactional(readOnly = false)
	public void saveInsert(XbbCommunicate xbbCommunicate) {
		dao.insert(xbbCommunicate);
	}
	
}