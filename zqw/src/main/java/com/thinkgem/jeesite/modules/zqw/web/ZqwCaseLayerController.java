/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.xbb.helper.XbbException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseLayer;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCaseLayerService;

/**
 * 合作律师Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCaseLayer")
public class ZqwCaseLayerController extends BaseController {

	@Autowired
	private ZqwCaseLayerService zqwCaseLayerService;
	
	@ModelAttribute
	public ZqwCaseLayer get(@RequestParam(required=false) String id) {
		ZqwCaseLayer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCaseLayerService.get(id);
		}
		if (entity == null){
			entity = new ZqwCaseLayer();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCaseLayer:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCaseLayer zqwCaseLayer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCaseLayer> page = zqwCaseLayerService.findPage(new Page<ZqwCaseLayer>(request, response), zqwCaseLayer); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCaseLayerList";
	}

	@RequiresPermissions("zqw:zqwCaseLayer:view")
	@RequestMapping(value = "form")
	public String form(ZqwCaseLayer zqwCaseLayer, Model model) {
		model.addAttribute("zqwCaseLayer", zqwCaseLayer);
		return "modules/zqw/zqwCaseLayerForm";
	}

	@RequiresPermissions("zqw:zqwCaseLayer:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCaseLayer zqwCaseLayer, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwCaseLayer)){
			return form(zqwCaseLayer, model);
		}
		zqwCaseLayerService.save(zqwCaseLayer);
		addMessage(redirectAttributes, "保存合作律师成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseLayer/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCaseLayer:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCaseLayer zqwCaseLayer, RedirectAttributes redirectAttributes) {
		zqwCaseLayerService.delete(zqwCaseLayer);
		addMessage(redirectAttributes, "删除合作律师成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseLayer/?repage";
	}



}