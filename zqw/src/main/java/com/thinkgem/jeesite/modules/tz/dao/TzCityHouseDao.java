/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.tz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.tz.entity.TzCityHouse;

/**
 * 城市管理DAO接口
 * @author coder_cheng@126.com
 * @version 2019-06-11
 */
@MyBatisDao
public interface TzCityHouseDao extends CrudDao<TzCityHouse> {
	
}