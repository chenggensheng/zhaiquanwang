/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbUser;

/**
 * 肖帮帮用户管理DAO接口
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
@MyBatisDao
public interface XbbUserDao extends CrudDao<XbbUser> {
	
}