/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 肖邦邦跟进记录管理Entity
 * @author coder_cheng@126.com
 * @version 2019-09-01
 */
public class XbbCommunicate extends DataEntity<XbbCommunicate> {
	
	private static final long serialVersionUID = 1L;
	private String address;		// address
	private String createTimes;		// create_times
	private String picture;		// picture
	private String customerId;		// customer_id
	private String customerName;		// customer_name
	private String vistTimes;		// vist_times
	private String updateTimes;		// update_times
	private String vistPerson;		// vist_person
	private String vistPersonId;		// vist_person_id
	private String vistWay;		// vist_way
	private String video;		// video
	private String xbbInsuranceId;		// xbb_insurance_id
	
	public XbbCommunicate() {
		super();
	}

	public XbbCommunicate(String id){
		super(id);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=0, max=64, message="create_times长度必须介于 0 和 64 之间")
	public String getCreateTimes() {
		return createTimes;
	}

	public void setCreateTimes(String createTimes) {
		this.createTimes = createTimes;
	}
	
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	@Length(min=0, max=64, message="customer_id长度必须介于 0 和 64 之间")
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	@Length(min=0, max=10240, message="customer_name长度必须介于 0 和 10240 之间")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Length(min=0, max=64, message="vist_times长度必须介于 0 和 64 之间")
	public String getVistTimes() {
		return vistTimes;
	}

	public void setVistTimes(String vistTimes) {
		this.vistTimes = vistTimes;
	}
	
	@Length(min=0, max=64, message="update_times长度必须介于 0 和 64 之间")
	public String getUpdateTimes() {
		return updateTimes;
	}

	public void setUpdateTimes(String updateTimes) {
		this.updateTimes = updateTimes;
	}
	
	@Length(min=0, max=128, message="vist_person长度必须介于 0 和 128 之间")
	public String getVistPerson() {
		return vistPerson;
	}

	public void setVistPerson(String vistPerson) {
		this.vistPerson = vistPerson;
	}
	
	@Length(min=0, max=64, message="vist_person_id长度必须介于 0 和 64 之间")
	public String getVistPersonId() {
		return vistPersonId;
	}

	public void setVistPersonId(String vistPersonId) {
		this.vistPersonId = vistPersonId;
	}
	
	@Length(min=0, max=1024, message="vist_way长度必须介于 0 和 1024 之间")
	public String getVistWay() {
		return vistWay;
	}

	public void setVistWay(String vistWay) {
		this.vistWay = vistWay;
	}
	
	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}
	
	@Length(min=0, max=64, message="xbb_insurance_id长度必须介于 0 和 64 之间")
	public String getXbbInsuranceId() {
		return xbbInsuranceId;
	}

	public void setXbbInsuranceId(String xbbInsuranceId) {
		this.xbbInsuranceId = xbbInsuranceId;
	}
	
}