/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseIndictment;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCaseIndictmentService;

/**
 * 民事起诉状Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCaseIndictment")
public class ZqwCaseIndictmentController extends BaseController {

	@Autowired
	private ZqwCaseIndictmentService zqwCaseIndictmentService;
	
	@ModelAttribute
	public ZqwCaseIndictment get(@RequestParam(required=false) String id) {
		ZqwCaseIndictment entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCaseIndictmentService.get(id);
		}
		if (entity == null){
			entity = new ZqwCaseIndictment();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCaseIndictment:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCaseIndictment zqwCaseIndictment, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCaseIndictment> page = zqwCaseIndictmentService.findPage(new Page<ZqwCaseIndictment>(request, response), zqwCaseIndictment); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCaseIndictmentList";
	}

	@RequiresPermissions("zqw:zqwCaseIndictment:view")
	@RequestMapping(value = "form")
	public String form(ZqwCaseIndictment zqwCaseIndictment, Model model) {
		model.addAttribute("zqwCaseIndictment", zqwCaseIndictment);
		return "modules/zqw/zqwCaseIndictmentForm";
	}

	@RequiresPermissions("zqw:zqwCaseIndictment:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCaseIndictment zqwCaseIndictment, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwCaseIndictment)){
			return form(zqwCaseIndictment, model);
		}
		zqwCaseIndictmentService.save(zqwCaseIndictment);
		addMessage(redirectAttributes, "保存民事起诉状成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseIndictment/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCaseIndictment:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCaseIndictment zqwCaseIndictment, RedirectAttributes redirectAttributes) {
		zqwCaseIndictmentService.delete(zqwCaseIndictment);
		addMessage(redirectAttributes, "删除民事起诉状成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseIndictment/?repage";
	}

}