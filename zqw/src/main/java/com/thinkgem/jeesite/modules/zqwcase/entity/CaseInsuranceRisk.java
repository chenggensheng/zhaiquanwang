package com.thinkgem.jeesite.modules.zqwcase.entity;

public class CaseInsuranceRisk {

    private String oneMounthCount;
    private String towMounthCount;
    private String threeMounthCount;
    private String typeName;
    private String type;

    public String getOneMounthCount() {
        return oneMounthCount;
    }

    public void setOneMounthCount(String oneMounthCount) {
        this.oneMounthCount = oneMounthCount;
    }

    public String getTowMounthCount() {
        return towMounthCount;
    }

    public void setTowMounthCount(String towMounthCount) {
        this.towMounthCount = towMounthCount;
    }

    public String getThreeMounthCount() {
        return threeMounthCount;
    }

    public void setThreeMounthCount(String threeMounthCount) {
        this.threeMounthCount = threeMounthCount;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
