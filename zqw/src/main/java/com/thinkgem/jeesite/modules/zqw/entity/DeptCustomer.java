/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 客户管理Entity
 * @author coder_cheng@126.com
 * @version 2018-10-23
 */
public class DeptCustomer extends DataEntity<DeptCustomer> {
	
	private static final long serialVersionUID = 1L;
	private String deptCustomer;		// 客户名称
	private String deptCustomerPic;		// 客户图片
	
	public DeptCustomer() {
		super();
	}

	public DeptCustomer(String id){
		super(id);
	}

	@Length(min=0, max=128, message="客户名称长度必须介于 0 和 128 之间")
	public String getDeptCustomer() {
		return deptCustomer;
	}

	public void setDeptCustomer(String deptCustomer) {
		this.deptCustomer = deptCustomer;
	}
	
	@Length(min=0, max=10240, message="客户图片长度必须介于 0 和 10240 之间")
	public String getDeptCustomerPic() {
		return deptCustomerPic;
	}

	public void setDeptCustomerPic(String deptCustomerPic) {
		this.deptCustomerPic = deptCustomerPic;
	}
	
}