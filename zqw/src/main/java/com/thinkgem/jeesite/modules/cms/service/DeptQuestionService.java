/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.cms.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.cms.entity.DeptQuestion;
import com.thinkgem.jeesite.modules.cms.dao.DeptQuestionDao;

/**
 * 咨询方案Service
 * @author coder_cheng@126.com
 * @version 2018-12-17
 */
@Service
@Transactional(readOnly = true)
public class DeptQuestionService extends CrudService<DeptQuestionDao, DeptQuestion> {

	public DeptQuestion get(String id) {
		return super.get(id);
	}
	
	public List<DeptQuestion> findList(DeptQuestion deptQuestion) {
		return super.findList(deptQuestion);
	}
	
	public Page<DeptQuestion> findPage(Page<DeptQuestion> page, DeptQuestion deptQuestion) {
		return super.findPage(page, deptQuestion);
	}
	
	@Transactional(readOnly = false)
	public void save(DeptQuestion deptQuestion) {
		super.save(deptQuestion);
	}
	
	@Transactional(readOnly = false)
	public void delete(DeptQuestion deptQuestion) {
		super.delete(deptQuestion);
	}
	
}