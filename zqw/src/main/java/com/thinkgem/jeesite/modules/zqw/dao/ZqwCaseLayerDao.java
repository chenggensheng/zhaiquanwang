/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseLayer;

/**
 * 合作律师DAO接口
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@MyBatisDao
public interface ZqwCaseLayerDao extends CrudDao<ZqwCaseLayer> {
	
}