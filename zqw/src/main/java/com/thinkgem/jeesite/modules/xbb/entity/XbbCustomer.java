/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 肖帮帮客户管理Entity
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
public class XbbCustomer extends DataEntity<XbbCustomer> {
	
	private static final long serialVersionUID = 1L;
	private String nextContactTime;		// 下次预约联系时间
	private String createid;		// 创建人ID
	private String createtime;		// 创建时间
	private String districtcounty;		// 区/县
	private String picture;		// 图片
	private String addr;		// 地址
	private String customerfocuscategory;		// 客户关注类别
	private String customerclassification;		// 客户分级
	private String customer;		// 客户名称
	private String customerwebsite;		// 客户官网
	private String customernature;		// 客户性质
	private String customersource;		// 客户来源
	private String customerstatus;		// 客户状态
	private String customerphone;		// customerphone
	private String customertype;		// 客户类型
	private String customerindustry;		// 客户行业
	private String customerassociationname;		// 展会/客户会名称
	private String city;		// 市
	private String filetemp;		// 文件
	private String updatetime;		// 更新时间
	private String registeredcapital;		// 注册资金
	private String legalrepresentative;		// 法人代表
	private String nameofchannel;		// 渠道/代理商名称
	private String province;		// 省
	private String iatitude;		// 纬度
	private String iongitude;		// 经度
	private String creditcode;		// 身份证号/信用代码
	private String importance;		// 重要程度
	private String customerabbreviation;		// 客户简称
	private String clientprofile;		// 客户简介
	
	public XbbCustomer() {
		super();
	}

	public XbbCustomer(String id){
		super(id);
	}

	@Length(min=0, max=64, message="下次预约联系时间长度必须介于 0 和 64 之间")
	public String getNextContactTime() {
		return nextContactTime;
	}

	public void setNextContactTime(String nextContactTime) {
		this.nextContactTime = nextContactTime;
	}
	
	@Length(min=0, max=64, message="创建人ID长度必须介于 0 和 64 之间")
	public String getCreateid() {
		return createid;
	}

	public void setCreateid(String createid) {
		this.createid = createid;
	}
	
	@Length(min=0, max=255, message="创建时间长度必须介于 0 和 255 之间")
	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	
	@Length(min=0, max=255, message="区/县长度必须介于 0 和 255 之间")
	public String getDistrictcounty() {
		return districtcounty;
	}

	public void setDistrictcounty(String districtcounty) {
		this.districtcounty = districtcounty;
	}
	
	@Length(min=0, max=1255, message="图片长度必须介于 0 和 1255 之间")
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	@Length(min=0, max=1255, message="地址长度必须介于 0 和 1255 之间")
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	@Length(min=0, max=255, message="客户关注类别长度必须介于 0 和 255 之间")
	public String getCustomerfocuscategory() {
		return customerfocuscategory;
	}

	public void setCustomerfocuscategory(String customerfocuscategory) {
		this.customerfocuscategory = customerfocuscategory;
	}
	
	@Length(min=0, max=255, message="客户分级长度必须介于 0 和 255 之间")
	public String getCustomerclassification() {
		return customerclassification;
	}

	public void setCustomerclassification(String customerclassification) {
		this.customerclassification = customerclassification;
	}
	
	@Length(min=0, max=255, message="客户名称长度必须介于 0 和 255 之间")
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
	@Length(min=0, max=255, message="客户官网长度必须介于 0 和 255 之间")
	public String getCustomerwebsite() {
		return customerwebsite;
	}

	public void setCustomerwebsite(String customerwebsite) {
		this.customerwebsite = customerwebsite;
	}
	
	@Length(min=0, max=255, message="客户性质长度必须介于 0 和 255 之间")
	public String getCustomernature() {
		return customernature;
	}

	public void setCustomernature(String customernature) {
		this.customernature = customernature;
	}
	
	@Length(min=0, max=255, message="客户来源长度必须介于 0 和 255 之间")
	public String getCustomersource() {
		return customersource;
	}

	public void setCustomersource(String customersource) {
		this.customersource = customersource;
	}
	
	@Length(min=0, max=255, message="客户状态长度必须介于 0 和 255 之间")
	public String getCustomerstatus() {
		return customerstatus;
	}

	public void setCustomerstatus(String customerstatus) {
		this.customerstatus = customerstatus;
	}
	
	@Length(min=0, max=255, message="customerphone长度必须介于 0 和 255 之间")
	public String getCustomerphone() {
		return customerphone;
	}

	public void setCustomerphone(String customerphone) {
		this.customerphone = customerphone;
	}
	
	@Length(min=0, max=255, message="客户类型长度必须介于 0 和 255 之间")
	public String getCustomertype() {
		return customertype;
	}

	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}
	
	@Length(min=0, max=255, message="客户行业长度必须介于 0 和 255 之间")
	public String getCustomerindustry() {
		return customerindustry;
	}

	public void setCustomerindustry(String customerindustry) {
		this.customerindustry = customerindustry;
	}
	
	@Length(min=0, max=255, message="展会/客户会名称长度必须介于 0 和 255 之间")
	public String getCustomerassociationname() {
		return customerassociationname;
	}

	public void setCustomerassociationname(String customerassociationname) {
		this.customerassociationname = customerassociationname;
	}
	
	@Length(min=0, max=255, message="市长度必须介于 0 和 255 之间")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getFiletemp() {
		return filetemp;
	}

	public void setFiletemp(String filetemp) {
		this.filetemp = filetemp;
	}
	
	@Length(min=0, max=255, message="更新时间长度必须介于 0 和 255 之间")
	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	
	@Length(min=0, max=255, message="注册资金长度必须介于 0 和 255 之间")
	public String getRegisteredcapital() {
		return registeredcapital;
	}

	public void setRegisteredcapital(String registeredcapital) {
		this.registeredcapital = registeredcapital;
	}
	
	@Length(min=0, max=255, message="legalrepresentative长度必须介于 0 和 255 之间")
	public String getLegalrepresentative() {
		return legalrepresentative;
	}

	public void setLegalrepresentative(String legalrepresentative) {
		this.legalrepresentative = legalrepresentative;
	}
	
	@Length(min=0, max=255, message="渠道/代理商名称长度必须介于 0 和 255 之间")
	public String getNameofchannel() {
		return nameofchannel;
	}

	public void setNameofchannel(String nameofchannel) {
		this.nameofchannel = nameofchannel;
	}
	
	@Length(min=0, max=255, message="省长度必须介于 0 和 255 之间")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@Length(min=0, max=255, message="纬度长度必须介于 0 和 255 之间")
	public String getIatitude() {
		return iatitude;
	}

	public void setIatitude(String iatitude) {
		this.iatitude = iatitude;
	}
	
	@Length(min=0, max=255, message="经度长度必须介于 0 和 255 之间")
	public String getIongitude() {
		return iongitude;
	}

	public void setIongitude(String iongitude) {
		this.iongitude = iongitude;
	}
	
	@Length(min=0, max=255, message="身份证号/信用代码长度必须介于 0 和 255 之间")
	public String getCreditcode() {
		return creditcode;
	}

	public void setCreditcode(String creditcode) {
		this.creditcode = creditcode;
	}
	
	@Length(min=0, max=255, message="重要程度长度必须介于 0 和 255 之间")
	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}
	
	public String getCustomerabbreviation() {
		return customerabbreviation;
	}

	public void setCustomerabbreviation(String customerabbreviation) {
		this.customerabbreviation = customerabbreviation;
	}
	
	public String getClientprofile() {
		return clientprofile;
	}

	public void setClientprofile(String clientprofile) {
		this.clientprofile = clientprofile;
	}
	
}