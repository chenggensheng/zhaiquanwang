/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 肖帮帮用户管理Entity
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
public class XbbUser extends DataEntity<XbbUser> {
	
	private static final long serialVersionUID = 1L;
	private String active;		// active
	private String addtime;		// addtime
	private String avatar;		// avatar
	private String department;		// department
	private String isleaderindepts;		// isleaderindepts
	private String name;		// name
	private String position;		// position
	private String updatetime;		// updatetime
	
	public XbbUser() {
		super();
	}

	public XbbUser(String id){
		super(id);
	}

	@Length(min=0, max=255, message="active长度必须介于 0 和 255 之间")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
	@Length(min=0, max=255, message="addtime长度必须介于 0 和 255 之间")
	public String getAddtime() {
		return addtime;
	}

	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}
	
	@Length(min=0, max=1024, message="avatar长度必须介于 0 和 1024 之间")
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	@Length(min=0, max=255, message="department长度必须介于 0 和 255 之间")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Length(min=0, max=255, message="isleaderindepts长度必须介于 0 和 255 之间")
	public String getIsleaderindepts() {
		return isleaderindepts;
	}

	public void setIsleaderindepts(String isleaderindepts) {
		this.isleaderindepts = isleaderindepts;
	}
	
	@Length(min=0, max=255, message="name长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="position长度必须介于 0 和 255 之间")
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	@Length(min=0, max=255, message="updatetime长度必须介于 0 和 255 之间")
	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	
}