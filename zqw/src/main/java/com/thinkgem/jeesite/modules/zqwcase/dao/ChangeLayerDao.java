/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.ChangeLayer;

/**
 * 生成律师律所DAO接口
 * @author coder_cheng@126.com
 * @version 2019-09-14
 */
@MyBatisDao
public interface ChangeLayerDao extends CrudDao<ChangeLayer> {
	
}