/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.cms.web.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.cms.entity.DeptQuestion;
import com.thinkgem.jeesite.modules.cms.service.DeptQuestionService;

/**
 * 咨询方案Controller
 * @author coder_cheng@126.com
 * @version 2018-12-17
 */
@Controller
@RequestMapping(value = "${frontPath}/deptQuestion")
public class DeptQuestionController extends BaseController {

	@Autowired
	private DeptQuestionService deptQuestionService;
	
	@ModelAttribute
	public DeptQuestion get(@RequestParam(required=false) String id) {
		DeptQuestion entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = deptQuestionService.get(id);
		}
		if (entity == null){
			entity = new DeptQuestion();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(DeptQuestion deptQuestion, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<DeptQuestion> page = deptQuestionService.findPage(new Page<DeptQuestion>(request, response), deptQuestion); 
		model.addAttribute("page", page);
		return "modules/cms/front/deptQuestionList";
	}

	@RequestMapping(value = "form")
	public String form(DeptQuestion deptQuestion, Model model) {
		model.addAttribute("deptQuestion", deptQuestion);
		return "modules/cms/front/deptQuestionForm";
	}

	@RequestMapping(value = "save")
	public String save(DeptQuestion deptQuestion, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, deptQuestion)){
			return form(deptQuestion, model);
		}
		deptQuestionService.save(deptQuestion);
		addMessage(redirectAttributes, "保存咨询方案成功");
		return "redirect:"+Global.getFrontPath()+"/deptQuestion/form/?id="+deptQuestion.getId();
	}
	
	@RequestMapping(value = "delete")
	public String delete(DeptQuestion deptQuestion, RedirectAttributes redirectAttributes) {
		deptQuestionService.delete(deptQuestion);
		addMessage(redirectAttributes, "删除咨询方案成功");
		return "redirect:"+Global.getAdminPath()+"/cms/front/deptQuestion/?repage";
	}

}