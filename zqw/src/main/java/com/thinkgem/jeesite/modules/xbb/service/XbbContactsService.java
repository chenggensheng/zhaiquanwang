/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xbb.dao.XbbContactsDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbContacts;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 联系人管理Service
 * @author coder_cheng@126.com
 * @version 2018-08-31
 */
@Service
@Transactional(readOnly = true)
public class XbbContactsService extends CrudService<XbbContactsDao, XbbContacts> {

	public XbbContacts get(String id) {
		return super.get(id);
	}
	
	public List<XbbContacts> findList(XbbContacts xbbContacts) {
		return super.findList(xbbContacts);
	}
	
	public Page<XbbContacts> findPage(Page<XbbContacts> page, XbbContacts xbbContacts) {
		return super.findPage(page, xbbContacts);
	}
	
	@Transactional(readOnly = false)
	public void save(XbbContacts xbbContacts) {
		super.save(xbbContacts);
	}
	
	@Transactional(readOnly = false)
	public void delete(XbbContacts xbbContacts) {
		super.delete(xbbContacts);
	}
	
}