/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCaseDoing;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCaseDoingService;

/**
 * 案件进展Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCaseDoing")
public class ZqwCaseDoingController extends BaseController {

	@Autowired
	private ZqwCaseDoingService zqwCaseDoingService;
	
	@ModelAttribute
	public ZqwCaseDoing get(@RequestParam(required=false) String id) {
		ZqwCaseDoing entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCaseDoingService.get(id);
		}
		if (entity == null){
			entity = new ZqwCaseDoing();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCaseDoing:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCaseDoing zqwCaseDoing, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCaseDoing> page = zqwCaseDoingService.findPage(new Page<ZqwCaseDoing>(request, response), zqwCaseDoing); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCaseDoingList";
	}

	@RequiresPermissions("zqw:zqwCaseDoing:view")
	@RequestMapping(value = "form")
	public String form(ZqwCaseDoing zqwCaseDoing, Model model) {
		model.addAttribute("zqwCaseDoing", zqwCaseDoing);
		return "modules/zqw/zqwCaseDoingForm";
	}

	@RequiresPermissions("zqw:zqwCaseDoing:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCaseDoing zqwCaseDoing, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwCaseDoing)){
			return form(zqwCaseDoing, model);
		}
		zqwCaseDoingService.save(zqwCaseDoing);
		addMessage(redirectAttributes, "保存案件进展成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseDoing/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCaseDoing:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCaseDoing zqwCaseDoing, RedirectAttributes redirectAttributes) {
		zqwCaseDoingService.delete(zqwCaseDoing);
		addMessage(redirectAttributes, "删除案件进展成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCaseDoing/?repage";
	}

}