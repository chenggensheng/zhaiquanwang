package com.thinkgem.jeesite.modules.zqw.utils;

import com.deepoove.poi.XWPFTemplate;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.utils.*;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCase;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseInsuranceDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseInsurance;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: coder_cheng@126.com
 * @description:
 * @Date: Created in 10:38 2019-06-18
 * @Modified By:
 */

public class WordUtilsTemp {

    public static CaseInsuranceDao caseInsuranceDao = SpringContextHolder.getBean(CaseInsuranceDao.class);


    /**
     * 下载单个案件全套资料，打包下载
     * @param caseInsurance
     * @param response
     * @throws Exception
     */
    public static void downloadZip(CaseInsurance caseInsurance,HttpServletResponse response)throws Exception{

        String [] exportSources = caseInsurance.getInsuranceSubrogation().split(",");

        String filePath = Global.getUserfilesBaseDir() +File.separator+"word"+File.separator+System.currentTimeMillis()+caseInsurance.getId() +File.separator+caseInsurance.getCaseCustomer().getCreditor()+"-"+caseInsurance.getInsured();
        FileUtils.createDirectory(filePath);
            if (caseInsurance.getCaseType().equals("0")){//追偿
                for (String temp : exportSources) {
                    FileUtils.createDirectory(filePath + File.separator + temp);
                    String templetFile = Global.getTempletFilesBaseDir()+File.separator+"insurance"+ File.separator +temp;
                    List<String> templeFiles = FileUtils.findChildrenList(new File(templetFile),false);
                    if (temp.contains("01")){
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                datas.put("insured", caseInsurance.getInsured());
                                datas.put("insuredContactTelephone", caseInsurance.getInsuredContactTelephone());
                                datas.put("insuredVehicleDriverTelephone", caseInsurance.getInsuredVehicleDriverTelephone());
                                datas.put("insuranceClaimsContractors", caseInsurance.getInsuranceClaimsContractors());
                                datas.put("insuranceClaimsTelephone", caseInsurance.getInsuranceClaimsTelephone());
                                datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                datas.put("caseIntroduction",caseInsurance.getCaseIntroduction());
                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }else if (temp.contains("02") ){ // 电话短信话书
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("insured", caseInsurance.getInsured());
                                datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }else if ( temp.contains("03") ){ // 电话短信话书 ----律师函
                           if (caseInsurance.getLayerNumber()==0){
                               Integer layerNumber =  caseInsuranceDao.getMax();
                               if (layerNumber !=0){
                                   String yearString = layerNumber.toString().substring(0,4);
                                   if(yearString.equals(DateUtils.getYear())){
                                       layerNumber = layerNumber +1;
                                   }else {
                                       layerNumber = Integer.valueOf(DateUtils.getYear()+"00001");
                                   }
                               }else {
                                   layerNumber = Integer.valueOf(DateUtils.getYear()+"00001");
                               }
                               caseInsurance.setLayerNumber(layerNumber);
                               if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                   caseInsurance.setLayerNumberOne("-1");
                               }else {
                                   caseInsurance.setLayerNumberOne("-1");
                                   caseInsurance.setLayerNumberTow("-2");
                               }
                               caseInsuranceDao.updateLayerNumber(caseInsurance);
                           }


                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (caseInsurance.getInsured()!=null&&caseInsurance.getInsuredVehicleDriver()!=null &&caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                    if (tempString.contains("为一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                        datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));
                                        datas.put("type","-1");
                                        datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                        datas.put("insured", caseInsurance.getInsured());
                                        datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                        datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                        datas.put("month",DateUtils.getMonth());
                                        datas.put("day",DateUtils.getDay());
                                        datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsuredVehicleDriver()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }else {
                                    if (tempString.contains("-车主")|| tempString.contains("-驾驶员")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));

                                    if (tempString.contains("车主")){
                                        datas.put("type","-2");
                                    }else {
                                        datas.put("type","-1");
                                    }
                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());

                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());

                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                        datas.put("month",DateUtils.getMonth());
                                        datas.put("day",DateUtils.getDay());

                                    if (tempString.contains("车主")){
                                            String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsured()+"-"+tempString;
                                            changeToWord(datas,tempFile,outFile);

                                        }else {
                                            String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsuredVehicleDriver()+"-"+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }



                                }
                                }



                            }
                        }
                    }


                    else if (temp.contains("05")){// 民事起诉状
                        if (caseInsurance.getIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                    datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                    datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                    datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                    datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());
                                    datas.put("insuredContactTelephone",caseInsurance.getInsuredContactTelephone());

                                    datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        if (tempString.contains("为一人-个人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());




                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                        datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                        datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                        datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }else {
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        if (tempFile.contains("不是同一人-个人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                            datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());

                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                            datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                            datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                            datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                            datas.put("insured",caseInsurance.getInsured());
                                            datas.put("insuredSex",caseInsurance.getInsuredSex());
                                            datas.put("insuredPeople",caseInsurance.getInsuredPeople());
                                            datas.put("insuredBith",caseInsurance.getInsuredBith());
                                            datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                            datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                            datas.put("insuredContactTelephone",caseInsurance.getInsuredContactTelephone());

                                            datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                            datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                            datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                            datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                            datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                            datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                            datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());



                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }
                        }
                    }else if (temp.contains("06")){
                        if (caseInsurance.getIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                        datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                        datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                        datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                        datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {

                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        //车主
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                        //驾驶员
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                        datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                        datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                        datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                        datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }

                            }

                        }
                    } else if(temp.contains("07")){
                        if (caseInsurance.getGrantAuthorization().equals("0")){  //一般
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("一般授权")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                            datas.put("insured", caseInsurance.getInsured());
                                        }else {
                                            datas.put("insured", caseInsurance.getInsured()+"、"+caseInsurance.getInsuredVehicleDriver());
                                        }

//                                        datas.put("insured", caseInsurance.getVictimVehicleOwner());
//                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("特别授权")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

//                                        datas.put("insured", caseInsurance.getVictimVehicleOwner());
//                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                            datas.put("insured", caseInsurance.getInsured());
                                        }else {
                                            datas.put("insured", caseInsurance.getInsured()+"、"+caseInsurance.getInsuredVehicleDriver());
                                        }

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }

                    }else if (temp.contains("08")){
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("人为同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("reasonsRecourse" ,caseInsurance.getReasonsRecourse());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("不为同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("reasonsRecourse" ,caseInsurance.getReasonsRecourse());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }

                        }

                    }else if (temp.contains("09") ){ // 合作律师
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("undertakingLawyerName", caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("insured", caseInsurance.getInsured());
                                datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());

                                datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                datas.put("recoveryCasesAmount",caseInsurance.getRecoveryCasesAmount());
                                datas.put("investmentInquiries",caseInsurance.getCaseSpeed().getInvestmentInquiries());
                                datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());


                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("reasonsRecourse",caseInsurance.getReasonsRecourse());
                                datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日"));
                                datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());
                                datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }else if (temp.contains("11")){// 查控申请书
                        if (caseInsurance.getIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                        datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                        datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        if (tempString.contains("主是同一人")){
                                            String tempFile = templetFile+File.separator+tempString;
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());



                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }else {
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        if (tempFile.contains("主不是同一人")){
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("insured",caseInsurance.getInsured());
                                            datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                            datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());

                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }
                        }
                    }else if (temp.contains("12")){// 恢复执行申请书
                        if (caseInsurance.getIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                        datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                        datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());

                                        datas.put("executionNum",caseInsurance.getCaseSpeed().getExecutionNum());
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        if (tempString.contains("主是同一人")){
                                            String tempFile = templetFile+File.separator+tempString;
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());

                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }else {
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        if (tempFile.contains("主不是同一人")){
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("insured",caseInsurance.getInsured());
                                            datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                            datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());



                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());


                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                String zipFilePath = Global.getUserfilesBaseDir() +File.separator+"zip"+File.separator+DateUtils.getDate()+caseInsurance.getId();
                FileUtils.createDirectory(zipFilePath);
                FileOutputStream fos1 = new FileOutputStream(new File(zipFilePath+File.separator+"保险追偿.zip"));
                ZipUtils.toZip(filePath, fos1,true);
                try {
                    String fileName = "保险追偿.zip";
                    File file = new File(zipFilePath+File.separator+"保险追偿.zip");
                    response.setCharacterEncoding("UTF-8");
                    response.setHeader("Content-Disposition",
                            "attachment; filename=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1"));
                    response.setContentLength((int) file.length());
                    response.setContentType("application/zip");// 定义输出类型
                    FileInputStream fis = new FileInputStream(file);
                    BufferedInputStream buff = new BufferedInputStream(fis);
                    byte[] b = new byte[1024];// 相当于我们的缓存
                    long k = 0;// 该值用于计算当前实际下载了多少字节
                    OutputStream myout = response.getOutputStream();// 从response对象中得到输出流,准备下载
                    // 开始循环下载
                    while (k < file.length()) {
                        int j = buff.read(b, 0, 1024);
                        k += j;
                        myout.write(b, 0, j);
                    }
                    myout.flush();
                    buff.close();
                    file.delete();
                } catch (Exception e) {
                    System.out.println(e);
                }

            }else if(caseInsurance.getCaseType().equals("1")){//代位

                for (String temp : exportSources) {
                    FileUtils.createDirectory(filePath + File.separator + temp);
                    String templetFile = Global.getTempletFilesBaseDir()+File.separator+"subrogation"+ File.separator +temp;
                    List<String> templeFiles = FileUtils.findChildrenList(new File(templetFile),false);
                    if (temp.contains("01")){
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("victimVehicleOwer",caseInsurance.getVictimVehicleOwner());
                                datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                datas.put("insured", caseInsurance.getInsured());
                                datas.put("insuredContactTelephone", caseInsurance.getInsuredContactTelephone());
                                datas.put("victimVehicleOwnerTelephone",caseInsurance.getVictimVehicleOwnerTelephone());
                                datas.put("injuriousInsuranceCompany",caseInsurance.getInjuriousInsuranceCompany());
                                datas.put("injuriousCompanyTelephone",caseInsurance.getInsuredContactTelephone());
                                datas.put("insuranceClaimsContractors", caseInsurance.getInsuranceClaimsContractors());
                                datas.put("insuranceClaimsTelephone", caseInsurance.getInsuranceClaimsTelephone());
                                datas.put("caseIntroduction",caseInsurance.getCaseIntroduction());
                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }else if (temp.contains("02")){ // 电话短信话书 ----律师函
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                datas.put("victimDriverHarmful", caseInsurance.getVictimDriverHarmful());
                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }



                    else if (temp.contains("03") ){ // 电话短信话书 ----律师函
                        if (templeFiles != null && templeFiles.size()>0){
                             if (caseInsurance.getLayerNumber() ==0) {
                                 Integer layerNumber = caseInsuranceDao.getMax();
                                 if (layerNumber != 0) {
                                     String yearString = layerNumber.toString().substring(0, 4);
                                     if (yearString.equals(DateUtils.getYear())) {
                                         layerNumber = layerNumber + 1;
                                     } else {
                                         layerNumber = Integer.valueOf(DateUtils.getYear() + "00001");
                                     }
                                 } else {
                                     layerNumber = Integer.valueOf(DateUtils.getYear() + "00001");
                                 }
                                 caseInsurance.setLayerNumber(layerNumber);
                                 if (caseInsurance.getVictimVehicleOwner().equals(caseInsurance.getVictimDriverHarmful())){
                                     caseInsurance.setLayerNumberOne("-1");
                                 }else {
                                     caseInsurance.setLayerNumberOne("-1");
                                     caseInsurance.setLayerNumberTow("-2");
                                 }
                                 caseInsuranceDao.updateLayerNumber(caseInsurance);
                             }

                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (caseInsurance.getVictimVehicleOwner()!=null &&caseInsurance.getVictimDriverHarmful()!=null && caseInsurance.getVictimVehicleOwner().equals(caseInsurance.getVictimDriverHarmful())){
                                    if (tempString.contains("车主为一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));
                                    datas.put("type","-1");
                                    datas.put("victimDriverHarmful", caseInsurance.getVictimDriverHarmful());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                    datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());
                                    String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getVictimDriverHarmful()+"-"+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                    }
                                }else {
                                    if (tempString.contains("-车主")||tempString.contains("-驾驶员")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));

                                    if (tempString.contains("车主")){
                                        datas.put("type","-2");
                                    }else {
                                        datas.put("type","-1");
                                    }
                                        datas.put("victimDriverHarmful", caseInsurance.getVictimDriverHarmful());
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                        datas.put("insured", caseInsurance.getInsured());
                                        datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                        datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                        datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                        datas.put("month",DateUtils.getMonth());
                                        datas.put("day",DateUtils.getDay());

                                        if (tempString.contains("车主")){

                                            String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getVictimVehicleOwner()+"-"+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }else {
                                            String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getVictimDriverHarmful()+"-"+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }


                                }
                                }



                            }
                        }
                    }

                    else if (temp.contains("05")){// 民事起诉状
                        if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulSex",caseInsurance.getVictimDriverHarmfulSex());
                                        datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerTelephone());
                                        datas.put("victimDriverHarmfulBith",caseInsurance.getVictimDriverHarmfulBith());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                        datas.put("victimDriverHarmfulTelephone",caseInsurance.getVictimDriverHarmfulTelephone());
                                        if(StringUtils.isNotBlank(caseInsurance.getSpecialAmountPaidBack())) {
                                            datas.put("specialAmountPaidBack", "-" + caseInsurance.getSpecialAmountPaidBack());
                                        }

                                        if(StringUtils.isNotBlank(caseInsurance.getVictimDriverHarmfulResponsibility())){
                                            if (caseInsurance.getVictimDriverHarmfulResponsibility().equals("全部")){
                                                datas.put("ratioShow","100%");

                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("主要")){
                                                datas.put("ratioShow","70%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("次要")){
                                                datas.put("ratioShow","30%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("同等")){
                                                datas.put("ratioShow","50%");
                                            }else {
                                                datas.put("ratioShow","0");
                                            }
                                        }

                                        datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                        datas.put("victimVehicleOwnerHousehlod",caseInsurance.getVictimVehicleOwnerHousehlod());
                                        datas.put("victimVehicleOwnerIdcard",caseInsurance.getVictimVehicleOwnerIdcard());
                                        datas.put("victimVehicleOwnerResponsiblity",caseInsurance.getVictimVehicleOwnerResponsiblity());
                                        datas.put("victimVehicleOwnerDuty",caseInsurance.getVictimVehicleOwnerDuty());
                                        datas.put("victimVehicleOwnerTelephone",caseInsurance.getVictimVehicleOwnerTelephone());


                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());


                                        datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

//                                      datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());


                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        if (tempString.contains("为一人-个人")){
                                            String tempFile = templetFile+File.separator+tempString;
                                            Map<String, Object> datas = new HashMap<String, Object>();
                                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                            datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());

                                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                            datas.put("victimDriverHarmfulSex",caseInsurance.getVictimDriverHarmfulSex());
                                            datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerTelephone());
                                            datas.put("victimDriverHarmfulBith",caseInsurance.getVictimDriverHarmfulBith());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                            datas.put("victimDriverHarmfulTelephone",caseInsurance.getVictimDriverHarmfulTelephone());
                                            if(StringUtils.isNotBlank(caseInsurance.getSpecialAmountPaidBack())) {
                                                datas.put("specialAmountPaidBack", "-" + caseInsurance.getSpecialAmountPaidBack());
                                            }

                                            if(StringUtils.isNotBlank(caseInsurance.getVictimDriverHarmfulResponsibility())){
                                                if (caseInsurance.getVictimDriverHarmfulResponsibility().equals("全部")){
                                                    datas.put("ratioShow","100%");

                                                }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("主要")){
                                                    datas.put("ratioShow","70%");
                                                }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("次要")){
                                                    datas.put("ratioShow","30%");
                                                }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("同等")){
                                                    datas.put("ratioShow","50%");
                                                }else {
                                                    datas.put("ratioShow","0");
                                                }
                                            }

                                            datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                            datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                            datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                            datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                            datas.put("insured",caseInsurance.getInsured());
                                            datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                            datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }else {
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        if (tempFile.contains("不是同一人-个人")){
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                            datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());

                                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                            datas.put("victimDriverHarmfulSex",caseInsurance.getVictimDriverHarmfulSex());
                                            datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerTelephone());
                                            datas.put("victimDriverHarmfulBith",caseInsurance.getVictimDriverHarmfulBith());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                            datas.put("victimDriverHarmfulTelephone",caseInsurance.getVictimDriverHarmfulTelephone());
                                            if(StringUtils.isNotBlank(caseInsurance.getSpecialAmountPaidBack())) {
                                                datas.put("specialAmountPaidBack", "-" + caseInsurance.getSpecialAmountPaidBack());
                                            }

                                            if(StringUtils.isNotBlank(caseInsurance.getVictimDriverHarmfulResponsibility())){
                                                if (caseInsurance.getVictimDriverHarmfulResponsibility().equals("全部")){
                                                    datas.put("ratioShow","100%");

                                                }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("主要")){
                                                    datas.put("ratioShow","70%");
                                                }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("次要")){
                                                    datas.put("ratioShow","30%");
                                                }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("同等")){
                                                    datas.put("ratioShow","50%");
                                                }else {
                                                    datas.put("ratioShow","0");
                                                }
                                            }
                                            datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                            datas.put("victimVehicleOwnerSex",caseInsurance.getVictimVehicleOwnerSex());
                                            datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerPeople());
                                            datas.put("victimVehicleOwnerBith",caseInsurance.getVictimVehicleOwnerBith());
                                            datas.put("victimVehicleOwnerHousehlod",caseInsurance.getVictimVehicleOwnerHousehlod());
                                            datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());

                                            datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                            datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                            datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                            datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                            datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                            datas.put("insured",caseInsurance.getInsured());
                                            datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());

                                            datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }
                        }
                    }else if (temp.contains("06")){
                        if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                        datas.put("victimVehicleOwnerIdcard",caseInsurance.getVictimVehicleOwnerIdcard());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                        datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                        datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                        datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {

                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                    datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());

                                    datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                    datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());


                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                    datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                    datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }

                            }

                        }
                    } else if(temp.contains("07")){
                        if (caseInsurance.getGrantAuthorization().equals("0")){  //一般
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("一般授权")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                        if(caseInsurance.getVictimVehicleOwner().equals(caseInsurance.getVictimDriverHarmful())){
                                            datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                        }else {
                                            datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner()+"、"+caseInsurance.getVictimDriverHarmful());
                                        }

//                                        datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
//                                        datas.put("victimDriverHarmful",caseInsurance.getInsuredVehicleDriver());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("特别授权")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                        datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                        datas.put("victimDriverHarmful",caseInsurance.getInsuredVehicleDriver());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }

                    }else if (temp.contains("08")){
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("人为同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());

                                        datas.put("insuredCommercialInsurance",caseInsurance.getInsuredCommercialInsurance());
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("不为同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());

                                        datas.put("insuredCommercialInsurance",caseInsurance.getInsuredCommercialInsurance());
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());


                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }

                        }

                    }else if (temp.contains("09") ){ // 合作律师
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();

                                datas.put("undertakingLawyerName", caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());

                                datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());
                                datas.put("victimVehicleOwnerIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                datas.put("victimVehicleOwnerHousehlod",caseInsurance.getVictimVehicleOwnerHousehlod());
                                datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());

                                datas.put("recoveryCasesAmount",caseInsurance.getRecoveryCasesAmount());
                                datas.put("investmentInquiries",caseInsurance.getCaseSpeed().getInvestmentInquiries());

                                datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());

                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());

                                datas.put("insured",caseInsurance.getInsured());
                                datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }else if (temp.contains("11")){// 查控申请书
                        if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("单位")){

                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                    }
                                }
                            }else{
                            if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        if (tempString.contains("主是同一人")){
                                            String tempFile = templetFile+File.separator+tempString;
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());


                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }else {
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        if (tempFile.contains("主不是同一人-个人")){
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                            datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                            datas.put("victimVehicleOwnerSex",caseInsurance.getVictimVehicleOwnerSex());
                                            datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerPeople());
                                            datas.put("victimVehicleOwnerBith",caseInsurance.getVictimVehicleOwnerBith());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());


                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }
                        }

                    }else if (temp.contains("12")){// 恢复执行申请书
                        if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }else {
                            if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        if (tempString.contains("主是同一人")){
                                            String tempFile = templetFile+File.separator+tempString;
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }else {
                                if (templeFiles != null && templeFiles.size()>0){
                                    for (String tempString : templeFiles) {
                                        String tempFile = templetFile+File.separator+tempString;
                                        if (tempFile.contains("主不是同一人-个人")){
                                            Map<String, Object> datas = new HashMap<String, Object>();

                                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                            datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                            datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                            datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                            datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                            datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                            datas.put("victimVehicleOwnerSex",caseInsurance.getVictimVehicleOwnerSex());
                                            datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerPeople());
                                            datas.put("victimVehicleOwnerBith",caseInsurance.getVictimVehicleOwnerBith());
                                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                            datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());


                                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                            datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                                            changeToWord(datas,tempFile,outFile);
                                        }
                                    }
                                }
                            }
                        }

                    }

                }

                String zipFilePath = Global.getUserfilesBaseDir() +File.separator+"zip"+File.separator+DateUtils.getDate()+caseInsurance.getId();
                FileUtils.createDirectory(zipFilePath);
                FileOutputStream fos1 = new FileOutputStream(new File(zipFilePath+File.separator+"代位追偿.zip"));
                ZipUtils.toZip(filePath, fos1,true);
                try {
                    String fileName = "代位追偿.zip";
                    File file = new File(zipFilePath+File.separator+"代位追偿.zip");
                    response.setCharacterEncoding("UTF-8");
                    response.setHeader("Content-Disposition",
                            "attachment; filename=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1"));
                    response.setContentLength((int) file.length());
                    response.setContentType("application/zip");// 定义输出类型
                    FileInputStream fis = new FileInputStream(file);
                    BufferedInputStream buff = new BufferedInputStream(fis);
                    byte[] b = new byte[1024];// 相当于我们的缓存
                    long k = 0;// 该值用于计算当前实际下载了多少字节
                    OutputStream myout = response.getOutputStream();// 从response对象中得到输出流,准备下载
                    // 开始循环下载
                    while (k < file.length()) {
                        int j = buff.read(b, 0, 1024);
                        k += j;
                        myout.write(b, 0, j);
                    }
                    myout.flush();
                    buff.close();
                   file.delete();
                } catch (Exception e) {
                    System.out.println(e);
                }

            }
    }



    public static void downloadZip(List<CaseInsurance>caseInsurances,String exportSourceString,HttpServletResponse response)throws Exception{
        String FileName ="批量下载"+System.currentTimeMillis();
        if (caseInsurances !=null && caseInsurances.size()>0){
            for (CaseInsurance caseInsurance:caseInsurances){
                createAllWord(caseInsurance,exportSourceString,FileName,response);
            }
        }
        String filePath = Global.getUserfilesBaseDir() +File.separator+"word"+File.separator+FileName;
        String zipFilePath = Global.getUserfilesBaseDir() +File.separator+"zip"+File.separator+DateUtils.getDate()+FileName;
        FileUtils.createDirectory(zipFilePath);
        FileOutputStream fos1 = new FileOutputStream(new File(zipFilePath+File.separator+"批量下载.zip"));
        ZipUtils.toZip(filePath, fos1,true);
        try {
            String fileName = "批量下载.zip";
            File file = new File(zipFilePath+File.separator+"批量下载.zip");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Disposition",
                    "attachment; filename=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1"));
            response.setContentLength((int) file.length());
            response.setContentType("application/zip");// 定义输出类型
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream buff = new BufferedInputStream(fis);
            byte[] b = new byte[1024];// 相当于我们的缓存
            long k = 0;// 该值用于计算当前实际下载了多少字节
            OutputStream myout = response.getOutputStream();// 从response对象中得到输出流,准备下载
            // 开始循环下载
            while (k < file.length()) {
                int j = buff.read(b, 0, 1024);
                k += j;
                myout.write(b, 0, j);
            }
            myout.flush();
            buff.close();
            file.delete();
        } catch (Exception e) {
            System.out.println(e);
        }



    }


    public static void  createAllWord(CaseInsurance caseInsurance,String exportSourceString,String FileName,HttpServletResponse response)throws Exception{

        String [] exportSources = exportSourceString.split(",");
        String filePath = Global.getUserfilesBaseDir() +File.separator+"word"+File.separator+FileName+File.separator+caseInsurance.getCaseCustomer().getCreditor()+"-"+caseInsurance.getInsured();

//        String filePath = Global.getUserfilesBaseDir() +File.separator+"word"+File.separator+FileName+File.separator+System.currentTimeMillis()+File.separator+caseInsurance.getCaseCustomer().getCreditor()+"-"+caseInsurance.getInsured();
        FileUtils.createDirectory(filePath);







        if (caseInsurance.getCaseType().equals("0")){//追偿
            for (String temp : exportSources) {
                FileUtils.createDirectory(filePath + File.separator + temp);
                String templetFile = Global.getTempletFilesBaseDir()+File.separator+"insurance"+ File.separator +temp;
                List<String> templeFiles = FileUtils.findChildrenList(new File(templetFile),false);

                if (temp.contains("01")){
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("insuredContactTelephone", caseInsurance.getInsuredContactTelephone());
                            datas.put("insuredVehicleDriverTelephone", caseInsurance.getInsuredVehicleDriverTelephone());
                            datas.put("insuranceClaimsContractors", caseInsurance.getInsuranceClaimsContractors());
                            datas.put("insuranceClaimsTelephone", caseInsurance.getInsuranceClaimsTelephone());
                            datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                            datas.put("caseIntroduction",caseInsurance.getCaseIntroduction());
                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if (temp.contains("02") ){ // 电话短信话书
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                            datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                            datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                            datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());

                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if ( temp.contains("03") ){ // 电话短信话书 ----律师函
                    if (caseInsurance.getLayerNumber()==0){
                        Integer layerNumber =  caseInsuranceDao.getMax();
                        if (layerNumber !=0){
                            String yearString = layerNumber.toString().substring(0,4);
                            if(yearString.equals(DateUtils.getYear())){
                                layerNumber = layerNumber +1;
                            }else {
                                layerNumber = Integer.valueOf(DateUtils.getYear()+"00001");
                            }
                        }else {
                            layerNumber = Integer.valueOf(DateUtils.getYear()+"00001");
                        }
                        caseInsurance.setLayerNumber(layerNumber);
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                            caseInsurance.setLayerNumberOne("-1");
                        }else {
                            caseInsurance.setLayerNumberOne("-1");
                            caseInsurance.setLayerNumberTow("-2");
                        }
                        caseInsuranceDao.updateLayerNumber(caseInsurance);
                    }


                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            if (caseInsurance.getInsured()!=null&&caseInsurance.getInsuredVehicleDriver()!=null &&caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                if (tempString.contains("为一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));
                                    datas.put("type","-1");
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());
                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                    String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsuredVehicleDriver()+"-"+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }else {
                                if (tempString.contains("-车主")|| tempString.contains("-驾驶员")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));

                                    if (tempString.contains("车主")){
                                        datas.put("type","-2");
                                    }else {
                                        datas.put("type","-1");
                                    }
                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());

                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());

                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());

                                    if (tempString.contains("车主")){
                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsured()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);

                                    }else {
                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsuredVehicleDriver()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }



                                }
                            }



                        }
                    }
                }


                else if (temp.contains("05")){// 民事起诉状
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                    datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                    datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                    datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                    datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());
                                    datas.put("insuredContactTelephone",caseInsurance.getInsuredContactTelephone());

                                    datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("为一人-个人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());




                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                        datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                        datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                        datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("不是同一人-个人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                        datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                        datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                        datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredSex",caseInsurance.getInsuredSex());
                                        datas.put("insuredPeople",caseInsurance.getInsuredPeople());
                                        datas.put("insuredBith",caseInsurance.getInsuredBith());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                        datas.put("insuredContactTelephone",caseInsurance.getInsuredContactTelephone());

                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());



                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }else if (temp.contains("06")){
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                    datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                    datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {

                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                //车主
                                datas.put("insured",caseInsurance.getInsured());
                                datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                //驾驶员
                                datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }

                        }

                    }
                } else if(temp.contains("07")){
                    if (caseInsurance.getGrantAuthorization().equals("0")){  //一般
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("一般授权")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                        datas.put("insured", caseInsurance.getInsured());
                                    }else {
                                        datas.put("insured", caseInsurance.getInsured()+"、"+caseInsurance.getInsuredVehicleDriver());
                                    }

//                                        datas.put("insured", caseInsurance.getVictimVehicleOwner());
//                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("特别授权")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

//                                        datas.put("insured", caseInsurance.getVictimVehicleOwner());
//                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                        datas.put("insured", caseInsurance.getInsured());
                                    }else {
                                        datas.put("insured", caseInsurance.getInsured()+"、"+caseInsurance.getInsuredVehicleDriver());
                                    }

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }

                }else if (temp.contains("08")){
                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (tempFile.contains("人为同一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("reasonsRecourse" ,caseInsurance.getReasonsRecourse());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (tempFile.contains("不为同一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("reasonsRecourse" ,caseInsurance.getReasonsRecourse());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }

                    }

                }else if (temp.contains("09") ){ // 合作律师
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("undertakingLawyerName", caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());

                            datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                            datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                            datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                            datas.put("recoveryCasesAmount",caseInsurance.getRecoveryCasesAmount());
                            datas.put("investmentInquiries",caseInsurance.getCaseSpeed().getInvestmentInquiries());
                            datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());


                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                            datas.put("reasonsRecourse",caseInsurance.getReasonsRecourse());
                            datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                            datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                            datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日"));
                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                            datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());
                            datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if (temp.contains("11")){// 查控申请书
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());

                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("主是同一人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());



                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("主不是同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }else if (temp.contains("12")){// 恢复执行申请书
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());

                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());

                                    datas.put("executionNum",caseInsurance.getCaseSpeed().getExecutionNum());
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("主是同一人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("主不是同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());



                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());


                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }

                if (temp.contains("01")){
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("insuredContactTelephone", caseInsurance.getInsuredContactTelephone());
                            datas.put("insuredVehicleDriverTelephone", caseInsurance.getInsuredVehicleDriverTelephone());
                            datas.put("insuranceClaimsContractors", caseInsurance.getInsuranceClaimsContractors());
                            datas.put("insuranceClaimsTelephone", caseInsurance.getInsuranceClaimsTelephone());
                            datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                            datas.put("caseIntroduction",caseInsurance.getCaseIntroduction());
                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if (temp.contains("02") ){ // 电话短信话书
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                            datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                            datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                            datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());

                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if ( temp.contains("03") ){ // 电话短信话书 ----律师函

                    if (caseInsurance.getLayerNumber()==0){
                        Integer layerNumber =  caseInsuranceDao.getMax();
                        if (layerNumber !=0){
                            String yearString = layerNumber.toString().substring(0,4);
                            if(yearString.equals(DateUtils.getYear())){
                                layerNumber = layerNumber +1;
                            }else {
                                layerNumber = Integer.valueOf(DateUtils.getYear()+"00001");
                            }
                        }else {
                            layerNumber = Integer.valueOf(DateUtils.getYear()+"00001");
                        }
                        caseInsurance.setLayerNumber(layerNumber);
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                            caseInsurance.setLayerNumberOne("-1");
                        }else {
                            caseInsurance.setLayerNumberOne("-1");
                            caseInsurance.setLayerNumberTow("-2");
                        }
                        caseInsuranceDao.updateLayerNumber(caseInsurance);
                    }


                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            if (caseInsurance.getInsured()!=null&&caseInsurance.getInsuredVehicleDriver()!=null &&caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                if (tempString.contains("为一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));
                                    datas.put("type","-1");
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());


                                    String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsuredVehicleDriver()+"-"+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }else {
                                if (tempString.contains("-车主")|| tempString.contains("-驾驶员")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));

                                    if (tempString.contains("车主")){
                                        datas.put("type","-2");
                                    }else {
                                        datas.put("type","-1");
                                    }
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));
                                    datas.put("type","-1");
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());

                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());

                                    if (tempString.contains("车主")){
                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsured()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);

                                    }else {
                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getInsuredVehicleDriver()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }

//                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
//                                    changeToWord(datas,tempFile,outFile);

                                }
                            }



                        }
                    }


                }




                else if (temp.contains("05")){// 民事起诉状
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                    datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                    datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                    datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                    datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());
                                    datas.put("insuredContactTelephone",caseInsurance.getInsuredContactTelephone());

                                    datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("为一人-个人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();


                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());




                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                        datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                        datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                        datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("不是同一人-个人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();


                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverSex",caseInsurance.getInsuredVehicleDriverSex());
                                        datas.put("insuredVehicleDriverPeople",caseInsurance.getInsuredVehicleDriverPeople());
                                        datas.put("insuredVehicleDriverBith",caseInsurance.getInsuredVehicleDriverBith());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());
                                        datas.put("insuredVehicleDriverTelephone",caseInsurance.getInsuredVehicleDriverTelephone());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredSex",caseInsurance.getInsuredSex());
                                        datas.put("insuredPeople",caseInsurance.getInsuredPeople());
                                        datas.put("insuredBith",caseInsurance.getInsuredBith());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                        datas.put("insuredContactTelephone",caseInsurance.getInsuredContactTelephone());

                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                        datas.put("recoveryCaseAmount",caseInsurance.getRecoveryCasesAmount());

                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                        datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));



                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }else if (temp.contains("06")){
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                    datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                    datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {

                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                //车主
                                datas.put("insured",caseInsurance.getInsured());
                                datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                                //驾驶员
                                datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                                datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }

                        }

                    }
                } else if(temp.contains("07")){
                    if (caseInsurance.getGrantAuthorization().equals("0")){  //一般
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("一般授权")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                        datas.put("insured", caseInsurance.getInsured());
                                    }else {
                                        datas.put("insured", caseInsurance.getInsured()+"、"+caseInsurance.getInsuredVehicleDriver());
                                    }
//                                    datas.put("insured", caseInsurance.getVictimVehicleOwner());
//                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("特别授权")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                                        datas.put("insured", caseInsurance.getInsured());
                                    }else {
                                        datas.put("insured", caseInsurance.getInsured()+"、"+caseInsurance.getInsuredVehicleDriver());
                                    }
//                                    datas.put("insured", caseInsurance.getVictimVehicleOwner());
//                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }

                }else if (temp.contains("08")){
                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (tempFile.contains("人为同一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("reasonsRecourse" ,caseInsurance.getReasonsRecourse());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (tempFile.contains("不为同一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("reasonsRecourse" ,caseInsurance.getReasonsRecourse());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                                    datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());
                                    datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }

                    }

                }else if (temp.contains("09") ){ // 合作律师
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("undertakingLawyerName", caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());

                            datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());
                            datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                            datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                            datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                            datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                            datas.put("recoveryCasesAmount",caseInsurance.getRecoveryCasesAmount());
                            datas.put("investmentInquiries",caseInsurance.getCaseSpeed().getInvestmentInquiries());

                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                            datas.put("reasonsRecourse",caseInsurance.getReasonsRecourse());
                            datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                            datas.put("vehicleDriverInsuredParty",caseInsurance.getVehicleDriverInsuredParty());
                            datas.put("vehicleDriverInsuredLicense",caseInsurance.getVehicleDriverInsuredLicense());
                            datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));
                            datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());

                            datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日"));
                            datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                            datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                            datas.put("vehicleInsuredVictim",caseInsurance.getVehicleInsuredVictim());
                            datas.put("judgmentAmount",caseInsurance.getJudgmentAmount());

                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if (temp.contains("11")){// 查控申请书
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());

                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("主是同一人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());



                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("主不是同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }else if (temp.contains("12")){// 恢复执行申请书
                    if (caseInsurance.getIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                    datas.put("insuredIdcard",caseInsurance.getInsuredIdcard());
                                    datas.put("insuredPersonCharge",caseInsurance.getInsuredPersonCharge());
                                    datas.put("insuredPersonChargePosition",caseInsurance.getInsuredPersonChargePosition());

                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                    datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());

                                    datas.put("executionNum",caseInsurance.getCaseSpeed().getExecutionNum());
                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("主是同一人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("主不是同一人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("insuredDomicile",caseInsurance.getInsuredDomicile());
                                        datas.put("insuredIdNumber",caseInsurance.getInsuredIdNumber());



                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredVehicleDriverHousehlod",caseInsurance.getInsuredVehicleDriverHousehlod());
                                        datas.put("insuredVehicleDriverNumber",caseInsurance.getInsuredVehicleDriverNumber());

                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("unexecutedContent",caseInsurance.getCaseSpeed().getUnexecutedContent());


                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }

            }



        }else if(caseInsurance.getCaseType().equals("1")){//代位

            for (String temp : exportSources) {
                FileUtils.createDirectory(filePath + File.separator + temp);
                String templetFile = Global.getTempletFilesBaseDir()+File.separator+"subrogation"+ File.separator +temp;
                List<String> templeFiles = FileUtils.findChildrenList(new File(templetFile),false);
                if (temp.contains("01")){
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("victimVehicleOwer",caseInsurance.getVictimVehicleOwner());
                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                            datas.put("insured", caseInsurance.getInsured());
                            datas.put("insuredContactTelephone", caseInsurance.getInsuredContactTelephone());
                            datas.put("victimVehicleOwnerTelephone",caseInsurance.getVictimVehicleOwnerTelephone());
                            datas.put("injuriousInsuranceCompany",caseInsurance.getInjuriousInsuranceCompany());
                            datas.put("injuriousCompanyTelephone",caseInsurance.getInsuredContactTelephone());
                            datas.put("insuranceClaimsContractors", caseInsurance.getInsuranceClaimsContractors());
                            datas.put("insuranceClaimsTelephone", caseInsurance.getInsuranceClaimsTelephone());
                            datas.put("caseIntroduction",caseInsurance.getCaseIntroduction());
                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if (temp.contains("02")){ // 电话短信话书 ----律师函
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();
                            datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                            datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                            datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                            datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                            datas.put("victimDriverHarmful", caseInsurance.getVictimDriverHarmful());
                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }


                else if (temp.contains("03") ){ // 电话短信话书 ----律师函

                    // 电话短信话书 ----律师函
                    if (templeFiles != null && templeFiles.size()>0){
                        if (caseInsurance.getLayerNumber() ==0) {
                            Integer layerNumber = caseInsuranceDao.getMax();
                            if (layerNumber != 0) {
                                String yearString = layerNumber.toString().substring(0, 4);
                                if (yearString.equals(DateUtils.getYear())) {
                                    layerNumber = layerNumber + 1;
                                } else {
                                    layerNumber = Integer.valueOf(DateUtils.getYear() + "00001");
                                }
                            } else {
                                layerNumber = Integer.valueOf(DateUtils.getYear() + "00001");
                            }
                            caseInsurance.setLayerNumber(layerNumber);
                            if (caseInsurance.getVictimVehicleOwner().equals(caseInsurance.getVictimDriverHarmful())){
                                caseInsurance.setLayerNumberOne("-1");
                            }else {
                                caseInsurance.setLayerNumberOne("-1");
                                caseInsurance.setLayerNumberTow("-2");
                            }
                            caseInsuranceDao.updateLayerNumber(caseInsurance);
                        }

                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            if (caseInsurance.getVictimVehicleOwner()!=null &&caseInsurance.getVictimDriverHarmful()!=null && caseInsurance.getVictimVehicleOwner().equals(caseInsurance.getVictimDriverHarmful())){
                                if (tempString.contains("车主为一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));
                                    datas.put("type","-1");
                                    datas.put("victimDriverHarmful", caseInsurance.getVictimDriverHarmful());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                    datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());
                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());
                                    String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getVictimDriverHarmful()+"-"+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }else {
                                if (tempString.contains("-车主")||tempString.contains("-驾驶员")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("year",Integer.toString(caseInsurance.getLayerNumber()).substring(0,4));
                                    datas.put("num",Integer.toString(caseInsurance.getLayerNumber()).substring(4,9));

                                    if (tempString.contains("车主")){
                                        datas.put("type","-2");
                                    }else {
                                        datas.put("type","-1");
                                    }
                                    datas.put("victimDriverHarmful", caseInsurance.getVictimDriverHarmful());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver", caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                    datas.put("insured", caseInsurance.getInsured());
                                    datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());


                                    datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                    datas.put("undertakingLawyerName",caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("undertakingLawyerPhone",caseInsurance.getCaseCustomer().getUndertakingLawyerPhone());

//                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
//                                    changeToWord(datas,tempFile,outFile);
                                    datas.put("month",DateUtils.getMonth());
                                    datas.put("day",DateUtils.getDay());

                                    if (tempString.contains("车主")){

                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getVictimVehicleOwner()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }else {
                                        String outFile = filePath + File.separator + temp+File.separator+caseInsurance.getVictimDriverHarmful()+"-"+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }



                        }
                    }

                }



                else if (temp.contains("05")){// 民事起诉状
                    if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                    datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                    datas.put("victimDriverHarmfulSex",caseInsurance.getVictimDriverHarmfulSex());
                                    datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerTelephone());
                                    datas.put("victimDriverHarmfulBith",caseInsurance.getVictimDriverHarmfulBith());
                                    datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                    datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                    datas.put("victimDriverHarmfulTelephone",caseInsurance.getVictimDriverHarmfulTelephone());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                    if(StringUtils.isNotBlank(caseInsurance.getSpecialAmountPaidBack())) {
                                        datas.put("specialAmountPaidBack", "-" + caseInsurance.getSpecialAmountPaidBack());
                                    }
                                    datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                    datas.put("victimVehicleOwnerHousehlod",caseInsurance.getVictimVehicleOwnerHousehlod());
                                    datas.put("victimVehicleOwnerIdcard",caseInsurance.getVictimVehicleOwnerIdcard());
                                    datas.put("victimVehicleOwnerResponsiblity",caseInsurance.getVictimVehicleOwnerResponsiblity());
                                    datas.put("victimVehicleOwnerDuty",caseInsurance.getVictimVehicleOwnerDuty());
                                    datas.put("victimVehicleOwnerTelephone",caseInsurance.getVictimVehicleOwnerTelephone());

                                    if(StringUtils.isNotBlank(caseInsurance.getVictimDriverHarmfulResponsibility())){
                                        if (caseInsurance.getVictimDriverHarmfulResponsibility().equals("全部")){
                                            datas.put("ratioShow","100%");

                                        }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("主要")){
                                            datas.put("ratioShow","70%");
                                        }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("次要")){
                                            datas.put("ratioShow","30%");
                                        }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("同等")){
                                            datas.put("ratioShow","50%");
                                        }else {
                                            datas.put("ratioShow","0");
                                        }
                                    }


                                    datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                    datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                    datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("为一人-个人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulSex",caseInsurance.getVictimDriverHarmfulSex());
                                        datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerTelephone());
                                        datas.put("victimDriverHarmfulBith",caseInsurance.getVictimDriverHarmfulBith());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                        datas.put("victimDriverHarmfulTelephone",caseInsurance.getVictimDriverHarmfulTelephone());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                        if(StringUtils.isNotBlank(caseInsurance.getVictimDriverHarmfulResponsibility())){
                                            if (caseInsurance.getVictimDriverHarmfulResponsibility().equals("全部")){
                                                datas.put("ratioShow","100%");

                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("主要")){
                                                datas.put("ratioShow","70%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("次要")){
                                                datas.put("ratioShow","30%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("同等")){
                                                datas.put("ratioShow","50%");
                                            }else {
                                                datas.put("ratioShow","0");
                                            }
                                        }
                                        datas.put("insuredVehicleDriverLiabilty",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                        if(StringUtils.isNotBlank(caseInsurance.getSpecialAmountPaidBack())) {
                                            datas.put("specialAmountPaidBack", "-" + caseInsurance.getSpecialAmountPaidBack());
                                        }

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("不是同一人-个人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();


                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
                                        datas.put("attorneyLitigationTelphone",caseInsurance.getCaseSpeed().getAttorneyLitigationTelphone());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulSex",caseInsurance.getVictimDriverHarmfulSex());
                                        datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerTelephone());
                                        datas.put("victimDriverHarmfulBith",caseInsurance.getVictimDriverHarmfulBith());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                                        datas.put("victimDriverHarmfulTelephone",caseInsurance.getVictimDriverHarmfulTelephone());
                                        if(StringUtils.isNotBlank(caseInsurance.getSpecialAmountPaidBack())) {
                                            datas.put("specialAmountPaidBack", "-" + caseInsurance.getSpecialAmountPaidBack());
                                        }

                                        if(StringUtils.isNotBlank(caseInsurance.getVictimDriverHarmfulResponsibility())){
                                            if (caseInsurance.getVictimDriverHarmfulResponsibility().equals("全部")){
                                                datas.put("ratioShow","100%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("主要")){
                                                datas.put("ratioShow","70%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("次要")){
                                                datas.put("ratioShow","30%");
                                            }else if(caseInsurance.getVictimDriverHarmfulResponsibility().equals("同等")){
                                                datas.put("ratioShow","50%");
                                            }else {
                                                datas.put("ratioShow","0");
                                            }
                                        }
                                        datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                        datas.put("victimVehicleOwnerSex",caseInsurance.getVictimVehicleOwnerSex());
                                        datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerPeople());
                                        datas.put("victimVehicleOwnerBith",caseInsurance.getVictimVehicleOwnerBith());
                                        datas.put("victimVehicleOwnerHousehlod",caseInsurance.getVictimVehicleOwnerHousehlod());
                                        datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());

                                        datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                                        datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                        datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                        datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                        datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                        datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());
                                        datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());
                                        datas.put("insured",caseInsurance.getInsured());
                                        datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());
                                        datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));


                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }
                }else if (temp.contains("06")){
                    if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                    datas.put("victimVehicleOwnerIdcard",caseInsurance.getVictimVehicleOwnerIdcard());

                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                    datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                    datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                    datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {

                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();
                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());

                                datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());


                                datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                datas.put("creditorAmount" ,caseInsurance.getCreditorAmount());
                                datas.put("accountNameInformation",caseInsurance.getCaseCustomer().getAccountNameInformation());
                                datas.put("openingBankInformation",caseInsurance.getCaseCustomer().getOpeningBankInformation());
                                datas.put("accountInformation",caseInsurance.getCaseCustomer().getAccountInformation());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }

                        }

                    }
                } else if(temp.contains("07")){
                    if (caseInsurance.getGrantAuthorization().equals("0")){  //一般
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("一般授权")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                    if(caseInsurance.getVictimVehicleOwner().equals(caseInsurance.getVictimDriverHarmful())){
                                        datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                    }else {
                                        datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner()+"、"+caseInsurance.getVictimDriverHarmful());
                                    }
//                                    datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
//                                    datas.put("victimDriverHarmful",caseInsurance.getInsuredVehicleDriver());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("特别授权")){
                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());

                                    datas.put("victimVehicleOwner", caseInsurance.getVictimVehicleOwner());
                                    datas.put("victimDriverHarmful",caseInsurance.getInsuredVehicleDriver());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }

                }else if (temp.contains("08")){
                    if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (tempFile.contains("人为同一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());

                                    datas.put("insuredCommercialInsurance",caseInsurance.getInsuredCommercialInsurance());
                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else {
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                if (tempFile.contains("不为同一人")){
                                    Map<String, Object> datas = new HashMap<String, Object>();
                                    datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                                    datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                                    datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                                    datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                                    datas.put("insuredVehicleDriverLiability",DictUtils.getDictLabel(caseInsurance.getInsuredVehicleDriverLiability(),"driver_liability",""));

                                    datas.put("victimDriverHarmfulResponsibility",caseInsurance.getVictimDriverHarmfulResponsibility());

                                    datas.put("insuredCommercialInsurance",caseInsurance.getInsuredCommercialInsurance());
                                    datas.put("insured",caseInsurance.getInsured());
                                    datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());


                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }

                    }

                }else if (temp.contains("09") ){ // 合作律师
                    if (templeFiles != null && templeFiles.size()>0){
                        for (String tempString : templeFiles) {
                            String tempFile = templetFile+File.separator+tempString;
                            Map<String, Object> datas = new HashMap<String, Object>();

                            datas.put("undertakingLawyerName", caseInsurance.getCaseCustomer().getUndertakingLawyerName());
                            datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                            datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                            datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());

                            datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());
                            datas.put("victimVehicleOwnerIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
                            datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                            datas.put("victimVehicleOwnerHousehlod",caseInsurance.getVictimVehicleOwnerHousehlod());
                            datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());

                            datas.put("recoveryAmount",caseInsurance.getRecoveryAmount());
                            datas.put("investmentInquiries",caseInsurance.getCaseSpeed().getInvestmentInquiries());


                            datas.put("accidentDate", DateUtils.formatDate(caseInsurance.getAccidentDate(),"yyyy年MM月dd日") );
                            datas.put("insuredVehicleDriver",caseInsurance.getInsuredVehicleDriver());
                            datas.put("insuredLicenseNumber",caseInsurance.getInsuredLicenseNumber());
                            datas.put("victimVehicleOwnerLicense",caseInsurance.getVictimVehicleOwnerLicense());
                            datas.put("vehicleDriverInsuredResponsibility",caseInsurance.getVehicleDriverInsuredResponsibility());

                            datas.put("insured",caseInsurance.getInsured());
                            datas.put("claimAmountInsuranceCompany",caseInsurance.getClaimAmountInsuranceCompany());

                            String outFile = filePath + File.separator + temp+File.separator+tempString;
                            changeToWord(datas,tempFile,outFile);
                        }
                    }
                }else if (temp.contains("11")){// 查控申请书

                    if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                if (tempString.contains("单位")){

                                    String tempFile = templetFile+File.separator+tempString;
                                    Map<String, Object> datas = new HashMap<String, Object>();

                                    datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                    datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                    datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                    datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                    datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                    datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                    datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                    datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                    datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                    datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                    datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                    datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                                    changeToWord(datas,tempFile,outFile);
                                }
                            }
                        }
                    }else{
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("主是同一人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());


                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("主不是同一人-个人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                        datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                        datas.put("victimVehicleOwnerSex",caseInsurance.getVictimVehicleOwnerSex());
                                        datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerPeople());
                                        datas.put("victimVehicleOwnerBith",caseInsurance.getVictimVehicleOwnerBith());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());


                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }

                }else if (temp.contains("12")){// 恢复执行申请书
//                    if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
//                        if (templeFiles != null && templeFiles.size()>0){
//                            for (String tempString : templeFiles) {
//                                String tempFile = templetFile+File.separator+tempString;
//                                Map<String, Object> datas = new HashMap<String, Object>();
//
//                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
//                                datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
//                                datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
//                                datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
//                                datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());
//
//                                datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
//                                datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
//                                datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());
//
//                                datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
//                                datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
//                                datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
//                                datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());
//
//                                String outFile = filePath + File.separator + temp+File.separator+tempString;
//                                changeToWord(datas,tempFile,outFile);
//                            }
//                        }
//                    }

                    if (caseInsurance.getVictimVehicleOwenrIsCompany().equals("0")){ //单位
                        if (templeFiles != null && templeFiles.size()>0){
                            for (String tempString : templeFiles) {
                                String tempFile = templetFile+File.separator+tempString;
                                Map<String, Object> datas = new HashMap<String, Object>();

                                datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                String outFile = filePath + File.separator + temp+File.separator+tempString;
                                changeToWord(datas,tempFile,outFile);
                            }
                        }
                    }else {
                        if (caseInsurance.getInsured().equals(caseInsurance.getInsuredVehicleDriver())){//同一人
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    if (tempString.contains("主是同一人")){
                                        String tempFile = templetFile+File.separator+tempString;
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }else {
                            if (templeFiles != null && templeFiles.size()>0){
                                for (String tempString : templeFiles) {
                                    String tempFile = templetFile+File.separator+tempString;
                                    if (tempFile.contains("主不是同一人-个人")){
                                        Map<String, Object> datas = new HashMap<String, Object>();

                                        datas.put("creditor", caseInsurance.getCaseCustomer().getCreditor());
                                        datas.put("creditorDomicile",caseInsurance.getCaseCustomer().getCreditorDomicile());
                                        datas.put("creditorIdcart",caseInsurance.getCaseCustomer().getCreditorIdcart());
                                        datas.put("creditorCharge",caseInsurance.getCaseCustomer().getCreditorCharge());
                                        datas.put("creditorChargePositon",caseInsurance.getCaseCustomer().getCreditorChargePositon());

                                        datas.put("victimDriverHarmful",caseInsurance.getVictimDriverHarmful());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimDriverHarmfulIdcard",caseInsurance.getVictimDriverHarmfulIdcard());

                                        datas.put("victimVehicleOwner",caseInsurance.getVictimVehicleOwner());
                                        datas.put("victimVehicleOwnerSex",caseInsurance.getVictimVehicleOwnerSex());
                                        datas.put("victimVehicleOwnerPeople",caseInsurance.getVictimVehicleOwnerPeople());
                                        datas.put("victimVehicleOwnerBith",caseInsurance.getVictimVehicleOwnerBith());
                                        datas.put("victimDriverHarmfulHousehlod",caseInsurance.getVictimDriverHarmfulHousehlod());
                                        datas.put("victimVehicleOwnerNumber",caseInsurance.getVictimVehicleOwnerNumber());


                                        datas.put("courtOfJudgment",caseInsurance.getCourtOfJudgment());
                                        datas.put("judgmentNumber",caseInsurance.getJudgmentNumber());
                                        datas.put("accidentJudgmentDate",DateUtils.formatDate(caseInsurance.getAccidentJudgmentDate(),"yyyy年MM月dd日") );
                                        datas.put("courtOfExecution",caseInsurance.getCaseSpeed().getCourtOfExecution());

                                        String outFile = filePath + File.separator + temp+File.separator+tempString;
                                        changeToWord(datas,tempFile,outFile);
                                    }
                                }
                            }
                        }
                    }

                }

            }
        }
    }

    public static void downloadZip(ZqwCase zqwCase,HttpServletResponse response)throws Exception {
        String [] insuranceRecourses = zqwCase.getInsuranceSubrogation().split(",");
        String [] insuranceSubrogations = zqwCase.getInsuranceSubrogation().split(",");
        //保险代位求偿
        String filePath = Global.getUserfilesBaseDir() +File.separator+"word"+File.separator+DateUtils.getDate()+zqwCase.getId() +File.separator+"subrogation";
        FileUtils.createDirectory(filePath);
        //创建目录及生成文件
        for (String temp:insuranceRecourses) {
            FileUtils.createDirectory(filePath + File.separator + temp);
            String templetFile = Global.getTempletFilesBaseDir()+File.separator+"subrogation"+ File.separator +temp;
            List<String> templeFiles = FileUtils.findChildrenList(new File(templetFile),false);
            if (templeFiles != null && templeFiles.size()>0){
                for (String tempString : templeFiles) {
                    String tempFile = templetFile+File.separator+tempString;
                    Map<String, Object> datas = new HashMap<String, Object>();
                    datas.put("creditor", zqwCase.getNameOfInfringer());
                    datas.put("obligor", "程根生");
                    datas.put("caseType", "保险代位求偿");
                    datas.put("insured", zqwCase.getInsured());
                    datas.put("insuredPhone", "18579132585");
                    datas.put("dirver", zqwCase.getDriver());
                    datas.put("dirverPhone", "18579132585");
                    String outFile = filePath + File.separator + temp+File.separator+tempString;
                    changeToWord(datas,tempFile,outFile);
                }

            }
        }//filePath+File.separator+
        String zipFilePath = Global.getUserfilesBaseDir() +File.separator+"zip"+File.separator+DateUtils.getDate()+zqwCase.getId();
        FileUtils.createDirectory(zipFilePath);
        FileUtils.zipFiles(filePath,"*",zipFilePath+File.separator+"insuranceRecourses.zip");
        try {
            String fileName = "insuranceRecourses.zip";
            File file = new File(zipFilePath+File.separator+"insuranceRecourses.zip");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Disposition",
                    "attachment; filename=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1"));
            response.setContentLength((int) file.length());
            response.setContentType("application/zip");// 定义输出类型
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream buff = new BufferedInputStream(fis);
            byte[] b = new byte[1024];// 相当于我们的缓存
            long k = 0;// 该值用于计算当前实际下载了多少字节
            OutputStream myout = response.getOutputStream();// 从response对象中得到输出流,准备下载
            // 开始循环下载
            while (k < file.length()) {
                int j = buff.read(b, 0, 1024);
                k += j;
                myout.write(b, 0, j);
            }
            myout.flush();
            buff.close();
            file.delete();
        } catch (Exception e) {
            System.out.println(e);
        }
        //保险追偿
    }

    @SuppressWarnings("serial")
    public static void changeToWord(Map<String ,Object> datas, String templateFile,String outFile ) throws Exception {
        //"src/test/resources/lawyer/subrogation/01/caseContrnt.docx"
        XWPFTemplate template = XWPFTemplate.compile(templateFile).render(datas);;
        FileOutputStream out = new FileOutputStream(outFile);
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }


    public static  void main(String [] args){

    String filePath = Global.getProjectPath()+File.separator +"webapp"+File.separator+"userfiles" +File.separator+"word"+File.separator+"subrogation";
    System.out.println(filePath);

    }



}