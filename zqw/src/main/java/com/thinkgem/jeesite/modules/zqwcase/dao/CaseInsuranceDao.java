/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseInsurance;

/**
 * 案件信息DAO接口
 * @author coder_cheng@126.com
 * @version 2019-07-04
 */
@MyBatisDao
public interface CaseInsuranceDao extends CrudDao<CaseInsurance> {

    public Integer getMax();

    public void updateLayerNumber(CaseInsurance caseInsurance);

    public List<CaseInsurance> findNumList(CaseInsurance caseInsurance);

    public List<CaseInsurance> findTypeList(CaseInsurance caseInsurance);
	
}