/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.dao;

import java.util.List;
import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqw.entity.DeptAction;

/**
 * 债行动DAO接口
 * @author coder_cheng@126.com
 * @version 2018-10-20
 */
@MyBatisDao
public interface DeptActionDao extends CrudDao<DeptAction> {

  List<DeptAction> findListLimit();

}