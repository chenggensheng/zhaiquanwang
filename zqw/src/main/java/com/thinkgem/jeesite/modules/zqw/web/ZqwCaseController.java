/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.zqw.utils.WordUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCase;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCaseService;

/**
 * 案件管理Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCase")
public class ZqwCaseController extends BaseController {

	@Autowired
	private ZqwCaseService zqwCaseService;
	
	@ModelAttribute
	public ZqwCase get(@RequestParam(required=false) String id) {
		ZqwCase entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCaseService.get(id);
		}
		if (entity == null){
			entity = new ZqwCase();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCase:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCase zqwCase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCase> page = zqwCaseService.findPage(new Page<ZqwCase>(request, response), zqwCase); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCaseList";
	}

	@RequiresPermissions("zqw:zqwCase:view")
	@RequestMapping(value = "form")
	public String form(ZqwCase zqwCase, Model model) {
		model.addAttribute("zqwCase", zqwCase);
		return "modules/zqw/zqwCaseForm";
	}

	@RequiresPermissions("zqw:zqwCase:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCase zqwCase, Model model, RedirectAttributes redirectAttributes,HttpServletResponse response) {
		if (!beanValidator(model, zqwCase)){
			return form(zqwCase, model);
		}
		zqwCaseService.save(zqwCase);
		try {
			WordUtils.downloadZip(zqwCase,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		addMessage(redirectAttributes, "保存案件管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCase/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCase:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCase zqwCase, RedirectAttributes redirectAttributes) {
		zqwCaseService.delete(zqwCase);
		addMessage(redirectAttributes, "删除案件管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCase/?repage";
	}

}