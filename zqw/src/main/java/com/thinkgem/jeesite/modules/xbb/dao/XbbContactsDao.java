/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbContacts;

/**
 * 联系人管理DAO接口
 * @author coder_cheng@126.com
 * @version 2018-08-31
 */
@MyBatisDao
public interface XbbContactsDao extends CrudDao<XbbContacts> {
	
}