/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.google.common.collect.Lists;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

/**
 * 内部邮件Entity
 * @author Huang
 * @version 2018-04-27
 */
public class OaMail extends DataEntity<OaMail> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 标题
	private String tag;		// 发送选项
	private String content;		// 邮件正文
	private String attach;		// 附件
	private String status;		// 状态
	private Date sendDate;		// 发送时间

	private String link;		// 外部链接
	private String sourceMail;		// 原始邮件
	
	private String preMail;          // 上一封邮件

	private OaMailPerson mailPerson = new OaMailPerson();
	
	private List<Dict> tagList = Lists.newArrayList(); // 标签集合

	private User toUser  = new User();
	private User ccUser  = new User();
	private User bccUser = new User();
	
	// 非po字段
	private String sendDateString;		// 发送时间
	
	private String pageFlag;
	
	private String pageTitle;

	public OaMail() {
		super();
	}

	public OaMail(String id){
		super(id);
	}
	public List<String> getTagidList() {
		List<String> list = Lists.newArrayList();
		if (tag != null){
			for (String s : StringUtils.split(tag, ",")) {
				list.add(s);
			}
		}
		return list;
	}
	public void setTagidList(List<String> list) {
		tag = StringUtils.join(list, ",");
	}
	public List<Dict> getTagList() {
		this.tagList = Lists.newArrayList();
		tagList = DictUtils.getDictListByValue(this.tag,"oa_mail_tag");
		return tagList;
	}

	public void setTagList(List<Dict> tagList) {
		this.tagList = tagList;
		tag = StringUtils.join(tagList, ",");
	}
	public OaMailPerson getMailPerson() {
		return mailPerson;
	}

	public User getToUser() {
		return toUser;
	}

	public void setToUser(User toUser) {
		this.toUser = toUser;
	}

	public User getCcUser() {
		return ccUser;
	}

	public void setCcUser(User ccUser) {
		this.ccUser = ccUser;
	}

	public User getBccUser() {
		return bccUser;
	}

	public void setBccUser(User bccUser) {
		this.bccUser = bccUser;
	}

	public void setMailPerson(OaMailPerson mailPerson) {
		this.mailPerson = mailPerson;
	}
	@Length(min=1, max=255, message="标题长度必须介于 1 和 255 之间")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Length(min=0, max=64, message="发送选项长度必须介于 0 和 64 之间")
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}
	
	@Length(min=1, max=64, message="状态长度必须介于 1 和 64 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getSendDate() {
		return sendDate;
	}
	public String getSendDateString() {
		if(this.sendDate == null){
			this.sendDateString = DateUtils.getNewDate(this.createDate);
		}else{
			this.sendDateString = DateUtils.getNewDate(this.sendDate);
		}
		
		return sendDateString;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	

	@Length(min=0, max=255, message="外部链接长度必须介于 0 和 255 之间")
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	@Length(min=0, max=64, message="原始邮件长度必须介于 0 和 64 之间")
	public String getSourceMail() {
		return sourceMail;
	}

	public void setSourceMail(String sourceMail) {
		this.sourceMail = sourceMail;
	}

	public void setSendDateString(String sendDateString) {
		this.sendDateString = sendDateString;
	}

	public String getPageFlag() {
		return pageFlag;
	}

	public void setPageFlag(String pageFlag) {
		this.pageFlag = pageFlag;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPreMail() {
		return preMail;
	}

	public void setPreMail(String preMail) {
		this.preMail = preMail;
	}
	
}