/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCustom;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwCustomDao;

/**
 * 客户管理Service
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Service
@Transactional(readOnly = true)
public class ZqwCustomService extends CrudService<ZqwCustomDao, ZqwCustom> {

	public ZqwCustom get(String id) {
		return super.get(id);
	}
	
	public List<ZqwCustom> findList(ZqwCustom zqwCustom) {
		return super.findList(zqwCustom);
	}
	
	public Page<ZqwCustom> findPage(Page<ZqwCustom> page, ZqwCustom zqwCustom) {
		return super.findPage(page, zqwCustom);
	}
	
	@Transactional(readOnly = false)
	public void save(ZqwCustom zqwCustom) {
		super.save(zqwCustom);
	}
	
	@Transactional(readOnly = false)
	public void delete(ZqwCustom zqwCustom) {
		super.delete(zqwCustom);
	}
	
}