/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqw.entity.DeptAction;
import com.thinkgem.jeesite.modules.zqw.dao.DeptActionDao;

/**
 * 债行动Service
 * @author coder_cheng@126.com
 * @version 2018-10-20
 */
@Service
@Transactional(readOnly = true)
public class DeptActionService extends CrudService<DeptActionDao, DeptAction> {

	public DeptAction get(String id) {
		return super.get(id);
	}
	
	public List<DeptAction> findList(DeptAction deptAction) {
		return super.findList(deptAction);
	}


	public List<DeptAction> findListLimit(){ return dao.findListLimit();}
	
	public Page<DeptAction> findPage(Page<DeptAction> page, DeptAction deptAction) {
		return super.findPage(page, deptAction);
	}
	
	@Transactional(readOnly = false)
	public void save(DeptAction deptAction) {
		super.save(deptAction);
	}
	
	@Transactional(readOnly = false)
	public void delete(DeptAction deptAction) {
		super.delete(deptAction);
	}
	
}