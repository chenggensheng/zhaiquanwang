package com.thinkgem.jeesite.modules.zqw.utils;

import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.modules.zqw.dao.ZqwLayerDao;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwLayer;
import java.util.List;

public class ZqwLayerUtils {


    private static ZqwLayerDao zqwLayerDao = SpringContextHolder.getBean(ZqwLayerDao.class);

    public static List<ZqwLayer> getZqwLayerLists(String type){
        ZqwLayer zqwLayer = new ZqwLayer();
        zqwLayer.setLayerType(type);
        List<ZqwLayer> zqwLayers = zqwLayerDao.findList(zqwLayer);
        return zqwLayers;
    }

}
