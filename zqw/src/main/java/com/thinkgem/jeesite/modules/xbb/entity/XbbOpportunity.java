/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 肖帮帮销售计划管理Entity
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
public class XbbOpportunity extends DataEntity<XbbOpportunity> {
	
	private static final long serialVersionUID = 1L;
	private String decisionmaker;		// 决策者
	private String createtime;		// 创建时间
	private String contractexpirationdate;		// 合同到期日期
	private String contracttermsbusiness;		// contracttermsbusiness
	private String contractscanning;		// 合同扫描件
	private String contracttext;		// 合同文本
	private String contractsigningdate;		// contractsigningdate
	private String typeofcontract;		// 合同类型
	private String customerid;		// 客户ID
	private String customername;		// 客户名称
	private String currency;		// 币种
	private String updatetime;		// 更新时间
	private String competitor;		// 竞争对手
	private String importance;		// 重要程度
	private String salesopportunity;		// 销售机会
	private String salesopportunitynumber;		// 销售机会编号
	private String salesphase;		// 销售阶段
	private String estimatedtime;		// 预计结束时间
	private String estimatedamount;		// 预计金额
	
	public XbbOpportunity() {
		super();
	}

	public XbbOpportunity(String id){
		super(id);
	}

	@Length(min=0, max=255, message="决策者长度必须介于 0 和 255 之间")
	public String getDecisionmaker() {
		return decisionmaker;
	}

	public void setDecisionmaker(String decisionmaker) {
		this.decisionmaker = decisionmaker;
	}
	
	@Length(min=0, max=255, message="创建时间长度必须介于 0 和 255 之间")
	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	
	@Length(min=0, max=255, message="合同到期日期长度必须介于 0 和 255 之间")
	public String getContractexpirationdate() {
		return contractexpirationdate;
	}

	public void setContractexpirationdate(String contractexpirationdate) {
		this.contractexpirationdate = contractexpirationdate;
	}
	
	@Length(min=0, max=255, message="contracttermsbusiness长度必须介于 0 和 255 之间")
	public String getContracttermsbusiness() {
		return contracttermsbusiness;
	}

	public void setContracttermsbusiness(String contracttermsbusiness) {
		this.contracttermsbusiness = contracttermsbusiness;
	}
	
	@Length(min=0, max=255, message="合同扫描件长度必须介于 0 和 255 之间")
	public String getContractscanning() {
		return contractscanning;
	}

	public void setContractscanning(String contractscanning) {
		this.contractscanning = contractscanning;
	}
	
	@Length(min=0, max=255, message="合同文本长度必须介于 0 和 255 之间")
	public String getContracttext() {
		return contracttext;
	}

	public void setContracttext(String contracttext) {
		this.contracttext = contracttext;
	}
	
	@Length(min=0, max=255, message="contractsigningdate长度必须介于 0 和 255 之间")
	public String getContractsigningdate() {
		return contractsigningdate;
	}

	public void setContractsigningdate(String contractsigningdate) {
		this.contractsigningdate = contractsigningdate;
	}
	
	@Length(min=0, max=255, message="合同类型长度必须介于 0 和 255 之间")
	public String getTypeofcontract() {
		return typeofcontract;
	}

	public void setTypeofcontract(String typeofcontract) {
		this.typeofcontract = typeofcontract;
	}
	
	@Length(min=0, max=255, message="客户ID长度必须介于 0 和 255 之间")
	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
	@Length(min=0, max=255, message="客户名称长度必须介于 0 和 255 之间")
	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
	@Length(min=0, max=255, message="币种长度必须介于 0 和 255 之间")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Length(min=0, max=255, message="更新时间长度必须介于 0 和 255 之间")
	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	
	@Length(min=0, max=255, message="竞争对手长度必须介于 0 和 255 之间")
	public String getCompetitor() {
		return competitor;
	}

	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}
	
	@Length(min=0, max=255, message="重要程度长度必须介于 0 和 255 之间")
	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}
	
	@Length(min=0, max=255, message="销售机会长度必须介于 0 和 255 之间")
	public String getSalesopportunity() {
		return salesopportunity;
	}

	public void setSalesopportunity(String salesopportunity) {
		this.salesopportunity = salesopportunity;
	}
	
	@Length(min=0, max=255, message="销售机会编号长度必须介于 0 和 255 之间")
	public String getSalesopportunitynumber() {
		return salesopportunitynumber;
	}

	public void setSalesopportunitynumber(String salesopportunitynumber) {
		this.salesopportunitynumber = salesopportunitynumber;
	}
	
	@Length(min=0, max=255, message="销售阶段长度必须介于 0 和 255 之间")
	public String getSalesphase() {
		return salesphase;
	}

	public void setSalesphase(String salesphase) {
		this.salesphase = salesphase;
	}
	
	@Length(min=0, max=255, message="预计结束时间长度必须介于 0 和 255 之间")
	public String getEstimatedtime() {
		return estimatedtime;
	}

	public void setEstimatedtime(String estimatedtime) {
		this.estimatedtime = estimatedtime;
	}
	
	@Length(min=0, max=255, message="预计金额长度必须介于 0 和 255 之间")
	public String getEstimatedamount() {
		return estimatedamount;
	}

	public void setEstimatedamount(String estimatedamount) {
		this.estimatedamount = estimatedamount;
	}
	
}