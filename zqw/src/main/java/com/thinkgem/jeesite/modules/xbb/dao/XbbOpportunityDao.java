/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbOpportunity;

/**
 * 肖帮帮销售计划管理DAO接口
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
@MyBatisDao
public interface XbbOpportunityDao extends CrudDao<XbbOpportunity> {
	
}