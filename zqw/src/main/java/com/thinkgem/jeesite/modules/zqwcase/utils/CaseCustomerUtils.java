package com.thinkgem.jeesite.modules.zqwcase.utils;

import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseCustomerDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseCustomer;
import java.util.List;

public class CaseCustomerUtils {

    private static CaseCustomerDao caseCustomerDao = SpringContextHolder.getBean(CaseCustomerDao.class);


    public static List<CaseCustomer>  getCaseCustomerLists(){
        return caseCustomerDao.findAllList(new CaseCustomer());
    }

}
