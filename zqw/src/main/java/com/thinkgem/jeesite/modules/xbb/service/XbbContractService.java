/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xbb.dao.XbbContractDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbContract;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 肖帮帮合同管理Service
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
@Service
@Transactional(readOnly = true)
public class XbbContractService extends CrudService<XbbContractDao, XbbContract> {

	public XbbContract get(String id) {
		return super.get(id);
	}
	
	public List<XbbContract> findList(XbbContract xbbContract) {
		return super.findList(xbbContract);
	}
	
	public Page<XbbContract> findPage(Page<XbbContract> page, XbbContract xbbContract) {
		return super.findPage(page, xbbContract);
	}
	
	@Transactional(readOnly = false)
	public void save(XbbContract xbbContract) {
		super.save(xbbContract);
	}
	
	@Transactional(readOnly = false)
	public void delete(XbbContract xbbContract) {
		super.delete(xbbContract);
	}
	
}