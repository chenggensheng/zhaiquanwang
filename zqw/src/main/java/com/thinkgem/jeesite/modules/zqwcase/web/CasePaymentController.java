/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CasePayment;
import com.thinkgem.jeesite.modules.zqwcase.service.CasePaymentService;

/**
 * 肖邦邦合同回款管理Controller
 * @author coder_cheng@126.com
 * @version 2019-09-03
 */
@Controller
@RequestMapping(value = "${adminPath}/zqwcase/casePayment")
public class CasePaymentController extends BaseController {

	@Autowired
	private CasePaymentService casePaymentService;
	
	@ModelAttribute
	public CasePayment get(@RequestParam(required=false) String id) {
		CasePayment entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = casePaymentService.get(id);
		}
		if (entity == null){
			entity = new CasePayment();
		}
		return entity;
	}
	
	@RequiresPermissions("zqwcase:casePayment:view")
	@RequestMapping(value = {"list", ""})
	public String list(CasePayment casePayment, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CasePayment> page = casePaymentService.findPage(new Page<CasePayment>(request, response), casePayment); 
		model.addAttribute("page", page);
		return "modules/zqwcase/casePaymentList";
	}

	@RequiresPermissions("zqwcase:casePayment:view")
	@RequestMapping(value = "form")
	public String form(CasePayment casePayment, Model model) {
		model.addAttribute("casePayment", casePayment);
		return "modules/zqwcase/casePaymentForm";
	}

	@RequiresPermissions("zqwcase:casePayment:edit")
	@RequestMapping(value = "save")
	public String save(CasePayment casePayment, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, casePayment)){
			return form(casePayment, model);
		}
		casePaymentService.save(casePayment);
		addMessage(redirectAttributes, "保存肖邦邦合同回款管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/casePayment/?repage";
	}
	
	@RequiresPermissions("zqwcase:casePayment:edit")
	@RequestMapping(value = "delete")
	public String delete(CasePayment casePayment, RedirectAttributes redirectAttributes) {
		casePaymentService.delete(casePayment);
		addMessage(redirectAttributes, "删除肖邦邦合同回款管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqwcase/casePayment/?repage";
	}

}