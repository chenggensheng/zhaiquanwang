/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqw.entity.ZqwCustom;
import com.thinkgem.jeesite.modules.zqw.service.ZqwCustomService;

/**
 * 客户管理Controller
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
@Controller
@RequestMapping(value = "${adminPath}/zqw/zqwCustom")
public class ZqwCustomController extends BaseController {

	@Autowired
	private ZqwCustomService zqwCustomService;
	
	@ModelAttribute
	public ZqwCustom get(@RequestParam(required=false) String id) {
		ZqwCustom entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = zqwCustomService.get(id);
		}
		if (entity == null){
			entity = new ZqwCustom();
		}
		return entity;
	}
	
	@RequiresPermissions("zqw:zqwCustom:view")
	@RequestMapping(value = {"list", ""})
	public String list(ZqwCustom zqwCustom, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ZqwCustom> page = zqwCustomService.findPage(new Page<ZqwCustom>(request, response), zqwCustom); 
		model.addAttribute("page", page);
		return "modules/zqw/zqwCustomList";
	}

	@RequiresPermissions("zqw:zqwCustom:view")
	@RequestMapping(value = "form")
	public String form(ZqwCustom zqwCustom, Model model) {
		model.addAttribute("zqwCustom", zqwCustom);
		return "modules/zqw/zqwCustomForm";
	}

	@RequiresPermissions("zqw:zqwCustom:edit")
	@RequestMapping(value = "save")
	public String save(ZqwCustom zqwCustom, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, zqwCustom)){
			return form(zqwCustom, model);
		}
		zqwCustomService.save(zqwCustom);
		addMessage(redirectAttributes, "保存客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCustom/?repage";
	}
	
	@RequiresPermissions("zqw:zqwCustom:edit")
	@RequestMapping(value = "delete")
	public String delete(ZqwCustom zqwCustom, RedirectAttributes redirectAttributes) {
		zqwCustomService.delete(zqwCustom);
		addMessage(redirectAttributes, "删除客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/zqw/zqwCustom/?repage";
	}

}