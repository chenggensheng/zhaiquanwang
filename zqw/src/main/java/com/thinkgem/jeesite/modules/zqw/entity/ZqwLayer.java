/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqw.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 律师管理Entity
 * @author coder_cheng@126.com
 * @version 2019-06-14
 */
public class ZqwLayer extends DataEntity<ZqwLayer> {
	
	private static final long serialVersionUID = 1L;
	private String layerName;		// layer_name
	private String layerPhone;		// layer_phone
	private String layerCard;		// layer_card
	private String layerType;		// layer_type
	private String layerWhere;		// layer_where
	private String layerShow;		// layer_show
	
	public ZqwLayer() {
		super();
	}

	public ZqwLayer(String id){
		super(id);
	}

	@Length(min=0, max=1024, message="layer_name长度必须介于 0 和 1024 之间")
	public String getLayerName() {
		return layerName;
	}

	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}
	
	@Length(min=0, max=255, message="layer_phone长度必须介于 0 和 255 之间")
	public String getLayerPhone() {
		return layerPhone;
	}

	public void setLayerPhone(String layerPhone) {
		this.layerPhone = layerPhone;
	}
	
	@Length(min=0, max=255, message="layer_card长度必须介于 0 和 255 之间")
	public String getLayerCard() {
		return layerCard;
	}

	public void setLayerCard(String layerCard) {
		this.layerCard = layerCard;
	}
	
	@Length(min=0, max=255, message="layer_type长度必须介于 0 和 255 之间")
	public String getLayerType() {
		return layerType;
	}

	public void setLayerType(String layerType) {
		this.layerType = layerType;
	}
	
	@Length(min=0, max=1024, message="layer_where长度必须介于 0 和 1024 之间")
	public String getLayerWhere() {
		return layerWhere;
	}

	public void setLayerWhere(String layerWhere) {
		this.layerWhere = layerWhere;
	}
	
	public String getLayerShow() {
		return layerShow;
	}

	public void setLayerShow(String layerShow) {
		this.layerShow = layerShow;
	}
	
}