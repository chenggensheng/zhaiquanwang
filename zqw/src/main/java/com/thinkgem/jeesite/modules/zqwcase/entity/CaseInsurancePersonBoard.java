package com.thinkgem.jeesite.modules.zqwcase.entity;

public class CaseInsurancePersonBoard {

    private String name;
    private String nonComplaintCount;// 非诉催收
    private String nonOverOneComplatinCount;//超过一个月
    private String nonOverTowComplatinCount;//超过二个月

    private String yCount;  //意
    private String eCount;  //恶
    private String wCount;  //无
    private String sCount;  //失
    private String qCount;  //其
    private String xCount;  //修
    private String qqCount; //缺
    private String bCount;  //补


    private String litigationsCount;//诉讼阶段
    private String selfLitigationsCount;//自办总数
    private String selfOverOneCount;// 超1未立
    private String selfOverTowCount;//超2未立
    private String selflhwkCount;//立后无开
    private String selfykwpCount;//已开未判
    private String selfypwsCount;//已判未生
    private String selfpjsxCount;//判决生效
    private String selfqsCount;//缺
    private String selfpcCount;//补充


    private String outLitigationsCount;//委外
    private String outOverfiveCount;//委外超15天
    private String outOverthreeCount;//委外超30天
    private String outOverOneCount;// 超1未立
    private String outOverTowCount;//超2未立
    private String outlhwkCount;//立后无开
    private String outykwpCount;//已开未判
    private String outypwsCount;//已判未生
    private String outpjsxCount;//判决生效
    private String outqsCount;//缺
    private String outpcCount;//补充

    private String todayFollowAllCaseCount;
    private String todayFollowCaseCount;
    private String todayFollowCount;
    private String todayFollowOtherCount;

    private String implementsCount;//执行阶段
    private String selfImplementsCount;//自办
    private String selfOverOneImplementsCount;//超1未申立
    private String selfOverTowImplementsCount;//超2未申立
    private String selfOverThreeImplementsCount;//超3未终
    private String selfOverSixImplementsCount;//超6未终

    private String outImplementsCount;//委外
    private String outFiveImplementsCount;//委外15
    private String outThreeImplementsCount;//委外30
    private String outOverOneImplementsCount;//超1未申立
    private String outOverTowImplementsCount;//超2未申立
    private String outOverThreeImplementsCount;//超3未终
    private String outOverSixImplementsCount;//超6未终


    private String fLackFunds;  //F资缺
    private String fSupplement; //F资补
    private String sShortage;   //S资缺
    private String sSupplement; //S资补
    private String swShortage; //SW资缺
    private String swSupplement;//SW资补
    private String caseInsuranceName;  //按钮名字




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNonComplaintCount() {
        return nonComplaintCount;
    }

    public void setNonComplaintCount(String nonComplaintCount) {
        this.nonComplaintCount = nonComplaintCount;
    }

    public String getLitigationsCount() {
        return litigationsCount;
    }

    public void setLitigationsCount(String litigationsCount) {
        this.litigationsCount = litigationsCount;
    }

    public String getImplementsCount() {
        return implementsCount;
    }

    public void setImplementsCount(String implementsCount) {
        this.implementsCount = implementsCount;
    }

    public String getyCount() {
        return yCount;
    }

    public void setyCount(String yCount) {
        this.yCount = yCount;
    }

    public String geteCount() {
        return eCount;
    }

    public void seteCount(String eCount) {
        this.eCount = eCount;
    }

    public String getwCount() {
        return wCount;
    }

    public void setwCount(String wCount) {
        this.wCount = wCount;
    }

    public String getsCount() {
        return sCount;
    }

    public void setsCount(String sCount) {
        this.sCount = sCount;
    }

    public String getqCount() {
        return qCount;
    }

    public void setqCount(String qCount) {
        this.qCount = qCount;
    }

    public String getxCount() {
        return xCount;
    }

    public void setxCount(String xCount) {
        this.xCount = xCount;
    }

    public String getQqCount() {
        return qqCount;
    }

    public void setQqCount(String qqCount) {
        this.qqCount = qqCount;
    }

    public String getbCount() {
        return bCount;
    }

    public void setbCount(String bCount) {
        this.bCount = bCount;
    }

    public String getSelfLitigationsCount() {
        return selfLitigationsCount;
    }

    public void setSelfLitigationsCount(String selfLitigationsCount) {
        this.selfLitigationsCount = selfLitigationsCount;
    }

    public String getSelfOverOneCount() {
        return selfOverOneCount;
    }

    public void setSelfOverOneCount(String selfOverOneCount) {
        this.selfOverOneCount = selfOverOneCount;
    }

    public String getSelfOverTowCount() {
        return selfOverTowCount;
    }

    public void setSelfOverTowCount(String selfOverTowCount) {
        this.selfOverTowCount = selfOverTowCount;
    }

    public String getSelflhwkCount() {
        return selflhwkCount;
    }

    public void setSelflhwkCount(String selflhwkCount) {
        this.selflhwkCount = selflhwkCount;
    }

    public String getSelfykwpCount() {
        return selfykwpCount;
    }

    public void setSelfykwpCount(String selfykwpCount) {
        this.selfykwpCount = selfykwpCount;
    }

    public String getSelfypwsCount() {
        return selfypwsCount;
    }

    public void setSelfypwsCount(String selfypwsCount) {
        this.selfypwsCount = selfypwsCount;
    }

    public String getSelfpjsxCount() {
        return selfpjsxCount;
    }

    public void setSelfpjsxCount(String selfpjsxCount) {
        this.selfpjsxCount = selfpjsxCount;
    }

    public String getSelfqsCount() {
        return selfqsCount;
    }

    public void setSelfqsCount(String selfqsCount) {
        this.selfqsCount = selfqsCount;
    }

    public String getSelfpcCount() {
        return selfpcCount;
    }

    public void setSelfpcCount(String selfpcCount) {
        this.selfpcCount = selfpcCount;
    }

    public String getOutLitigationsCount() {
        return outLitigationsCount;
    }

    public void setOutLitigationsCount(String outLitigationsCount) {
        this.outLitigationsCount = outLitigationsCount;
    }

    public String getOutOverfiveCount() {
        return outOverfiveCount;
    }

    public void setOutOverfiveCount(String outOverfiveCount) {
        this.outOverfiveCount = outOverfiveCount;
    }

    public String getOutOverthreeCount() {
        return outOverthreeCount;
    }

    public void setOutOverthreeCount(String outOverthreeCount) {
        this.outOverthreeCount = outOverthreeCount;
    }

    public String getOutOverOneCount() {
        return outOverOneCount;
    }

    public void setOutOverOneCount(String outOverOneCount) {
        this.outOverOneCount = outOverOneCount;
    }

    public String getOutOverTowCount() {
        return outOverTowCount;
    }

    public void setOutOverTowCount(String outOverTowCount) {
        this.outOverTowCount = outOverTowCount;
    }

    public String getOutlhwkCount() {
        return outlhwkCount;
    }

    public void setOutlhwkCount(String outlhwkCount) {
        this.outlhwkCount = outlhwkCount;
    }

    public String getOutykwpCount() {
        return outykwpCount;
    }

    public void setOutykwpCount(String outykwpCount) {
        this.outykwpCount = outykwpCount;
    }

    public String getOutypwsCount() {
        return outypwsCount;
    }

    public void setOutypwsCount(String outypwsCount) {
        this.outypwsCount = outypwsCount;
    }

    public String getOutpjsxCount() {
        return outpjsxCount;
    }

    public void setOutpjsxCount(String outpjsxCount) {
        this.outpjsxCount = outpjsxCount;
    }

    public String getOutqsCount() {
        return outqsCount;
    }

    public void setOutqsCount(String outqsCount) {
        this.outqsCount = outqsCount;
    }

    public String getOutpcCount() {
        return outpcCount;
    }

    public void setOutpcCount(String outpcCount) {
        this.outpcCount = outpcCount;
    }

    public String getTodayFollowAllCaseCount() {
        return todayFollowAllCaseCount;
    }

    public void setTodayFollowAllCaseCount(String todayFollowAllCaseCount) {
        this.todayFollowAllCaseCount = todayFollowAllCaseCount;
    }

    public String getTodayFollowCaseCount() {
        return todayFollowCaseCount;
    }

    public void setTodayFollowCaseCount(String todayFollowCaseCount) {
        this.todayFollowCaseCount = todayFollowCaseCount;
    }

    public String getTodayFollowCount() {
        return todayFollowCount;
    }

    public void setTodayFollowCount(String todayFollowCount) {
        this.todayFollowCount = todayFollowCount;
    }

    public String getTodayFollowOtherCount() {
        return todayFollowOtherCount;
    }

    public void setTodayFollowOtherCount(String todayFollowOtherCount) {
        this.todayFollowOtherCount = todayFollowOtherCount;
    }

    public String getSelfImplementsCount() {
        return selfImplementsCount;
    }

    public void setSelfImplementsCount(String selfImplementsCount) {
        this.selfImplementsCount = selfImplementsCount;
    }

    public String getSelfOverOneImplementsCount() {
        return selfOverOneImplementsCount;
    }

    public void setSelfOverOneImplementsCount(String selfOverOneImplementsCount) {
        this.selfOverOneImplementsCount = selfOverOneImplementsCount;
    }

    public String getSelfOverTowImplementsCount() {
        return selfOverTowImplementsCount;
    }

    public void setSelfOverTowImplementsCount(String selfOverTowImplementsCount) {
        this.selfOverTowImplementsCount = selfOverTowImplementsCount;
    }

    public String getSelfOverThreeImplementsCount() {
        return selfOverThreeImplementsCount;
    }

    public void setSelfOverThreeImplementsCount(String selfOverThreeImplementsCount) {
        this.selfOverThreeImplementsCount = selfOverThreeImplementsCount;
    }

    public String getSelfOverSixImplementsCount() {
        return selfOverSixImplementsCount;
    }

    public void setSelfOverSixImplementsCount(String selfOverSixImplementsCount) {
        this.selfOverSixImplementsCount = selfOverSixImplementsCount;
    }

    public String getOutImplementsCount() {
        return outImplementsCount;
    }

    public void setOutImplementsCount(String outImplementsCount) {
        this.outImplementsCount = outImplementsCount;
    }

    public String getOutFiveImplementsCount() {
        return outFiveImplementsCount;
    }

    public void setOutFiveImplementsCount(String outFiveImplementsCount) {
        this.outFiveImplementsCount = outFiveImplementsCount;
    }

    public String getOutThreeImplementsCount() {
        return outThreeImplementsCount;
    }

    public void setOutThreeImplementsCount(String outThreeImplementsCount) {
        this.outThreeImplementsCount = outThreeImplementsCount;
    }

    public String getOutOverOneImplementsCount() {
        return outOverOneImplementsCount;
    }

    public void setOutOverOneImplementsCount(String outOverOneImplementsCount) {
        this.outOverOneImplementsCount = outOverOneImplementsCount;
    }

    public String getOutOverTowImplementsCount() {
        return outOverTowImplementsCount;
    }

    public void setOutOverTowImplementsCount(String outOverTowImplementsCount) {
        this.outOverTowImplementsCount = outOverTowImplementsCount;
    }

    public String getOutOverThreeImplementsCount() {
        return outOverThreeImplementsCount;
    }

    public void setOutOverThreeImplementsCount(String outOverThreeImplementsCount) {
        this.outOverThreeImplementsCount = outOverThreeImplementsCount;
    }

    public String getOutOverSixImplementsCount() {
        return outOverSixImplementsCount;
    }

    public void setOutOverSixImplementsCount(String outOverSixImplementsCount) {
        this.outOverSixImplementsCount = outOverSixImplementsCount;
    }

    public String getNonOverOneComplatinCount() {
        return nonOverOneComplatinCount;
    }

    public void setNonOverOneComplatinCount(String nonOverOneComplatinCount) {
        this.nonOverOneComplatinCount = nonOverOneComplatinCount;
    }

    public String getNonOverTowComplatinCount() {
        return nonOverTowComplatinCount;
    }

    public void setNonOverTowComplatinCount(String nonOverTowComplatinCount) {
        this.nonOverTowComplatinCount = nonOverTowComplatinCount;
    }

    public String getfLackFunds() {
        return fLackFunds;
    }

    public void setfLackFunds(String fLackFunds) {
        this.fLackFunds = fLackFunds;
    }

    public String getfSupplement() {
        return fSupplement;
    }

    public void setfSupplement(String fSupplement) {
        this.fSupplement = fSupplement;
    }

    public String getsShortage() {
        return sShortage;
    }

    public void setsShortage(String sShortage) {
        this.sShortage = sShortage;
    }

    public String getsSupplement() {
        return sSupplement;
    }

    public void setsSupplement(String sSupplement) {
        this.sSupplement = sSupplement;
    }

    public String getSwShortage() {
        return swShortage;
    }

    public void setSwShortage(String swShortage) {
        this.swShortage = swShortage;
    }

    public String getSwSupplement() {
        return swSupplement;
    }

    public void setSwSupplement(String swSupplement) {
        this.swSupplement = swSupplement;
    }

    public String getCaseInsuranceName() {
        return caseInsuranceName;
    }

    public void setCaseInsuranceName(String caseInsuranceName) {
        this.caseInsuranceName = caseInsuranceName;
    }


}
