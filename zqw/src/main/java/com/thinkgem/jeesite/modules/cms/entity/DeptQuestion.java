/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.cms.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 咨询方案Entity
 * @author coder_cheng@126.com
 * @version 2018-12-17
 */
public class DeptQuestion extends DataEntity<DeptQuestion> {
	
	private static final long serialVersionUID = 1L;
	private String layerName;		// 律师姓名
	private String layerPhone;		// 律师电话
	private String name;		// 咨询人姓名
	private String phone;		// 咨询人电话
	private String deptType; //1债权催收 2 维权打假

	public String getDeptType() {
		return deptType;
	}

	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}

	public DeptQuestion() {
		super();
	}

	public DeptQuestion(String id){
		super(id);
	}

	@Length(min=0, max=252, message="律师姓名长度必须介于 0 和 252 之间")
	public String getLayerName() {
		return layerName;
	}

	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}
	
	@Length(min=0, max=265, message="律师电话长度必须介于 0 和 265 之间")
	public String getLayerPhone() {
		return layerPhone;
	}

	public void setLayerPhone(String layerPhone) {
		this.layerPhone = layerPhone;
	}
	
	@Length(min=0, max=256, message="咨询人姓名长度必须介于 0 和 256 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=254, message="咨询人电话长度必须介于 0 和 254 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}