/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.xbb.entity.XbbContacts;
import com.thinkgem.jeesite.modules.xbb.entity.XbbContract;
import com.thinkgem.jeesite.modules.xbb.entity.XbbCustomer;
import com.thinkgem.jeesite.modules.xbb.entity.XbbOpportunity;
import com.thinkgem.jeesite.modules.xbb.service.XbbContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 肖帮帮合同管理Controller
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
@Controller
@RequestMapping(value = "${adminPath}/xbb/xbbContract")
public class XbbContractController extends BaseController {

	@Autowired
	private XbbContractService xbbContractService;
	
	@ModelAttribute
	public XbbContract get(@RequestParam(required=false) String id) {
		XbbContract entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xbbContractService.get(id);
		}
		if (entity == null){
			entity = new XbbContract();
		}
		return entity;
	}


//	@RequiresPermissions("xbb:xbbContract:view")
	@RequestMapping(value = {"list", ""})
	public String list(XbbContract xbbContract, HttpServletRequest request, HttpServletResponse response, Model model) {
	    Page<XbbContract> page = xbbContractService.findPage(new Page<XbbContract>(request, response), xbbContract);
		model.addAttribute("page", page);
		return "modules/xbb/xbbContractList";

	}

    @ResponseBody
//	@RequiresPermissions("xbb:xbbContract:view")
	@RequestMapping(value = "listJSON")
	public Map<String, Object> listJSON(XbbContract xbbContract, HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		Page<XbbContract> page = xbbContractService.findPage(new Page<XbbContract>(request, response), xbbContract);
		model.addAttribute("page", page);
		returnMap.put("total", page.getCount());
		returnMap.put("rows", page.getList());
		return returnMap;
	}

//	@RequiresPermissions("xbb:xbbContract:view")
	@RequestMapping(value = "form")
	public String form(XbbContract xbbContract, Model model) throws Exception{
		XbbCustomer xbbCustomer = CustomerApi.getXbbCustomer(ConfigConstant.corpid,ConfigConstant.token, ConfigConstant.GET_CUSTOMER,ConfigConstant.customerOneKey,xbbContract.getCustomerid());
		Page<XbbOpportunity> page = new Page<XbbOpportunity>();
		page.setPageSize(100);
		xbbContract.setClaimnumber("NO."+DateUtils.getYear()+DateUtils.getMonth()+DateUtils.getDay()+"0XXX");
		xbbContract.setCustomerid(xbbCustomer.getId());
		xbbContract.setCustomername(xbbCustomer.getCustomer());
		xbbContract.setCustomerabbreviation(xbbCustomer.getCustomerabbreviation());
		xbbContract.setSigneddate(DateUtils.getYear()+"-"+DateUtils.getMonth()+"-"+DateUtils.getDay());
		User user = UserUtils.getUser();
		List<XbbOpportunity> xbbOpportunities = (List<XbbOpportunity>) CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,page.getPageNo(),page.getPageSize(), ConfigConstant.OPPORTUNITY_LIST,ConfigConstant.opportunityKey,xbbContract.getCustomerid(),null).get("rows");
		List<XbbContacts> xbbContactsList = (List<XbbContacts>) CustomerApi.returnMap(ConfigConstant.corpid,ConfigConstant.token,page.getPageNo(),page.getPageSize(), ConfigConstant.CUSTOMER_CONTACTS_LIST,ConfigConstant.opportunityKey,xbbContract.getCustomerid(),null).get("rows");

		model.addAttribute("xbbContactsList",xbbContactsList);
		model.addAttribute("xbbOpportunities",xbbOpportunities);
		model.addAttribute("xbbContract", xbbContract);
		model.addAttribute("user",user);
		return "modules/xbb/xbbContractForm";
	}

//	@RequiresPermissions("xbb:xbbContract:edit")
	@RequestMapping(value = "save")
	public String save(XbbContract xbbContract, Model model, RedirectAttributes redirectAttributes) throws Exception {
		CustomerApi.addContract(ConfigConstant.corpid,ConfigConstant.token,xbbContract.getCustomerid(),xbbContract,ConfigConstant.ADD_CUSTOMER_CONTACTS);
		if (!beanValidator(model, xbbContract)){
			return form(xbbContract, model);
		}
		xbbContractService.save(xbbContract);
		addMessage(redirectAttributes, "保存肖帮帮合同管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbContract/?repage";
	}
	
//	@RequiresPermissions("xbb:xbbContract:edit")
	@RequestMapping(value = "delete")
	public String delete(XbbContract xbbContract, RedirectAttributes redirectAttributes) {
		xbbContractService.delete(xbbContract);
		addMessage(redirectAttributes, "删除肖帮帮合同管理成功");
		return "redirect:"+Global.getAdminPath()+"/xbb/xbbContract/?repage";
	}

}