/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 肖帮帮合同管理Entity
 * @author coder_cheng@126.com
 * @version 2018-08-21
 */
public class XbbContract extends DataEntity<XbbContract> {
	
	private static final long serialVersionUID = 1L;
	private String debtorpartycontactmode;		// 债务人/对方联系方式
	private String nameofdebtorpartyname;		// 债务人名称/对方名称
	private String debtorcontactsandduties;		// 债务人联系人及职务
	private String claimnumber;		// 债权编号/项目编号
	private String typesrelatedcontracts;		// 关联合同类型
	private String createid;		// 创建人ID
	private String createname;		// 创建人
	private String contracttermsbusiness;		// 合同商务条款
	private String statecontract;		// 合同状态
	private String contractamount;		// 合同金额
	private String actualrepaymentdate;		// 实际最后还款日
	private String customerid;		// 客户ID
	private String customername;		// 客户名称
	private String customerabbreviation;		// 客户简称
	private String customerdemand;		// 客户需求
	private String hostteam;		// 承办团队
	private String toundertakelawyer;		// 承办律师
	private String hoststate;		// 承办状态
	private String chargemode;		// 收费方式
	private String materialscanning;		// 材料扫描件
	private String materiallist;		// 材料清单
	private String entryname;		// 案件/服务/项目名称
	private String briefintroduction;		// 案情简介
	private String totalamounarrears;		// 欠款总金额
	private String officialclaimnumber;		// 正式债权编号
	private String signedperson;		// 签订人
	private String signedpersonid;		// 签订人ID
	private String signeddate;		// 签订日期
	private String disputesigningdate;		// 纠纷合同签订日
	private String agreedrepaymentdate;		// 约定最后还款日
	private String certificatenumber;		// 证照号码
	private String pastcollection;		// 过往催收经过
	private String reasooverdue;		// 逾期原因
	private String approvanumber;		// 钉钉用印审批编号
	private  String customerContract;

	public String getCustomerContract() {
		return customerContract;
	}

	public void setCustomerContract(String customerContract) {
		this.customerContract = customerContract;
	}

	public XbbContract() {
		super();
	}

	public XbbContract(String id){
		super(id);
	}

	@Length(min=0, max=1025, message="debtorpartycontactmode长度必须介于 0 和 1025 之间")
	public String getDebtorpartycontactmode() {
		return debtorpartycontactmode;
	}

	public void setDebtorpartycontactmode(String debtorpartycontactmode) {
		this.debtorpartycontactmode = debtorpartycontactmode;
	}
	
	@Length(min=0, max=1025, message="债务人名称/对方名称长度必须介于 0 和 1025 之间")
	public String getNameofdebtorpartyname() {
		return nameofdebtorpartyname;
	}

	public void setNameofdebtorpartyname(String nameofdebtorpartyname) {
		this.nameofdebtorpartyname = nameofdebtorpartyname;
	}
	
	@Length(min=0, max=1025, message="债务人联系人及职务长度必须介于 0 和 1025 之间")
	public String getDebtorcontactsandduties() {
		return debtorcontactsandduties;
	}

	public void setDebtorcontactsandduties(String debtorcontactsandduties) {
		this.debtorcontactsandduties = debtorcontactsandduties;
	}
	
	@Length(min=0, max=255, message="债权编号/项目编号长度必须介于 0 和 255 之间")
	public String getClaimnumber() {
		return claimnumber;
	}

	public void setClaimnumber(String claimnumber) {
		this.claimnumber = claimnumber;
	}
	
	@Length(min=0, max=255, message="关联合同类型长度必须介于 0 和 255 之间")
	public String getTypesrelatedcontracts() {
		return typesrelatedcontracts;
	}

	public void setTypesrelatedcontracts(String typesrelatedcontracts) {
		this.typesrelatedcontracts = typesrelatedcontracts;
	}
	
	@Length(min=0, max=255, message="创建人ID长度必须介于 0 和 255 之间")
	public String getCreateid() {
		return createid;
	}

	public void setCreateid(String createid) {
		this.createid = createid;
	}
	
	@Length(min=0, max=255, message="创建人长度必须介于 0 和 255 之间")
	public String getCreatename() {
		return createname;
	}

	public void setCreatename(String createname) {
		this.createname = createname;
	}
	
	@Length(min=0, max=1025, message="合同商务条款长度必须介于 0 和 1025 之间")
	public String getContracttermsbusiness() {
		return contracttermsbusiness;
	}

	public void setContracttermsbusiness(String contracttermsbusiness) {
		this.contracttermsbusiness = contracttermsbusiness;
	}
	
	@Length(min=0, max=255, message="合同状态长度必须介于 0 和 255 之间")
	public String getStatecontract() {
		return statecontract;
	}

	public void setStatecontract(String statecontract) {
		this.statecontract = statecontract;
	}
	
	@Length(min=0, max=255, message="合同金额长度必须介于 0 和 255 之间")
	public String getContractamount() {
		return contractamount;
	}

	public void setContractamount(String contractamount) {
		this.contractamount = contractamount;
	}
	
	@Length(min=0, max=255, message="实际最后还款日长度必须介于 0 和 255 之间")
	public String getActualrepaymentdate() {
		return actualrepaymentdate;
	}

	public void setActualrepaymentdate(String actualrepaymentdate) {
		this.actualrepaymentdate = actualrepaymentdate;
	}
	
	@Length(min=0, max=255, message="客户ID长度必须介于 0 和 255 之间")
	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
	@Length(min=0, max=255, message="客户名称长度必须介于 0 和 255 之间")
	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
	@Length(min=0, max=1025, message="客户简称长度必须介于 0 和 1025 之间")
	public String getCustomerabbreviation() {
		return customerabbreviation;
	}

	public void setCustomerabbreviation(String customerabbreviation) {
		this.customerabbreviation = customerabbreviation;
	}
	
	public String getCustomerdemand() {
		return customerdemand;
	}

	public void setCustomerdemand(String customerdemand) {
		this.customerdemand = customerdemand;
	}
	
	public String getHostteam() {
		return hostteam;
	}

	public void setHostteam(String hostteam) {
		this.hostteam = hostteam;
	}
	
	@Length(min=0, max=1025, message="承办律师长度必须介于 0 和 1025 之间")
	public String getToundertakelawyer() {
		return toundertakelawyer;
	}

	public void setToundertakelawyer(String toundertakelawyer) {
		this.toundertakelawyer = toundertakelawyer;
	}
	
	@Length(min=0, max=255, message="承办状态长度必须介于 0 和 255 之间")
	public String getHoststate() {
		return hoststate;
	}

	public void setHoststate(String hoststate) {
		this.hoststate = hoststate;
	}
	
	@Length(min=0, max=255, message="收费方式长度必须介于 0 和 255 之间")
	public String getChargemode() {
		return chargemode;
	}

	public void setChargemode(String chargemode) {
		this.chargemode = chargemode;
	}
	
	@Length(min=0, max=1025, message="材料扫描件长度必须介于 0 和 1025 之间")
	public String getMaterialscanning() {
		return materialscanning;
	}

	public void setMaterialscanning(String materialscanning) {
		this.materialscanning = materialscanning;
	}
	
	@Length(min=0, max=1025, message="材料清单长度必须介于 0 和 1025 之间")
	public String getMateriallist() {
		return materiallist;
	}

	public void setMateriallist(String materiallist) {
		this.materiallist = materiallist;
	}
	
	@Length(min=0, max=1025, message="案件/服务/项目名称长度必须介于 0 和 1025 之间")
	public String getEntryname() {
		return entryname;
	}

	public void setEntryname(String entryname) {
		this.entryname = entryname;
	}
	
	@Length(min=0, max=1025, message="案情简介长度必须介于 0 和 1025 之间")
	public String getBriefintroduction() {
		return briefintroduction;
	}

	public void setBriefintroduction(String briefintroduction) {
		this.briefintroduction = briefintroduction;
	}
	
	@Length(min=0, max=255, message="欠款总金额长度必须介于 0 和 255 之间")
	public String getTotalamounarrears() {
		return totalamounarrears;
	}

	public void setTotalamounarrears(String totalamounarrears) {
		this.totalamounarrears = totalamounarrears;
	}
	
	@Length(min=0, max=255, message="正式债权编号长度必须介于 0 和 255 之间")
	public String getOfficialclaimnumber() {
		return officialclaimnumber;
	}

	public void setOfficialclaimnumber(String officialclaimnumber) {
		this.officialclaimnumber = officialclaimnumber;
	}
	
	@Length(min=0, max=255, message="签订人长度必须介于 0 和 255 之间")
	public String getSignedperson() {
		return signedperson;
	}

	public void setSignedperson(String signedperson) {
		this.signedperson = signedperson;
	}
	
	@Length(min=0, max=255, message="签订人ID长度必须介于 0 和 255 之间")
	public String getSignedpersonid() {
		return signedpersonid;
	}

	public void setSignedpersonid(String signedpersonid) {
		this.signedpersonid = signedpersonid;
	}
	
	@Length(min=0, max=255, message="签订日期长度必须介于 0 和 255 之间")
	public String getSigneddate() {
		return signeddate;
	}

	public void setSigneddate(String signeddate) {
		this.signeddate = signeddate;
	}
	
	@Length(min=0, max=255, message="纠纷合同签订日长度必须介于 0 和 255 之间")
	public String getDisputesigningdate() {
		return disputesigningdate;
	}

	public void setDisputesigningdate(String disputesigningdate) {
		this.disputesigningdate = disputesigningdate;
	}
	
	@Length(min=0, max=255, message="约定最后还款日长度必须介于 0 和 255 之间")
	public String getAgreedrepaymentdate() {
		return agreedrepaymentdate;
	}

	public void setAgreedrepaymentdate(String agreedrepaymentdate) {
		this.agreedrepaymentdate = agreedrepaymentdate;
	}
	
	@Length(min=0, max=255, message="证照号码长度必须介于 0 和 255 之间")
	public String getCertificatenumber() {
		return certificatenumber;
	}

	public void setCertificatenumber(String certificatenumber) {
		this.certificatenumber = certificatenumber;
	}
	
	@Length(min=0, max=255, message="过往催收经过长度必须介于 0 和 255 之间")
	public String getPastcollection() {
		return pastcollection;
	}

	public void setPastcollection(String pastcollection) {
		this.pastcollection = pastcollection;
	}
	
	public String getReasooverdue() {
		return reasooverdue;
	}

	public void setReasooverdue(String reasooverdue) {
		this.reasooverdue = reasooverdue;
	}
	
	@Length(min=0, max=255, message="钉钉用印审批编号长度必须介于 0 和 255 之间")
	public String getApprovanumber() {
		return approvanumber;
	}

	public void setApprovanumber(String approvanumber) {
		this.approvanumber = approvanumber;
	}
	
}