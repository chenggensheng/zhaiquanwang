/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.xbb.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.xbb.dao.XbbCustomerDao;
import com.thinkgem.jeesite.modules.xbb.entity.XbbCustomer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 肖帮帮客户管理Service
 * @author coder_cheng@126.com
 * @version 2018-08-20
 */
@Service
@Transactional(readOnly = true)
public class XbbCustomerService extends CrudService<XbbCustomerDao, XbbCustomer> {

	public XbbCustomer get(String id) {
		return super.get(id);
	}
	
	public List<XbbCustomer> findList(XbbCustomer xbbCustomer) {
		return super.findList(xbbCustomer);
	}
	
	public Page<XbbCustomer> findPage(Page<XbbCustomer> page, XbbCustomer xbbCustomer) {
		return super.findPage(page, xbbCustomer);
	}
	
	@Transactional(readOnly = false)
	public void save(XbbCustomer xbbCustomer) {
		super.save(xbbCustomer);
	}
	
	@Transactional(readOnly = false)
	public void delete(XbbCustomer xbbCustomer) {
		super.delete(xbbCustomer);
	}
	
}