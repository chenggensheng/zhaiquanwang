/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseXbbInsurance;

import java.util.List;

/**
 * 肖邦邦案件管理DAO接口
 * @author coder_cheng@126.com
 * @version 2019-07-20
 */
@MyBatisDao
public interface CaseXbbInsuranceDao extends CrudDao<CaseXbbInsurance> {


    public List<CaseXbbInsurance> findCountList();

    public List<CaseXbbInsurance> findNewList(CaseXbbInsurance caseXbbInsurance);

	
}