package com.thinkgem.jeesite.modules.tz.web;


import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.modules.tz.entity.TzCityHouse;
import com.thinkgem.jeesite.modules.tz.service.TzCityHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "${frontPath}/tz/frontHouse")
public class TzCityHouseFrontController {

    @Autowired
    TzCityHouseService tzCityHouseService;

    @RequestMapping(value = {"index", ""})
    public String index(TzCityHouse tzCityHouse, HttpServletRequest request, HttpServletResponse response, Model model) {
        String searchValue = request.getParameter("searchValue");
        tzCityHouse.setSearchValue(searchValue);
        List<TzCityHouse> tzCityHouseLists = tzCityHouseService.findList(tzCityHouse);
        model.addAttribute("searchValue",searchValue);
        model.addAttribute("tzCityHouseLists", tzCityHouseLists);
        return "modules/tz/tzCityHouseFrontList";
    }
}
