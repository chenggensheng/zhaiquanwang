/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.service;

import java.util.Date;
import java.util.List;

import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseCustomer;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseInsurance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseXbbInsurance;
import com.thinkgem.jeesite.modules.zqwcase.dao.CaseXbbInsuranceDao;

/**
 * 肖邦邦案件管理Service
 * @author coder_cheng@126.com
 * @version 2019-07-20
 */
@Service
@Transactional(readOnly = true)
public class CaseXbbInsuranceService extends CrudService<CaseXbbInsuranceDao, CaseXbbInsurance> {

	@Autowired
	CaseInsuranceService caseInsuranceService;

	@Autowired
	CaseCustomerService caseCustomerService;

	public CaseXbbInsurance get(String id) {
		return super.get(id);
	}
	
	public List<CaseXbbInsurance> findList(CaseXbbInsurance caseXbbInsurance) {
		return super.findList(caseXbbInsurance);
	}
	
	public Page<CaseXbbInsurance> findPage(Page<CaseXbbInsurance> page, CaseXbbInsurance caseXbbInsurance) {
		return super.findPage(page, caseXbbInsurance);
	}
	
	@Transactional(readOnly = false)
	public void save(CaseXbbInsurance caseXbbInsurance) {
		super.save(caseXbbInsurance);
	}


	@Transactional(readOnly = false)
	public void saveInsert(CaseXbbInsurance caseXbbInsurance) {
		caseXbbInsurance.setUpdateDate(new Date());
		caseXbbInsurance.setCreateDate(new Date());
		dao.insert(caseXbbInsurance);
	}
	
	@Transactional(readOnly = false)
	public void delete(CaseXbbInsurance caseXbbInsurance) {
		super.delete(caseXbbInsurance);
	}


	public List<CaseXbbInsurance> findNewList(CaseXbbInsurance caseXbbInsurance){return dao.findNewList(caseXbbInsurance);}


	@Transactional(readOnly = false)
	public void saveFromCrm(List<CaseXbbInsurance> caseXbbInsurances, List<CaseInsurance> caseInsurances){
		if (caseXbbInsurances !=null && caseXbbInsurances.size()>0){
			for (CaseXbbInsurance caseXbbInsurance :caseXbbInsurances){
				   boolean flag = false;
				   CaseInsurance caseInsuranceTrue =null;
				   if (caseInsurances !=null && caseInsurances.size()>0){
					   for (CaseInsurance caseInsurance:caseInsurances){
                            if (caseXbbInsurance.getCaseNumber().equals(caseInsurance.getAcceptanceNumebr())){
                            	flag = true;
								if (StringUtils.isNotBlank(caseXbbInsurance.getAccidentDate())){
									caseInsurance.setAccidentDate(DateUtils.parseDate(caseXbbInsurance.getAccidentDate()));
								}

								caseInsurance.setCaseIntroduction(caseXbbInsurance.getCaseIntroduction());
								caseInsurance.setCreditorAmount(caseXbbInsurance.getCompernsatorAmount());
								caseInsurance.setAcceptanceNumebrOld(caseXbbInsurance.getReportNum());
								caseInsurance.setDelFlag(caseXbbInsurance.getDelFlag());
								caseInsuranceService.save(caseInsurance);
								CaseXbbInsurance caseXbbInsuranceSearch = get(caseXbbInsurance.getId());
								caseXbbInsurance.setCaseInsuranceId(caseInsurance.getId());
								if (caseXbbInsuranceSearch !=null && StringUtils.isNotBlank(caseXbbInsuranceSearch.getId())){
									save(caseXbbInsurance);
								}else {
									saveInsert(caseXbbInsurance);
								}
							}
					   }
					   if (flag){//已经有数据，同步数据即可；

					   }else {
//						   CaseCustomer caseCustomerSearch = caseCustomerService.get(caseXbbInsurance.getCaseCustomerId());
//						   if (caseCustomerSearch !=null && StringUtils.isNotBlank(caseCustomerSearch.getId())){
//						   }else {
							   CaseCustomer caseCustomer = new CaseCustomer();
							   caseCustomer.setCrmId(caseXbbInsurance.getCaseCustomerId());
							   caseCustomer.setCreditor(caseXbbInsurance.getCaseCustomerName());
							   caseCustomer.setCrmCreditor(caseXbbInsurance.getCaseCustomerName());
							   caseCustomerService.save(caseCustomer);
//						   }
						   CaseInsurance caseInsuranceTemp = new CaseInsurance();
						   if (StringUtils.isNotBlank(caseXbbInsurance.getAccidentDate())){
							   caseInsuranceTemp.setAccidentDate(DateUtils.parseDate(caseXbbInsurance.getAccidentDate()));
						   }
						   caseInsuranceTemp.setCaseIntroduction(caseXbbInsurance.getCaseIntroduction());
						   caseInsuranceTemp.setCreditorAmount(caseXbbInsurance.getCompernsatorAmount());

//						   if (caseXbbInsurance.getReportNum().equals(caseInsurance.getAcceptanceNumebr())){
						   caseInsuranceTemp.setAcceptanceNumebr(caseXbbInsurance.getCaseNumber());
						   caseInsuranceTemp.setAcceptanceNumebrOld(caseXbbInsurance.getReportNum());
						   caseInsuranceTemp.setCaseCustomerId(caseCustomer.getId());
						   caseInsuranceTemp.setDelFlag(caseXbbInsurance.getDelFlag());

						   if (caseXbbInsurance.getCaseType().equals("追偿")){
							   caseInsuranceTemp.setCaseType("0");
						   }else if (caseXbbInsurance.getCaseType().equals("代位求偿")){
							   caseInsuranceTemp.setCaseType("1");
						   }else {
							   caseInsuranceTemp.setCaseType("2");
						   }
						   caseInsuranceService.save(caseInsuranceTemp);
						   CaseXbbInsurance caseXbbInsuranceSearch = get(caseXbbInsurance.getId());
						   caseXbbInsurance.setCaseInsuranceId(caseInsuranceTemp.getId());
						   if (caseXbbInsuranceSearch !=null && StringUtils.isNotBlank(caseXbbInsuranceSearch.getId())){
							   save(caseXbbInsurance);
						   }else {
							   saveInsert(caseXbbInsurance);
						   }


					   }
				   }
			}
		}

	}
}