/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.zqwcase.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.FileUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.zqwcase.entity.OaMail;
import com.thinkgem.jeesite.modules.zqwcase.entity.OaMailPerson;
import com.thinkgem.jeesite.modules.zqwcase.service.OaMailService;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 内部邮件Controller
 * @author Huang
 * @version 2018-04-27
 */
@Controller
@RequestMapping(value = "${adminPath}/oa/oaMail")
public class OaMailController extends BaseController {

	@Autowired
	private OaMailService oaMailService;

	@ModelAttribute
	public OaMail get(@RequestParam(required=false) String id) {
		OaMail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = oaMailService.get(id);
		}
		if (entity == null){
			entity = new OaMail();
		}
		return entity;
	}


	/**
	 * 未读邮件
	 * @param oaMail
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = {"unRead"})
	public String unRead(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model) {  
		oaMail.getMailPerson().setUser(UserUtils.getUser());
		oaMail.getMailPerson().setReadFlag("0");
		oaMail.getMailPerson().setImportantFlag(null);
		// 不读取垃圾邮件
		oaMail.getMailPerson().setDustbinFlag("0");
		Page<OaMail> page = oaMailService.findReceivePage(new Page<OaMail>(request, response), oaMail);
		model.addAttribute("page", page);
		// 页面跳转标记
		oaMail.setPageFlag("unRead");
		oaMail.setPageTitle("未读邮件");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailBox";
	}


	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "findLimitFive")
	public List<OaMail> findLimitFive(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model){
		oaMail.getMailPerson().setUser(UserUtils.getUser());
		oaMail.getMailPerson().setReadFlag("0");
		oaMail.getMailPerson().setDustbinFlag("0");
		oaMail.getMailPerson().setImportantFlag(null);
		List<OaMail> oaMails = oaMailService.findLimitFive(oaMail);
		return oaMails;
	}



	/**
	 * 收件箱
	 * @param oaMail
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = {"receiveBox"})
	public String receiveBox(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model) {
		oaMail.getMailPerson().setUser(UserUtils.getUser());
		oaMail.getMailPerson().setReadFlag(null);
		oaMail.getMailPerson().setImportantFlag(null);
		// 不读取垃圾邮件
		oaMail.getMailPerson().setDustbinFlag("0");
		Page<OaMail> page = oaMailService.findReceivePage(new Page<OaMail>(request, response), oaMail);
		model.addAttribute("page", page);
		// 页面跳转标记
		oaMail.setPageFlag("receiveBox");
		oaMail.setPageTitle("收件箱");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailBox";
	}


	/**
	 * 重要邮件
	 * @param oaMail
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = {"important"})
	public String important(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model) {
		oaMail.getMailPerson().setUser(UserUtils.getUser());
		oaMail.getMailPerson().setReadFlag(null);
		oaMail.getMailPerson().setImportantFlag("1");
		// 不读取垃圾邮件
		oaMail.getMailPerson().setDustbinFlag("0");
		Page<OaMail> page = oaMailService.findReceivePage(new Page<OaMail>(request, response), oaMail);
		model.addAttribute("page", page);
		// 页面跳转标记
		oaMail.setPageFlag("important");
		oaMail.setPageTitle("收藏邮件");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailBox";
	}


	/**
	 * 垃圾箱
	 * @param oaMail
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = {"dustbin"})
	public String dustbin(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model) {
		oaMail.getMailPerson().setUser(UserUtils.getUser());
		oaMail.getMailPerson().setReadFlag(null);
		oaMail.getMailPerson().setImportantFlag(null);
		oaMail.getMailPerson().setDustbinFlag("1");
		Page<OaMail> page = oaMailService.findReceivePage(new Page<OaMail>(request, response), oaMail);
		model.addAttribute("page", page);
		// 页面跳转标记
		oaMail.setPageFlag("dustbin");
		oaMail.setPageTitle("垃圾箱");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailBox";
	}


	/**
	 * 发件箱
	 * @param oaMail
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = {"send"})
	public String send(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model) {
		oaMail.setStatus("1");
		oaMail.setCreateBy(UserUtils.getUser());
		Page<OaMail> page = oaMailService.findSendPage(new Page<OaMail>(request, response), oaMail);
		model.addAttribute("page", page);
		// 页面跳转标记
		oaMail.setPageFlag("send");
		oaMail.setPageTitle("发件箱");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaSendMailBox";
	}


	/**
	 * 草稿箱
	 * @param oaMail
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = {"draft"})
	public String draft(OaMail oaMail, HttpServletRequest request, HttpServletResponse response, Model model) {
		oaMail.setStatus("0");
		oaMail.setCreateBy(UserUtils.getUser());
		Page<OaMail> page = oaMailService.findSendPage(new Page<OaMail>(request, response), oaMail);
		model.addAttribute("page", page);
		// 页面跳转标记
		oaMail.setPageFlag("draft");
		oaMail.setPageTitle("草稿箱");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaSendMailBox";
	}


	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "form")
	public String form(OaMail oaMail, Model model) {
		if (StringUtils.isBlank(oaMail.getId())){
			oaMail.setStatus("0");
		}
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailForm";
	}


	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "resend")
	public String resend(OaMail oldOaMail, Model model) {
		OaMail oaMail = new OaMail();
		oaMail.setTitle("回复："+oldOaMail.getTitle());
		String content = getNewContent(oldOaMail);
		oaMail.setContent(content);
		User toUser = UserUtils.get(oldOaMail.getCreateBy().getId());
		oaMail.setToUser(toUser);
		oaMail.setPreMail(oldOaMail.getId());
		oaMail.setSourceMail(oldOaMail.getSourceMail());
		if (StringUtils.isBlank(oaMail.getId())){
			oaMail.setStatus("0");
		}
		oaMail.setPageFlag("resend");
		oaMail.setPageTitle("邮件回复");
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailForm";
	}


	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "forward")
	public String forward(OaMail oldOaMail, Model model) {
		OaMail oaMail = new OaMail();
		oaMail.setTitle("转发："+oldOaMail.getTitle());
		String content = getNewContent(oldOaMail);
		oaMail.setContent(content);
		oaMail.setPreMail(oldOaMail.getId());
		oaMail.setSourceMail(oldOaMail.getSourceMail());
		if (StringUtils.isBlank(oaMail.getId())){
			oaMail.setStatus("0");
		}
		oaMail.setPageFlag("forward");
		oaMail.setPageTitle("邮件转发");
		// modify by lfb 20190514 修复转发邮件没有附件
		oaMail.setAttach(oldOaMail.getAttach());
		// end modify
		model.addAttribute("oaMail", oaMail);
		return "modules/oa/oaMailForm";
	}


	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "view")
	public String view(OaMail oaMail, Model model,String ids) {
		String attach = oaMail.getAttach();
		if (StringUtils.isNotBlank(attach)){
			List<Map<String,String>> attachList = getAttachList(attach);
			model.addAttribute("attachList", attachList);
		}

		OaMailPerson oaMailPerson = oaMailService.getOaMailPerson(oaMail.getId(),UserUtils.getUser());
		oaMail.setMailPerson(oaMailPerson);

		model.addAttribute("oaMail", oaMail);
		model.addAttribute("ids", ids);
		List<String> idList = Lists.newArrayList();
		idList.add(oaMail.getId());
		// 批量标记为已读
		oaMailService.updateReadFlag(UserUtils.getUser(), idList);
		return "modules/oa/oaMailView";
	}


	/**
	 * 写邮件
	 * @param oaMail
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:edit")
	@RequestMapping(value = "save")
	public String save(OaMail oaMail, Model model, RedirectAttributes redirectAttributes) {
		boolean flag = false;
		if (!beanValidator(model, oaMail)){
			model.addAttribute("flag", flag);
			return form(oaMail, model);
		}
		String status = oaMail.getStatus();
		if (StringUtils.isBlank(status)){
			status = "";
		}
		String message = "";
		if (StringUtils.isNotBlank(oaMail.getId())){
			OaMail oldOaMail = oaMailService.get(oaMail.getId());
			// 判断当前登陆人是否时邮件的编辑人
			User currUser = UserUtils.getUser();
			if (!currUser.getId().equals(oldOaMail.getCreateBy().getId())){
				message = "操作失败，您不是该邮件的所有者！";
				addMessage(model, message);
				model.addAttribute("flag", flag);
				return form(oaMail, model);
			}
			if (status.equals(oldOaMail.getStatus()) && "1".equals(status)){
				message = "操作失败，该邮件的状态已经改变！";
				addMessage(model, message);
				model.addAttribute("flag", flag);
				return form(oaMail, model);
			}
		}
		if("1".equals(status)){
			oaMail.setSendDate(new Date());
			oaMailService.save(oaMail);
			message = "发送成功！";
		}else if ("0".equals(status)){
			oaMailService.save(oaMail);
			message = "保存成功！";
		}else{
			message = "操作失败，请刷新后重试！";
		}
		flag = true;
		model.addAttribute("flag", flag);
		addMessage(model, message);
		return form(oaMail, model);
	}


	/**
	 * 发件人删除邮件
	 * @param oaMail
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:edit")
	@RequestMapping(value = "delete")
	public String delete(OaMail oaMail, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		String ids = request.getParameter("ids");
		if (StringUtils.isNotBlank(ids)){
			List<String> idList = Arrays.asList(StringUtils.split(ids,','));
			oaMailService.delete(UserUtils.getUser(), idList);
			addMessage(redirectAttributes, "删除成功");
		}else{
			addMessage(redirectAttributes, "删除失败，请选择需要删除的邮件");
		}
		return "redirect:"+Global.getAdminPath()+"/oa/oaMail/"+oaMail.getPageFlag()+"?repage";
	}


	/**
	 * 标记为已读
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "updateReadFlag")
	@ResponseBody
	public Map<String,Object> updateReadFlag(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map = Maps.newHashMap();
		try{
			String ids = request.getParameter("ids");
			if (StringUtils.isBlank(ids)){
				map.put("errCode", "-1");
				map.put("msg", "邮件标记失败，请选择需要需要操作的邮件");
				return map;
			}
			List<String> idList = Arrays.asList(StringUtils.split(ids,','));
			// 批量标记为已读
			oaMailService.updateReadFlag(UserUtils.getUser(), idList);
			map.put("errCode", "0");
			map.put("msg", "邮件标记成功");
		}catch(Exception e){
			map.put("errCode", "-1");
			map.put("msg", "邮件标记失败，请重试！");
		}
		return map;
	}
	
	
	/**
	 * 全部标记为已读
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "updateAllReadFlag")
	@ResponseBody
	public Map<String,Object> updateAllReadFlag(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map = Maps.newHashMap();
		try{
			// 批量标记为已读
			oaMailService.updateAllReadFlag(UserUtils.getUser());
			map.put("errCode", "0");
			map.put("msg", "邮件标记成功");
		}catch(Exception e){
			map.put("errCode", "-1");
			map.put("msg", "邮件标记失败，请重试！");
		}
		return map;
	}




	/**
	 * 标记为重要邮件
	 * @param oaMail
	 * @param redirectAttributes
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "updateImportantFlag")
	public String updateImportantFlag(OaMail oaMail, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		String ids = request.getParameter("ids");
		if (StringUtils.isNotBlank(ids)){
			List<String> idList = Arrays.asList(StringUtils.split(ids,','));
			String importantFlag = oaMail.getMailPerson() == null?"":oaMail.getMailPerson().getImportantFlag();
			// 默认是取消标记
			if (StringUtils.isBlank(importantFlag)){
				importantFlag = "0";
			}
			oaMailService.updateImportantFlag(UserUtils.getUser(), idList, importantFlag);
			addMessage(redirectAttributes, "邮件标记成功");
		}else{
			addMessage(redirectAttributes, "邮件标记失败，请选择需要需要操作的邮件");
		}
		return "redirect:"+Global.getAdminPath()+"/oa/oaMail/"+oaMail.getPageFlag()+"?repage";
	}


	/**
	 * 标记为重要邮件
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "updateImportantFlagAjax")
	@ResponseBody
	public Map<String,Object> updateImportantFlag(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map = Maps.newHashMap();
		try{
			String ids = request.getParameter("ids");
			if (StringUtils.isBlank(ids)){
				map.put("errCode", "-1");
				map.put("msg", "邮件标记失败，请选择需要需要操作的邮件");
				return map;
			}
			String importantFlag = request.getParameter("importantFlag");
			// 默认是取消标记
			if (StringUtils.isBlank(importantFlag)){
				importantFlag = "0";
			}
			List<String> idList = Arrays.asList(StringUtils.split(ids,','));
			// 批量标记
			oaMailService.updateImportantFlag(UserUtils.getUser(), idList, importantFlag);
			map.put("errCode", "0");
			map.put("msg", "邮件标记成功");
		}catch(Exception e){
			map.put("errCode", "-1");
			map.put("msg", "邮件标记失败，请重试！");
		}
		return map;
	}


	/**
	 * 标记为垃圾邮件
	 * @param oaMail
	 * @param redirectAttributes
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "updateDustbinFlag")
	public String updateDustbinFlag(OaMail oaMail, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		String ids = request.getParameter("ids");
		if (StringUtils.isNotBlank(ids)){
			List<String> idList = Arrays.asList(StringUtils.split(ids,','));
			String dustbinFlag = oaMail.getMailPerson() == null?"":oaMail.getMailPerson().getDustbinFlag();
			// 默认是取消标记
			if (StringUtils.isBlank(dustbinFlag)){
				dustbinFlag = "0";
			}
			oaMailService.updateDustbinFlag(UserUtils.getUser(), idList, dustbinFlag);
			addMessage(redirectAttributes, "邮件标记成功");
		}else{
			addMessage(redirectAttributes, "邮件标记失败，请选择需要需要操作的邮件");
		}
		return "redirect:"+Global.getAdminPath()+"/oa/oaMail/"+oaMail.getPageFlag()+"?repage";
	}


	/**
	 * 标记为垃圾邮件
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("oa:oaMail:view")
	@RequestMapping(value = "updateDustbinFlagAjax")
	@ResponseBody
	public Map<String,Object> updateDustbinFlag(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map = Maps.newHashMap();
		try{
			String ids = request.getParameter("ids");
			if (StringUtils.isBlank(ids)){
				map.put("errCode", "-1");
				map.put("msg", "邮件标记失败，请选择需要需要操作的邮件");
				return map;
			}
			String dustbinFlag = request.getParameter("dustbinFlag");
			// 默认是取消标记
			if (StringUtils.isBlank(dustbinFlag)){
				dustbinFlag = "0";
			}
			List<String> idList = Arrays.asList(StringUtils.split(ids,','));
			// 批量标记
			oaMailService.updateDustbinFlag(UserUtils.getUser(), idList, dustbinFlag);
			map.put("errCode", "0");
			map.put("msg", "邮件标记成功");
		}catch(Exception e){
			map.put("errCode", "-1");
			map.put("msg", "邮件标记失败，请重试！");
		}
		return map;
	}


	/**
	 * 获取邮件的附件
	 * @param attach
	 * @return
	 */
	private List<Map<String,String>> getAttachList(String attach){
		List<Map<String,String>> attachList = Lists.newArrayList();
		if (StringUtils.isBlank(attach)){
			return attachList;
		}
		for (String filePath : StringUtils.split(attach,'|')) {
			filePath = filePath.trim();
			if (StringUtils.isBlank(filePath)){
				continue;
			}
			Map<String,String> map = Maps.newHashMap();
			map.put("filePath", filePath);
			String fileName = filePath.substring(filePath.lastIndexOf("/")+1);
			if (StringUtils.isBlank(fileName)){
				fileName = filePath.substring(filePath.lastIndexOf("\\")+1);
			}
			try {
				fileName = URLDecoder.decode(fileName,"UTF-8");
			} catch (UnsupportedEncodingException e) {
				fileName = "";
			}
			String fileType = "file";
			if (StringUtils.isNotBlank(fileName)){
				String suffix = FileUtils.getFileExtension(fileName);
				if (StringUtils.isBlank(suffix)){
					suffix = "";
				}
				if (suffix.equals("png") || suffix.equals("jpg") || suffix.equals("gif") 
						|| suffix.equals("jpeg") || suffix.equals("bmp") ){
					fileType = "img";
				}
			}
			map.put("fileName",  fileName);
			map.put("fileType", fileType);
			attachList.add(map);
		}
		return attachList;
	}


	/**
	 * 获取转发和回复的邮件的新内容
	 * @param oaMail
	 * @return
	 */
	private String getNewContent(OaMail oaMail){
		String content = oaMail.getContent();
		String creatUserName = oaMail.getCreateBy().getName();
		String sendDate = oaMail.getSendDateString();
		String title = oaMail.getTitle();
		String ccUserName = oaMail.getCcUser().getName();
		String toUserName = oaMail.getToUser().getName();

		String newContent = "<div><br></div><div><br></div>"
				+ "<div style=\"font-size: 12px;font-family: Arial Narrow;padding:2px 0 2px 0;\">"
				+ "------------------&nbsp;原始邮件&nbsp;------------------"
				+ "</div>"
				+ "<div style=\"font-size: 12px;background:#efefef;padding:8px;\">"
				+ "<div><b>发件人:&nbsp;</b>"
				+ creatUserName
				+ "</div>"
				+ "<div><b>发送时间:&nbsp;</b>"
				+ sendDate
				+ "</div>"
				+ "<div><b>收件人:&nbsp;</b>"
				+ toUserName
				+ "</div>";
		if (StringUtils.isNotBlank(ccUserName)){
			newContent +=  "<div><b>抄送人:&nbsp;</b>"
					+ ccUserName
					+ "</div>";
		}
		newContent += "<div><b>主题:&nbsp;</b>"
				+ title
				+ "</div></div><div><br></div>"
				+ content;
		return  newContent;
	}

}