package com.thinkgem.jeesite.common.xbb.customer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.common.xbb.helper.HttpRequestUtils;
import com.thinkgem.jeesite.common.xbb.helper.XbbException;
import com.thinkgem.jeesite.modules.xbb.entity.*;
import com.google.common.collect.Lists;
import com.thinkgem.jeesite.modules.zqwcase.entity.CasePayment;
import com.thinkgem.jeesite.modules.zqwcase.entity.CaseXbbInsurance;
import com.thinkgem.jeesite.modules.zqwcase.entity.XbbCommunicate;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerApi {
	
	/**
	 * 获取客户简要列表接口
	 * @param corpid
	 * @param token
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws XbbException
	 */
	public static JSONArray simpleList(String corpid, String token, Integer page, Integer pageSize,String apiUrl,String key) throws XbbException {
		JSONArray retArray = null;
		
		String url = ConfigConstant.getApiUrl(ConfigConstant.CUSTOMER_SIMPLE_LIST);
		JSONObject data = new JSONObject();
		data.put("corpid", corpid);
		data.put("page", page);
		data.put("pageSize", pageSize);
		//计算token， 可以用
		String sign = ConfigConstant.getDataSign(data, token);
		String response = null;
		JSONObject responseJson = null;
		try {
			response = HttpRequestUtils.post(url, data.toJSONString(), sign);
		} catch(Exception e) {
			throw new XbbException(-1, "http post访问出错");
		}
		try {
			responseJson = JSON.parseObject(response);
		} catch(Exception e) {
			throw new XbbException(-1, "json解析出错");
		}
		
		if(responseJson.containsKey("errorCode") && responseJson.getInteger("errorCode").equals(0)) {
			retArray = responseJson.getJSONArray("customerList");
			return retArray;
		} else {
			throw new XbbException(responseJson.getInteger("errorCode"), responseJson.getString("msg"));
		}
	}



	public  static XbbCustomer getXbbCustomer(String corpid,String token,String apiUrl,String key,String customerId) throws XbbException{
		String url = ConfigConstant.getApiUrl(apiUrl);
		JSONObject data = new JSONObject();
		data.put("corpid", corpid);
		data.put("customerId",customerId);
		//计算token， 可以用
		String sign = ConfigConstant.getDataSign(data, token);
		String response = null;
		JSONObject responseJson = null;
		try {
			response = HttpRequestUtils.post(url, data.toJSONString(), sign);
		} catch(Exception e) {
			throw new XbbException(-1, "http post访问出错");
		}
		try {
			responseJson = JSON.parseObject(response);
		} catch(Exception e) {
			throw new XbbException(-1, "json解析出错");
		}
		if(responseJson.containsKey("errorCode") && responseJson.getInteger("errorCode").equals(0)) {
			        JSONObject map = responseJson.getJSONObject(key);
					XbbCustomer xbbCustomer = new XbbCustomer();
					xbbCustomer.setNextContactTime((String) map.get("下次预约联系时间"));
					xbbCustomer.setNameofchannel((String) map.get("渠道/代理商名称"));
					xbbCustomer.setProvince((String) map.get("省"));
					xbbCustomer.setCity((String) map.get("市"));
					xbbCustomer.setPicture((String) map.get("图片"));
					xbbCustomer.setFiletemp((String) map.get("文件"));
					xbbCustomer.setCustomersource((String) map.get("客户来源"));
					xbbCustomer.setCustomerstatus((String) map.get("客户状态"));
					xbbCustomer.setClientprofile((String)map.get("客户简介"));
					xbbCustomer.setRemarks((String)map.get("备注"));
					xbbCustomer.setCustomerindustry((String)map.get("客户行业"));
					xbbCustomer.setCreditcode((String)map.get("身份证号/信用代码"));
					xbbCustomer.setDistrictcounty((String)map.get("区/县"));
					xbbCustomer.setCustomernature((String)map.get("客户性质"));
					xbbCustomer.setCustomerclassification((String)map.get("客户分级"));
					xbbCustomer.setImportance((String)map.get("重要程度"));
					xbbCustomer.setAddr((String)map.get("地址"));
					xbbCustomer.setId(Integer.toString((Integer) map.get("id")));
					xbbCustomer.setCustomerfocuscategory((String)map.get("客户关注类别"));
					xbbCustomer.setCustomerassociationname((String)map.get("展会/客户会名称"));
					xbbCustomer.setCreateid((String)map.get("创建人ID"));
					xbbCustomer.setCustomerabbreviation((String)map.get("客户简称"));
					xbbCustomer.setCustomernature((String)map.get("客户类型"));
					xbbCustomer.setNextContactTime((String)map.get("下次预约联系时间"));
					xbbCustomer.setCustomerwebsite((String)map.get("客户官网"));
					xbbCustomer.setUpdatetime(((Integer)map.get("更新时间")).toString());
					xbbCustomer.setLegalrepresentative((String)map.get("法定代表人"));
					xbbCustomer.setRegisteredcapital((String)map.get("注册资金"));
					xbbCustomer.setCreatetime(((Integer)map.get("创建时间")).toString());
					xbbCustomer.setCustomer((String)map.get("客户名称"));
					xbbCustomer.setCustomerphone((String)map.get("客户电话"));
				    return  xbbCustomer;
		} else {
			throw new XbbException(responseJson.getInteger("errorCode"), responseJson.getString("msg"));
		}


	}
	public static Map<String, Object> returnMap(String corpid,String token,Integer page,Integer pageSize ,String apiUrl,String key,String customerId,String updateTime) throws XbbException{
		JSONArray retArray = null;

		String url = ConfigConstant.getApiUrl(apiUrl);
		JSONObject data = new JSONObject();
		data.put("corpid", corpid);
		data.put("page", page);
		data.put("pageSize", pageSize);

		if (StringUtils.isNotBlank(customerId) && !key.equals("communicateList")){
			data.put("customerId",Integer.valueOf(customerId));
		}

		if (key.equals("contractList")){
			data.put("templateId",2315821);
			data.put("isDel",1);
		}


		if (StringUtils.isNotBlank(customerId) && key.equals("communicateList")){
			data.put("contarctId",Integer.valueOf(customerId));
		}


		if (StringUtils.isNotBlank(updateTime)){
			data.put("updateTimeStart",updateTime);
		}

		Map<String, Object> returnMap = new HashMap<String, Object>();


		//计算token， 可以用
		String sign = ConfigConstant.getDataSign(data, token);
		String response = null;
		JSONObject responseJson = null;
		try {
			response = HttpRequestUtils.post(url, data.toJSONString(), sign);
		} catch(Exception e) {
			throw new XbbException(-1, "http post访问出错");
		}
		try {
			responseJson = JSON.parseObject(response);
		} catch(Exception e) {
			throw new XbbException(-1, "json解析出错");
		}

		if((responseJson.containsKey("errorCode") && responseJson.getInteger("errorCode").equals(0))|| (responseJson.containsKey("errorcode") && responseJson.getInteger("errorcode").equals(0))) {
			retArray = responseJson.getJSONArray(key);
            String totalCount = responseJson.getString("totalCount");
            if (key.equals("userList")){
				List<XbbUser> xbbUsers = Lists.newArrayList();
            	for (int i =0;i<retArray.size();i++){
            		XbbUser xbbUser = new XbbUser();
					JSONObject map = retArray.getJSONObject(i);
					xbbUser.setAddtime(((Integer) map.get("addTime")).toString());
					xbbUser.setIsleaderindepts((String) map.get("isLeaderInDepts"));
					xbbUser.setName((String) map.get("name"));
					xbbUser.setActive(((Integer)map.get("active")).toString());
					xbbUser.setUpdatetime(((Integer) map.get("updateTime")).toString());
					xbbUser.setAvatar((String) map.get("avatar"));
					xbbUser.setId(((Integer) map.get("id")).toString());
					xbbUser.setPosition((String) map.get("position"));
					xbbUser.setDepartment((String) map.get("department"));
					xbbUsers.add(xbbUser);
				}
				returnMap.put("rows", xbbUsers);
			}else if (key.equals("customerList")){
            	List<XbbCustomer> xbbCustomers = Lists.newArrayList();
				if (null != retArray && retArray.size()>0){
				for (int i =0;i<retArray.size();i++){
					JSONObject map = retArray.getJSONObject(i);
					XbbCustomer xbbCustomer = new XbbCustomer();
					xbbCustomer.setNextContactTime((String) map.get("下次预约联系时间"));
					xbbCustomer.setNameofchannel((String) map.get("渠道/代理商名称"));
					xbbCustomer.setProvince((String) map.get("省"));
					xbbCustomer.setCity((String) map.get("市"));
					xbbCustomer.setPicture((String) map.get("图片"));
					xbbCustomer.setFiletemp((String) map.get("文件"));
					xbbCustomer.setCustomersource((String) map.get("客户来源"));
					xbbCustomer.setCustomerstatus((String) map.get("客户状态"));
					xbbCustomer.setClientprofile((String)map.get("客户简介"));
					xbbCustomer.setRemarks((String)map.get("备注"));
					xbbCustomer.setCustomerindustry((String)map.get("客户行业"));
					xbbCustomer.setCreditcode((String)map.get("身份证号/信用代码"));
					xbbCustomer.setDistrictcounty((String)map.get("区/县"));
					xbbCustomer.setCustomernature((String)map.get("客户性质"));
					xbbCustomer.setCustomerclassification((String)map.get("客户分级"));
					xbbCustomer.setImportance((String)map.get("重要程度"));
					xbbCustomer.setAddr((String)map.get("地址"));
					xbbCustomer.setId(Integer.toString((Integer) map.get("id")));
					xbbCustomer.setCustomerfocuscategory((String)map.get("客户关注类别"));
					xbbCustomer.setCustomerassociationname((String)map.get("展会/客户会名称"));
					xbbCustomer.setCreateid((String)map.get("创建人ID"));
					xbbCustomer.setCustomerabbreviation((String)map.get("客户简称"));
					xbbCustomer.setCustomernature((String)map.get("客户类型"));
					xbbCustomer.setNextContactTime((String)map.get("下次预约联系时间"));
					xbbCustomer.setCustomerwebsite((String)map.get("客户官网"));
					xbbCustomer.setUpdatetime(((Integer)map.get("更新时间")).toString());
					xbbCustomer.setLegalrepresentative((String)map.get("法定代表人"));
					xbbCustomer.setRegisteredcapital((String)map.get("注册资金"));
//					xbbCustomer.setIatitude((String)map.get("纬度"));
//					xbbCustomer.setIongitude((String)map.get("经度"));
					xbbCustomer.setCreatetime(((Integer)map.get("创建时间")).toString());
					xbbCustomer.setCustomer((String)map.get("客户名称"));
					xbbCustomer.setCustomerphone((String)map.get("客户电话"));
					xbbCustomers.add(xbbCustomer);
				}
				}
				returnMap.put("rows",xbbCustomers);
			}else if(key.equals("opportunityList")) {
				List<XbbOpportunity> xbbOpportunities = Lists.newArrayList();
				if (null != retArray && retArray.size()>0){
				for (int i =0;i<retArray.size();i++){
					JSONObject map = retArray.getJSONObject(i);
					XbbOpportunity xbbOpportunity = new XbbOpportunity();
					xbbOpportunity.setDecisionmaker((String) map.get("决策人"));
					xbbOpportunity.setContractexpirationdate((String) map.get("合同到期日期"));

					xbbOpportunity.setContracttermsbusiness((String) map.get("合同商务条款"));
					xbbOpportunity.setContractscanning((String) map.get("合同扫描件"));
					xbbOpportunity.setContracttext((String) map.get("合同文本"));
					xbbOpportunity.setContractsigningdate((String) map.get("合同签订日期"));
					xbbOpportunity.setTypeofcontract((String) map.get("合同类型"));
					xbbOpportunity.setCustomername((String) map.get("客户名称"));
					xbbOpportunity.setCurrency((String) map.get("币种"));
					xbbOpportunity.setCompetitor((String) map.get("竞争对手"));
					xbbOpportunity.setImportance((String) map.get("重要程度"));
					xbbOpportunity.setSalesopportunity((String) map.get("销售机会名称"));
					xbbOpportunity.setSalesopportunitynumber((String) map.get("销售机会编号"));
					xbbOpportunity.setSalesopportunity((String) map.get("销售阶段"));
					xbbOpportunity.setEstimatedtime((String) map.get("预计结束时间"));
					xbbOpportunity.setEstimatedamount((String) map.get("预计金额"));

					xbbOpportunities.add(xbbOpportunity);
				}
				}
				returnMap.put("rows",xbbOpportunities);
			}else  if (key.equals("contactsList")){
            	List<XbbContacts> xbbContactsList = Lists.newArrayList();
            	for (int i =0;i<retArray.size();i++){
            		JSONObject map = retArray.getJSONObject(i);
            		XbbContacts xbbContacts = new XbbContacts();
            		xbbContacts.setSort1((String)map.get("sort1"));
            		xbbContacts.setSort2((String)map.get("sort2"));
            		xbbContacts.setSort3((String)map.get("sort2"));
            		xbbContacts.setCustomerid((String)map.get("关联客户"));
            		xbbContacts.setJcgx((String)map.get("决策关系"));
            		xbbContacts.setXq((String)map.get("区/县"));
            		xbbContacts.setDz((String)map.get("地址"));
            		xbbContacts.setBz((String)map.get("备注"));
            		xbbContacts.setXm((String)map.get("姓名"));
            		xbbContacts.setSjx((String)map.get("市"));
            		xbbContacts.setXb((String)map.get("性别"));
            		xbbContacts.setSj((String)map.get("手机"));
            		xbbContacts.setSjxx((String)map.get("省"));
            		xbbContacts.setJb((String)map.get("级别"));
            		xbbContacts.setZw((String)map.get("职务"));
            		xbbContacts.setBm((String)map.get("部门"));
            		xbbContacts.setZycd((String)map.get("重要程度"));
            		xbbContactsList.add(xbbContacts);
				}
				returnMap.put("rows",xbbContactsList);
			}else  if (key.equals("contractList")){
				List<CaseXbbInsurance> caseXbbInsurances = Lists.newArrayList();
				for (int i =0;i<retArray.size();i++){
					JSONObject map = retArray.getJSONObject(i);
					CaseXbbInsurance caseXbbInsurance = new CaseXbbInsurance();
					caseXbbInsurance.setId(((Integer)map.get("id")).toString());
					caseXbbInsurance.setDelFlag(((Integer)map.get("del")).toString());
					caseXbbInsurance.setLimitTime((String)map.get("举证期限"));
					caseXbbInsurance.setAccidentPlace((String)map.get("事故发生地"));
					caseXbbInsurance.setAccidentDate((String)map.get("事故发生日期"));
					caseXbbInsurance.setCreatePerson((String)map.get("创建人"));
					caseXbbInsurance.setMaturityDate((String)map.get("到期日期"));
					caseXbbInsurance.setCooperativeLawyer((String)map.get("合作律师信息"));
                    caseXbbInsurance.setCommercialClause((String)map.get("合同商务条款"));
                    caseXbbInsurance.setContractStatus((String)map.get("合同状态"));
					caseXbbInsurance.setContractType((String)map.get("合同类型"));
					caseXbbInsurance.setContractAmount((String)map.get("合同金额"));
					caseXbbInsurance.setOutSourcing((String)map.get("委外情况"));
					caseXbbInsurance.setOutSourcingDate((String)map.get("委外日期"));
					caseXbbInsurance.setCaseCustomerId(((Integer)map.get("客户ID")).toString());
					caseXbbInsurance.setCaseCustomerName((String)map.get("客户名称"));
					caseXbbInsurance.setCaseCustmoerContract((String)map.get("客户联系人"));
                    caseXbbInsurance.setCaseCustomerDemand((String)map.get("客户需求"));
					caseXbbInsurance.setOpeningTime((String)map.get("开庭日期"));
					caseXbbInsurance.setUndertakingTeam((String)map.get("承办团队"));
					caseXbbInsurance.setUndertakingLawyer((String)map.get("承办人员"));
					caseXbbInsurance.setUndertakingStatus((String)map.get("承办状态"));
					caseXbbInsurance.setReportNum((String)map.get("报案号"));
					caseXbbInsurance.setChargingMethod((String)map.get("收费方式收费方式"));
					caseXbbInsurance.setItFiled((String)map.get("是否归档"));
					caseXbbInsurance.setUpTime(DateUtils.stampToTime(((Integer)map.get("更新时间")).toString()));
					caseXbbInsurance.setListMaterials((String)map.get("材料清单"));
					caseXbbInsurance.setCaseName((String)map.get("案件名称"));
					caseXbbInsurance.setCaseStar((String)map.get("案件星级"));
					caseXbbInsurance.setCaseType((String)map.get("案件类型"));
					caseXbbInsurance.setCaseNumber((String)map.get("案件编号"));
					caseXbbInsurance.setCaseIntroduction((String)map.get("案情简介"));
					caseXbbInsurance.setCaseCreditorNub((String)map.get("正式债权编号"));
					caseXbbInsurance.setClaimDate((String)map.get("理赔日期"));
					caseXbbInsurance.setExecutionDate((String)map.get("申请执行日期"));
					caseXbbInsurance.setCurrentStage((String)map.get("目前阶段"));
					caseXbbInsurance.setSignPersone((String)map.get("签订人"));
					caseXbbInsurance.setSigningDate((String)map.get("签订日期"));
					caseXbbInsurance.setJurisdictionalCourt((String)map.get("管辖法院"));
					caseXbbInsurance.setRefrigerationDate((String)map.get("续封续冻日期"));
					caseXbbInsurance.setLimitationDate((String)map.get("诉讼/执行时效届满日"));
					if (StringUtils.isNotBlank((String)map.get("诉讼立案日期"))){
					caseXbbInsurance.setLawsuitFillingDate(DateUtils.parseDate((String)map.get("诉讼立案日期")));}
					caseXbbInsurance.setCompensatorName((String)map.get("赔偿人名称"));
					caseXbbInsurance.setCompensatorContact((String)map.get("赔偿人联系方式"));
					caseXbbInsurance.setCompernsatorAmount((String)map.get("赔偿总金额（元）"));
					caseXbbInsurance.setNonProsecutio((String)map.get("非诉开始日期"));
					if (StringUtils.isNotBlank((String)map.get("非诉开始日期"))){
						caseXbbInsurance.setNonPresecutioDate(DateUtils.parseDate((String)map.get("非诉开始日期")));}
					caseXbbInsurance.setCreateTime(DateUtils.parseDate(DateUtils.stampToTime(((Integer)map.get("创建时间")).toString())));
					if (StringUtils.isNotBlank((String)map.get("诉讼开始日期"))){
						caseXbbInsurance.setLawsuitStartDate(DateUtils.parseDate((String)map.get("诉讼开始日期")));}
					if (StringUtils.isNotBlank((String)map.get("执行开始日期"))){
						caseXbbInsurance.setExecutionStratDate(DateUtils.parseDate((String)map.get("执行开始日期")));}
					caseXbbInsurance.setLawyerLetter((String)map.get("律师函"));
					caseXbbInsurance.setRepaymentIntention((String)map.get("还款意向"));
					caseXbbInsurances.add(caseXbbInsurance);
				}
				returnMap.put("rows",caseXbbInsurances);


			}else  if (key.equals("communicateList")){
				List<XbbCommunicate> xbbCommunicates = Lists.newArrayList();
				for (int i =0;i<retArray.size();i++){
					JSONObject map = retArray.getJSONObject(i);
					XbbCommunicate xbbCommunicate = new XbbCommunicate();
					xbbCommunicate.setId(((Integer)map.get("id")).toString());
					xbbCommunicate.setAddress((String)map.get("位置"));
					xbbCommunicate.setPicture((String)map.get("图片"));
					xbbCommunicate.setRemarks((String)map.get("备注"));
					xbbCommunicate.setCustomerId( ((Integer)map.get("客户ID")).toString());
					xbbCommunicate.setCustomerName((String)map.get("客户名称"));
					xbbCommunicate.setVistPerson((String)map.get("跟进人"));
					xbbCommunicate.setVistPersonId((String)map.get("跟进人ID"));
					xbbCommunicate.setVistWay((String)map.get("跟进方式"));
					if (map.get("合同ID")!=null){
					xbbCommunicate.setXbbInsuranceId(((Integer)map.get("合同ID")).toString());}
					xbbCommunicate.setCreateTimes(DateUtils.stampToTime(((Integer)map.get("创建时间")).toString()));
					xbbCommunicate.setVistTimes(DateUtils.stampToTime((String)map.get("拜访时间")));
					xbbCommunicate.setUpdateTimes(DateUtils.stampToTime(((Integer)map.get("更新时间")).toString()));
					xbbCommunicates.add(xbbCommunicate);
				}
				returnMap.put("rows",xbbCommunicates);
			}else if (key.equals("paymentList")){
				List<CasePayment> casePayments = Lists.newArrayList();
				for (int i =0;i<retArray.size();i++){
					JSONObject map = retArray.getJSONObject(i);
					CasePayment casePayment = new CasePayment();
					casePayment.setId(((Integer)map.get("id")).toString());
					casePayment.setAccountPeriod(((Integer)map.get("accountPeriod")).toString());
					casePayment.setAddTime(DateUtils.parseDate(DateUtils.stampToTime(((Integer)map.get("addTime")).toString())));
					casePayment.setAmount( map.get("amount").toString());
					casePayment.setContractId(((Integer)map.get("contractId")).toString());
					casePayment.setCustomerId( ((Integer)map.get("customerId")).toString());
					casePayment.setContractName((String)map.get("contractName"));
					casePayment.setEstimateTime(DateUtils.parseDate(DateUtils.stampToTime(((Integer)map.get("addTime")).toString())));
					casePayment.setPayTime(DateUtils.parseDate(DateUtils.stampToTime(((Integer)map.get("payTime")).toString())));
					casePayment.setPaymentNo((String)map.get("paymentNo"));
					casePayment.setStatus( ((Integer)map.get("status")).toString());
					casePayment.setUpdateTime(DateUtils.parseDate(DateUtils.stampToTime(((Integer)map.get("updateTime")).toString())));
					casePayment.setUserId((String)map.get("userId"));
					casePayment.setUserName((String)map.get("userName"));
					casePayments.add(casePayment);
				}
				returnMap.put("rows",casePayments);
			}

			else {

			}
			returnMap.put("total", totalCount);
			return returnMap;
		} else {
			throw new XbbException(responseJson.getInteger("errorCode"), responseJson.getString("msg"));
		}
	}


	public static String updateContrancts(String corpid,String token,CaseXbbInsurance xbbInsurance,String apiUrl) throws XbbException{
		JSONArray retArray = null;
		String url =  ConfigConstant.getApiUrl(apiUrl);
		JSONObject data = new JSONObject();
		data.put("corpid",corpid);
		JSONObject map = new JSONObject();
		map.put("contractId",xbbInsurance.getId());
//		map.put("诉讼立案日期",DateUtils.formatDate(xbbInsurance.getLawsuitFillingDate()));
		map.put("承办状态",xbbInsurance.getUndertakingStatus());
		map.put("目前阶段",xbbInsurance.getCurrentStage());
//		map.put("律师函",xbbInsurance.getLawyerLetter());
		map.put("还款意向",xbbInsurance.getRepaymentIntention());


		data.put("contract",map);
		//计算token， 可以用
		String sign = ConfigConstant.getDataSign(data, token);
		String response = null;
		JSONObject responseJson = null;
		try {
			response = HttpRequestUtils.post(url, data.toJSONString(), sign);
		} catch(Exception e) {
			throw new XbbException(-1, "http post访问出错");
		}
		return response;
	}

	public static String updateContranct(String corpid,String token,CaseXbbInsurance xbbInsurance,String apiUrl) throws XbbException{
		JSONArray retArray = null;
		String url =  ConfigConstant.getApiUrl(apiUrl);
		JSONObject data = new JSONObject();
		data.put("corpid",corpid);
		JSONObject map = new JSONObject();
		map.put("contractId",xbbInsurance.getId());
		map.put("合作律师信息",xbbInsurance.getCooperativeLawyer());
		map.put("诉讼立案日期",DateUtils.formatDate(xbbInsurance.getLawsuitFillingDate()));
		map.put("承办状态",xbbInsurance.getUndertakingStatus());
		map.put("目前阶段",xbbInsurance.getCurrentStage());
		map.put("律师函",xbbInsurance.getLawyerLetter());
		map.put("还款意向",xbbInsurance.getRepaymentIntention());


		data.put("contract",map);
        //计算token， 可以用
		String sign = ConfigConstant.getDataSign(data, token);
		String response = null;
		JSONObject responseJson = null;
		try {
			response = HttpRequestUtils.post(url, data.toJSONString(), sign);
		} catch(Exception e) {
			throw new XbbException(-1, "http post访问出错");
		}
		return response;
	}

    public static void addContract(String corpid,String token,String customerId,XbbContract xbbContract,String apiUrl)throws XbbException{
		JSONArray retArray = null;
		String url = ConfigConstant.getApiUrl(apiUrl);
		JSONObject data = new JSONObject();
		data.put("corpid", corpid);
		data.put("customerId",customerId);
		JSONObject map = new JSONObject();
		map.put("债务人/对方联系方式",xbbContract.getDebtorpartycontactmode());
		map.put("债务人名称/对方名称",xbbContract.getNameofdebtorpartyname());
		map.put("债务人联系人及职务",xbbContract.getDebtorcontactsandduties());
		map.put("债权编号/项目编号",xbbContract.getClaimnumber());
		map.put("创建人","212112");
        data.put("contract",map);
		//计算token， 可以用
		String sign = ConfigConstant.getDataSign(data, token);
		String response = null;
		JSONObject responseJson = null;
		try {
			response = HttpRequestUtils.post(url, data.toJSONString(), sign);
		} catch(Exception e) {
			throw new XbbException(-1, "http post访问出错");
		}
	}

}
