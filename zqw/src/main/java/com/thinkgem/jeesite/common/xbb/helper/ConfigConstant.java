package com.thinkgem.jeesite.common.xbb.helper;

import com.alibaba.fastjson.JSONObject;

public class ConfigConstant {
	//接口根地址
	public static final String XBB_API_ROOT = "http://dingapi.xbongbong.com"; 
	//本公司访问接口的token
	public static final String token = "c2bac56c8b56f186e8465b8eb3258ada";
	//本公司corpid
	public static final String corpid = "ding92205c195b0a88c4";
	
	/* ---------------------------------------------客户模块接口地址------------------------------------------------*/
	//客户列表接口
	public static final String CUSTOMER_SIMPLE_LIST = "/api/v1/customer/simpleList.html";

	public static final String CUSTOMER_LIST = "/api/v1/customer/list.html";

	//用户列表接口
	public static final String USER_ALL_LIST = "/api/v1/user/list.html";

	//销售机会接口
	public static final String OPPORTUNITY_LIST="/api/v1/opportunity/list.html";

	//合同列表
	public static final String CONTACT_LIST = "/api/v1/contract/list.html";

	//跟进记录列表
	public  static  final  String COMMUNICATE_LIST = "/api/v1/communicate/list.html";

	//回款计划列表
	public static  final String PAYMENT_LIST="/api/v1/contract/payment/list.html";





	//客户联系人列表接口
	public  static final  String CUSTOMER_CONTACTS_LIST = "/api/v1/customer/contact.html";


	//添加合同
	public static final String ADD_CUSTOMER_CONTACTS = "/api/v1/contract/add.html";


	//修改合同
	public static final String  UPDATE_CONTACTS ="/api/v1/contract/update.html";


	public static  final  String GET_CUSTOMER ="/api/v1/customer/get.do";


	public static final String userKey = "userList";
	public static final String customerKey = "customerList";
	public static final String customerOneKey = "customer";
	public static final String opportunityKey = "opportunityList";
	public static final String contactListKey = "contractList";
	public static final String communicateListKey ="communicateList";
	public static final String paymentListKey ="paymentList";
	
	/**
	 * 获取接口地址
	 * @param restApiUrl
	 * @return
	 */
	public static String getApiUrl(String restApiUrl) {
		return XBB_API_ROOT + restApiUrl;
	}
	
	/**
	 * 获取签名
	 * @param data
	 * @param token
	 * @return
	 */
	public static String getDataSign(JSONObject data, String token) {
		return getDataSign(data.toJSONString(), token);
	}
	
	/**
	 * 获取签名
	 * @param data
	 * @param token
	 * @return
	 */
	public static String getDataSign(String data, String token) {
		return  DigestUtil.Encrypt(data + token, "SHA-256");
	}
}
