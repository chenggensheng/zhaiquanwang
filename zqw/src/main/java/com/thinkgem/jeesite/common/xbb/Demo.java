package com.thinkgem.jeesite.common.xbb;

import com.alibaba.fastjson.JSONArray;
import com.thinkgem.jeesite.common.xbb.customer.CustomerApi;
import com.thinkgem.jeesite.common.xbb.helper.ConfigConstant;
import com.thinkgem.jeesite.common.xbb.helper.XbbException;

public class Demo {
	
	public static void main(String[] args) {
		try {
			JSONArray customerList = CustomerApi.simpleList(ConfigConstant.corpid, ConfigConstant.token, 1, 15,ConfigConstant.CUSTOMER_SIMPLE_LIST,ConfigConstant.userKey);
			System.out.println(customerList.toJSONString());
		} catch (XbbException e) {
			e.printStackTrace();
		}
	}
}
