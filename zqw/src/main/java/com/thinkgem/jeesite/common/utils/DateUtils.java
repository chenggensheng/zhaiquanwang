/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.common.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;



import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 日期工具类, 继承org.apache.commons.lang.time.DateUtils类
 * @author ThinkGem
 * @version 2014-4-15
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {
	
	private static String[] parsePatterns = {
		"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", 
		"yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
		"yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM",
	    "yyyy年MM月dd日"};

	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd）
	 */
	public static String getDate() {
		return getDate("yyyy-MM-dd");
	}
	
	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String getDate(String pattern) {
		return DateFormatUtils.format(new Date(), pattern);
	}
	
	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, Object... pattern) {
		if(date ==null) return "";
		String formatDate = null;
		if (pattern != null && pattern.length > 0) {
			formatDate = DateFormatUtils.format(date, pattern[0].toString());
		} else {
			formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		}
		return formatDate;
	}
	
	/**
	 * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String formatDateTime(Date date) {
		return formatDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前时间字符串 格式（HH:mm:ss）
	 */
	public static String getTime() {
		return formatDate(new Date(), "HH:mm:ss");
	}

	/**
	 * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String getDateTime() {
		return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前年份字符串 格式（yyyy）
	 */
	public static String getYear() {
		return formatDate(new Date(), "yyyy");
	}

	/**
	 * 得到当前月份字符串 格式（MM）
	 */
	public static String getMonth() {
		String monthString = formatDate(new Date(), "MM");
//		if (monthString.startsWith("0")){
//			return monthString.substring(1,monthString.length());
//		}else {
			return monthString;
//		}
	}

	/**
	 * 得到当天字符串 格式（dd）
	 */
	public static String getDay() {

		 String dayString = formatDate(new Date(), "dd");
		return  dayString;
	}


	public static String getLastDay(Date date) {

		String dayString = formatDate(date, "dd");
		return  dayString;
	}
	public static int getIntDay() {

		String dayString = formatDate(new Date(), "dd");
		if (dayString.startsWith("0")){
			return Integer.valueOf(dayString.substring(1,dayString.length()));
		}else {
			return Integer.valueOf(dayString);
		}
	}


	public static int getIntDay(Date date) {

		String dayString = formatDate(date, "dd");
		if (dayString.startsWith("0")){
			return Integer.valueOf(dayString.substring(1,dayString.length()));
		}else {
			return Integer.valueOf(dayString);
		}
	}


	// 将传入时间与当前时间进行对比，是否今天\昨天\前天\同一年
	public static String getNewDate(Date date) {
		if(date == null ){
			return "";
		}
		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(date);
		Date now = new Date();
		Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.setTime(now);
		todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
		todayCalendar.set(Calendar.MINUTE, 0);
		if (dateCalendar.after(todayCalendar)) {// 判断是不是今天
			return "今天" + DateFormatUtils.format(date, " HH:mm");
		} else {
			todayCalendar.add(Calendar.DATE, -1);
			if (dateCalendar.after(todayCalendar)) {// 判断是不是昨天
				return "昨天" + DateFormatUtils.format(date, " HH:mm");
			}
			todayCalendar.add(Calendar.DATE, -2);
			if (dateCalendar.after(todayCalendar)) {// 判断是不是前天
				return "前天" + DateFormatUtils.format(date, " HH:mm");
			}

		}
		return DateFormatUtils.format(date, "yyyy-MM-dd  HH:mm");
	}


	/**
	 * 得到当前星期字符串 格式（E）星期几
	 */
	public static String getWeek() {
		return formatDate(new Date(), "E");
	}
	
	/**
	 * 日期型字符串转化为日期 格式
	 * { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", 
	 *   "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm",
	 *   "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm" }
	 */
	public static Date parseDate(Object str) {
		if (str == null){
			return null;
		}
		try {
			return parseDate(str.toString(), parsePatterns);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 获取过去的天数
	 * @param date
	 * @return
	 */
	public static long pastDays(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(24*60*60*1000);
	}

	/**
	 * 获取过去的小时
	 * @param date
	 * @return
	 */
	public static long pastHour(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(60*60*1000);
	}
	
	/**
	 * 获取过去的分钟
	 * @param date
	 * @return
	 */
	public static long pastMinutes(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(60*1000);
	}
	
	/**
	 * 转换为时间（天,时:分:秒.毫秒）
	 * @param timeMillis
	 * @return
	 */
    public static String formatDateTime(long timeMillis){
		long day = timeMillis/(24*60*60*1000);
		long hour = (timeMillis/(60*60*1000)-day*24);
		long min = ((timeMillis/(60*1000))-day*24*60-hour*60);
		long s = (timeMillis/1000-day*24*60*60-hour*60*60-min*60);
		long sss = (timeMillis-day*24*60*60*1000-hour*60*60*1000-min*60*1000-s*1000);
		return (day>0?day+",":"")+hour+":"+min+":"+s+"."+sss;
    }
	
	/**
	 * 获取两个日期之间的天数
	 * 
	 * @param before
	 * @param after
	 * @return
	 */
	public static double getDistanceOfTwoDate(Date before, Date after) {
		if (after == null) return 999;
		if (before == null) return 0;
 		long beforeTime = before.getTime();
		long afterTime = after.getTime();
	   double t=	(afterTime - beforeTime) / (1000 * 60 * 60 * 24);
		return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
	}


	public static Integer StringToTimestamp(String time){

		int times = 0;
		try {
			times = (int) ((Timestamp.valueOf(time).getTime())/1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(times==0){
			System.out.println("String转10位时间戳失败");
		}
		return times;

	}


	public static String stampToTime(String stamp) {
      String sd = "";
      Date d = new Date();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
      sd = sdf.format(new Date(Long.parseLong(stamp)*1000)); // 时间戳转换日期
       return sd;
 	}

 	public static Date getNextDay(Date date) {
		if(date == null){
			System.out.println("入参为空");
			return null;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		//calendar.add(Calendar.DAY_OF_MONTH, +1);//明天
		//calendar.add(Calendar.DAY_OF_MONTH, 0);//今天
		calendar.add(Calendar.DAY_OF_MONTH, -1);//昨天
		return calendar.getTime();
	}

	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
//        Date t = new Date();
//        for (int i =0;i<10000;i++){
//        	System.out
//		}
//		System.out.println(DateUtils.getDistanceOfTwoDate(,new Date()));

//		System.out.println(formatDate(parseDate("2010/3/6")));
//		System.out.println(getDate("yyyy年MM月dd日 E"));
//		long time = new Date().getTime()-parseDate("2012-11-19").getTime();
//		System.out.println(time/(24*60*60*1000));
	}
}
