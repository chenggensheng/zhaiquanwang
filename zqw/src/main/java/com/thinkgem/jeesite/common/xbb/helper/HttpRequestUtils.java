package com.thinkgem.jeesite.common.xbb.helper;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class HttpRequestUtils {

	public static String post(String url, String param) {
		return post(url, param, null);
	}
	
	// post提交表单
	public static String post(String url, String param, String sign) {

		String body = null;
		// 创建默认的httpClient实例.
		CloseableHttpClient httpclient = HttpClients.createDefault();
		// 创建httppost
		HttpPost httppost = new HttpPost(url);
		httppost.addHeader("sign", sign);
		// 创建参数队列
		List<BasicNameValuePair> formparams = new ArrayList<BasicNameValuePair>();
		formparams.add(new BasicNameValuePair("data", param));
		UrlEncodedFormEntity uefEntity;

		//参数设置
		RequestConfig requestConfig = RequestConfig.custom()  
		        .setConnectTimeout(10000)
		        .setSocketTimeout(10000).build();
		httppost.setConfig(requestConfig);
		
		try {

			uefEntity = new UrlEncodedFormEntity(formparams, "UTF-8");
			httppost.setEntity(uefEntity);
			System.out.println("executing request " + httppost.getURI());
			CloseableHttpResponse response = httpclient.execute(httppost);

			try {

				HttpEntity entity = response.getEntity();
				if (entity != null) {
					body = EntityUtils.toString(entity, "UTF-8");
					System.out.println("--------------------------------------");
					System.out.println("Response content: " + body);
					System.out.println("--------------------------------------");
				}

			} finally {
				response.close();
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace(System.out);
//			e.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace(System.out);
//			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace(System.out);
//			e.printStackTrace();
		} finally {

			// 关闭连接,释放资源
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return body;
	}

	// 给请求
	public static String get(String url) {

		String body = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = null;

		try {

			// 创建httpget.
			HttpGet httpget = new HttpGet(url);
			System.out.println("executing request " + httpget.getURI());
			// 执行get请求.
			response = httpclient.execute(httpget);
			// 获取响应实体
			HttpEntity entity = response.getEntity();
			System.out.println("--------------------------------------");
			// 打印响应状态
			System.out.println(response.getStatusLine());

			if (entity != null) {
				body = EntityUtils.toString(entity);
				// 打印响应内容长度
				System.out.println("Response content length: " + entity.getContentLength());
				// 打印响应内容
				System.out.println("Response content: " + body);
			}
			System.out.println("------------------------------------");

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 关闭连接,释放资源
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return body;
	}
}